<?php

namespace app\traits;

use Yii;
use yii\base\Model;
use yii\web\Response;
use kartik\form\ActiveForm;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
trait AjaxValidationTrait
{
    /**
     * Performs ajax validation.
     *
     * @param Model $model
     *
     * @throws \yii\base\ExitException
     */
    protected function performAjaxValidation(Model $model)
    {
        if ($this->request->isAjax && $model->load($this->request->post())) {
            $this->response->format = Response::FORMAT_JSON;
            $this->response->data   = ActiveForm::validate($model);
            $this->response->send();
            Yii::$app->end();
        }
    }
}