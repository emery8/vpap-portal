<?php

namespace app\traits;

use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\base\InvalidArgumentException;

/**
 * @author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>
 */
trait AttachmentTrait
{
    /**
     * @param string $attribute
     * @return string
     */
    public function getCaption($attribute)
    {
        try {
            $content = Json::decode($this->$attribute);
            if (is_array($content) && isset($content['caption'])) {
                return $content['caption'];
            }
        } catch (InvalidArgumentException $e) {
            throw $e;
        }
    }

    /**
     * @param string $attribute
     * @return integer
     */
    public function getSize($attribute)
    {
        try {
            $content = Json::decode($this->$attribute);
            if (is_array($content) && isset($content['size'])) {
                return $content['size'];
            }
        } catch (InvalidArgumentException $e) {
            throw $e;
        }
    }

    /**
     * @param string $attribute
     * @return string
     */
    public function getFileType($attribute)
    {
        try {
            $content = Json::decode($this->$attribute);
            if (is_array($content) && isset($content['type'])) {
                switch ($content['type']) {
                    case 'application/msword':
                    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                        return 'office';
                        break;
                    case 'application/pdf':
                        return 'pdf';
                        break;
                    case 'image/png':
                    case 'image/jpeg':
                        return 'image';
                        break;
                }
            }
        } catch (InvalidArgumentException $e) {
            throw $e;
        }
    }
}