<?php

namespace app\traits;

use yii\db\Query;
use app\models\Event;
use app\models\EventAttendance;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
trait ChartTrait
{
    public $nullTextDisplay = 'Not Filled In';

    /**
     * @param Event $event
     * @return EventAttendance
     */
    protected function getEventAttendance(Event $event)
    {
        $attendances = (new Query())
            ->select(['[[member_id]]'])
            ->from(['{{%event_attendance}}'])
            ->andWhere(['[[event_id]]' => $event->id])
            ->andWhere(['[[attended]]' => 1])
            ->all();

        return $attendances;
    }

    /**
     * @param array $arr
     * @param boolean $lower
     * @return array
     */
    protected function array_icount_values($arr, $lower = true)
    {
        $arr2 = [];
        if (! is_array($arr['0'])) {
            $arr = array($arr);
        }

        foreach ($arr as $k => $v) {
            foreach($v as $v2) {
                if ($lower == true) {
                    $v2=strtolower($v2);
                }
                if( ! isset($arr2[$v2])) {
                    $arr2[$v2] =1 ;
                } else {
                    $arr2[$v2]++;
                }
            }
        }
        return $arr2;
    }
}
