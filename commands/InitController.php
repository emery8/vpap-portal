<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use app\models\Event;
use app\models\EventAttendance;
use app\models\SpeciesOfSpecialization;
use app\models\TypeOfPractice;
use app\models\VeterinaryInstitution;
use app\models\Sponsor;
use app\models\User;
use app\models\UserEducation;
use app\models\UserEmployment;
use app\models\UserMembership;
use app\models\UserProfession;
use app\models\UserProfile;
use app\models\Municipality;
use app\models\Province;
use app\models\Region;
use app\models\Country;
use app\models\enums\EventInvitationType;
use app\models\enums\EventAttendanceType;
use app\models\enums\MembershipType;
use app\models\enums\PaymentMethod;

/**
 * @author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>
 */
class InitController extends Controller
{
    /**
     * @var array
     */
    private $_species_of_specialization = [
        'Canine',
        'Feline',
        'Equine',
        'Ruminant',
        'Swine',
        'Poultry',
        'Wildlife and Exotic',
        'Laboratory Animal',
        'Aquatic Animal',
    ];

    /**
     * @var array
     */
    private $_type_of_practice = [
        'Academe',
        'Clinic or Hospital Facility',
        'Consultancy',
        'Corporate',
        'Government',
        'Non-Government Organization',
        'OFW',
        'Research Facility',
    ];

    /**
     * @var array
     */
    private $_veterinary_institution = [
        'Aklan State University',
        'Benguet State University',
        'Cagayan State University',
        'Capiz State University',
        'Cavite State University',
        'Cebu Technological University',
        'Central Bicol State University / Camarines Sur State Agricultural College',
        'Central Luzon State University',
        'Central Mindanao University',
        'De La Salle Araneta University / Gregorio Araneta University Foundation',
        'Don Mariano Marcos Memorial State University',
        'Dr. Yanga\'s Colleges, Inc.',
        'Isabela State University',
        'Nueva Viscaya State University',
        'Our Lady of Fatima University',
        'Pampanga State Agricultural University',
        'Philippine College of Technological Resources',
        'Southwestern University',
        'Tarlac Agricultural University',
        'University of Eastern Philippines',
        'University of Southern Mindanao',
        'University of the Philippines',
        'Virgen Milagrosa University Foundation',
        'Visayas Sate University',
        'Others (Local)',
        'Others (International)',
    ];

    /**
     * @var array
     */
    private $_sponsor = [
        [
            'old_id' => 5,
            'company' => 'Ariela Marketing',
        ],
        [
            'old_id' => 6,
            'company' => 'AVLI Biocare, Inc.',
        ],
        [
            'old_id' => 2,
            'company' => 'Boehringer Ingelheim Animal Health Philippines, Inc.',
        ],
        [
            'old_id' => 7,
            'company' => 'Compania JM',
        ],
        [
            'old_id' => 26,
            'company' => 'Consumer Care Products, Inc. (CCPI)',
        ],
        [
            'old_id' => 8,
            'company' => 'Fanfreluche Enterprises, Inc.',
        ],
        [
            'old_id' => 4,
            'company' => 'Hill\'s / Animal Science',
        ],
        [
            'old_id' => 9,
            'company' => 'InterHolistics Asia Inc.',
        ],
        [
            'old_id' => 28,
            'company' => 'Maveson Enterprise',
        ],
        [
            'old_id' => 11,
            'company' => 'MSD Animal Health',
        ],
        [
            'old_id' => 27,
            'company' => 'Pet Food Institute (PFI)',
        ],
        [
            'old_id' => 13,
            'company' => 'Pet Ideas, Inc.',
        ],
        [
            'old_id' => 15,
            'company' => 'Philippine Animal Hospital Association',
        ],
        [
            'old_id' => 16,
            'company' => 'Philippine Veterinary Medical Association',
        ],
        [
            'old_id' => 14,
            'company' => 'Plaridel Inc.',
        ],
        [
            'old_id' => 17,
            'company' => 'Royal Canin',
        ],
        [
            'old_id' => 18,
            'company' => 'UNAHCO',
        ],
        [
            'old_id' => 24,
            'company' => 'US Defense Threat Reduction Agency',
        ],
        [
            'old_id' => 19,
            'company' => 'Vaxilife Corp.',
        ],
        [
            'old_id' => 20,
            'company' => 'Vetmate Farma, Inc.',
        ],
        [
            'old_id' => 21,
            'company' => 'Virbac Philippines Corp.',
        ],
        [
            'old_id' => 23,
            'company' => 'Zoetis Philippines Inc.',
        ],
    ];

    /**
     * @return int Exit code
     */
    public function actionIndex()
    {
        $this->_initSpeciesOfSpecialization();
        $this->_initTypeOfPractice();
        $this->_initVeterinaryInstitution();
        $this->_initSponsor();        
        return ExitCode::OK;
    }

    /**
     * @return int Exit code
     */
    public function actionUsers()
    {
        $query = new Query();
        $duplicate_license_no = $query->select(['license_no', 'duplicate_license_no' => 'count(*)'])
            ->from('vpap_odb.user_profession')
            ->leftJoin('vpap_odb.usr', 'vpap_odb.usr.id = vpap_odb.user_profession.user_id')
            ->where(['vpap_odb.usr.archive' => 0])
            ->groupBy('license_no')
            ->having(['>', 'duplicate_license_no', 1])
            ->all();
        
        $exclude_license_no = [];
        if (is_array($duplicate_license_no) && count($duplicate_license_no)) {
            $exclude_license_no = ArrayHelper::getColumn($duplicate_license_no, 'license_no');
        }

        $affectedRows = 0;
        $limit = null;
        $query = new Query();

        $query->select('
                a.id, a.archive, a.email, a.role,
                b.user_id AS b_user_id, b.comp_name, b.pos, b.address, b.village_subdivision, b.barangay, b.contact, b.town_city, b.province, b.zipcode, b.country,
                c.user_id AS c_user_id, c.school, c.year,
                d.user_id AS d_user_id, d.fname, d.mname, d.lname, d.suffx, d.nickname, d.address1, d.village_subdivision AS d_village_subdivision, d.barangay AS d_barangay, d.town_city AS d_town_city, d.province AS d_province, d.zipcode AS d_zipcode, d.country AS d_country, d.homephone, d.mobile, d.bday,
                e.user_id AS e_user_id, e.license_no, e.date_exp, e.type_practice, e.spec_spec,
                f.user_id AS f_user_id, f.vpap_mem_type, f.year_mem, f.no_yrs_active, f.disc_con, f.name_appear_certificate
            ')
            ->from('vpap_odb.usr a')
            ->join('LEFT JOIN', 'vpap_odb.user_company b', 'b.user_id = a.id')
            ->join('LEFT JOIN', 'vpap_odb.user_educ c', 'c.user_id = a.id')
            ->join('LEFT JOIN', 'vpap_odb.user_personal_info d', 'd.user_id = a.id')
            ->join('LEFT JOIN', 'vpap_odb.user_profession e', 'e.user_id = a.id')
            ->join('LEFT JOIN', 'vpap_odb.user_vpap f', 'f.user_id = a.id')
            ->andWhere(['a.archive' => 0])
            ->andWhere(['not in', 'e.license_no', $exclude_license_no]);

        if ($limit) {
            $query->limit($limit);
        }

        $veterinary_institutions = \app\models\VeterinaryInstitution::listData();

        foreach ($query->each() as $model) {
            $user = Yii::createObject(User::class);
            $user->setAttribute('old_id', $model['id']);
            $user->setAttribute('email', $model['email']);
            $user->setPassword('changeme');
            $user->generateAuthKey();
            if ($user->save()) {
                $auth = Yii::$app->authManager;
                $role = $auth->getRole($model['role'] ? 'admin' : 'member');
                $auth->assign($role, $user->id);

                $profile = Yii::createObject(UserProfile::class);
                $profile->setAttribute('user_id', $user->id);
                $profile->setAttribute('name_on_certificate', $model['name_appear_certificate'] ? $model['name_appear_certificate'] : null);
                $profile->setAttribute('first_name', $model['fname']);
                $profile->setAttribute('middle_name', $model['mname']);
                $profile->setAttribute('last_name', $model['lname']);
                $profile->setAttribute('suffix', $model['suffx'] ? $model['suffx'] : null);
                $profile->setAttribute('nickname', $model['nickname'] ? $model['nickname'] : null);
                $profile->setAttribute('birth_date', (($model['bday'] == '0000-00-00') ? null : $model['bday']));
                $profile->save();

                $education = Yii::createObject(UserEducation::class);
                $education->setAttribute('user_id', $user->id);
                $education->setAttribute('institution', ($key = array_search($model['school'], $veterinary_institutions)) ? $key : null);
                $education->setAttribute('year_graduated', $model['year'] ? $model['year'] : null);
                $education->save();

                $profession = Yii::createObject(UserProfession::class);
                $profession->setAttribute('user_id', $user->id);
                $profession->setAttribute('license_number', $model['license_no'] ? $model['license_no'] : uniqid());
                $profession->save();

                $membership_type = $model['vpap_mem_type'] ? MembershipType::getValueByName($model['vpap_mem_type']) : null;
                $membership = Yii::createObject(UserMembership::class);
                $membership->setAttribute('user_id', $user->id);
                $membership->setAttribute('type', $membership_type);
                $membership->setAttribute('number_years_active', null);
                $membership->setAttribute('year_initial_joined', ($membership_type && $membership_type != MembershipType::NONMEMBER) ? ($model['year_mem'] > 0) ? $model['year_mem'] : null : null);
                $membership->setAttribute('discount_type', null);
                $membership->save();

                $employment = Yii::createObject(UserEmployment::class);
                $employment->link('user', $user);

                $affectedRows++;
                echo "Importing user: {$model['email']}...\n";
            }
        }

        echo "Done importing {$affectedRows} records.\n";
        return ExitCode::OK;
    }

    public function actionUsers1()
    {
        $affectedRows = 0;
        $limit = null;
        $query = new Query();

        $query->select('
                a.id, a.archive, a.email, a.role,
                b.user_id AS b_user_id, b.comp_name, b.pos, b.address, b.village_subdivision, b.barangay, b.contact, b.town_city, b.province, b.zipcode, b.country,
                c.user_id AS c_user_id, c.school, c.year,
                d.user_id AS d_user_id, d.fname, d.mname, d.lname, d.suffx, d.nickname, d.address1, d.village_subdivision AS d_village_subdivision, d.barangay AS d_barangay, d.town_city AS d_town_city, d.province AS d_province, d.zipcode AS d_zipcode, d.country AS d_country, d.homephone, d.mobile, d.bday,
                e.user_id AS e_user_id, e.license_no, e.date_exp, e.type_practice, e.spec_spec,
                f.user_id AS f_user_id, f.vpap_mem_type, f.year_mem, f.no_yrs_active, f.disc_con, f.name_appear_certificate
            ')
            ->from('vpap_odb.usr a')
            ->join('LEFT JOIN', 'vpap_odb.user_company b', 'b.user_id = a.id')
            ->join('LEFT JOIN', 'vpap_odb.user_educ c', 'c.user_id = a.id')
            ->join('LEFT JOIN', 'vpap_odb.user_personal_info d', 'd.user_id = a.id')
            ->join('LEFT JOIN', 'vpap_odb.user_profession e', 'e.user_id = a.id')
            ->join('LEFT JOIN', 'vpap_odb.user_vpap f', 'f.user_id = a.id')
            ->andWhere(['a.archive' => 0])
            ->andWhere(['in', 'a.id', [708, 805, 873 ,966, 1283, 1286]]);

        if ($limit) {
            $query->limit($limit);
        }

        $veterinary_institutions = \app\models\VeterinaryInstitution::listData();

        foreach ($query->each() as $model) {
            $user = Yii::createObject(User::class);
            $user->setAttribute('old_id', $model['id']);
            $user->setAttribute('email', $model['email']);
            $user->setPassword('changeme');
            $user->generateAuthKey();
            if ($user->save()) {
                $auth = Yii::$app->authManager;
                $role = $auth->getRole($model['role'] ? 'admin' : 'member');
                $auth->assign($role, $user->id);

                $profile = Yii::createObject(UserProfile::class);
                $profile->setAttribute('user_id', $user->id);
                $profile->setAttribute('name_on_certificate', $model['name_appear_certificate'] ? $model['name_appear_certificate'] : null);
                $profile->setAttribute('first_name', $model['fname']);
                $profile->setAttribute('middle_name', $model['mname']);
                $profile->setAttribute('last_name', $model['lname']);
                $profile->setAttribute('suffix', $model['suffx'] ? $model['suffx'] : null);
                $profile->setAttribute('nickname', $model['nickname'] ? $model['nickname'] : null);
                $profile->setAttribute('birth_date', (($model['bday'] == '0000-00-00') ? null : $model['bday']));
                $profile->save();

                $education = Yii::createObject(UserEducation::class);
                $education->setAttribute('user_id', $user->id);
                $education->setAttribute('institution', ($key = array_search($model['school'], $veterinary_institutions)) ? $key : null);
                $education->setAttribute('year_graduated', $model['year'] ? $model['year'] : null);
                $education->save();

                $profession = Yii::createObject(UserProfession::class);
                $profession->setAttribute('user_id', $user->id);
                $profession->setAttribute('license_number', $model['license_no'] ? $model['license_no'] : uniqid());
                $profession->save();

                $membership_type = $model['vpap_mem_type'] ? MembershipType::getValueByName($model['vpap_mem_type']) : null;
                $membership = Yii::createObject(UserMembership::class);
                $membership->setAttribute('user_id', $user->id);
                $membership->setAttribute('type', $membership_type);
                $membership->setAttribute('number_years_active', null);
                $membership->setAttribute('year_initial_joined', ($membership_type && $membership_type != MembershipType::NONMEMBER) ? ($model['year_mem'] > 0) ? $model['year_mem'] : null : null);
                $membership->setAttribute('discount_type', null);
                $membership->save();

                $employment = Yii::createObject(UserEmployment::class);
                $employment->link('user', $user);

                $affectedRows++;
                echo "Importing user: {$model['email']}...\n";
            }
        }

        echo "Done importing {$affectedRows} records.\n";
        return ExitCode::OK;
    }

    /**
     * @return int Exit code
     */
    public function actionEvents()
    {
        $query = new Query();
        $query->from('vpap_odb.event_tbl a')
            ->andWhere(['a.archived' => 0]);

        $affectedRows = 0;
        foreach ($query->each() as $model) {
            $event = Yii::createObject(Event::class);
            $event->setAttribute('old_id', $model['id']);
            $event->setAttribute('type', $this->getEventType($model['event_type']));
            $event->setAttribute('title', $model['event_title']);
            $event->setAttribute('location', $model['event_location']);
            $event->setAttribute('start_date', $model['event_startdate']);
            $event->setAttribute('end_date', $model['event_enddate']);
            $event->setAttribute('enable_discount', ($model['event_disc'] === 'Enable') ? 1 : 0);
            $event->setAttribute('membership_fee', $model['event_fee_disc']);
            $event->setAttribute('lifetime_member_fee', $model['event_fee_lifetime']);
            $event->setAttribute('regular_member_fee', $model['event_fee_member']);
            $event->setAttribute('non_member_fee', $model['event_fee_non_member']);
            $event->setAttribute('attendance_type', $this->getAttendanceType($model['event_att_type']));
            $event->setAttribute('cpd_point', $model['event_cpd']);
            $event->setAttribute('maximum_participant', $model['event_max_part']);

            if ($event->save()) {
                $affectedRows++;
                echo "Importing event: {$model['event_title']}...\n";
            }
        }

        echo "Done importing {$affectedRows} records.\n";
        return ExitCode::OK;
    }

    /**
     * @return int Exit code
     */
    public function actionAttendances()
    {
        $query = new Query();
        $query->from('vpap_odb.event_attendee a')
            ->andWhere(['a.archived' => 0]);

        $affectedRows = 0;
        foreach ($query->each() as $model) {
            $user = User::findOne(['old_id' => $model['user_id']]);
            $event = Event::findOne(['old_id' => $model['event_id']]);
            $sponsor = Sponsor::findOne(['old_id' => $model['sponsor_id']]);

            if ($user && $event) {
                $event_attendance = Yii::createObject(EventAttendance::class);
                $event_attendance->setAttribute('member_id', $user->id);
                $event_attendance->setAttribute('event_id', $event->id);
                $event_attendance->setAttribute('sponsor_id', $sponsor ? $sponsor->id : null);
                $event_attendance->setAttribute('attendance_type', EventAttendanceType::getValueByName($model['att_type']));
                $event_attendance->setAttribute('member_type', null);
                $event_attendance->setAttribute('payment_method', PaymentMethod::getValueByName($model['payment_type']));
                $event_attendance->setAttribute('discount_type', null);
                $event_attendance->setAttribute('event_fee', null);
                $event_attendance->setAttribute('amount_paid', $model['payment_amt']);
                $event_attendance->setAttribute('cpd_point_earned', $model['cpd_earned']);
                $event_attendance->setAttribute('payment_confirmed', $model['payment_status']);
                $event_attendance->setAttribute('attended', $model['confirm']);

                if ($event_attendance->save()) {
                    $affectedRows++;
                    echo "Importing attendance: {$user->email} - {$event->type}...\n";
                }
            }
        }

        echo "Done importing {$affectedRows} records.\n";
        return ExitCode::OK;
    }

    public function actionCountries()
    {
        foreach (countries() as $country) {
            $model = Yii::createObject(Country::class);
            $model->name = ucwords(mb_strtolower($country));
            $model->save();
        }
    }

    public function actionRegions()
    {
        $model = Yii::createObject(Region::class);
        $model->name = 'Overseas';
        $model->save();

        $json = $this->getPhilippinePlacesList();
        foreach ($json as $container) {
            $model = Yii::createObject(Region::class);
            $model->name = str_replace('REGION', 'Region', $container['region_name']);
            $model->save();
        }
    }

    public function actionProvinces()
    {
        $model = Yii::createObject(Province::className());
        $model->region_id = 1;
        $model->name = 'Overseas';
        $model->save();

        $json = $this->getPhilippinePlacesList();
        foreach ($json as $container) {
            foreach ($container as $provinceList) {
                if (is_array($provinceList)) {
                    foreach ($provinceList as $provinceName => $municipalityList) {
                        $region = Region::findOne(['name' => $container['region_name']]);
                        if ($region) {
                            $model = Yii::createObject(Province::className());
                            $model->region_id = $region->id;
                            $model->name = ucwords(mb_strtolower($provinceName));
                            $model->save();
                        }
                    }
                }
            }
        }
    }

    public function actionMunicipalities()
    {
        $model = Yii::createObject(Municipality::className());
        $model->province_id = 1;
        $model->name = 'Overseas';
        $model->save();

        $json = $this->getPhilippinePlacesList();
        foreach ($json as $container) {
            foreach ($container as $provinceList) {
                if (is_array($provinceList)) {
                    foreach ($provinceList as $provinceName => $municipalityContainer) {
                        if (is_array($municipalityContainer)) {
                            foreach ($municipalityContainer as $municipalityKey => $municipalityList) {
                                foreach ($municipalityList as $municipalityName => $barangayContainer) {
                                    $provinceModel = Province::findOne(['name' => $provinceName]);
                                    if ($provinceModel) {
                                        $model = Yii::createObject(Municipality::className());
                                        $model->province_id = $provinceModel->id;
                                        $model->name = ucwords(mb_strtolower($municipalityName)) . ' (' . $provinceModel->name . ')';
                                        $model->save();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private function getPhilippinePlacesList()
    {
        $content = file_get_contents('https://raw.githubusercontent.com/flores-jacob/philippine-regions-provinces-cities-municipalities-barangays/master/philippine_provinces_cities_municipalities_and_barangays_2019v2.json');
        $json = \yii\helpers\Json::decode($content);
        return $json;

    }

    private function getMembershipType($type)
    {
        switch ($type) {}
    }

    /**
     * @param string $type
     * @return integer
     */
    private function getAttendanceType($type)
    {
        switch ($type) {
            case 'invite':
                return 3;
            case 'open':
                return 1;
            case 'member':
                return 2;
            default:
                return null;
        }
    }

    /**
     * @param string $type
     * @return integer
     */
    private function getEventType($type) {
        switch ($type) {
            case 'Seminar':
                return 2;
            case 'Annual Conference':
                return 4;
            case 'Regional Conference':
                return 3;
            case 'General Membership Meeting':
                return 1;
        }
    }

    /**
     * @return boolean
     */
    private function _initSpeciesOfSpecialization()
    {
        $result = true;
        if (is_array($this->_species_of_specialization) && count($this->_species_of_specialization)) {
            foreach ($this->_species_of_specialization as $data) {
                $model = Yii::createObject(SpeciesOfSpecialization::class);
                $model->setAttributes(['description' => $data]);
                $result = $result && $model->save();
            }
        }
        return $result;
    }

    /**
     * @return boolean
     */
    private function _initTypeOfPractice()
    {
        $result = true;
        if (is_array($this->_type_of_practice) && count($this->_type_of_practice)) {
            foreach ($this->_type_of_practice as $data) {
                $model = Yii::createObject(TypeOfPractice::class);
                $model->setAttributes(['description' => $data]);
                $result = $result && $model->save();
            }
        }
        return $result;
    }

    /**
     * @return boolean
     */
    private function _initVeterinaryInstitution()
    {
        $result = true;
        if (is_array($this->_veterinary_institution) && count($this->_veterinary_institution)) {
            foreach ($this->_veterinary_institution as $data) {
                $model = Yii::createObject(VeterinaryInstitution::class);
                $model->setAttributes(['institution' => $data]);
                $result = $result && $model->save();
            }
        }
        return $result;
    }

    /**
     * @return boolean
     */
    private function _initSponsor()
    {
        $result = true;
        if (is_array($this->_sponsor) && count($this->_sponsor)) {
            foreach ($this->_sponsor as $data) {
                $model = Yii::createObject(Sponsor::class);
                $model->setAttributes(['old_id' => $data['old_id']]);
                $model->setAttributes(['company_name' => $data['company']]);
                $result = $result && $model->save();
            }
        }
        return $result;
    }
}
