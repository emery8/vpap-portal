<?php

namespace app\commands;

use Yii;
use yii\base\InvalidParamException;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use app\models\Announcement;
use app\models\Event;
use app\models\UserMembership;
use app\models\MembershipActivity;
use app\models\EventAttendance;
use app\models\enums\EventType;
use app\models\enums\MembershipType;

/**
 * @author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>
 */
class RunController extends Controller
{
    /**
     * @return int Exit code
     */
    public function actionUpdateMembership()
    {
        $query = UserMembership::find();
        $affectedRows = 0;
        foreach ($query->each() as $membership) {
            $membership->scenario = UserMembership::SCENARIO_UPDATE;
            if ($membership->update() > 0) {
                echo "Updating member_id: {$membership->user_id}\n";
                $affectedRows++;
            }
        }

        if ($affectedRows > 0) {
            echo "Done updating {$affectedRows} records.\n";
        }
        return ExitCode::OK;
    }

    /**
     * @return int Exit code
     */
    public function actionUpdateMembershipActivity()
    {
        $event_type = [EventType::ANNUAL_CONFERENCE];
        $membership_type = [MembershipType::LIFETIME, MembershipType::REGULAR];

        $affectedRows = 0;
        $memberships = UserMembership::find()
            ->andWhere(['in', 'type', $membership_type]);

        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($memberships->each() as $membership) {
                $membershipActivity = MembershipActivity::findOne(['member_id' => $membership->user_id]);
                if ($membershipActivity === null) {
                    $membershipActivity = Yii::createObject(MembershipActivity::class);
                }

                if ($membership->is_lifetime) {
                    $years_attended = [];
                    $year_joined = $membership->year_initial_joined;
                    $event = Event::find()
                        ->select('start_date')
                        ->andWhere(['type' => EventType::ANNUAL_CONFERENCE])
                        ->orderBy(['start_date' => SORT_DESC])
                        ->limit(1)
                        ->one();
                    if ($year_joined && $event) {
                        $pieces = explode('-', $event->start_date);
                        if (isset($pieces[0]) && (strlen($pieces[0]) === 4)) {
                            $year_latest_event = $pieces[0];
                            for ($i = $year_joined; $i <= $year_latest_event; $i++) {
                                $years_attended[] = $i;
                            }
                            sort($years_attended);
                        }
                    }
                } else {
                    $years_attended = [];
                    $year_joined = $membership->year_initial_joined;
                    if ($year_joined) {
                        $attendances = EventAttendance::find()
                            ->select(['{{%event}}.*', '{{%event_attendance}}.*', 'aggregate_start_date' => new Expression('year({{%event}}.[[start_date]])')])
                            ->joinWith(['event'])
                            ->andWhere(['in', '{{%event}}.[[type]]', $event_type])
                            ->andWhere(['[[attended]]' => 1])
                            ->andWhere(['[[member_id]]' => $membership->user_id])
                            ->andWhere(['>=', new Expression('year({{%event}}.[[start_date]])'), $membership->year_initial_joined])
                            ->all();

                        if (is_array($attendances) && count($attendances)) {
                            $years_attended = ArrayHelper::getColumn($attendances, 'aggregate_start_date');
                            sort($years_attended);
                        }
                    }
                }

                $membershipActivity->setAttribute('year_event_attended', $years_attended);
                $membershipActivity->setAttribute('member_id', $membership->user_id);
                if (! $membershipActivity->save()) {
                    $transaction->rollBack();
                    throw new InvalidParamException(Yii::t('app', 'Unable to continue due to an error in the batch operation.'));
                    return 0;
                } else {
                    echo "Updating member_id: {$membershipActivity->member_id}\n";
                    $affectedRows++;
                }
            }

            $transaction->commit();
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch(\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }

        if ($affectedRows > 0) {
            echo "Done updating {$affectedRows} records.\n";
        }
        return ExitCode::OK;
    }

    /**
     * @return int Exit code
     */
    public function actionSendAnnouncement()
    {
        $announcements = Announcement::find()
            ->andWhere(['status' => Announcement::STATUS_PUBLISH])
            ->andWhere(['email_sent' => 0])
            ->all();

        if (is_array($announcements) && count($announcements)) {
            foreach ($announcements as $announcement) {
                $rows = (new \yii\db\Query())
                    ->select('email')
                    ->from('{{%user}}');

                $is_valid = true;
                /*$emails = [
                    ['email' => 'tomas.cabagay'],
                    ['email' => 'tomas.cabagay@gmail.com'],
                    ['email' => 'tomasjr.cabagay@upou.edu.ph'],
                    ['email' => 'tomasjr.cabagay'],
                ];
                foreach ($emails as $row) {*/
                foreach ($rows->each() as $row) {
                    if (isset($row['email']) && ! empty($row['email'])) {
                        try {
                            $queue = Yii::$app->mailer
                                ->compose(
                                    ['html' => 'announcement-html', 'text' => 'announcement-text'],
                                    ['content' => $announcement->content]
                                )
                                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                                ->setTo($row['email'])
                                ->setSubject($announcement->title)
                                ->unique(Yii::$app->security->generateRandomString())
                                ->queue();

                            // $is_valid = $queue && $queue;
                        } catch (\Exception $e) {}
                    }
                }

                if ($is_valid) {
                    Yii::$app->db->createCommand()
                        ->update('{{%announcement}}', ['email_sent' => 1], 'id = :id', [':id' => $announcement->id])
                        ->execute();
                }
            }
        }
    }
}
