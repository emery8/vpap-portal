<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\console\widgets\Table;
use app\models\User;
use app\models\UserEducation;
use app\models\UserEmployment;
use app\models\UserMembership;
use app\models\UserProfession;
use app\models\UserProfile;

/**
 * @author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>
 */
class AdminController extends Controller
{
    /**
     * @var string
     */
    public $email;
    public $first_name;
    public $last_name;
    public $license_number;

    /**
     * {@inheritDoc}
     */
    public function options($actionId)
    {
        $options = [];
        if ($actionId === 'create') {
            $options = ['email', 'first_name', 'last_name', 'license_number'];
        }

        return array_merge(parent::options($actionId), $options);
    }

    /**
     * @return int Exit code
     */
    public function actionCreate()
    {
        $transaction = Yii::$app->db->beginTransaction();
        $password = Yii::$app->security->generateRandomString(8);
        $errMessage = "Unable to create the account.\nUsage: 'php yii admin/create --email=admin@example.com --first_name=John --last_name=Doe --license_number=1234'\n(note: do not include the brackets)";

        $user = Yii::createObject(User::class);
        $user->email = $this->email;
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        $user->emailActive();
        if (! $user->save()) {
            echo $errMessage;
            $transaction->rollBack();
            return ExitCode::DATAERR;
        }

        $education = Yii::createObject(UserEducation::class);
        $education->link('user', $user);

        $employment = Yii::createObject(UserEmployment::class);
        $employment->link('user', $user);

        $membership = Yii::createObject(UserMembership::class);
        $membership->link('user', $user);

        $profile = Yii::createObject([
            'class' => UserProfile::class,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
        ]);
        $profile->link('user', $user);

        $profession = Yii::createObject([
            'class' => UserProfession::class,
            'license_number' => $this->license_number,
        ]);
        $profession->link('user', $user);

        $auth = Yii::$app->authManager;
        $role = $auth->getRole('admin');
        $auth->assign($role, $user->getId());

        $transaction->commit();
        echo "The account has been created. Please use the email and password below to login.\n";
        echo Table::widget([
            'headers' => ['Email', 'Password'],
            'rows' => [
                [$this->email, $password],
            ],
        ]);
        return ExitCode::OK;
    }
}
