<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>
 */
class CoreUIAsset extends AssetBundle
{
    public $sourcePath = '@vendor/coreui/coreui/dist';
    public $css = [
        'css/coreui.min.css',
    ];
    public $js = [
        'js/coreui.bundle.min.js',
        'js/coreui-utilities.min.js',
    ];
}
