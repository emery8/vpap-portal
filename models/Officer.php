<?php

namespace app\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%officer}}".
 *
 * @property int $id
 * @property int $member_id
 * @property int|null $was_president
 * @property string $year_served
 * @property int|null $number_years_served
 *
 * @property User $member
 */
class Officer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%officer}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'year_served'], 'required'],
            [['member_id', 'number_years_served'], 'integer'],
            [['was_president'], 'boolean'],
            [['year_served'], 'in', 'range' => array_values(static::getYearServedRange()), 'allowArray' => true],
            [['member_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['member_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', 'Member'),
            'was_president' => Yii::t('app', 'Was President'),
            'year_served' => Yii::t('app', 'Year Served'),
            'number_years_served' => Yii::t('app', 'Number Years Served'),
            'officer_name' => Yii::t('app', 'Officer Complete Name'),
        ];
    }

    /**
     * Gets query for [[Member]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getMember()
    {
        return $this->hasOne(User::className(), ['id' => 'member_id']);
    }

    /**
     * {@inheritdoc}
     * @return OfficerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OfficerQuery(get_called_class());
    }

    /**
     * {@inheritDoc}
     */
    public function beforeSave($insert)
    {
        $year_served = $this->year_served;
        $this->setAttributes([
            'year_served' => Json::encode($year_served),
            'number_years_served' => count($year_served),
        ]);
        return parent::beforeSave($insert);
    }

    /**
     * {@inheritDoc}
     */
    public function afterFind()
    {
        parent::afterFind();
        $year_served = Json::decode($this->year_served);
        if (is_array($year_served)) {
            sort($year_served, SORT_REGULAR);
        }

        $this->setAttribute('year_served', $year_served);
    }

    /**
     * @return string
     */
    public function getComplete_name()
    {
        return $this->member !== null ? $this->member->userProfile->complete_name : null;
    }

    /**
     * @return string
     */
    public function getFirst_name()
    {
        return $this->member !== null ? $this->member->userProfile->first_name : null;
    }

    /**
     * @return string
     */
    public function getLast_name()
    {
        return $this->member !== null ? $this->member->userProfile->last_name : null;
    }

    /**
     * @return integer
     */
    public function getYear_served_count()
    {
        $year_served = $this->year_served;
        $count = 0;

        if (is_string($year_served)) {
            $year_served = Json::decode($year_served);
        }

        if (is_array($year_served)) {
            $count = count($year_served);
        }

        return $count;
    }

    /**
     * @return array
     */
    public static function getYearServedRange()
    {
        $range = range(Yii::$app->formatter->asDate(time(), 'yyyy'), 1950);
        return array_combine($range, $range);
    }
}
