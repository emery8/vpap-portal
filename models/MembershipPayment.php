<?php

namespace app\models;

use Yii;
use yii\base\InvalidParamException;
use yii\bootstrap4\Html;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%membership_payment}}".
 *
 * @property int $id
 * @property int $member_id
 * @property int $type
 * @property float $amount
 * @property int|null $number_of_year
 * @property float $amount_paid
 * @property int|null $payment_confirmed
 * @property int $created_at
 * @property int $updated_at
 *
 * @property User $member
 */
class MembershipPayment extends \yii\db\ActiveRecord
{
    /**
     * @var integer
     */
    public $sum_number_of_year;

    /**
     * @var app\models\RegularMembershipFee
     */
    private $_regular_membership;

    /**
     * @var app\models\LifetimeMembershipFee
     */
    private $_lifetime_membership;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%membership_payment}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'in', 'range' => [RegularMembershipFee::TYPE, LifetimeMembershipFee::TYPE]],
            [['number_of_year'],
                'required',
                'when' => function ($model) {
                    return $model->type === RegularMembershipFee::TYPE;
                },
                'whenClient' => 'function (attribute, value) {
                    const selected_type = jQuery(\'[name="' . Html::getInputName($this, 'type') . '"]:checked\').val();
                    return  selected_type == ' . RegularMembershipFee::TYPE . '; 
                }',
            ],
            [['member_id', 'type', 'payment_confirmed', 'created_at', 'updated_at'], 'integer'],
            [['number_of_year'], 'integer', 'min' => 1],
            [['amount', 'amount_paid'], 'number'],
            [['member_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['member_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', 'Member'),
            'type' => Yii::t('app', 'Type'),
            'amount' => Yii::t('app', 'Amount'),
            'number_of_year' => Yii::t('app', 'Number of Year/s to Pay'),
            'amount_paid' => Yii::t('app', 'Amount Paid'),
            'payment_confirmed' => Yii::t('app', 'Payment Confirmed'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Member]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getMember()
    {
        return $this->hasOne(User::className(), ['id' => 'member_id']);
    }

    /**
     * {@inheritdoc}
     * @return MembershipPaymentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MembershipPaymentQuery(get_called_class());
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function beforeSave($insert)
    {
        $regular_membership = $this->regular_membership;
        $lifetime_membership = $this->lifetime_membership;

        if ($this->type == RegularMembershipFee::TYPE) {
            $this->setAttribute('amount', $regular_membership->amount);
            $this->setAttribute('amount_paid', ($regular_membership->amount * $this->number_of_year));
        } else {
            $this->setAttribute('amount', $lifetime_membership->amount);
            $this->setAttribute('amount_paid', ($lifetime_membership->amount));
        }

        return parent::beforeSave($insert);
    }

    /**
     * @var app\models\RegularMembershipFee
     */
    public function getRegular_membership()
    {
        if ($this->_regular_membership === null) {
            $this->_regular_membership = RegularMembershipFee::findOne(['type' => RegularMembershipFee::TYPE]);
        }
        return $this->_regular_membership;
    }

    /**
     * @var app\models\LifetimeMembershipFee
     */
    public function getLifetime_membership()
    {
        if ($this->_lifetime_membership === null) {
            $this->_lifetime_membership = LifetimeMembershipFee::findOne(['type' => LifetimeMembershipFee::TYPE]);
        }
        return $this->_lifetime_membership;
    }

    /**
     * @return string
     */
    public function getPayment_title()
    {
        $title = Yii::$app->formatter->asMembershipFee($this->type);
        if ($this->number_of_year > 0) {
            $title = Yii::t('app', '{count,plural,=1{{title} for (1) year.} other{{title} for (#) years.}}', [
                'count' => $this->number_of_year,
                'title' => $title,
            ]);
        }
        return $title;
    }

    /**
     * @return string
     */
    public function getComplete_name()
    {
        return $this->member !== null ? $this->member->userProfile->complete_name : null;
    }

    /**
     * @return string
     */
    public function getFirst_name()
    {
        return $this->member !== null ? $this->member->userProfile->first_name : null;
    }

    /**
     * @return string
     */
    public function getLast_name()
    {
        return $this->member !== null ? $this->member->userProfile->last_name : null;
    }
}
