<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TypeOfPractice]].
 *
 * @see TypeOfPractice
 */
class TypeOfPracticeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            'softDelete' => ['class' => 'yii2tech\ar\softdelete\SoftDeleteQueryBehavior'],
        ];
    }

    /**
     * {@inheritdoc}
     * @return TypeOfPractice[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TypeOfPractice|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
