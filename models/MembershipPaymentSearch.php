<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MembershipPayment;

/**
 * MembershipPaymentSearch represents the model behind the search form of `app\models\MembershipPayment`.
 */
class MembershipPaymentSearch extends MembershipPayment
{
    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = MembershipPayment::find();

        // add conditions that should always apply here
        $query->andWhere(['member_id' => Yii::$app->user->identity->getId()]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
