<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%sponsor}}".
 *
 * @property int $id
 * @property int|null $old_id
 * @property string $company_name
 * @property int|null $is_deleted
 */
class Sponsor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sponsor}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_name'], 'required'],
            [['company_name'], 'trim'],
            [['old_id', 'is_deleted'], 'integer'],
            [['company_name'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'old_id' => Yii::t('app', 'Old ID'),
            'company_name' => Yii::t('app', 'Company Name'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return SponsorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SponsorQuery(get_called_class());
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            'softDeleteBehavior' => [
                'class' => 'yii2tech\ar\softdelete\SoftDeleteBehavior',
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                ],
                'replaceRegularDelete' => true,
            ],
        ];
    }

    /**
     * @return array
     */
    public static function listData()
    {
        return ArrayHelper::map(
            static::find()->orderBy(['company_name' => SORT_ASC])->notDeleted()->all(),
            'id',
            'company_name'
        );
    }
}
