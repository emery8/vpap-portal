<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class ChangePasswordForm extends Model
{
    /**
     * @var string
     */
    public $old_password;
    public $new_password;
    public $new_password_repeat;

    private $_user = false;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['old_password', 'new_password', 'new_password_repeat'], 'required'],
            [['new_password_repeat'], 'compare', 'compareAttribute' => 'new_password'],
            [['new_password'], 'string', 'min' => Yii::$app->params['user.passwordMinLength']],
            [['old_password'], 'validatePassword'],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'old_password' => Yii::t('app', 'Old Password'),
            'new_password' => Yii::t('app', 'New Password'),
            'new_password_repeat' => Yii::t('app', 'Confirm New Password'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->old_password)) {
                $this->addError($attribute, 'Your old password does not match.');
            }
        }
    }

    /**
     * @return boolean
     */
    public function changePassword()
    {
        if (! $this->validate()) {
            return false;
        }

        $user = $this->getUser();
        $user->setPassword($this->new_password);
        if (! $user->save()) {
            return false;
        }
        return true;
    }

    /**
     * @return app\models\User
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findOne(['id' => Yii::$app->user->identity->getId()]);
        }
        return $this->_user;
    }
}