<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use app\models\Event;
use app\models\EventAttendance;
use app\models\FeaturedImage;
use app\models\ProgramTemplate;
use app\models\InvitationTemplate;
use app\models\enums\EventType;

/**
 * EventSearch represents the model behind the search form of `app\models\Event`.
 */
class MyEventAttendanceSearch extends Model
{
    public $id;
    public $member_id;
    public $event_id;
    public $cpd_point_earned;
    public $created_at;
    public $event_title;
    public $event_type;
    public $event_slug;
    public $event_start_date;
    public $event_end_date;
    public $evaluation_link;
    public $invitation_link;
    public $payment_confirmed;
    public $attended;

    public $program_template;
    public $invitation_template;
    public $attendance_certificate_template;
    public $cpd_certificate_template;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_type'], 'integer'],
            [['event_type'], 'in', 'range' => array_keys(EventType::listData())],
            [['event_start_date', 'event_end_date'], 'date', 'format' => 'yyyy-MM-dd'],
            [['event_title'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'event_type' => Yii::t('app', 'Type'),
            'event_title' => Yii::t('app', 'Title'),
            'event_start_date' => Yii::t('app', 'Start Date'),
            'event_end_date' => Yii::t('app', 'End Date'),
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventAttendance::find()->select([
            'id' => '{{%event_attendance}}.[[id]]',
            'member_id' => '{{%event_attendance}}.[[member_id]]',
            'event_id' => '{{%event_attendance}}.[[event_id]]',
            'cpd_point_earned' => '{{%event_attendance}}.[[cpd_point_earned]]',
            'payment_confirmed' => '{{%event_attendance}}.[[payment_confirmed]]',
            'created_at' => '{{%event_attendance}}.[[created_at]]',
            'attended' => '{{%event_attendance}}.[[attended]]',
            'event_type' => '{{%event}}.[[type]]',
            'event_title' => '{{%event}}.[[title]]',
            'event_start_date' => '{{%event}}.[[start_date]]',
            'event_end_date' => '{{%event}}.[[end_date]]',
        ]);

        // add conditions that should always apply here
        $query->joinWith([
            'event' => function ($query) {
                $query->with(['programTemplate', 'invitationTemplate', 'attendanceCertificateTemplate', 'cpdCertificateTemplate']);
            },
        ]);
        $query->andWhere(['member_id' => Yii::$app->user->identity->getId()])
            ->indexBy('id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['event_start_date' => SORT_DESC],
            ],
        ]);

        $dataProvider->sort->attributes['event_title'] = [
            'asc' => ['{{%event}}.[[title]]' => SORT_ASC],
            'desc' => ['{{%event}}.[[title]]' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['event_type'] = [
            'asc' => ['{{%event}}.[[type]]' => SORT_ASC],
            'desc' => ['{{%event}}.[[type]]' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['event_start_date'] = [
            'asc' => ['{{%event}}.[[start_date]]' => SORT_ASC],
            'desc' => ['{{%event}}.[[start_date]]' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['event_end_date'] = [
            'asc' => ['{{%event}}.[[end_date]]' => SORT_ASC],
            'desc' => ['{{%event}}.[[end_date]]' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['evaluation_link'] = [
            'asc' => ['{{%event}}.[[evaluation_link]]' => SORT_ASC],
            'desc' => ['{{%event}}.[[evaluation_link]]' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['invitation_link'] = [
            'asc' => ['{{%event}}.[[invitation_link]]' => SORT_ASC],
            'desc' => ['{{%event}}.[[invitation_link]]' => SORT_DESC],
        ];

        /*$dataProvider->sort->attributes['program_template'] = [
            'asc' => ['{{%attachment}}.[[event_id]]' => SORT_ASC],
            'desc' => ['{{%attachment}}.[[event_id]]' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['invitation_template'] = [
            'asc' => ['{{%attachment}}.[[event_id]]' => SORT_ASC],
            'desc' => ['{{%attachment}}.[[event_id]]' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['certificate_template'] = [
            'asc' => ['{{%attachment}}.[[event_id]]' => SORT_ASC],
            'desc' => ['{{%attachment}}.[[event_id]]' => SORT_DESC],
        ];*/

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            '{{%event}}.[[type]]' => $this->event_type,
            '{{%event}}.[[start_date]]' => $this->event_start_date,
            '{{%event}}.[[end_date]]' => $this->event_end_date,
        ]);

        $query->andFilterWhere(['like', '{{%event}}.[[title]]', $this->event_title]);

        return $dataProvider;
    }
}
