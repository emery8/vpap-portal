<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%municipality}}".
 *
 * @property int $id
 * @property int $province_id
 * @property string $name
 *
 * @property Province $province
 */
class Municipality extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%municipality}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['province_id', 'name'], 'required'],
            [['province_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['province_id'], 'exist', 'skipOnError' => true, 'targetClass' => Province::className(), 'targetAttribute' => ['province_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'province_id' => Yii::t('app', 'Province ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * Gets query for [[Province]].
     *
     * @return \yii\db\ActiveQuery|ProvinceQuery
     */
    public function getProvince()
    {
        return $this->hasOne(Province::className(), ['id' => 'province_id']);
    }

    /**
     * {@inheritdoc}
     * @return MunicipalityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MunicipalityQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function listData($province_id)
    {
        $out = [];
        $models = static::find()
            ->andWhere(['province_id' => $province_id])
            ->orderBy(['name' => SORT_ASC])
            ->all();
        if (count($models)) {
            foreach ($models as $model) {
                $out['output'][] = [
                    'id' => $model->name,
                    'name' => $model->name,
                ];
            }
        }
        $out['selected'] = '';
        return $out;
    }

    /**
     * @return array
     */
    public static function asOverseas()
    {
        $out = [];
        $model = static::findOne(1);
        if ($model) {
            $out[] = [
                'id' => $model->name,
                'name' => $model->name,
            ];
        }

        return $out;
    }
}
