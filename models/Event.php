<?php

namespace app\models;

use Yii;
use app\models\FeaturedImage;
use app\models\AttendanceCertificateTemplate;
use app\models\CpdCertificateTemplate;
use app\models\IdentificationTemplate;
use app\models\InvitationTemplate;
use app\models\ProgramTemplate;
use app\models\enums\EventType;
use app\models\enums\MembershipType;
use app\models\enums\EventInvitationType;
use yii\base\InvalidArgumentException;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\SluggableBehavior;
use yii\bootstrap4\Html;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%event}}".
 *
 * @property int $id
 * @property int|null $old_id
 * @property int $type
 * @property string $title
 * @property string $slug
 * @property string $location
 * @property string $start_date
 * @property string $end_date
 * @property int|null $enable_discount
 * @property float|null $membership_fee
 * @property float $lifetime_member_fee
 * @property float $regular_member_fee
 * @property float $non_member_fee
 * @property int $attendance_type
 * @property float $cpd_point
 * @property int $maximum_participant
 * @property string|null $evaluation_link
 * @property string|null $invitation_link
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Attachment[] $attachments
 * @property EventAttendance[] $eventAttendances
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%event}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['old_id', 'type', 'enable_discount', 'attendance_type', 'maximum_participant', 'created_at', 'updated_at'], 'integer'],
            [['type', 'title', 'location', 'start_date', 'end_date', 'lifetime_member_fee', 'regular_member_fee', 'non_member_fee', 'attendance_type', 'cpd_point', 'maximum_participant'], 'required'],
            [['title', 'location', 'start_date', 'end_date', 'membership_fee', 'lifetime_member_fee', 'regular_member_fee', 'non_member_fee', 'cpd_point', 'maximum_participant'], 'trim'],
            [['title', 'slug'], 'string'],
            [['start_date', 'end_date'], 'date', 'format' => 'yyyy-MM-dd'],
            [['start_date'], 'compare', 'compareAttribute' => 'end_date', 'operator' => '<=', 'enableClientValidation' => false],
            [['lifetime_member_fee', 'regular_member_fee', 'non_member_fee', 'cpd_point'], 'number'],
            [['location'], 'string', 'max' => 1000],
            [['evaluation_link', 'invitation_link'], 'string', 'max' => 255],
            [['evaluation_link', 'invitation_link'], 'url'],
            [['type'], 'in', 'range' => array_keys(EventType::listData())],
            [['membership_fee'], 'required',
                'when' => function ($model) {
                    return (int) $model->type === EventType::ANNUAL_CONFERENCE;
                }, 'enableClientValidation' => false, 'message' => Yii::t('app', 'Membership Fee cannot be blank if event type is set to "{event_type}".', ['event_type' => EventType::getLabel(EventType::ANNUAL_CONFERENCE)]),
            ],
            /*[['membership_fee'], 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'number',
                'when' => function ($model) {
                    return (int) $model->type === EventType::ANNUAL_CONFERENCE;
                }, 'enableClientValidation' => false, 'message' => Yii::t('app', 'Membership Fee must be greater than "{amount}".', ['amount' => Yii::$app->formatter->asCurrency(number_format(0, 2))]),
            ],*/
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'old_id' => Yii::t('app', 'Old ID'),
            'type' => Yii::t('app', 'Type'),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
            'location' => Yii::t('app', 'Location'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'enable_discount' => Yii::t('app', 'Enable Discount'),
            'membership_fee' => Yii::t('app', 'Membership Fee'),
            'lifetime_member_fee' => Yii::t('app', 'Lifetime Member Fee'),
            'regular_member_fee' => Yii::t('app', 'Regular Member Fee'),
            'non_member_fee' => Yii::t('app', 'Non Member Fee'),
            'attendance_type' => Yii::t('app', 'Attendance Type'),
            'cpd_point' => Yii::t('app', 'CPD Point'),
            'maximum_participant' => Yii::t('app', 'Maximum Participant'),
            'evaluation_link' => Yii::t('app', 'Evaluation Link'),
            'invitation_link' => Yii::t('app', 'Invitation Link'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Attachments]].
     *
     * @return \yii\db\ActiveQuery|AttachmentQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::className(), ['event_id' => 'id']);
    }

    /**
     * Gets query for [[Attachments]].
     *
     * @return \yii\db\ActiveQuery|AttachmentQuery
     */
    public function getFeaturedImage()
    {
        return $this->hasOne(FeaturedImage::className(), ['event_id' => 'id']);
    }

    /**
     * Gets query for [[Attachments]].
     *
     * @return \yii\db\ActiveQuery|AttachmentQuery
     */
    public function getProgramTemplate()
    {
        return $this->hasOne(ProgramTemplate::className(), ['event_id' => 'id']);
    }

    /**
     * Gets query for [[Attachments]].
     *
     * @return \yii\db\ActiveQuery|AttachmentQuery
     */
    public function getInvitationTemplate()
    {
        return $this->hasOne(InvitationTemplate::className(), ['event_id' => 'id']);
    }

    /**
     * Gets query for [[Attachments]].
     *
     * @return \yii\db\ActiveQuery|AttachmentQuery
     */
    public function getAttendanceCertificateTemplate()
    {
        return $this->hasOne(AttendanceCertificateTemplate::className(), ['event_id' => 'id']);
    }

    /**
     * Gets query for [[Attachments]].
     *
     * @return \yii\db\ActiveQuery|AttachmentQuery
     */
    public function getCpdCertificateTemplate()
    {
        return $this->hasOne(CpdCertificateTemplate::className(), ['event_id' => 'id']);
    }

    /**
     * Gets query for [[EventAttendances]].
     *
     * @return \yii\db\ActiveQuery|EventAttendanceQuery
     */
    public function getEventAttendances()
    {
        return $this->hasMany(EventAttendance::className(), ['event_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return EventQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EventQuery(get_called_class());
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'title',
            ],
        ];
    }

    /**
     * @return string
     */
    public function getEvent_schedule()
    {
        return Yii::$app->formatter->asDate($this->start_date) . ' - ' .
            Yii::$app->formatter->asDate($this->end_date);
    }

    /**
     * @return boolean
     */
    public function getIs_discount_enabled()
    {
        return (bool) $this->enable_discount === true;
    }

    /**
     * @return boolean
     */
    public function getIs_annual_conference()
    {
        return (int) $this->type === EventType::ANNUAL_CONFERENCE;
    }

    /**
     * @return boolean
     */
    public function getHas_reached_maximum_participants()
    {
        return $this->participants_count >= $this->maximum_participant;
    }

    /**
     * @return integer|null
     */
    public function getParticipants_count()
    {
        if ($this->isNewRecord) {
            return null;
        }

        return empty($this->participantsAggregation) ? 0 :
            $this->participantsAggregation[0]['counted'];
    }

    /**
     * @return array
     */
    public function getParticipantsAggregation()
    {
        return $this->getEventAttendances()
            ->select(['event_id', 'counted' => 'count(*)'])
            ->groupBy('event_id')
            ->asArray(true);
    }

    /**
     * @param app\models\UserMembership $model
     * @throws InvalidArgumentException
     */
    public function getFeeByMembershipType(UserMembership $model)
    {
        switch ($model->type) {
            case MembershipType::LIFETIME:
                return (float) $this->lifetime_member_fee;
            case MembershipType::REGULAR:
                return (float) $this->regular_member_fee;
            case MembershipType::NONMEMBER:
                return (float) $this->non_member_fee;
            default:
                throw new InvalidArgumentException(Yii::t('app', 'User membership type is not set.'));
        }
    }

    /**
     * @param integer $attendance_type
     * @return boolean
     */
    public static function canAttendEvent($attendance_type)
    {
        $membership_type = Yii::$app->user->identity->userMembership->type;
        switch ($attendance_type) {
            case EventInvitationType::OPEN:
                return ($membership_type === MembershipType::LIFETIME) || ($membership_type === MembershipType::REGULAR) || ($membership_type === MembershipType::NONMEMBER);
            case EventInvitationType::MEMBER:
                return ($membership_type === MembershipType::LIFETIME) || ($membership_type === MembershipType::REGULAR);
            case EventInvitationType::INVITE:
                return true;
            default:
                return false;
        }
    }
}

