<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%province}}".
 *
 * @property int $id
 * @property int $region_id
 * @property string $name
 *
 * @property Region $region
 */
class Province extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%province}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['region_id', 'name'], 'required'],
            [['region_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Region::className(), 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * Gets query for [[Region]].
     *
     * @return \yii\db\ActiveQuery|RegionQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * {@inheritdoc}
     * @return ProvinceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProvinceQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function listData($region_id)
    {
        $out = [];
        $models = static::find()
            ->andWhere(['region_id' => $region_id])
            ->orderBy(['name' => SORT_ASC])
            ->all();
        if (count($models)) {
            foreach ($models as $model) {
                $out['output'][] = [
                    'id' => $model->name,
                    'name' => $model->name,
                ];
            }
        }
        $out['selected'] = '';
        return $out;
    }

    /**
     * @return array
     */
    public static function asOverseas()
    {
        $out = [];
        $model = static::findOne(1);
        if ($model) {
            $out[] = [
                'id' => $model->name,
                'name' => $model->name,
            ];
        }

        return $out;
    }
}
