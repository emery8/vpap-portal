<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Municipality]].
 *
 * @see Municipality
 */
class MunicipalityQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Municipality[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Municipality|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
