<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UserEmployment]].
 *
 * @see UserEmployment
 */
class UserEmploymentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return UserEmployment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UserEmployment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
