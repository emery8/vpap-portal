<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%type_of_practice}}".
 *
 * @property int $id
 * @property string $description
 * @property int|null $is_deleted
 */
class TypeOfPractice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%type_of_practice}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description'], 'trim'],
            [['is_deleted'], 'integer'],
            [['description'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'description' => Yii::t('app', 'Description'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return TypeOfPracticeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TypeOfPracticeQuery(get_called_class());
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            'softDeleteBehavior' => [
                'class' => 'yii2tech\ar\softdelete\SoftDeleteBehavior',
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                ],
                'replaceRegularDelete' => true,
            ],
        ];
    }

    /**
     * @return array
     */
    public static function listData()
    {
        return ArrayHelper::map(
            static::find()->notDeleted()->orderBy(['description' => SORT_ASC])->all(),
            'id',
            'description'
        );
    }
}
