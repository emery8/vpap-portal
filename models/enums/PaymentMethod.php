<?php

namespace app\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 * @author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>
 */
class PaymentMethod extends BaseEnum
{
    const SPONSOR = 1;
    const PERSONAL = 2;
    const FREE = 3;

    /**
     * @var array
     */
    public static $list = [
        self::SPONSOR => 'Sponsor',
        self::PERSONAL => 'Personal Account',
        self::FREE => 'Free',
    ];
}
