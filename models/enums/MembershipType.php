<?php

namespace app\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 * @author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>
 */
class MembershipType extends BaseEnum
{
    const LIFETIME = 1;
    const REGULAR = 2;
    const NONMEMBER = 3;

    /**
     * @var array
     */
    public static $list = [
        self::LIFETIME => 'Lifetime',
        self::REGULAR => 'Regular',
        self::NONMEMBER => 'Non-member',
    ];
}
