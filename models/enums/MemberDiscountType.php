<?php

namespace app\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 * @author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>
 */
class MemberDiscountType extends BaseEnum
{
    const FULL_PAYMENT = 1;
    const DISCOUNT_50 = 2;
    const DISCOUNT_20 = 3;
    const FULL_DISCOUNT = 4;

    /**
     * @var array
     */
    public static $list = [
        self::FULL_PAYMENT => 'Full Payment',
        self::DISCOUNT_50 => '50',
        self::DISCOUNT_20 => '20',
        self::FULL_DISCOUNT => 'Free',
    ];
}
