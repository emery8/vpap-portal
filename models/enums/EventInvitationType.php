<?php

namespace app\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 * @author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>
 */
class EventInvitationType extends BaseEnum
{
    const OPEN = 1;
    const MEMBER = 2;
    const INVITE = 3;

    /**
     * @var array
     */
    public static $list = [
        self::OPEN => 'Open',
        self::MEMBER => 'Member Only',
        self::INVITE => 'Invite Only',
    ];
}
