<?php

namespace app\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 * @author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>
 */
class EventType extends BaseEnum
{
    const MEMBERSHIP_MEETING = 1;
    const SEMINAR_WORKSHOP = 2;
    const REGIONAL_CONFERENCE = 3;
    const ANNUAL_CONFERENCE = 4;
    const OTHER_CONFERENCE = 5;

    /**
     * @var array
     */
    public static $list = [
        self::MEMBERSHIP_MEETING => 'Membership Meeting',
        self::SEMINAR_WORKSHOP => 'Seminar and Workshop',
        self::REGIONAL_CONFERENCE => 'Regional Conference',
        self::ANNUAL_CONFERENCE => 'Annual Conference',
        self::OTHER_CONFERENCE => 'Other Conference',
    ];
}
