<?php

namespace app\models\enums;

use yii2mod\enum\helpers\BaseEnum;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class EventAttendanceType extends BaseEnum
{
    const PARTICIPANT = 1;
    const SPEAKER = 2;
    const MODERATOR = 3;
    const ORGANIZER = 4;
    const PRC_REPRESENTATIVE = 5;

    /**
     * @var array
     */
    public static $list = [
        self::PARTICIPANT => 'Participant',
        self::SPEAKER => 'Speaker',
        self::MODERATOR => 'Moderator',
        self::ORGANIZER => 'Organizer',
        self::PRC_REPRESENTATIVE => 'PRC Representative',
    ];
}
