<?php

namespace app\models;

use Yii;
use yii\base\InvalidArgumentException;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%membership_activity}}".
 *
 * @property int $id
 * @property int $member_id
 * @property string|null  $year_event_attended
 * @property string|null $year_membership_paid
 *
 * @property User $member
 */
class MembershipActivity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%membership_activity}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id'], 'required'],
            [['member_id'], 'integer'],
            [['year_event_attended', 'year_membership_paid'], 'in', 'range' => array_values(static::getYearServedRange()), 'allowArray' => true],
            [['member_id'], 'unique'],
            [['member_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['member_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', 'Member ID'),
            'year_event_attended' => Yii::t('app', 'Year Attended the Annual Conference'),
            'year_membership_paid' => Yii::t('app', 'Year Membership Fee Paid'),
        ];
    }

    /**
     * Gets query for [[Member]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getMember()
    {
        return $this->hasOne(User::className(), ['id' => 'member_id']);
    }

    /**
     * {@inheritdoc}
     * @return MembershipActivityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MembershipActivityQuery(get_called_class());
    }

    /**
     * {@inheritDoc}
     */
    public function beforeSave($insert)
    {
        if ($this->year_event_attended !== null) {
            $this->setAttribute('year_event_attended', Json::encode($this->year_event_attended));
        }

        if ($this->year_membership_paid !== null) {
            $this->setAttribute('year_membership_paid', Json::encode($this->year_membership_paid));
        }
        return parent::beforeSave($insert);
    }

    /**
     * {@inheritDoc}
     */
    public function afterFind()
    {
        try {
            if ($this->year_event_attended !== null) {
                $this->setAttribute('year_event_attended', Json::decode($this->year_event_attended));
            }

            if ($this->year_membership_paid !== null) {
                $this->setAttribute('year_membership_paid', Json::decode($this->year_membership_paid));
            }
        } catch (InvalidArgumentException $e) {
            throw $e;
        }
        parent::afterFind();
    }

    /**
     * @return string
     */
    public function getComplete_name()
    {
        return $this->member !== null ? $this->member->userProfile->complete_name : null;
    }

    /**
     * @return string
     */
    public function getFirst_name()
    {
        return $this->member !== null ? $this->member->userProfile->first_name : null;
    }

    /**
     * @return string
     */
    public function getLast_name()
    {
        return $this->member !== null ? $this->member->userProfile->last_name : null;
    }

    /**
     * @return string
     */
    public function getMembership_type()
    {
        return $this->member->userMembership->type;
    }

    /**
     * @return string
     */
    public function getYear_initial_joined()
    {
        return $this->member->userMembership->year_initial_joined;
    }

    /**
     * @return integer
     */
    public function getEvent_attendance_count()
    {
        $arr1 = (is_null($this->year_event_attended)) ? [] : $this->year_event_attended;
        return count($arr1, SORT_REGULAR);
    }

    /**
     * @return array
     */
    public static function getYearServedRange()
    {
        $range = range(Yii::$app->formatter->asDate(time(), 'yyyy'), 1950);
        return array_combine($range, $range);
    }
}
