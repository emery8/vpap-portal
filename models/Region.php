<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%region}}".
 *
 * @property int $id
 * @property string $name
 *
 * @property Province[] $provinces
 */
class Region extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%region}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * Gets query for [[Provinces]].
     *
     * @return \yii\db\ActiveQuery|ProvinceQuery
     */
    public function getProvinces()
    {
        return $this->hasMany(Province::className(), ['region_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return RegionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RegionQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function listData()
    {
        $out = [];
        $models = static::find()->orderBy(['name' => SORT_ASC])->all();
        if (count($models)) {
            foreach ($models as $model) {
                if ($model->name === 'Overseas') {
                    continue;
                }
                $out['output'][] = [
                    'id' => $model->name,
                    'name' => $model->name,
                ];
            }
        }
        $out['selected'] = '';
        return $out;
    }

    /**
     * @return array
     */
    public static function asOverseas()
    {
        $out = [];
        $model = static::findOne(1);
        if ($model) {
            $out[] = [
                'id' => $model->name,
                'name' => $model->name,
            ];
        }

        return $out;
    }
}
