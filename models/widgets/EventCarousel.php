<?php

namespace app\models\widgets;

use Yii;
use yii\base\BaseObject;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use app\models\Event;
use app\models\FeaturedImage;
use app\models\enums\EventInvitationType;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class EventCarousel extends BaseObject
{
    /**
     * @var string
     */
    public $itemTemplate;

    /**
     * @var array
     */
    public $buttonOptions = [
        'class' => 'btn btn-outline-light btn-pill px-4 mb-2',
    ];

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        parent::init();

        if ($this->itemTemplate === null) {
            $this->itemTemplate = "\n{title}\n{schedule}\n{button}\n";
        }
    }

    /**
     * @return array
     */
    public function getItems()
    {
        $models = $this->getModels();
        $data = [];

        foreach ($models as $model) {
            $data[] = [
                'content' => $model['content'],
                'caption' => preg_replace_callback('/{\\w+}/', function ($matches) use ($model) {
                    $content = $this->renderSection($matches[0], $model);
        
                    return ($content === false) ? $matches[0] : $content;
                }, $this->itemTemplate),
            ];
        }

        return $data;
    }

    /**
     * @param string $name
     * @param array $model
     * @return void
     */
    protected function renderSection($name, $model)
    {
        switch ($name) {
            case '{title}':
                return $this->renderTitle($model['title']);
            case '{event_schedule}':
                return $this->renderSchedule($model['schedule']);
            case '{button}':
                return $this->renderButton($model['slug']);
        }
    }

    /**
     * @return array
     */
    protected function getModels()
    {
        $now = Yii::$app->formatter->asDate(time(), 'yyyy-MM-dd');
        $query = Event::find()
            ->select([
                '{{%event}}.[[id]]',
                '{{%event}}.[[slug]]',
                '{{%event}}.[[title]]',
                '{{%event}}.[[start_date]]',
                '{{%event}}.[[end_date]]',
            ])->joinWith(['attachments' => function ($query) {
                $query->select([
                    '{{%attachment}}.[[event_id]]',
                    '{{%attachment}}.[[content]]',
                    '{{%attachment}}.[[type]]',
                ])->andWhere(['{{%attachment}}.[[type]]' => FeaturedImage::TYPE]);
            }])
            ->andWhere(['not in', '{{%event}}.[[attendance_type]]', [EventInvitationType::INVITE]])
            ->andWhere(['>=', '{{%event}}.[[end_date]]', $now]);

        $models = $query->all();
        $data = [];
        if (! empty($models) && is_array($models)) {
            foreach ($models as $model) {
                if (isset($model->attachments[0]) && ($model->attachments[0] instanceof FeaturedImage )) {
                    $featuredImage = $model->attachments[0];
                    try {
                        $content = Json::decode($featuredImage->content);
                        if (isset($content['caption'])) {
                            $data[] = [
                                'id' => $model->id,
                                'slug' => $model->slug,
                                'content' => Html::img($featuredImage->getThumbUploadUrl('content', 'carousel'), ['alt' => Html::encode($model->title), 'class' => 'd-block w-100']),
                                'title' => $model->title,
                                'schedule' => $model->event_schedule,
                            ];
                        }
                    } catch (InvalidArgumentException $e) {
                        // silence
                    }
                }
            }
        }
        return $data;
    }

    /**
     * @param string $title
     * @return string
     */
    private function renderTitle($title)
    {
        return Html::tag(
            'h1',
            Html::encode($title),
            ['class' => 'display-3 mb-5']
        );
    }

    /**
     * @param string $schedule
     * @return string
     */
    private function renderSchedule($schedule)
    {
        return Html::tag(
            'h3',
            Html::encode($schedule),
            ['class' => 'font-weight-light mb-4']
        );
    }

    /**
     * @param string $slug
     * @return string
     */
    private function renderButton($slug)
    {
        $url = ['slug' => $slug];
        $link = ArrayHelper::remove($this->buttonOptions, 'link');
        $label = ArrayHelper::remove($this->buttonOptions, 'label');

        if (! $link) {
            $link = 'event/signup';
        }

        if (! $label) {
            $label = Yii::t('app', 'REGISTER');
        }

        $url[0] = $link;
        return Html::a(
            $label,
            $url,
            $this->buttonOptions,
        );
    }
}
