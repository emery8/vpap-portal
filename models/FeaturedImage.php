<?php

namespace app\models;

use Yii;
use app\behaviors\UploadImageBehavior;
use app\traits\AttachmentTrait;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class FeaturedImage extends Attachment
{
    use AttachmentTrait;

    const TYPE = 'featured_image';
    const SCENARIO_INSERT = 'insert';
    const SCENARIO_UPDATE = 'update';

    /**
     * @var array
     */
    public $allowedMimeType = [
        'image/png',
        'image/jpeg',
    ];

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'required', 'on' => self::SCENARIO_INSERT],
            [['event_id'], 'required', 'on' => self::SCENARIO_UPDATE],
            [['event_id'], 'integer'],
            [['type'], 'string', 'max' => 50],
            [['content'], 'file', 'skipOnEmpty' => false, 'mimeTypes' => $this->allowedMimeType, 'on' => [self::SCENARIO_INSERT]],
            [['content'], 'file', 'skipOnEmpty' => true, 'mimeTypes' => $this->allowedMimeType, 'on' => [self::SCENARIO_UPDATE]],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['event_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'content' => Yii::t('app', 'Featured Image'),
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        $this->type = self::TYPE;
        parent::init();
    }

    /**
     * {@inheritDoc}
     */
    public static function find()
    {
        return new AttachmentQuery(get_called_class(), ['type' => self::TYPE, 'tableName' => self::tableName()]);
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'content',
                'scenarios' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE],
                'path' => '@webroot/uploads/' . self::TYPE . '/{event.slug}',
                'url' => '@web/uploads/' . self::TYPE . '/{event.slug}',
                'thumbs' => [
                    'carousel' => ['width' => 1920, 'height' => 500, 'bg_color' => 'ebedef'],
                    'event_thumb' => ['height' => 130, 'quality' => 100],
                ],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function beforeSave($insert)
    {
        $this->type = self::TYPE;
        return parent::beforeSave($insert);
    }
}
