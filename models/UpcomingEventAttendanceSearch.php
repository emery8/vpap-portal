<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use app\models\Event;
use app\models\EventAttendance;
use app\models\FeaturedImage;
use app\models\enums\EventType;
use app\models\enums\EventInvitationType;

/**
 * EventSearch represents the model behind the search form of `app\models\Event`.
 */
class UpcomingEventAttendanceSearch extends Event
{
    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Event::find();

        // add conditions that should always apply here
        $now = Yii::$app->formatter->asDate(time(), 'yyyy-MM-dd');
        $myEvents =ArrayHelper::getColumn(
            EventAttendance::find()
                ->select(['event_id'])
                ->where(['member_id' => Yii::$app->user->identity->getId()])
                ->all(),
            'event_id'
        );
        $query->with(['attachments'])
            ->where([
                'and',
                ['>=', '{{%event}}.[[end_date]]', $now],
                ['not in', '{{%event}}.[[id]]', $myEvents],
                ['not in', '{{%event}}.[[attendance_type]]', [EventInvitationType::INVITE]]
            ])
            ->indexBy('{{%event}}.[[id]]');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC],
            ],
            'pagination' => [
                'pageSize' => 1,
            ],
        ]);

        return $dataProvider;
    }
}
