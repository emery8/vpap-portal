<?php

namespace app\models;

use Yii;
use app\behaviors\UploadBehavior;
use app\traits\AttachmentTrait;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class CpdCertificateTemplate extends Attachment
{
    use AttachmentTrait;

    const TYPE = 'cpd_certificate_template';
    const SCENARIO_INSERT = 'insert';
    const SCENARIO_UPDATE = 'update';

    /**
     * @var array
     */
    public $allowedMimeType = [
        'image/*',
    ];

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_id'], 'integer'],
            [['type'], 'string', 'max' => 50],
            [['content'], 'file', 'skipOnEmpty' => true, 'mimeTypes' => $this->allowedMimeType, 'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['event_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'content' => Yii::t('app', 'CPD Certificate Template'),
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        $this->type = self::TYPE;
        parent::init();
    }

    /**
     * {@inheritDoc}
     */
    public static function find()
    {
        return new AttachmentQuery(get_called_class(), ['type' => self::TYPE, 'tableName' => self::tableName()]);
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::class,
                'attribute' => 'content',
                'scenarios' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE],
                'path' => '@webroot/uploads/' . self::TYPE . '/{event.slug}',
                'url' => '@web/uploads/' . self::TYPE . '/{event.slug}',
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function beforeSave($insert)
    {
        $this->type = self::TYPE;
        return parent::beforeSave($insert);
    }
}
