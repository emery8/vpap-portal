<?php

namespace app\models;

use Yii;
use app\behaviors\MemberDiscountTypeBehavior;
use app\models\enums\MemberDiscountType;
use app\models\enums\MembershipType;
use yii\base\InvalidArgumentException;

/**
 * This is the model class for table "{{%user_membership}}".
 *
 * @property int $user_id
 * @property int|null $type
 * @property int|null $number_years_active
 * @property string|null $year_initial_joined
 * @property int|null $discount_type
 *
 * @property User $user
 */
class UserMembership extends \yii\db\ActiveRecord
{
    const SCENARIO_INSERT = 'insert';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_PROFILE_UPDATE = 'profile_update';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_membership}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['year_initial_joined'], 'required', 'on' => self::SCENARIO_PROFILE_UPDATE],
            [['user_id', 'type', 'number_years_active'], 'integer'],
            [['discount_type'], 'integer', 'on' => [self::SCENARIO_INSERT, self::SCENARIO_UPDATE]],
            [['year_initial_joined'], 'string', 'max' => 4],
            [['user_id'], 'unique'],
            [['type'], 'in', 'range' => array_keys(MembershipType::listData())],
            [['discount_type'], 'in', 'range' => array_keys(MemberDiscountType::listData())],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'type' => Yii::t('app', 'Type'),
            'number_years_active' => Yii::t('app', 'Number Years Active'),
            'year_initial_joined' => Yii::t('app', 'Year Initial Joined'),
            'discount_type' => Yii::t('app', 'Discount Type'),
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return UserMembershipQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserMembershipQuery(get_called_class());
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => MemberDiscountTypeBehavior::class,
                'attribute' => 'discount_type',
                'scenarios' => [
                    self::SCENARIO_INSERT,
                    self::SCENARIO_UPDATE,
                ],
            ],
        ];
    }

    /**
     * @return boolean
     */
    public function getIs_lifetime()
    {
        return ($this->type === null) ? false :
            (int) $this->type === MembershipType::LIFETIME;
    }

    /**
     * @return boolean
     */
    public function getIs_regular()
    {
        return ($this->type === null) ? false :
            (int) $this->type === MembershipType::REGULAR;
    }

    /**
     * @return boolean
     */
    public function getIs_nonmember()
    {
        return ($this->type === null) ? false :
            (int) $this->type === MembershipType::NONMEMBER;
    }

    /**
     * @return boolean
     */
    public function getDiscount_none()
    {
        return ($this->discount_type === null) ? false:
            (int) $this->discount_type === MemberDiscountType::FULL_PAYMENT;
    }

    /**
     * @return boolean
     */
    public function getDiscount_50()
    {
        return ($this->discount_type === null) ? false:
            (int) $this->discount_type === MemberDiscountType::DISCOUNT_50;
    }

    /**
     * @return boolean
     */
    public function getDiscount_20()
    {
        return ($this->discount_type === null) ? false:
            (int) $this->discount_type === MemberDiscountType::DISCOUNT_20;
    }

    /**
     * @return boolean
     */
    public function getDiscount_full()
    {
        return ($this->discount_type === null) ? false:
            (int) $this->discount_type === MemberDiscountType::FULL_DISCOUNT;
    }

    /**
     * @return integer
     * @throws InvalidArgumentException
     */
    public function getDiscountRate()
    {
        $type = $this->discount_type;
        switch ($type) {
            case MemberDiscountType::FULL_PAYMENT:
                return 1;
            case MemberDiscountType::DISCOUNT_50:
                return 0.5;
            case MemberDiscountType::DISCOUNT_20:
                return 0.8;
            case MemberDiscountType::FULL_DISCOUNT:
                return 0;
            default:
                throw new InvalidArgumentException(Yii::t('app', 'User membership discount type is invalid or not set.'));
        }
    }

    /**
     * @param array $selection
     * @param integer $status
     * @return integer
     */
    public static function batchUpdateMembershipType($selection, $type)
    {
        $affectedRows = 0;
        $items = static::find()->where(['in', 'user_id', $selection])->all();
        if (is_array($items) && count($items)) {
            foreach ($items as $item) {
                $item->setAttribute('type', $type);

                if (MembershipType::isValidValue($type) && ((int) $type === MembershipType::LIFETIME)) {
                    $item->setAttribute('discount_type', MemberDiscountType::FULL_DISCOUNT);
                }

                $item->scenario = self::SCENARIO_UPDATE;
                if ($item->update() > 0) {
                    $affectedRows++;
                }
            }
        }

        return $affectedRows;
    }

    /**
     * @param array $selection
     * @param integer $status
     * @return integer
     */
    public static function batchUpdateDiscountType($selection, $type)
    {
        $affectedRows = 0;
        $items = static::find()->where(['in', 'user_id', $selection])->all();
        if (is_array($items) && count($items)) {
            foreach ($items as $item) {
                $item->setAttribute('discount_type', $type);

                if (MembershipType::isValidValue($type) && ((int) $type === MembershipType::LIFETIME)) {
                    $item->setAttribute('discount_type', MemberDiscountType::FULL_DISCOUNT);
                }

                if ($item->update() > 0) {
                    $affectedRows++;
                }
            }
        }

        return $affectedRows;
    }
}
