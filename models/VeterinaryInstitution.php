<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%veterinary_institution}}".
 *
 * @property int $id
 * @property string $institution
 * @property int|null $is_deleted
 */
class VeterinaryInstitution extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%veterinary_institution}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['institution'], 'required'],
            [['institution'], 'trim'],
            [['is_deleted'], 'integer'],
            [['institution'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'institution' => Yii::t('app', 'Institution'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return VeterinaryInstitutionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VeterinaryInstitutionQuery(get_called_class());
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            'softDeleteBehavior' => [
                'class' => 'yii2tech\ar\softdelete\SoftDeleteBehavior',
                'softDeleteAttributeValues' => [
                    'is_deleted' => true,
                ],
                'replaceRegularDelete' => true,
            ],
        ];
    }

    /**
     * @return array
     */
    public static function listData()
    {
        return ArrayHelper::map(
            static::find()->notDeleted()->all(),
            'id',
            'institution'
        );
    }
}
