<?php

namespace app\models;

use Yii;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class LifetimeMembershipFee extends MembershipFee
{
    const TYPE = 2;

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        $this->type = self::TYPE;
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'amount' => Yii::t('app', 'Lifetime Membership Fee'),
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public static function find()
    {
        return new MembershipFeeQuery(get_called_class(), ['type' => self::TYPE, 'tableName' => self::tableName()]);
    }

    /**
     * {@inheritDoc}
     */
    public function beforeSave($insert)
    {
        $this->type = self::TYPE;
        return parent::beforeSave($insert);
    }
}
