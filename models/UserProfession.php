<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%user_profession}}".
 *
 * @property int $user_id
 * @property string $license_number
 * @property string|null $expiration_date
 * @property string|null $type_of_practice
 * @property string|null $species_of_specialization
 *
 * @property User $user
 */
class UserProfession extends \yii\db\ActiveRecord
{
    const SCENARIO_UPDATE = 'update';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_profession}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'license_number'], 'required'],
            [['user_id', 'license_number', 'expiration_date', 'type_of_practice', 'species_of_specialization'], 'required', 'on' => self::SCENARIO_UPDATE],
            [['user_id'], 'integer'],
            [['expiration_date'], 'date', 'format' => 'yyyy-MM-dd'],
            [['type_of_practice', 'species_of_specialization'], 'safe'],
            [['license_number'], 'string', 'max' => 30],
            [['license_number'], 'unique'],
            [['user_id'], 'unique'],
            [['type_of_practice'], 'in', 'range' => array_keys(TypeOfPractice::listData()), 'allowArray' => true],
            [['species_of_specialization'], 'in', 'range' => array_keys(SpeciesOfSpecialization::listData()), 'allowArray' => true],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'license_number' => Yii::t('app', 'License Number'),
            'expiration_date' => Yii::t('app', 'Expiration Date'),
            'type_of_practice' => Yii::t('app', 'Type Of Practice'),
            'species_of_specialization' => Yii::t('app', 'Species Of Specialization'),
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return UserProfessionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserProfessionQuery(get_called_class());
    }

    /**
     * {@inheritDoc}
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_UPDATE => ['user_id', 'license_number', 'expiration_date', 'type_of_practice', 'species_of_specialization'],
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function beforeSave($insert)
    {
        $this->setAttribute('license_number', ltrim((integer) $this->license_number, 0));
        $this->setAttribute('type_of_practice', Json::encode($this->type_of_practice));
        $this->setAttribute('species_of_specialization', Json::encode($this->species_of_specialization));

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritDoc}
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->setAttributes([
            'type_of_practice' => Json::decode($this->type_of_practice),
            'species_of_specialization' => Json::decode($this->species_of_specialization),
        ]);
    }
}
