<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UserMembership]].
 *
 * @see UserMembership
 */
class UserMembershipQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return UserMembership[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UserMembership|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
