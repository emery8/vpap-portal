<?php

namespace app\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%attachment}}".
 *
 * @property int $id
 * @property int $event_id
 * @property string $content
 * @property string $type
 *
 * @property Event $event
 */
class Attachment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%attachment}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_id', 'content', 'type'], 'required'],
            [['event_id'], 'integer'],
            [['content'], 'string'],
            [['type'], 'string', 'max' => 50],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['event_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'event_id' => Yii::t('app', 'Event ID'),
            'content' => Yii::t('app', 'Content'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * Gets query for [[Event]].
     *
     * @return \yii\db\ActiveQuery|EventQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * {@inheritdoc}
     * @return AttachmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AttachmentQuery(get_called_class());
    }

    /**
     * {@inheritDoc}
     */
    public static function instantiate($row)
    {
        switch ($row['type']) {
            case IdentificationTemplate::TYPE:
                return new IdentificationTemplate();
            case FeaturedImage::TYPE:
                return new FeaturedImage();
            case ProgramTemplate::TYPE:
                return new ProgramTemplate();
            case InvitationTemplate::TYPE:
                return new InvitationTemplate();
            case AttendanceCertificateTemplate::TYPE:
                return new AttendanceCertificateTemplate();
            case CpdCertificateTemplate::TYPE:
                return new CpdCertificateTemplate();
            default:
                return new self;
        }
    }
}
