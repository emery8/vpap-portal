<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\bootstrap4\Html;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class SignupForm extends Model
{
    /**
     * @var string
     */
    public $first_name;
    public $last_name;
    public $email;
    public $license_number;
    public $password;
    public $password_repeat;
    public $privacy_notice = false;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'license_number', 'password', 'password_repeat'], 'required'],
            [['privacy_notice'], 'required', 'requiredValue' => 1, 'message' => Yii::t('app', 'Please read and agree to the Privacy Notice terms.')],
            [['first_name', 'last_name'], 'string', 'max' => 35],
            [['email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['email'], 'unique', 'targetClass' => '\app\models\User', 'message' => Yii::t('app', 'Please enter a valid email address.')],
            [['license_number'], 'string', 'max' => 30],
            [['license_number'], 'match', 'pattern' => '/^[\d\-]+$/', 'message' => Yii::t('app', 'Only digits and dash character are allowed.')],
            [['license_number'], 'unique', 'targetClass' => '\app\models\UserProfession', 'message' => Yii::t('app', 'Please enter a valid license number.')],
            [['password_repeat'], 'compare', 'compareAttribute' => 'password'],
            [['password'], 'string', 'min' => Yii::$app->params['user.passwordMinLength']],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email Address'),
            'license_number' => Yii::t('app', 'License Number'),
            'password' => Yii::t('app', 'Password'),
            'password_repeat' => Yii::t('app', 'Confirm Password'),
            'privacy_notice' => Yii::t('app', 'Privacy Notice'),
        ];
    }

    /**
     * @return boolean
     */
    public function signup()
    {
        if (! $this->validate()) {
            return false;
        }

        $session = Yii::$app->session;
        $errorMessage = Yii::t('app', 'There was an error in saving your account information. Please contact {supportEmail} for assistance.', ['supportEmail' => Html::tag('strong', Html::a(Yii::$app->params['supportEmail'], 'mailto:' . Yii::$app->params['supportEmail']))]);
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $user = Yii::createObject(User::class);
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->emailActive();
            if (! $user->save()) {
                $transaction->rollBack();
                $session->setFlash('danger', $errorMessage);
                return false;
            }

            $profile = Yii::createObject([
                'class' => UserProfile::class,
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
            ]);
            $profile->link('user', $user);
    
            $profession = Yii::createObject([
                'class' => UserProfession::class,
                'license_number' => $this->license_number,
            ]);
            $profession->link('user', $user);

            $membership = Yii::createObject(UserMembership::class);
            $membership->link('user', $user);

            $education = Yii::createObject(UserEducation::class);
            $education->link('user', $user);

            $employment = Yii::createObject(UserEmployment::class);
            $employment->link('user', $user);

            $auth = Yii::$app->authManager;
            $role = $auth->getRole('member');
            $auth->assign($role, $user->getId());

            // Login user
            Yii::$app->user->login($user);

            $transaction->commit();
            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            $session->setFlash('danger', $errorMessage);
            return false;
        } catch (\Throwable $e) {
            var_dump($e->getMessage()); die();
            $transaction->rollBack();
            $session->setFlash('danger', $errorMessage);
            return false;
        }
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'email_verify-html', 'text' => 'email_verify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($this->email)
            ->setSubject('Account sign up at ' . Yii::$app->name)
            ->unique(Yii::$app->security->generateRandomString())
            ->queue();
    }
}
