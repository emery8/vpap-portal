<?php

namespace app\models;

use Yii;
use yii\bootstrap4\Html;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use app\behaviors\EventFeeBehavior;
use app\models\enums\EventType;
use app\models\enums\EventAttendanceType;
use app\models\enums\MembershipType;
use app\models\enums\MemberDiscountType;
use app\models\enums\PaymentMethod;

/**
 * This is the model class for table "{{%event_attendance}}".
 *
 * @property int $id
 * @property int $member_id
 * @property int $event_id
 * @property int|null $sponsor_id
 * @property int $attendance_type
 * @property int|null $member_type
 * @property int|null $payment_method
 * @property int|null $discount_type
 * @property float|null $event_fee
 * @property float $amount_paid
 * @property float|null $cpd_point_earned
 * @property int|null $payment_confirmed
 * @property int|null $attended
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Event $event
 * @property User $member
 */
class EventAttendance extends \yii\db\ActiveRecord
{
    const SCENARIO_INSERT_ORDER = 'insert_order';
    const SCENARIO_UPDATE_ORDER = 'update_order';

    /**
     * @var array
     */
    public $aggregate_start_date;

    /**
     * @var string
     */
    protected $amount_summary_template = "\n{base_amount}\n{discounted_amount}\n{membership_fee}\n{total_amount}\n";

    /**
     * @var app\models\Event
     */
    private $_event_model;

    /**
     * @var app\models\UserMembership
     */
    private $_membership_model;

    /**
     * @var app\models\UserProfession
     */
    private $_professsion_model;

    /**
     * @var app\models\UserProfile
     */
    private $_profile_model;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%event_attendance}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'event_id', 'attendance_type'], 'required'],
            [['member_id', 'event_id', 'sponsor_id', 'attendance_type', 'member_type', 'payment_method', 'discount_type', 'payment_confirmed', 'attended', 'created_at', 'updated_at'], 'integer'],
            [['event_fee', 'cpd_point_earned'], 'number'],
            [['amount_paid'], 'number', 'on' => [self::SCENARIO_INSERT_ORDER, self::SCENARIO_UPDATE_ORDER]],
            [['payment_method'], 'required',
                'when' => function ($model) {
                    return (int) $model->attendance_type === EventAttendanceType::PARTICIPANT;
                }, 'enableClientValidation' => false,
            ],
            [['sponsor_id'], 'required',
                'when' => function ($model) {
                    return (int) $model->payment_method === PaymentMethod::SPONSOR;
                }, 'enableClientValidation' => false,
            ],
            [['sponsor_id'], 'in', 'range' => array_keys(Sponsor::listData())],
            [['attendance_type'], 'in', 'range' => array_keys(EventAttendanceType::listData())],
            [['member_type'], 'in', 'range' => array_keys(MembershipType::listData())],
            [['payment_method'], 'in', 'range' => array_keys(PaymentMethod::listData())],
            [['discount_type'], 'in', 'range' => array_keys(MemberDiscountType::listData())],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['event_id' => 'id']],
            [['member_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['member_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'member_id' => Yii::t('app', 'Member'),
            'event_id' => Yii::t('app', 'Event'),
            'sponsor_id' => Yii::t('app', 'Sponsor'),
            'attendance_type' => Yii::t('app', 'Attendance Type'),
            'member_type' => Yii::t('app', 'Member Type'),
            'payment_method' => Yii::t('app', 'Payment Method'),
            'discount_type' => Yii::t('app', 'Discount Type'),
            'event_fee' => Yii::t('app', 'Event Fee'),
            'amount_paid' => Yii::t('app', 'Amount Paid'),
            'cpd_point_earned' => Yii::t('app', 'CPD Point/s Earned'),
            'payment_confirmed' => Yii::t('app', 'Payment Confirmed'),
            'attended' => Yii::t('app', 'Attended'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),

            'event_type' => Yii::t('app', 'Type'),
            'event_title' => Yii::t('app', 'Title'),
            'event_start_date' => Yii::t('app', 'Start Date'),
            'event_end_date' => Yii::t('app', 'End Date'),
        ];
    }

    /**
     * Gets query for [[Event]].
     *
     * @return \yii\db\ActiveQuery|EventQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * Gets query for [[Member]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getMember()
    {
        return $this->hasOne(User::className(), ['id' => 'member_id']);
    }

    /**
     * {@inheritdoc}
     * @return EventAttendanceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EventAttendanceQuery(get_called_class());
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => EventFeeBehavior::class,
                'attribute' => 'amount_paid',
                'scenarios' => [
                    self::SCENARIO_INSERT_ORDER,
                    self::SCENARIO_UPDATE_ORDER,
                ],
            ],
        ];
    }

    /**
     * @return app\models\Event
     */
    public function getEventModel()
    {
        return $this->_event_model;
    }

    /**
     * @param app\models\Event $model
     * @return void
     */
    public function setEventModel(Event $model)
    {
        $this->_event_model = $model;
    }

    /**
     * @return app\models\UserMembership
     */
    public function getMembershipModel()
    {
        return $this->_membership_model;
    }

    /**
     * @param app\models\UserMembership $model
     * @return void
     */
    public function setMembershipModel(UserMembership $model)
    {
        $this->_membership_model = $model;
    }

    /**
     * @return boolean
     */
    public function getIs_participant()
    {
        return (int) $this->attendance_type === EventAttendanceType::PARTICIPANT;
    }

    /**
     * @return boolean
     */
    public function getIs_sponsored()
    {
        return (! $this->payment_method) ? false :
            ((int) $this->payment_method === PaymentMethod::SPONSOR);
    }

    /**
     * @return boolean
     */
    public function getIs_personal_account()
    {
        return (! $this->payment_method) ? false :
            ((int) $this->payment_method === PaymentMethod::PERSONAL);
    }

    /**
     * @return boolean
     */
    public function getHas_checkout_item()
    {
        // return $this->is_participant && $this->is_personal_account && (float) ($this->amount_paid > 0);
        return ((boolean) $this->payment_confirmed === false) && ((float) ($this->amount_paid > 0));
    }

    /**
     * @return string
     */
    public function getAmountSummary()
    {
        $text = '';
        $formatter = Yii::$app->formatter;
        $age = $this->member->userProfile->age;
        switch ($this->attendance_type) {
            case EventAttendanceType::PARTICIPANT:
                switch ($this->payment_method) {
                    case PaymentMethod::PERSONAL:
                        $event = $this->eventModel;
                        $membership = $this->membershipModel;
                        if ($membership->discount_full) {
                            $text .= Html::tag('del', Yii::t('app', '{amount} (Base Amount)', ['amount' => $formatter->asCurrency($this->event_fee)]), ['class' => 'text-danger']) .
                                Html::tag('br') .
                                Html::tag('span', Yii::t('app', ' (Free: Full discount)'), ['class' => 'text-success']);
                        } else {
                            if ($event->is_discount_enabled) {
                                $text .= Html::tag('del', Yii::t('app', '{amount} (Base Amount)', ['amount' => $formatter->asCurrency($this->event_fee)]), ['class' => 'text-danger']);
                                $discount_rate = $membership->getDiscountRate();
                                $discounted_amount = $this->applyDiscountRate($this->event_fee, $discount_rate);
                                $text .= Html::tag('br') .
                                    Html::tag('span', Yii::t('app', '{discounted_amount} ({discount_rate})', ['discounted_amount' => $formatter->asCurrency($discounted_amount), 'discount_rate' => $formatter->asMemberDiscountType($this->discount_type)]), ['class' => 'text-success']);
                            } else {
                                $text .= Html::tag('span', Yii::t('app', '{amount} (Base Amount)', ['amount' => $formatter->asCurrency($this->event_fee)]));
                            }

                            if ($event->is_annual_conference && $membership->is_regular && ($age < 60)) {
                                $text .= Html::tag('br') .
                                    Html::tag('span', Yii::t('app', ' + {amount} (Membership fee)', ['amount' => $formatter->asCurrency($event->membership_fee)]), ['class' => 'text-info']);
                            }
                        }
                        break;
                    case PaymentMethod::SPONSOR:
                        $text .= Html::tag('del', Yii::t('app', '{amount} (Base Amount)', ['amount' => $formatter->asCurrency($this->event_fee)]), ['class' => 'text-danger']) .
                            Html::tag('br') .
                            Html::tag('span', Yii::t('app', ' (Free: Sponsored by)'), ['class' => 'text-success']);
                        break;
                    default:
                        throw new InvalidValueException(Yii::t('app', 'The discounted amount could not be displayed.'));
                }
                break;

            case EventAttendanceType::SPEAKER:
                $text .= Html::tag('del', Yii::t('app', '{amount} (Base Amount)', ['amount' => $formatter->asCurrency($this->event_fee)]), ['class' => 'text-danger']) .
                    Html::tag('br') .
                    Html::tag('span', Yii::t('app', ' (Free: as a Speaker)'), ['class' => 'text-success']);
                break;
            case EventAttendanceType::MODERATOR:
                $text .= Html::tag('del', Yii::t('app', '{amount} (Base Amount)', ['amount' => $formatter->asCurrency($this->event_fee)]), ['class' => 'text-danger']) .
                    Html::tag('br') .
                    Html::tag('span', Yii::t('app', ' (Free: as a Moderator)'), ['class' => 'text-success']);
                break;
            case EventAttendanceType::ORGANIZER:
                $text .= Html::tag('del', Yii::t('app', '{amount} (Base Amount)', ['amount' => $formatter->asCurrency($this->event_fee)]), ['class' => 'text-danger']) .
                    Html::tag('br') .
                    Html::tag('span', Yii::t('app', ' (Free: as an Organizer)'), ['class' => 'text-success']);
                break;
            case EventAttendanceType::PRC_REPRESENTATIVE:
                $text .= Html::tag('del', Yii::t('app', '{amount} (Base Amount)', ['amount' => $formatter->asCurrency($this->event_fee)]), ['class' => 'text-danger']) .
                    Html::tag('br') .
                    Html::tag('span', Yii::t('app', ' (Free: as a PRC Representative)'), ['class' => 'text-success']);
                break;
            default:
                throw new InvalidValueException(Yii::t('app', 'The discounted amount could not be displayed.'));
        }

        $text .= Html::tag('br') .
            Html::tag('strong', Yii::t('app', ' = {amount_paid} (Total Amount)', ['amount_paid' => $formatter->asCurrency($this->amount_paid)]));

        return $text;

        /*$content = preg_replace_callback('/{\\w+}/', function ($matches) {
            $content = $this->renderAmountSummary($matches[0]);
            return ($content === false) ? $matches[0] : $content;
        }, $this->amount_summary_template);

        return $content;*/
    }

    /**
     * @return integer
     */
    public function getEvent_type()
    {
        return $this->event->type;
    }

    /**
     * @return string
     */
    public function getEvent_title()
    {
        return $this->event->title;
    }

    /**
     * @return string
     */
    public function getEvent_slug()
    {
        return $this->event->slug;
    }

    /**
     * @return string
     */
    public function getEvent_attachments()
    {
        return $this->event->attachments;
    }

    /**
     * @return app\models\ProgramTemplate
     */
    public function getProgram_template()
    {
        $file = $this->event->programTemplate;
        return $file === null ? null :
            $this->event->programTemplate->getUploadUrl('content');
    }

    /**
     * @return app\models\InvitationTemplate
     */
    public function getInvitation_template()
    {
        $file = $this->event->invitationTemplate;
        return $file === null ? null :
            $this->event->invitationTemplate->getUploadUrl('content');
    }

    /**
     * @return app\models\AttendanceCertificateTemplate
     */
    public function getAttendance_certificate_template()
    {
        $file = $this->event->attendanceCertificateTemplate;
        if ($this->attended) {
            return $file === null ? null :
                $this->event->attendanceCertificateTemplate->getUploadUrl('content');
        }
        return null;
    }

    /**
     * @return app\models\CpdCertificateTemplate
     */
    public function getCpd_certificate_template()
    {
        $file = $this->event->cpdCertificateTemplate;
        // if ($this->attended) {
            return $file === null ? null :
                $this->event->cpdCertificateTemplate->getUploadUrl('content');
        // }
        return null;
    }

    /**
     * @return string
     */
    public function getEvent_start_date()
    {
        return $this->event->start_date;
    }

    /**
     * @return string
     */
    public function getEvent_end_date()
    {
        return $this->event->end_date;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        $member = $this->member;
        return ($member === null) ? null :
            $member->email;
    }

    /**
     * @return string
     */
    public function getComplete_name()
    {
        $member = $this->member;
        return ($member === null) ? null :
            $member->complete_name;
    }

    /**
     * @return string
     */
    public function getFirst_name()
    {
        $member = $this->member;
        return ($member === null) ? null :
            $member->first_name;
    }

    /**
     * @return string
     */
    public function getMiddle_name()
    {
        $member = $this->member;
        return ($member === null) ? null :
            $member->middle_name;
    }

    /**
     * @return string
     */
    public function getLast_name()
    {
        $member = $this->member;
        return ($member === null) ? null :
            $member->last_name;
    }

    /**
     * @return string
     */
    public function getSuffix()
    {
        $member = $this->member;
        return ($member === null) ? null :
            $member->suffix;
    }

    /**
     * @return string
     */
    public function getNickname()
    {
        $member = $this->member;
        return ($member === null) ? null :
            $member->nickname;
    }

    /**
     * @return string
     */
    public function getLicense_number()
    {
        $member = $this->member;
        return ($member === null) ? null :
            $member->license_number;
    }

    /**
     * @return string|null
     */
    public function getExpiration_date()
    {
        $member = $this->member;
        return ($member === null) ? null :
            $member->expiration_date;
    }

    /**
     * @return string
     */
    public function getMembership_type()
    {
        $member = $this->member->userMembership;
        return ($member === null) ? null :
            $member->type;
    }

    /**
     * @param array $selection
     * @param integer $status
     * @return integer
     */
    public static function batchUpdatePaymentConfirmed($selection, $status)
    {
        $affectedRows = 0;
        $confirmed = 0;
        if ($status === 'Verified') {
            $confirmed = 1;
        }

        $items = static::find()->where(['in', 'id', $selection])->all();
        if (is_array($items) && count($items)) {
            foreach ($items as $item) {
                $item->setAttribute('payment_confirmed', $confirmed);
                if ($item->update() > 0) {
                    $affectedRows++;
                }
            }
        }

        return $affectedRows;
    }

    /**
     * @param array $selection
     * @param integer $status
     * @return integer
     */
    public static function batchUpdateAttended($selection, $status)
    {
        $affectedRows = 0;
        $confirmed = 0;
        if ($status === 'Yes') {
            $confirmed = 1;
        }

        $items = static::find()->where(['in', 'id', $selection])->all();
        if (is_array($items) && count($items)) {
            foreach ($items as $item) {
                $item->setAttribute('attended', $confirmed);
                if ($item->update() > 0) {
                    $affectedRows++;
                }
            }
        }

        return $affectedRows;
    }

    /**
     * @param integer $event_id
     * @return array
     */
    public static function downloadAsCsvByEventId($event_id)
    {
        $items = [];
        $sponsors = ArrayHelper::map(Sponsor::find()->all(), 'id', 'company_name');
        $models = static::find()
            ->joinWith(['member' => function ($query) {
                $query->joinWith(['userProfile']);
            }])
            ->andWhere(['event_id' => $event_id])
            ->orderBy([
                '{{%event_attendance}}.[[attended]]' => SORT_ASC,
                '{{%event_attendance}}.[[payment_confirmed]]' => SORT_ASC,
                '{{%event_attendance}}.[[created_at]]' => SORT_ASC,
            ]);

        $formatter = Yii::$app->formatter;
        foreach ($models->each() as $model) {
            $items[] = [
                'email' => $model->email,
                'first_name' => $model->suffix ? "{$model->first_name} {$model->suffix}" : $model->first_name,
                'middle_name' => $model->middle_name,
                'last_name' => $model->last_name,
                'nickname' => $model->nickname,
                'license_number' => $model->license_number,
                'expiration_date' => $model->expiration_date,
                'attendance_type' => $formatter->asEventAttendanceTypeWithSponsorName($model->attendance_type, $model->sponsor_id, $sponsors),
                'membership_type' => $formatter->asMembershipType($model->membership_type),
                'payment_method' => $formatter->asPaymentMethod($model->payment_method),
                'amount_paid' => $formatter->asCurrency($model->amount_paid),
                'payment_confirmed' => ((int) $model->payment_confirmed === 1) ? Yii::t('app', 'Yes') : Yii::t('app', 'No'),
                'attended' => ((int) $model->attended === 1) ? Yii::t('app', 'Yes') : Yii::t('app', 'No'),
                'cpd' => $model->cpd_point_earned,
            ];
        }

        return $items;
    }

    /**
     * @param integer $member_id
     * @return array
     */
    public static function downloadAsCsvByMemberId($member_id)
    {
        $items = [];
        $models = static::find()
            ->joinWith(['event'])
            ->where(['member_id' => $member_id]);

        $formatter = Yii::$app->formatter;
        foreach ($models->each() as $model) {
            $items[] = [
                'event_type' => $formatter->asEventType($model->event->type),
                'event_title' => $model->event->title,
                'event_start_date' => $formatter->asDate($model->event->start_date),
                'event_end_date' => $formatter->asDate($model->event->end_date),
                'cpd_point_earned' => $model->cpd_point_earned,
                'attended' => $model->attended,
            ];
        }

        return $items;
    }

    /**
     * @param string $name
     * @return string
     */
    protected function renderAmountSummary($name)
    {
        switch ($name) {
            case '{base_amount}':
                return $this->getBaseAmountText();
            case '{discounted_amount}':
                return $this->getDiscountedAmountText();
            case '{membership_fee}':
                return $this->getMembershipText();
            case '{total_amount}':
                return $this->getTotalAmountText();
        }
    }

    /**
     * @return string
     */
    protected function getBaseAmountText()
    {
        $text = '';
        $formatter = Yii::$app->formatter;
        $event = $this->eventModel;
        $membership = $this->membershipModel;
        $text = Yii::t('app', '{amount} (Base Amount)', [
            'amount' => $formatter->asCurrency((float) $this->event_fee),
        ]);

        if ($membership->discount_full) {
            return Html::tag('del', $text, ['class' => 'text-danger']) .
                Html::tag(
                    'span',
                    Yii::t('app', ' ({member_discount_type})', ['member_discount_type' => MemberDiscountType::getLabel(MemberDiscountType::FULL_DISCOUNT)]),
                    ['class' => 'text-success']
                );
        }

        if ($event->is_discount_enabled) {
            return Html::tag('del', $text, ['class' => 'text-danger']);
        }

        return $text;
    }

    /**
     * @return string
     */
    protected function getDiscountedAmountText()
    {
        $text = '';
        $formatter = Yii::$app->formatter;
        $event = $this->eventModel;

        if ($event->is_discount_enabled || $membership->discount_full) {
            $membership = $this->membershipModel;
            $discount_rate = $membership->getDiscountRate();
            $discounted_amount = $this->applyDiscountRate($this->event_fee, $discount_rate);
            $text = Yii::t('app', '{amount} ({discount_rate})', [
                'amount' => $formatter->asCurrency((float) $discounted_amount),
                'discount_rate' => $formatter->asMemberDiscountType($this->discount_type),
            ]);

            return Html::tag('span', $text, ['class' => 'text-success']);
        }

        return $text;
    }

    /**
     * @return string
     */
    protected function getMembershipText()
    {
        $text = '';
        $event = $this->eventModel;
        $membership = $this->membershipModel;
        if ($event->is_annual_conference && $membership->is_regular && ! $membership->discount_full) {
            $text = Yii::t('app', '+ {amount} (Membership Fee)', ['amount' => Yii::$app->formatter->asCurrency((float) $event->membership_fee)]);
        }

        return $text;
    }

    /**
     * @return string
     */
    protected function getTotalAmountText()
    {
        $text = '';
        $text = Yii::t('app', '= {amount}', ['amount' => Yii::$app->formatter->asCurrency((float) $this->amount_paid)]);
        return Html::tag('strong', $text);
    }
}
