<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%user_education}}".
 *
 * @property int $user_id
 * @property int|null $institution
 * @property string|null $year_graduated
 *
 * @property User $user
 */
class UserEducation extends \yii\db\ActiveRecord
{
    const SCENARIO_UPDATE = 'update';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_education}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'institution', 'year_graduated'], 'required', 'on' => self::SCENARIO_UPDATE],
            [['institution', 'year_graduated'], 'trim'],
            [['user_id', 'institution'], 'integer'],
            [['institution'], 'in', 'range' => array_keys(VeterinaryInstitution::listData())],
            [['year_graduated'], 'integer', 'min' => 1960, 'max' => date('Y'), 'message' => Yii::t('app', '{attribute} is invalid.', ['attribute' => $this->getAttributeLabel('year_graduated')])],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'institution' => Yii::t('app', 'Institution'),
            'year_graduated' => Yii::t('app', 'Year Graduated'),
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return UserEducationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserEducationQuery(get_called_class());
    }

    /**
     * {@inheritDoc}
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_UPDATE => ['user_id', 'institution', 'year_graduated'],
        ]);
    }
}
