<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\AttributeTypecastBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use app\behaviors\UploadImageBehavior;
use app\traits\AttachmentTrait;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property int $id
 * @property int|null $old_id
 * @property string $email
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property string|null $verification_token
 * @property string $auth_key
 * @property int $status
 * @property string|null $photo
 * @property int|null $is_profile_updated
 * @property int|null $lock_name_on_certificate
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Announcement[] $announcements
 * @property Announcement[] $announcements0
 * @property EventAttendance[] $eventAttendances
 * @property MembershipActivity $membershipActivity
 * @property MembershipPayment[] $membershipPayments
 * @property Officer[] $officers
 * @property UserEducation $userEducation
 * @property UserEmployment $userEmployment
 * @property UserMembership $userMembership
 * @property UserProfession $userProfession
 * @property UserProfile $userProfile
 */
class User extends ActiveRecord implements IdentityInterface
{
    use AttachmentTrait;

    const STATUS_DELETED = 0;
    const STATUS_FOR_APPROVAL = 5;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;
    const TYPE = 'photo';
    const SCENARIO_UPDATE = 'update';

    /**
     * @var array
     */
    public $allowedMimeType = [
        'image/png',
        'image/jpeg',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['old_id', 'status', 'is_profile_updated', 'lock_name_on_certificate', 'created_at', 'updated_at'], 'integer'],
            [['status'], 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_FOR_APPROVAL, self::STATUS_DELETED]],
            [['email', 'password_hash', 'auth_key'], 'required'],
            [['email', 'password_hash', 'password_reset_token', 'verification_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['photo'], 'file', 'skipOnEmpty' => true, 'mimeTypes' => $this->allowedMimeType, 'on' => [self::SCENARIO_UPDATE]],
            [['email'], 'unique'],
            [['email'], 'trim'],
            [['email'], 'email'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'old_id' => Yii::t('app', 'Old ID'),
            'email' => Yii::t('app', 'Email'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'verification_token' => Yii::t('app', 'Verification Token'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'status' => Yii::t('app', 'Status'),
            'photo' => Yii::t('app', 'Photo'),
            'is_profile_updated' => Yii::t('app', 'Is Profile Updated'),
            'lock_name_on_certificate' => Yii::t('app', 'Lock Name Of Certificate'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Announcements]].
     *
     * @return \yii\db\ActiveQuery|AnnouncementQuery
     */
    public function getAnnouncementsCreatedBy()
    {
        return $this->hasMany(Announcement::className(), ['created_by' => 'id']);
    }

    /**
     * Gets query for [[Announcements0]].
     *
     * @return \yii\db\ActiveQuery|AnnouncementQuery
     */
    public function getAnnouncementsUpdatedBy()
    {
        return $this->hasMany(Announcement::className(), ['updated_by' => 'id']);
    }

    /**
     * Gets query for [[EventAttendances]].
     *
     * @return \yii\db\ActiveQuery|EventAttendanceQuery
     */
    public function getEventAttendances()
    {
        return $this->hasMany(EventAttendance::className(), ['member_id' => 'id']);
    }

    /**
     * Gets query for [[MembershipActivity]].
     *
     * @return \yii\db\ActiveQuery|MembershipActivityQuery
     */
    public function getMembershipActivity()
    {
        return $this->hasOne(MembershipActivity::className(), ['member_id' => 'id']);
    }

    /**
     * Gets query for [[MembershipPayments]].
     *
     * @return \yii\db\ActiveQuery|MembershipPaymentQuery
     */
    public function getMembershipPayments()
    {
        return $this->hasMany(MembershipPayment::className(), ['member_id' => 'id']);
    }

    /**
     * Gets query for [[Officers]].
     *
     * @return \yii\db\ActiveQuery|OfficerQuery
     */
    public function getOfficers()
    {
        return $this->hasMany(Officer::className(), ['member_id' => 'id']);
    }

    /**
     * Gets query for [[UserEducation]].
     *
     * @return \yii\db\ActiveQuery|UserEducationQuery
     */
    public function getUserEducation()
    {
        return $this->hasOne(UserEducation::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[UserEmployment]].
     *
     * @return \yii\db\ActiveQuery|UserEmploymentQuery
     */
    public function getUserEmployment()
    {
        return $this->hasOne(UserEmployment::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[UserMembership]].
     *
     * @return \yii\db\ActiveQuery|UserMembershipQuery
     */
    public function getUserMembership()
    {
        return $this->hasOne(UserMembership::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[UserProfession]].
     *
     * @return \yii\db\ActiveQuery|UserProfessionQuery
     */
    public function getUserProfession()
    {
        return $this->hasOne(UserProfession::className(), ['user_id' => 'id']);
    }

    /**
     * Gets query for [[UserProfile]].
     *
     * @return \yii\db\ActiveQuery|UserProfileQuery
     */
    public function getUserProfile()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => UploadImageBehavior::class,
                'attribute' => 'photo',
                'scenarios' => [self::SCENARIO_UPDATE],
                'path' => '@webroot/uploads/' . self::TYPE . '/{id}',
                'placeholder' => '@webroot/img/avatar_placeholder.png',
                'url' => '@web/uploads/' . self::TYPE . '/{id}',
                'thumbs' => [
                    'thumb' => ['width' => 50, 'height' => 50, 'quality' => 100],
                    'preview' => ['width' => 400, 'height' => 400, 'quality' => 100],
                ],
            ],
            'typecast' => [
                'class' => AttributeTypecastBehavior::class,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token) {
        return static::findOne([
            'verification_token' => $token,
            'status' => self::STATUS_INACTIVE
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new token for email verification
     */
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Needs admin approval
     *
     */
    public function forApproval()
    {
        $this->status = self::STATUS_FOR_APPROVAL;
    }

    /**
     * Email verification token
     *
     */
    public function emailVerify()
    {
        $this->status = self::STATUS_INACTIVE;
    }

    /**
     * Activate account
     *
     */
    public function emailActive()
    {
        $this->status = self::STATUS_ACTIVE;
    }

    /**
     * Lock changing of certificate name
     */
    public function lockNameOnCertificate()
    {
        $this->lock_name_on_certificate = 1;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function userProfileHasUpdated()
    {
        $this->is_profile_updated = 1;
    }

    /**
     * @return boolean
     */
    public function getIs_for_approval()
    {
        return $this->status === self::STATUS_FOR_APPROVAL;
    }

    /**
     * @return boolean
     */
    public function getIs_inactive()
    {
        return $this->status === self::STATUS_INACTIVE;
    }

    /**
     * @return string|null
     */
    public function getComplete_name()
    {
        $profile = $this->userProfile;
        return ($profile === null) ? null :
            $profile->complete_name;
    }

    /**
     * @return string|null
     */
    public function getFirst_name()
    {
        $profile = $this->userProfile;
        return ($profile === null) ? null :
            $profile->first_name;
    }

    /**
     * @return string|null
     */
    public function getMiddle_name()
    {
        $profile = $this->userProfile;
        return ($profile === null) ? null :
            $profile->middle_name;
    }

    /**
     * @return string|null
     */
    public function getLast_name()
    {
        $profile = $this->userProfile;
        return ($profile === null) ? null :
            $profile->last_name;
    }

    /**
     * @return string|null
     */
    public function getSuffix()
    {
        $profile = $this->userProfile;
        return ($profile === null) ? null :
            $profile->suffix;
    }

    /**
     * @return string|null
     */
    public function getNickname()
    {
        $profile = $this->userProfile;
        return ($profile === null) ? null :
            $profile->nickname;
    }

    /**
     * @return string|null
     */
    public function getBirth_date()
    {
        $profile = $this->userProfile;
        return ($profile === null) ? null :
            $profile->birth_date;
    }

    /**
     * @return string|null
     */
    public function getAge()
    {
        $profile = $this->userProfile;
        return ($profile === null) ? null :
            $profile->age;
    }

    /**
     * @return string|null
     */
    public function getLicense_number()
    {
        $profession = $this->userProfession;
        return ($profession === null) ? null :
            $profession->license_number;
    }

    /**
     * @return string|null
     */
    public function getExpiration_date()
    {
        $profession = $this->userProfession;
        return ($profession === null) ? null :
            $profession->expiration_date;
    }

    /**
     * @return integer|null
     */
    public function getMembership_type()
    {
        $membership = $this->userMembership;
        return ($membership === null) ? null :
            $membership->type;
    }

    /**
     * @return integer|null
     */
    public function getDiscount_type()
    {
        $membership = $this->userMembership;
        return ($membership === null) ? null :
            $membership->discount_type;
    }

    /**
     * @return integer|null
     */
    public function getYear_initial_joined()
    {
        $membership = $this->userMembership;
        return ($membership === null) ? null :
            $membership->year_initial_joined;
    }

    /**
     * @return integer|null
     */
    public function getNumber_years_active()
    {
        $membership = $this->userMembership;
        return ($membership === null) ? null :
            $membership->number_years_active;
    }

    /**
     * @return integer|null
     */
    public function getCountries_count()
    {
        if ($this->isNewRecord) {
            return null;
        }

        return empty($this->countriesAggregation) ? 0 :
        $this->countriesAggregation[0]['counted'];
    }

    /**
     * @return array
     */
    public function getCountriesAggregation()
    {
        return $this->getUserProfile()
            ->select(['country', 'counted' => 'count(*)'])
            ->groupBy('country')
            ->asArray(true);
    }

    /**
     * @return boolean
     */
    public function sendEmail()
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'email_verify-html', 'text' => 'email_verify-text'],
                ['user' => $this]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($this->email)
            ->setSubject('Account sign up at ' . Yii::$app->name)
            ->unique(Yii::$app->security->generateRandomString())
            ->queue();
    }

    /**
     * @return array
     */
    public static function statusListData()
    {
        return [
            self::STATUS_FOR_APPROVAL => Yii::t('app', 'For Approval'),
            self::STATUS_INACTIVE => Yii::t('app', 'Inactive'),
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
        ];
    }

    public static function downloadAsCsv()
    {
        $allModels = static::find()
            ->alias('a')
            ->select([
                'a.email',
                'b.first_name',
                'b.middle_name',
                'b.last_name',
                'b.birth_date',
                'b.mobile_number',
                'c.license_number',
                'c.expiration_date',
                'membership_type' => 'd.type',
                'd.number_years_active',
                'd.year_initial_joined',
            ])
            ->joinWith(['userProfile b', 'userProfession c', 'userMembership d'])
            ->orderBy(['b.last_name' => SORT_ASC])
            ->asArray()
            ->all();
        if (empty($allModels)) {
            return [];
        }

        for ($i = 0; $i < count($allModels); $i++) {
            if (isset($allModels[$i]['membership_type']) && ! empty($allModels[$i]['membership_type'])) {
                $allModels[$i]['membership_type'] = Yii::$app->formatter->asMembershipType($allModels[$i]['membership_type']);
            }
        }
        return $allModels;
    }
}
