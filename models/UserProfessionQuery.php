<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UserProfession]].
 *
 * @see UserProfession
 */
class UserProfessionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return UserProfession[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UserProfession|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
