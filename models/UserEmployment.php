<?php

namespace app\models;

use Yii;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%user_employment}}".
 *
 * @property int $user_id
 * @property int|null $is_employed
 * @property string|null $company_name
 * @property string|null $position
 * @property string|null $address
 * @property string|null $subdivision
 * @property string|null $barangay
 * @property string|null $municipality
 * @property string|null $province
 * @property string|null $region
 * @property string|null $zipcode
 * @property string|null $country
 * @property string|null $phone_number
 *
 * @property User $user
 */
class UserEmployment extends \yii\db\ActiveRecord
{
    const SCENARIO_UPDATE = 'update';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_employment}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'company_name', 'position', 'municipality', 'province', 'region', 'country'],
                'required',
                'on' => self::SCENARIO_UPDATE,
                'when' => function ($model) {
                    return (boolean) $model->is_employed === true;
                },
                'whenClient' => 'function (attribute, value) {
                    const is_employed = jQuery("#' . Html::getInputId($this, 'is_employed') . '").val();
                    return  !!is_employed === true; 
                }',
            ],
            [['company_name', 'position', 'address', 'subdivision', 'barangay', 'municipality', 'province', 'region', 'zipcode', 'country', 'phone_number'], 'trim'],
            [['user_id', 'is_employed'], 'integer'],
            [['is_employed'], 'boolean'],
            [['company_name'], 'string', 'max' => 255],
            [['position', 'phone_number'], 'string', 'max' => 30],
            [['address'], 'string', 'max' => 1000],
            [['subdivision', 'barangay', 'municipality', 'province', 'region', 'country'], 'string', 'max' => 100],
            [['zipcode'], 'string', 'max' => 15],
            [['user_id'], 'unique'],
            [['phone_number'], 'match', 'pattern' => '/^[0-9\s\-\+\(\)]{8,30}$/'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'is_employed' => Yii::t('app', 'Is Employed'),
            'company_name' => Yii::t('app', 'Company Name'),
            'position' => Yii::t('app', 'Position'),
            'address' => Yii::t('app', 'Street Name and Number'),
            'subdivision' => Yii::t('app', 'Subdivision'),
            'barangay' => Yii::t('app', 'Barangay'),
            'municipality' => Yii::t('app', 'Municipality'),
            'province' => Yii::t('app', 'Province'),
            'region' => Yii::t('app', 'Region'),
            'zipcode' => Yii::t('app', 'Zip Code'),
            'country' => Yii::t('app', 'Country'),
            'phone_number' => Yii::t('app', 'Phone Number'),
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return UserEmploymentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserEmploymentQuery(get_called_class());
    }

    /**
     * {@inheritDoc}
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_UPDATE => ['user_id', 'company_name', 'position', 'address', 'subdivision', 'barangay', 'municipality', 'province', 'region', 'zipcode', 'country', 'phone_number'],
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function beforeSave($insert)
    {
        if ((bool) $this->is_employed === false) {
            $this->setAttribute('company_name', null);
            $this->setAttribute('position', null);
            $this->setAttribute('address', null);
            $this->setAttribute('subdivision', null);
            $this->setAttribute('barangay', null);
            $this->setAttribute('municipality', null);
            $this->setAttribute('province', null);
            $this->setAttribute('region', null);
            $this->setAttribute('zipcode', null);
            $this->setAttribute('country', null);
            $this->setAttribute('phone_number', null);
        }
        // $province = Province::findOne(['name' => $this->province]);
        // if ($province) {
        //     $region = Region::findOne($province->region_id);
        //     if ($region) {
        //         $this->setAttribute('region', $region->name);
        //     }
        // }

        return parent::beforeSave($insert);
    }
}
