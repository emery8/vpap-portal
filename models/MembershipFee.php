<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%membership_fee}}".
 *
 * @property int $id
 * @property float $amount
 * @property int $type
 */
class MembershipFee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%membership_fee}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['amount'], 'required'],
            [['amount'], 'number'],
            [['type'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'amount' => Yii::t('app', 'Amount'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return MembershipFeeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MembershipFeeQuery(get_called_class());
    }
}
