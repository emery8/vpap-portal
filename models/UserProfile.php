<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%user_profile}}".
 *
 * @property int $user_id
 * @property string $first_name
 * @property string|null $middle_name
 * @property string $last_name
 * @property string|null $suffix
 * @property string|null $name_on_certificate
 * @property string|null $nickname
 * @property string|null $address
 * @property string|null $subdivision
 * @property string|null $barangay
 * @property string|null $municipality
 * @property string|null $province
 * @property string|null $region
 * @property string|null $zipcode
 * @property string|null $country
 * @property string|null $phone_number
 * @property string|null $mobile_number
 * @property string|null $birth_date
 *
 * @property User $user
 */
class UserProfile extends \yii\db\ActiveRecord
{
    const SCENARIO_UPDATE = 'update';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user_profile}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'first_name', 'last_name'], 'required'],
            [['user_id', 'first_name', 'last_name', 'nickname', 'name_on_certificate', 'birth_date', 'mobile_number', 'municipality', 'province', 'region', 'country'], 'required', 'on' => self::SCENARIO_UPDATE],
            [['first_name', 'middle_name', 'last_name', 'suffix', 'name_on_certificate', 'nickname', 'address', 'subdivision', 'barangay', 'municipality', 'province', 'region', 'zipcode', 'country', 'phone_number', 'mobile_number', 'birth_date'], 'trim', 'skipOnEmpty' => true],
            [['middle_name', 'suffix', 'name_on_certificate', 'address', 'subdivision', 'barangay', 'municipality', 'zipcode', 'phone_number'], 'default', 'value' => null],
            [['user_id'], 'integer'],
            [['birth_date'], 'date', 'format' => 'yyyy-MM-dd'],
            [['first_name', 'middle_name', 'last_name', 'nickname'], 'string', 'max' => 35],
            [['suffix'], 'string', 'max' => 5],
            [['name_on_certificate'], 'string', 'max' => 255],
            [['address'], 'string', 'max' => 1000],
            [['subdivision', 'barangay', 'municipality', 'province', 'region', 'country'], 'string', 'max' => 100],
            [['zipcode'], 'string', 'max' => 15],
            [['phone_number', 'mobile_number'], 'string', 'max' => 30],
            [['zipcode'], 'match', 'pattern' => '/^[0-9\-]{4,15}$/'],
            [['phone_number', 'mobile_number'], 'match', 'pattern' => '/^[0-9\s\-\+\(\)]{8,30}$/'],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'middle_name' => Yii::t('app', 'Middle Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'suffix' => Yii::t('app', 'Suffix'),
            'name_on_certificate' => Yii::t('app', 'Name on Certificate'),
            'nickname' => Yii::t('app', 'Nickname'),
            'address' => Yii::t('app', 'Street Name and Number'),
            'subdivision' => Yii::t('app', 'Subdivision'),
            'barangay' => Yii::t('app', 'Barangay'),
            'municipality' => Yii::t('app', 'Municipality'),
            'province' => Yii::t('app', 'Province'),
            'region' => Yii::t('app', 'Region'),
            'zipcode' => Yii::t('app', 'Zip Code'),
            'country' => Yii::t('app', 'Country'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'mobile_number' => Yii::t('app', 'Mobile Number'),
            'birth_date' => Yii::t('app', 'Birth Date'),
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return UserProfileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserProfileQuery(get_called_class());
    }

    /**
     * {@inheritDoc}
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_UPDATE => ['user_id', 'first_name', 'middle_name', 'last_name', 'suffix', 'name_on_certificate', 'nickname', 'address', 'subdivision', 'barangay', 'municipality', 'province', 'region', 'zipcode', 'country', 'phone_number', 'mobile_number', 'birth_date'],
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function beforeSave($insert)
    {
        if ($this->scenario === self::SCENARIO_UPDATE) {
            if ($this->isAttributeChanged('name_on_certificate') === true) {
                $user = $this->user;
                $user->lockNameOnCertificate();
                $user->update();
            }
        }

        // $province = Province::findOne(['name' => $this->province]);
        // if ($province) {
        //     $region = Region::findOne($province->region_id);
        //     if ($region) {
        //         $this->setAttribute('region', $region->name);
        //     }
        // }

        return parent::beforeSave($insert);
    }

    /**
     * @return string
     */
    public function getComplete_name()
    {
        $complete_name = '';
        if ($this->first_name) {
            $complete_name = $this->first_name;
        }

        if ($this->last_name) {
            $complete_name = "{$complete_name} {$this->last_name}";
        }
        return $complete_name;
    }

    /**
     * @return integer
     */
    public function getAge()
    {
        $date1 = new \DateTime($this->birth_date);
        $date2 = new \DateTime();
        $diff = $date1->diff($date2);
        $age = $diff->y;
        return ($age > 0) ? $age :
            Yii::$app->formatter->nullDisplay;
    }

    /**
     * @return boolean
     */
    public function getIs_senior_age()
    {
        $senior_age = 60;
        $age = $this->age;
        return ($age >= $senior_age);
    }
}
