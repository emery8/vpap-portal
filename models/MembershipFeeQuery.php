<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MembershipFee]].
 *
 * @see MembershipFee
 */
class MembershipFeeQuery extends \yii\db\ActiveQuery
{
    /**
     * @var string
     */
    public $type;
    public $tableName;

    /**
     * {@inheritDoc}
     */
    public function prepare($builder)
    {
        if ($this->type !== null) {
            $this->andWhere(["$this->tableName.type" => $this->type]);
        }
        return parent::prepare($builder);
    }

    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return MembershipFee[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return MembershipFee|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
