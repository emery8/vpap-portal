<?php

namespace app\models;

use yii\db\Expression;

/**
 * This is the ActiveQuery class for [[UserProfile]].
 *
 * @see UserProfile
 */
class UserProfileQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @return yii\db\Query
     */
    public function birthdayByWeek()
    {
        return $this->select(['*', new Expression('birth_date - INTERVAL 1 DAY + INTERVAL (YEAR(NOW()) - YEAR(birth_date)) YEAR + INTERVAL 1 DAY')])
            ->andWhere([
                'between',
                new Expression('DATE(birth_date + INTERVAL (YEAR(NOW()) - YEAR(birth_date)) YEAR)'),
                new Expression('DATE(NOW() - INTERVAL WEEKDAY(NOW()) DAY)'),
                new Expression('DATE(NOW() + INTERVAL 6 - WEEKDAY(NOW()) DAY)'),
            ]);
    }

    /**
     * {@inheritdoc}
     * @return UserProfile[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UserProfile|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
