<?php

namespace app\components;

use Yii;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class DragonPayClientv2 extends BaseObject
{
    /**
     * @var string
     */
    private $_url;

    /**
     * @var string
     */
    private $_merchantid;

    /**
     * @var string
     */
    private $_merchantpassword;

    /**
     * @var string
     */
    private $_invoiceno;

    /**
     * @var string
     */
    private $_name;

    /**
     * @var float
     */
    private $_amount;

    /**
     * @var string
     */
    private $_ccy;

    /**
     * @var string
     */
    private $_remarks;

    /**
     * @var string
     */
    private $_email;

    /**
     * @var string
     */
    private $_payment_link;

    /**
     * {@inheritDoc}
     */
    protected function checkRequiredAttributes()
    {
        if ($this->url === null) {
            throw new InvalidConfigException(Yii::t('app', 'The "url" property must be set.'));
        }

        if ($this->merchantid === null) {
            throw new InvalidConfigException(Yii::t('app', 'The "merchantid" property must be set.'));
        }

        if ($this->invoiceno === null) {
            throw new InvalidConfigException(Yii::t('app', 'The "invoiceno" property must be set.'));
        }

        if ($this->name === null) {
            throw new InvalidConfigException(Yii::t('app', 'The "name" property must be set.'));
        }

        if ($this->amount === null) {
            throw new InvalidConfigException(Yii::t('app', 'The "amount" property must be set.'));
        }

        if ($this->ccy === null) {
            throw new InvalidConfigException(Yii::t('app', 'The "ccy" property must be set.'));
        }

        if ($this->remarks === null) {
            throw new InvalidConfigException(Yii::t('app', 'The "remarks" property must be set.'));
        }

        if ($this->email === null) {
            throw new InvalidConfigException(Yii::t('app', 'The "email" property must be set.'));
        }
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        if ($this->_url === null) {
            $url = rtrim(env('PAYMENT_GATEWAY_URL'), '?');
            $this->_url = "{$url}?";
        }

        return $this->_url;
    }

    /**
     * @return string
     */
    public function getMerchantid()
    {
        if ($this->_merchantid === null) {
            $this->_merchantid = env('PAYMENT_GATEWAY_MERCHANT_ID');
        }

        return $this->_merchantid;
    }

    /**
     * @return string
     */
    public function getMerchantpassword()
    {
        if ($this->_merchantpassword === null) {
            $this->_merchantpassword = env('PAYMENT_GATEWAY_MERCHANT_SECRET_KEY');
        }

        return $this->_merchantpassword;
    }

    /**
     * @return string
     */
    public function getInvoiceno()
    {
        return $this->_invoiceno;
    }
    
    /**
     * @param string $value
     * @return void
     */
    public function setInvoiceno($value)
    {
        $this->_invoiceno = $value;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }
    
    /**
     * @param string $value
     * @return void
     */
    public function setName($value)
    {
        $this->_name = $value;
    }

    /**
     * @return string
     */
    public function getAmount()
    {
        return $this->_amount;
    }
    
    /**
     * @param string $value
     * @return void
     */
    public function setAmount($value)
    {
        $this->_amount = number_format($value, 2, '.', '');
    }

    /**
     * @return string
     */
    public function getCcy()
    {
        return $this->_ccy;
    }
    
    /**
     * @param string $value
     * @return void
     */
    public function setCcy($value)
    {
        $this->_ccy = $value;
    }

    /**
     * @return string
     */
    public function getRemarks()
    {
        return $this->_remarks;
    }
    
    /**
     * @param string $value
     * @return void
     */
    public function setRemarks($value)
    {
        $this->_remarks = $value;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->_email;
    }
    
    /**
     * @param string $value
     * @return void
     */
    public function setEmail($value)
    {
        $this->_email = $value;
    }

    /**
     * @return string
     */
    public function getPayment_link()
    {
        $this->checkRequiredAttributes();
        $txnid = '000001';
        $parameters['merchantid'] = $this->merchantid;
        $parameters['txnid'] = $txnid;
        $parameters['amount'] = $this->amount;
        $parameters['ccy'] = $this->ccy;
        $parameters['description'] = $this->remarks;
        $parameters['email'] = $this->email;
        $parameters['key'] = $this->merchantpassword;

        $digest_string = implode(':', $parameters);

        unset($parameters['key']);
        $parameters['digest'] = sha1($digest_string);

        // https://test.dragonpay.ph/Pay.aspx?merchantid=VPAPGEN&txnid=000001&amount=1000.00&ccy=PHP&description=50th+VPAP+Annual+Conference&email=tomas.cabagay%40gmail.com&digest=182c9acf4ed8ecf7310b1d843b7638ddc8a558af
        // https://gw.dragonpay.ph/Pay.aspx?merchantid=VPAPGEN&txnid=000001&amount=1000.00&ccy=PHP&description=50th+VPAP+Annual+Conference&email=tomas.cabagay%40gmail.com&digest=182c9acf4ed8ecf7310b1d843b7638ddc8a558af

        // var_dump(sha1('VPAPGEN:abc1234def9876:1000.00:PHP:50th VPAP Annual Conference:tomas.cabagay@gmail.com:papalvisit2015') === 'bd63cbd51f5c893207090f5cb247e8c3ae120db4'); die();
        // var_dump($parameters); die();
        

        $url = $this->url;
        $url .= http_build_query($parameters, '', '&');
        $url = preg_replace('/(\v|\s)+/', '', $url);
        $this->_payment_link = $url;
        return $this->_payment_link;
    }
}
