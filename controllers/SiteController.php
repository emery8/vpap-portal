<?php

namespace app\controllers;

use Yii;
use yii\base\InvalidArgumentException;
use yii\filters\AccessControl;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\SignupForm;
use app\models\VerifyEmailForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\ResendVerificationEmailForm;
use app\models\UserEducation;
use app\models\UserEmployment;
use app\models\UserMembership;
use app\models\enums\MembershipType;
use app\models\enums\MemberDiscountType;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'layout' => 'centered',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        } else {
            if (Yii::$app->user->can('admin')) {
                return $this->redirect(['/admin/default/index']);
            } elseif (Yii::$app->user->can('member')) {
                return $this->redirect(['/member/default/index']);
            } else {
                return $this->redirect(['logout']);
            }
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'centered';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = Yii::createObject(LoginForm::class);
        if ($model->load($this->request->post()) && $model->login()) {
            if (Yii::$app->user->can('admin')) {
                return $this->redirect(['/admin/default/index']);
            } elseif (Yii::$app->user->can('member')) {
                return $this->redirect(['/member/default/index']);
            } else {
                return $this->redirect(['logout']);
            }
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Signup action
     *
     * @return void
     */
    public function actionSignup()
    {
        $this->layout = 'centered';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = Yii::createObject(SignupForm::class);
        if ($model->load($this->request->post()) && $model->signup()) {
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = 'centered';
        $model = Yii::createObject(PasswordResetRequestForm::class);
        if ($model->load($this->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('request-password-reset-token', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        $this->layout = 'centered';

        if ($model->load($this->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('reset-password', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        $session = Yii::$app->session;
        if ($user = $model->verifyEmail()) {
            $education = Yii::createObject(UserEducation::class);
            $education->link('user', $user);

            $employment = Yii::createObject(UserEmployment::class);
            $employment->link('user', $user);

            /*$membership = Yii::createObject([
                'class' => UserMembership::class,
                'type' => MembershipType::NONMEMBER,
                'discount_type' => MemberDiscountType::FULL_PAYMENT,
            ]);
            $membership->link('user', $user);*/

            $auth = Yii::$app->authManager;
            $role = $auth->getRole('member');
            $auth->assign($role, $user->getId());

            if (Yii::$app->user->login($user)) {
                $session->setFlash('success', Yii::t('app', 'Your email has been confirmed!'));
                return $this->redirect(['/member/default/index']);
            }
        }

        $session->setFlash('error', Yii::t('app', 'Sorry, we are unable to verify your account with provided token.'));
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $this->layout = 'centered';

        $model = Yii::createObject(ResendVerificationEmailForm::class);
        if ($model->load($this->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resend-verification-email', [
            'model' => $model
        ]);
    }
}
