<?php

/**
 * @author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>
 */
return [
    // 'class' => 'yii\swiftmailer\Mailer',
    // send all mails to a file by default. You have to set
    // 'useFileTransport' to false and configure a transport
    // for the mailer to send real emails.
    // 'useFileTransport' => true,

    'class' => '\tigrov\mailqueue\Mailer',
    'useFileTransport' => false,
    'transport' => [
        'class' => 'Swift_SmtpTransport',
        'encryption' => env('MAIL_ENCRYPTION'),
        'host' => env('MAIL_HOST'),
        'port' => env('MAIL_PORT'),
        'username' => env('MAIL_USERNAME'),
        'password' => env('MAIL_PASSWORD'),
    ],
];
