<?php

$dbhost = env('DB_HOST');
$dbuser = env('DB_USER');
$dbpassword = env('DB_PASSWORD');
$dbname = env('DB_NAME');

return [
    'class' => 'yii\db\Connection',
    'dsn' => "mysql:host={$dbhost};dbname={$dbname}",
    'username' => $dbuser,
    'password' => $dbpassword,
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    'enableSchemaCache' => true,
    'schemaCacheDuration' => 60,
    'schemaCache' => 'cache',
];
