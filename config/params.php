<?php

return [
    'adminEmail' => 'secretariat@vpap.com.ph',
    'supportEmail' => 'secretariat@vpap.com.ph',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600 * 24,
    'user.passwordMinLength' => 8,

    'bsVersion' => '4.x',
];
