<?php

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
return [
    'definitions' => [
        'yii\widgets\LinkPager' => 'yii\bootstrap4\LinkPager',
        'yii\bootstrap4\LinkPager' => [
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel'  => Yii::t('app', 'Last'),
        ],
    ],
];
