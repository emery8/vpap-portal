<?php

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
return [
    'admin' => [
        'class' => 'app\modules\admin\Module',
    ],
    'member' => [
        'class' => 'app\modules\member\Module',
    ],
    'gridview' => [
        'class' => '\kartik\grid\Module',
    ],
    'markdown' => [
        'class' => 'kartik\markdown\Module',
    ]
];