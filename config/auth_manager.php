<?php

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
return [
    'class' => 'yii\rbac\DbManager',
    // 'cache' => 'cache',
];