<?php

namespace app\behaviors;

use Yii;
use yii\db\BaseActiveRecord;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\base\InvalidValueException;
use yii\db\ActiveRecord;
use app\models\enums\EventAttendanceType;
use app\models\enums\PaymentMethod;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class EventFeeBehavior extends Behavior
{
    /**
     * @var string
     */
    public $attribute;

    /**
     * @var array
     */
    public $scenarios = [];

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        parent::init();

        if ($this->attribute === null) {
            throw new InvalidConfigException('The "attribute" property must be set.');
        }
    }

    /**
     * {@inheritDoc}
     */
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            BaseActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }

    /**
     * @return void
     */
    public function beforeSave()
    {
        $model = $this->owner;
        if (in_array($model->scenario, $this->scenarios)) {
            switch ($model->attendance_type) {
                case EventAttendanceType::PARTICIPANT:
                    switch ($model->payment_method) {
                        case PaymentMethod::PERSONAL:
                            $model->setAttribute('sponsor_id', null);
                            $model->setAttribute('payment_method', PaymentMethod::PERSONAL);
                            $model->setAttribute('amount_paid', number_format($model->calculateAmountPaid(), 2, '.', ''));
                            break;
                        case PaymentMethod::SPONSOR:
                            $model->setAttribute('payment_method', PaymentMethod::SPONSOR);
                            $model->setAttribute('discount_type', null);
                            $model->setAttribute('amount_paid', number_format(0, 2));
                            break;
                        default:
                            throw new InvalidValueException(Yii::t('app', 'The "amount_paid" could not be set.'));
                    }
                    break;
    
                case EventAttendanceType::SPEAKER:
                case EventAttendanceType::MODERATOR:
                case EventAttendanceType::ORGANIZER:
                case EventAttendanceType::PRC_REPRESENTATIVE:
                    $model->setAttribute('sponsor_id', null);
                    $model->setAttribute('payment_method', PaymentMethod::FREE);
                    $model->setAttribute('discount_type', null);
                    $model->setAttribute('amount_paid', number_format(0, 2));
                    break;
                default:
                    throw new InvalidValueException(Yii::t('app', 'The "amount_paid" could not be set.'));
            }
        }
    }

    /**
     * @return float
     */
    public function calculateAmountPaid()
    {
        $model = $this->owner;
        $event = $model->eventModel;

        $membership = $model->membershipModel;
        if ($membership->discount_full) {
            return number_format(0, 2);
        }

        $event_type = $event->type;
        $membership_fee = $event->membership_fee;
        $amount_paid = $model->event_fee;

        if ($event->is_discount_enabled) {
            $discount_rate = (float) $membership->getDiscountRate();
            $amount_paid = (float) $this->applyDiscountRate($amount_paid, $discount_rate);
        }

        if ($event->is_annual_conference && $membership->is_regular) {
            $amount_paid = (float) $this->addMembershipFee($amount_paid, $membership_fee);
        }

        return $amount_paid;
    }

    /**
     * @param float $base_amount
     * @param float $membership_fee
     * @return string
     */
    public function addMembershipFee(float $base_amount, float $membership_fee)
    {
        $model = $this->owner;
        $age = $model->member->userProfile->age;
        if ($age >= 60) {
            return number_format($base_amount, 2, '.', '');
        }

        $amount = $base_amount + $membership_fee;
        return number_format($amount, 2, '.', '');
    }

    /**
     * @param float $base_amount
     * @param float $discount_rate
     * @return string
     */
    public function applyDiscountRate(float $base_amount, float $discount_rate)
    {
        $amount = $base_amount * $discount_rate;
        return number_format($amount, 2, '.', '');
    }
}
