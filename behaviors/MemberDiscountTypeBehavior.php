<?php

namespace app\behaviors;

use Yii;
use yii\db\BaseActiveRecord;
use yii\db\Expression;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\base\InvalidValueException;
use yii\db\ActiveRecord;
use app\models\Event;
use app\models\EventAttendance;
use app\models\MembershipActivity;
use app\models\MembershipPayment;
use app\models\RegularMembershipFee;
use app\models\Officer;
use app\models\UserProfile;
use app\models\enums\EventType;
use app\models\enums\MembershipType;
use app\models\enums\MemberDiscountType;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class MemberDiscountTypeBehavior extends Behavior
{
    /**
     * @var string
     */
    public $attribute;

    /**
     * @var array
     */
    public $scenarios = [];

    /**
     * @var app\models\Officer
     */
    private $_officer;

    /**
     * @var array
     */
    private $_count_annual_conference_attended;

    /**
     * @var array
     */
    private $_count_attendance_from_year_joined;

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        parent::init();

        if ($this->attribute === null) {
            throw new InvalidConfigException('The "attribute" property must be set.');
        }
    }

    /**
     * {@inheritDoc}
     */
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            BaseActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }

    /**
     * @return void
     */
    public function beforeSave()
    {
        $model = $this->owner;
        if (in_array($model->scenario, $this->scenarios)) {
            $officer = $this->officer;

            if ($officer !== null) {
                if ($officer->was_president) {
                    $model->setAttribute('discount_type', MemberDiscountType::FULL_DISCOUNT);
                } else {
                    $year_served_count = $officer->year_served_count;
                    if ($year_served_count >= 5) {
                        $model->setAttribute('discount_type', MemberDiscountType::FULL_DISCOUNT);
                    } else {
                        $model->setAttribute('discount_type', $this->discountType);
                    }
                }
            } else {
                $model->setAttribute('discount_type', $this->discountType);
            }
            $model->setAttribute('number_years_active', $this->numberYearsActive);
            $model->setAttribute('year_initial_joined', $this->yearInitialJoined);
        }
    }

    public function getYearInitialJoined()
    {
        $model = $this->owner;
        $member_type = $model->type;

        switch ($member_type) {
            case MembershipType::LIFETIME:
            case MembershipType::REGULAR:
                return $model->year_initial_joined;
            case MembershipType::NONMEMBER:
                return null;
        }
    }

    public function getNumberYearsActive()
    {
        $model = $this->owner;
        $member_type = $model->type;

        switch ($member_type) {
            case MembershipType::LIFETIME:
                return $this->count_attendance_from_year_joined;
            case MembershipType::REGULAR:
                return $this->count_annual_conference_attended;
            case MembershipType::NONMEMBER:
                return null;
        }
    }

    /**
     * @return integer
     */
    public function getDiscountType()
    {
        $model = $this->owner;
        $profile = $model->user->userProfile;
        if (! $profile) {
            return null;
        }
        $member_type = $model->type;
        $years_active = $this->numberYearsActive;
        $discount_type = null;

        switch ($member_type) {
            case MembershipType::LIFETIME:
                if ($profile->is_senior_age) {
                    $discount_type = MemberDiscountType::FULL_DISCOUNT;
                } else {
                    $discount_type = MemberDiscountType::FULL_PAYMENT;
                }
                break;
            case MembershipType::REGULAR:
                if ($profile->is_senior_age) {
                    if ($years_active <= 5) {
                        $discount_type = MemberDiscountType::DISCOUNT_20;
                    } else if (($years_active >= 6) && ($years_active <= 10)) {
                        $discount_type = MemberDiscountType::DISCOUNT_50;
                    } else if ($years_active >= 11) {
                        $discount_type = MemberDiscountType::FULL_DISCOUNT;
                    }
                } else {
                    $discount_type = MemberDiscountType::FULL_PAYMENT;
                }
                break;
            case MembershipType::NONMEMBER:
                if ($profile->is_senior_age) {
                    $discount_type = MemberDiscountType::DISCOUNT_20;
                } else {
                    $discount_type = MemberDiscountType::FULL_PAYMENT;
                }
                break;
        }

        return $discount_type;
    }

    /**
     * @return app\models\Officer|null
     */
    public function getOfficer()
    {
        if ($this->_officer === null) {
            $model = $this->owner;
            $this->_officer = Officer::findOne(['member_id' => $model->user_id]);
        }
        return $this->_officer;
    }

    /**
     * @return integer
     */
    public function getCount_annual_conference_attended()
    {
        if ($this->_count_annual_conference_attended === null) {
            $model = $this->owner;
            $count_annual_conference_attended = EventAttendance::find()
                ->joinWith('event')
                ->andWhere(['member_id' => $model->user_id])
                ->andWhere(['[[attended]]' => 1])
                ->andWhere(['{{%event}}.[[type]]' => EventType::ANNUAL_CONFERENCE])
                ->andWhere(['>=', new Expression('year({{%event}}.[[start_date]])'), $model->year_initial_joined])
                ->count();
            /* First Option
            $membership_payment = MembershipPayment::find()
                ->select(['sum_number_of_year' => new Expression('sum(number_of_year)')])
                ->andWhere(['member_id' => $model->user_id])
                ->andWhere(['payment_confirmed' => 1])
                ->andWhere(['type' => RegularMembershipFee::TYPE])
                ->groupBy('member_id')
                ->one();

            $count_membership_payment = 0;
            if ($membership_payment !== null) {
                $count_membership_payment = $membership_payment->sum_number_of_year;
            }

            $this->_count_annual_conference_attended = $count_annual_conference_attended + $count_membership_payment;*/

            $membership_activity = MembershipActivity::find()
                ->andWhere(['member_id' => $model->user_id])
                ->one();

            $count_membership_activity = 0;
            if (($membership_activity !== null) && ($membership_activity->year_membership_paid !== null)) {
                $count_membership_activity = count($membership_activity->year_membership_paid);
            }

            $this->_count_annual_conference_attended = $count_annual_conference_attended + $count_membership_activity;
        }
        return $this->_count_annual_conference_attended;
    }

    /**
     * @return integer
     */
    public function getCount_attendance_from_year_joined()
    {
        if ($this->_count_attendance_from_year_joined === null) {
            $total = 0;
            $model = $this->owner;
            $year_joined = $model->year_initial_joined;
            $event = Event::find()
                ->select('start_date')
                ->andWhere(['type' => EventType::ANNUAL_CONFERENCE])
                ->orderBy(['start_date' => SORT_DESC])
                ->limit(1)
                ->one();
            if ($year_joined && $event) {
                $pieces = explode('-', $event->start_date);
                if (isset($pieces[0]) && (strlen($pieces[0]) === 4)) {
                    $year_latest_event = $pieces[0];
                    for ($i = $year_joined; $i <= $year_latest_event; $i++) {
                        $total++;
                    }
                }
            }

            $this->_count_attendance_from_year_joined = $total;
        }
        return $this->_count_attendance_from_year_joined;
    }
}
