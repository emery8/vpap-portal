<?php

use yii\db\Migration;

/**
 * Class m220811_183110_alter_officer_table
 */
class m220811_183110_alter_officer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        $delete = 'CASCADE';
        $update = 'CASCADE';
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->addColumn('{{%officer}}', 'number_years_served', $this->smallInteger()->defaultValue(0)->after('year_served'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%officer}}', 'number_years_served');
    }
}
