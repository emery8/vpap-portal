<?php

use yii\db\Migration;

/**
 * Class m220315_081810_alter_user_employment_table
 */
class m220315_081810_alter_user_employment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        $delete = 'CASCADE';
        $update = 'CASCADE';
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->addColumn('{{%user_employment}}', 'region', $this->string(100)->defaultValue(null)->after('province'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user_employment}}', 'region');
    }
}
