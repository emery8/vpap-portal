<?php

use yii\db\Migration;

/**
 * Class m220714_053602_alter_event_table
 */
class m220714_053602_alter_event_table extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $tableOptions = null;
        $delete = 'CASCADE';
        $update = 'CASCADE';
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->addColumn('{{%event}}', 'invitation_link', $this->string()->defaultValue(null)->after('evaluation_link'));
    }

    public function down()
    {
        $this->dropColumn('{{%event}}', 'invitation_link');
    }
}
