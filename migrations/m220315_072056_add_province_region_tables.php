<?php

use yii\db\Migration;

/**
 * Class m220315_072056_add_province_region_tables
 */
class m220315_072056_add_province_region_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        $delete = 'CASCADE';
        $update = 'CASCADE';
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%municipality}}', [
            'id' => $this->primaryKey()->unsigned(),
            'province_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string()->notNull(),
        ]);

        $this->createTable('{{%province}}', [
            'id' => $this->primaryKey()->unsigned(),
            'region_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string()->notNull(),
        ]);

        $this->createTable('{{%region}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string()->notNull(),
        ]);

        $this->createTable('{{%country}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string()->notNull(),
        ]);

        $this->addForeignKey('fk_municipality-province', '{{%municipality}}', 'province_id', '{{%province}}', 'id', $delete, $update);
        $this->addForeignKey('fk_province-region', '{{%province}}', 'region_id', '{{%region}}', 'id', $delete, $update);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%municipality}}');
        $this->dropTable('{{%province}}');
        $this->dropTable('{{%region}}');
        $this->dropTable('{{%country}}');
    }
}
