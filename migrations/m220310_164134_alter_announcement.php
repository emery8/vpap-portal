<?php

use yii\db\Migration;

/**
 * Class m220310_164134_alter_announcement
 */
class m220310_164134_alter_announcement extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        $delete = 'CASCADE';
        $update = 'CASCADE';
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->addColumn('{{%announcement}}', 'email_sent', $this->boolean()->defaultValue(false)->after('status'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%announcement}}', 'email_sent');
    }
}
