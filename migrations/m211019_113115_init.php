<?php

use yii\db\Migration;

/**
 * Class m211019_113115_init
 */
class m211019_113115_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        $delete = 'CASCADE';
        $update = 'CASCADE';
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey()->unsigned(),
            'old_id' => $this->integer()->unsigned(),
            'email' => $this->string()->notNull()->unique(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'verification_token' => $this->string()->defaultValue(null),
            'auth_key' => $this->string(32)->notNull(),
            'status' => $this->smallInteger()->unsigned()->notNull()->defaultValue(10),
            'photo' => $this->string(2048)->defaultValue(null),
            'is_profile_updated' => $this->boolean()->defaultValue(0),
            'lock_name_on_certificate' => $this->boolean()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%user_profile}}', [
            'user_id' => $this->integer()->unsigned()->notNull(),
            'first_name' => $this->string(35)->notNull(),
            'middle_name' => $this->string(35)->defaultValue(null),
            'last_name' => $this->string(35)->notNull(),
            'suffix' => $this->string(5)->defaultValue(null),
            'name_on_certificate' => $this->string()->defaultValue(null),
            'nickname' => $this->string(35)->defaultValue(null),
            'address' => $this->string(1000)->defaultValue(null),
            'subdivision' => $this->string(100)->defaultValue(null),
            'barangay' => $this->string(100)->defaultValue(null),
            'municipality' => $this->string(100)->defaultValue(null),
            'province' => $this->string(100)->defaultValue(null),
            'zipcode' => $this->string(15)->defaultValue(null),
            'country' => $this->string(100)->defaultValue(null),
            'phone_number' => $this->string(30)->defaultValue(null),
            'mobile_number' => $this->string(30)->defaultValue(null),
            'birth_date' => $this->date()->defaultValue(null),
        ], $tableOptions);

        $this->createTable('{{%user_profession}}', [
            'user_id' => $this->integer()->unsigned()->notNull(),
            'license_number' => $this->string(30)->notNull()->unique(),
            'expiration_date' => $this->date()->defaultValue(null),
            'type_of_practice' => $this->json()->defaultValue(null),
            'species_of_specialization' => $this->json()->defaultValue(null),
        ], $tableOptions);

        $this->createTable('{{%user_education}}', [
            'user_id' => $this->integer()->unsigned()->notNull(),
            'institution' => $this->integer(1)->unsigned()->defaultValue(null),
            'year_graduated' => $this->char(4)->defaultValue(null),
        ], $tableOptions);

        $this->createTable('{{%user_employment}}', [
            'user_id' => $this->integer()->unsigned()->notNull(),
            'is_employed' => $this->boolean()->defaultValue(0),
            'company_name' => $this->string()->defaultValue(null),
            'position' => $this->string(30)->defaultValue(null),
            'address' => $this->string(1000)->defaultValue(null),
            'subdivision' => $this->string(100)->defaultValue(null),
            'barangay' => $this->string(100)->defaultValue(null),
            'municipality' => $this->string(100)->defaultValue(null),
            'province' => $this->string(100)->defaultValue(null),
            'zipcode' => $this->string(15)->defaultValue(null),
            'country' => $this->string(100)->defaultValue(null),
            'phone_number' => $this->string(30)->defaultValue(null),
        ], $tableOptions);

        $this->createTable('{{%user_membership}}', [
            'user_id' => $this->integer()->unsigned()->notNull(),
            'type' => $this->smallInteger()->unsigned()->defaultValue(null),
            'number_years_active' => $this->smallInteger()->unsigned()->defaultValue(null),
            'year_initial_joined' => $this->char(4)->defaultValue(null),
            'discount_type' => $this->smallInteger()->unsigned()->defaultValue(null),
        ], $tableOptions);

        $this->createTable('{{%announcement}}', [
            'id' => $this->primaryKey()->unsigned(),
            'title' => $this->string(1000)->notNull(),
            'content' => $this->text()->notNull(),
            'status' => $this->smallInteger()->notNull(),
            'created_by' => $this->integer()->unsigned()->notNull(),
            'updated_by' => $this->integer()->unsigned()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%event}}', [
            'id' => $this->primaryKey()->unsigned(),
            'old_id' => $this->integer()->unsigned(),
            'type' => $this->smallInteger()->unsigned()->notNull(),
            'title' => $this->text()->notNull(),
            'slug' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->notNull(),
            'location' => $this->string(1000)->notNull(),
            'start_date' => $this->date()->notNull(),
            'end_date' => $this->date()->notNull(),
            'enable_discount' => $this->boolean()->defaultValue(0),
            'membership_fee' => $this->decimal(13,2)->defaultValue(number_format(0, 2)),
            'lifetime_member_fee' => $this->decimal(13,2)->notNull(),
            'regular_member_fee' => $this->decimal(13,2)->notNull(),
            'non_member_fee' => $this->decimal(13,2)->notNull(),
            'attendance_type' => $this->smallInteger()->unsigned()->notNull(),
            'cpd_point' => $this->decimal(8,2)->notNull(),
            'maximum_participant' => $this->smallInteger()->unsigned()->notNull(),
            'evaluation_link' => $this->string()->defaultValue(null),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%event_attendance}}', [
            'id' => $this->bigPrimaryKey()->unsigned(),
            'member_id' => $this->integer()->unsigned()->notNull(),
            'event_id' => $this->integer()->unsigned()->notNull(),
            'sponsor_id' => $this->integer()->unsigned()->defaultValue(null),
            'attendance_type' => $this->smallInteger()->unsigned()->notNull(),
            'member_type' => $this->smallInteger()->unsigned()->defaultValue(null),
            'payment_method' => $this->smallInteger()->unsigned()->defaultValue(null),
            'discount_type' => $this->smallInteger()->unsigned()->defaultValue(null),
            'event_fee' => $this->decimal(13,2)->defaultValue(null),
            'amount_paid' => $this->decimal(13,2)->notNull(),
            'cpd_point_earned' => $this->decimal(8,2)->defaultValue(null),
            'payment_confirmed' => $this->boolean()->defaultValue(0),
            'attended' => $this->boolean()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%officer}}', [
            'id' => $this->primaryKey()->unsigned(),
            'member_id' => $this->integer()->unsigned()->notNull(),
            'was_president' => $this->boolean()->defaultValue(0),
            'year_served' => $this->json()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%membership_activity}}', [
            'id' => $this->primaryKey()->unsigned(),
            'member_id' => $this->integer()->unsigned()->notNull(),
            'year_event_attended' => $this->json(),
            'year_membership_paid' => $this->json(),
        ], $tableOptions);

        $this->createTable('{{%attachment}}', [
            'id' => $this->bigPrimaryKey()->unsigned(),
            'event_id' => $this->integer()->unsigned()->notNull(),
            'content' => $this->json()->notNull(),
            'type' => $this->string(50)->notNull(),
        ], $tableOptions);

        $this->createTable('{{%type_of_practice}}', [
            'id' => $this->primaryKey()->unsigned(),
            'description' => $this->string(500)->notNull(),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ], $tableOptions);

        $this->createTable('{{%species_of_specialization}}', [
            'id' => $this->primaryKey()->unsigned(),
            'description' => $this->string(500)->notNull(),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ], $tableOptions);

        $this->createTable('{{%veterinary_institution}}', [
            'id' => $this->primaryKey()->unsigned(),
            'institution' => $this->string(500)->notNull(),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ], $tableOptions);

        $this->createTable('{{%sponsor}}', [
            'id' => $this->primaryKey()->unsigned(),
            'old_id' => $this->integer()->unsigned(),
            'company_name' => $this->string(500)->notNull(),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ], $tableOptions);

        $this->createTable('{{%membership_fee}}', [
            'id' => $this->primaryKey()->unsigned(),
            'amount' => $this->decimal(13, 2)->notNull(),
            'type' => $this->smallInteger()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%membership_payment}}', [
            'id' => $this->primaryKey()->unsigned(),
            'member_id' => $this->integer()->unsigned()->notNull(),
            'type' => $this->smallInteger()->notNull(),
            'amount' => $this->decimal(13, 2)->notNull(),
            'number_of_year' => $this->smallInteger(),
            'amount_paid' => $this->decimal(13, 2)->notNull(),
            'payment_confirmed' => $this->boolean()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-user_profile', '{{%user_profile}}', 'user_id', true);
        $this->createIndex('idx-user_profession', '{{%user_profession}}', 'user_id', true);
        $this->createIndex('idx-user_education', '{{%user_education}}', 'user_id', true);
        $this->createIndex('idx-user_employment', '{{%user_employment}}', 'user_id', true);
        $this->createIndex('idx-user_membership', '{{%user_membership}}', 'user_id', true);

        $this->addForeignKey('fk-user_profile-user', '{{%user_profile}}', 'user_id', '{{%user}}', 'id', $delete, $update);
        $this->addForeignKey('fk-user_profession-user', '{{%user_profession}}', 'user_id', '{{%user}}', 'id', $delete, $update);
        $this->addForeignKey('fk-user_education-user', '{{%user_education}}', 'user_id', '{{%user}}', 'id', $delete, $update);
        $this->addForeignKey('fk-user_employment-user', '{{%user_employment}}', 'user_id', '{{%user}}', 'id', $delete, $update);
        $this->addForeignKey('fk-user_membership-user', '{{%user_membership}}', 'user_id', '{{%user}}', 'id', $delete, $update);
        $this->addForeignKey('fk-announcement-created_by', '{{%announcement}}', 'created_by', '{{%user}}', 'id', $delete, $update);
        $this->addForeignKey('fk-announcement-updated_by', '{{%announcement}}', 'updated_by', '{{%user}}', 'id', $delete, $update);
        $this->addForeignKey('fk-attachment-event', '{{%attachment}}', 'event_id', '{{%event}}', 'id', $delete, $update);
        $this->addForeignKey('fk-event_attendance-user', '{{%event_attendance}}', 'member_id', '{{%user}}', 'id', $delete, $update);
        $this->addForeignKey('fk-event_attendance-event', '{{%event_attendance}}', 'event_id', '{{%event}}', 'id', $delete, $update);
        $this->addForeignKey('fk-officer-user', '{{%officer}}', 'member_id', '{{%user}}', 'id', $delete, $update);
        $this->addForeignKey('fk-membership_activity-member_id', '{{%membership_activity}}', 'member_id', '{{%user}}', 'id', $delete, $update);
        $this->addForeignKey('fk-membership_payment-member_id', '{{%membership_payment}}', 'member_id', '{{%user}}', 'id', $delete, $update);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%membership_payment}}');
        $this->dropTable('{{%membership_fee}}');
        $this->dropTable('{{%sponsor}}');
        $this->dropTable('{{%veterinary_institution}}');
        $this->dropTable('{{%species_of_specialization}}');
        $this->dropTable('{{%type_of_practice}}');
        $this->dropTable('{{%attachment}}');
        $this->dropTable('{{%membership_activity}}');
        $this->dropTable('{{%officer}}');
        $this->dropTable('{{%event_attendance}}');
        $this->dropTable('{{%event}}');
        $this->dropTable('{{%announcement}}');
        $this->dropTable('{{%user_membership}}');
        $this->dropTable('{{%user_employment}}');
        $this->dropTable('{{%user_education}}');
        $this->dropTable('{{%user_profession}}');
        $this->dropTable('{{%user_profile}}');
        $this->dropTable('{{%user}}');
    }
}
