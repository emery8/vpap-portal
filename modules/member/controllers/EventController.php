<?php

namespace app\modules\member\controllers;

use Yii;
use app\models\Event;
use app\models\EventAttendance;
use app\models\FeaturedImage;
use app\models\ProgramTemplate;
use app\models\InvitationTemplate;
use app\models\Sponsor;
use app\models\enums\EventAttendanceType;
use app\models\enums\PaymentMethod;
use app\traits\CheckEventAttendance;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{
    /**
     * Displays the Event model signup form.
     * @param string $slug slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSignup($slug)
    {
        $identity = Yii::$app->user->identity;
        $session = Yii::$app->session;

        if (! $identity->is_profile_updated) {
            return $this->redirect(['profile/index']);
        }

        $model = $this->findModel($slug);
        if (! Event::canAttendEvent($model->attendance_type)) {
            throw new BadRequestHttpException(Yii::t('app', 'You are not allowed to signup for this event.'));
        }

        $hasSignup = EventAttendance::find()
            ->where([
                'member_id' => $identity->getid(),
                'event_id' => $model->id,
                'payment_confirmed' => 0,
            ])
            ->exists();
        if ($hasSignup) {
            return $this->redirect(['event-attendance/summary', 'slug' => $model->slug]);
        }

        $featured_image = null;
        $featured_image_thumb = null;
        $program_template = null;
        $invitation_template = null;
        $certificate_template = null;

        try {
            if (is_array($model->attachments) && count($model->attachments)) {
                $attachments = $model->attachments;
                foreach ($attachments as $attachment) {
                    if ($attachment instanceof \app\models\FeaturedImage) {
                        $featured_image = $attachment->getUploadUrl('content');
                        $featured_image_thumb = $attachment->getThumbUploadUrl('content', 'event_thumb');
                    } else if ($attachment instanceof \app\models\ProgramTemplate) {
                        $program_template = $attachment;
                    } else if ($attachment instanceof \app\models\InvitationTemplate) {
                        $invitation_template = $attachment;
                    } else if ($attachment instanceof \app\models\CertificateTemplate) {
                        $certificate_template = $attachment;
                    }
                }
            }
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        return $this->render('signup', [
            'model' => $model,
            'featured_image' => $featured_image,
            'featured_image_thumb' => $featured_image_thumb,
            'program_template' => $program_template,
            'invitation_template' => $invitation_template,
            'certificate_template' => $certificate_template,
            'eventAttendanceModel' => Yii::createObject(EventAttendance::class),
            'eventAttendanceTypeListData' => EventAttendanceType::listData(),
            'paymentMethodListData' => PaymentMethod::listData(),
            'sponsorListData' => Sponsor::listData(),
        ]);
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug slug
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        if (($model = Event::find()->joinWith(['attachments'])->where(['slug' => $slug])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
