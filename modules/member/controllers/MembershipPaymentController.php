<?php

namespace app\modules\member\controllers;

use Yii;
use app\models\MembershipPayment;
use app\models\MembershipPaymentSearch;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\DragonPayClient;

/**
 * MembershipPaymentController implements the CRUD actions for MembershipPayment model.
 */
class MembershipPaymentController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all MembershipPayment models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = Yii::createObject(MembershipPaymentSearch::class);
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MembershipPayment model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSummary($id)
    {
        return $this->render('summary', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MembershipPayment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = Yii::createObject(MembershipPayment::class);
        $model->setAttribute('member_id', Yii::$app->user->identity->getId());

        if (! $model->regular_membership || ! $model->lifetime_membership) {
            throw new BadRequestHttpException(Yii::t('app', 'The VPAP membership fee setting is not set.'));
        }

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['summary', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MembershipPayment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['summary', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Checkout the EventAttendance model order.
     * @param string $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCheckout($id)
    {
        $model = $this->findModel($id);

        try {
            $identity = Yii::$app->user->identity;
            $profession = $identity->userProfession;
            $profile = $identity->userProfile;

            $client = new DragonPayClient();
            $client->invoiceno = $profession->license_number;
            $client->name = $profile->complete_name;
            $client->amount = $model->amount_paid;
            $client->ccy = Yii::$app->formatter->currencyCode;
            $client->remarks = $model->payment_title;
            $client->email = $identity->email;

            return $this->redirect($client->payment_link);
        } catch(\InvalidConfigException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
    }

    /**
     * Deletes an existing MembershipPayment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (($model = MembershipPayment::findOne($id)) === null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        $model->delete();
        return $this->redirect(['member/membership-payment']);
    }

    /**
     * Finds the MembershipPayment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return MembershipPayment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MembershipPayment::findOne(['id' => $id, 'member_id' => Yii::$app->user->identity->getId()])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
