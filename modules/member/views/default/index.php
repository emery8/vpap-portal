<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\Carousel;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\grid\GridView;
use sjaakp\loadmore\LoadMorePager;

/* @var $this yii\web\View */
/* @var $announcementDataProvider yii\data\ActiveDataProvider */
/* @var $birthdayDataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Dashboard');
$this->params['breadcrumbs'][] = $this->title;
$birthdayCelebrantCount = $birthdayDataProvider->getTotalCount();
?>
<?= $this->render('//widgets/carousel-event', [
    'items' => $carouselItems,
]) ?>
<div class="row">
    <div class="col-md-9 col-sm-12">
        <div class="card">
            <div class="card-body">
                <h2><?= Yii::t('app', 'Announcements') ?></h2>
                <?php if (Yii::$app->user->can('admin')): ?>
                    <div class="mb-4">
                        <?= Html::a(
                            Yii::t('app', 'Create Announcement'),
                            ['announcement/create'],
                            ['class' => 'btn btn-success px-4']
                        ) ?>
                        <?= Html::a(
                            Yii::t('app', 'Manage'),
                            ['announcement/index'],
                            ['class' => 'btn btn-dark px-4']
                        ) ?>
                    </div>
                <?php endif; ?>
                <?php Pjax::begin() ?>
                    <?= ListView::widget([
                        'dataProvider' => $announcementDataProvider,
                        'itemView' => '_announcement',
                        'layout' => "{items}\n{pager}",
                        'pager' => ['class' => 'yii\bootstrap4\LinkPager'],
                    ]) ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="row justify-content-start align-items-center">
                    <div class="col-3">
                        <h4>
                            <?= Yii::t('app', '{month}', [
                                'month' => mb_strtoupper(date('M')),
                            ]) ?>
                            <br>
                            <?= Yii::t('app', '{year}', [
                                'year' => date('Y'),
                            ]) ?>
                        </h4>
                    </div>
                    <div class="col-9">
                        <h2 class="text-info">
                            <?= Yii::t('app', '{total_count,plural,=0{NO CELEBRANT} =1{1 CELEBRANT} other{# CELEBRANTS}}', [
                                'total_count' => $birthdayCelebrantCount,
                            ]) ?>
                        </h2>
                    </div>
                </div>
                <?= GridView::widget([
                    'dataProvider' => $birthdayDataProvider,
                    'showHeader' => false,
                    'tableOptions' => ['class' => 'table'],
                    'pager' => ['class' => LoadMorePager::class, 'options' => ['class' => 'btn btn-sm btn-danger']],
                    'layout' => "{items}\n{pager}",
                    'columns' => [
                        [
                            'attribute' => 'complete_name',
                            'format' => 'raw',
                            'value' => function ($model, $key, $index, $column) {
                                return Yii::t('app', '{icon} {name}', [
                                    'icon' => Html::tag(
                                        'svg',
                                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-birthday-cake')]),
                                        ['class' => 'c-icon']
                                    ),
                                    'name' => ucwords(mb_strtolower(Html::encode($model->complete_name))),
                                ]);
                            },
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>