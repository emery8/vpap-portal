<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use kartik\helpers\Enum;
use newerton\fancybox3\FancyBox;

/* @var $this yii\web\View */
/* @var $model app\models\Event */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-8">
                <div class="row no-gutters bg-light mb-4">
                    <div class="col-md-12 col-sm-12 d-flex align-items-center p-4">
                        <div>
                            <?= Html::tag('h2', Html::encode($model->title), ['class' => 'mb-0']) ?>
                            <?= Html::tag('p', Yii::t('app', '{icon} {content}', [
                                'icon' => Html::tag(
                                    'svg',
                                    Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-calendar')]),
                                    ['class' => 'c-icon mr-1']
                                ),
                                'content' => Html::encode($model->event_schedule),
                            ]), ['class' => 'text-dark lead mb-0']) ?>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters justify-content-start">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <?= Html::tag('h4', Yii::t('app', 'Event Details'), ['class' => 'mb-4 card-title']) ?>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <dl class="row">
                                            <dt class="col-md-6"><?= $model->getAttributeLabel('type') ?></dt>
                                            <dd class="col-md-6 mb-0"><?= Html::encode(Yii::$app->formatter->asEventType($model->type)) ?></dd>
                                            <dt class="col-md-6"><?= $model->getAttributeLabel('Attendance Type') ?></dt>
                                            <dd class="col-md-6 mb-0"><?= Html::encode(Yii::$app->formatter->asEventInvitationType($model->attendance_type)) ?></dd>
                                            <dt class="col-md-6"><?= $model->getAttributeLabel('location') ?></dt>
                                            <dd class="col-md-6 mb-0"><?= Html::encode($model->location) ?></dd>
                                            <dt class="col-md-6"><?= $model->getAttributeLabel('cpd_point') ?></dt>
                                            <dd class="col-md-6 mb-0"><?= Html::encode($model->cpd_point) ?></dd>
                                            <dt class="col-md-6"><?= $model->getAttributeLabel('maximum_participant') ?></dt>
                                            <dd class="col-md-6 mb-0"><?= Html::tag('span', Html::encode($model->maximum_participant), ['class' => 'badge badge-danger']) ?></dd>
                                        </dl>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <dl class="row">
                                            <dt class="col-md-6"><?= Yii::t('app', 'Discount Enabled?') ?></dt>
                                            <dd class="col-md-6 mb-0"><?= Html::encode(Yii::$app->formatter->asBoolean($model->enable_discount)) ?></dd>
                                            <?php if ($model->is_annual_conference): ?>
                                            <dt class="col-md-6"><?= $model->getAttributeLabel('membership_fee') ?></dt>
                                            <dd class="col-md-6 mb-0"><?= Html::encode(Yii::$app->formatter->asCurrency($model->membership_fee)) ?></dd>
                                            <?php endif; ?>
                                            <dt class="col-md-6"><?= $model->getAttributeLabel('lifetime_member_fee') ?></dt>
                                            <dd class="col-md-6 mb-0"><?= Html::encode(Yii::$app->formatter->asCurrency($model->lifetime_member_fee)) ?></dd>
                                            <dt class="col-md-6"><?= $model->getAttributeLabel('regular_member_fee') ?></dt>
                                            <dd class="col-md-6 mb-0"><?= Html::encode(Yii::$app->formatter->asCurrency($model->regular_member_fee)) ?></dd>
                                            <dt class="col-md-6"><?= $model->getAttributeLabel('non_member_fee') ?></dt>
                                            <dd class="col-md-6 mb-0"><?= Html::encode(Yii::$app->formatter->asCurrency($model->non_member_fee)) ?></dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <?= $this->render('_signup-form', [
                            'event' => $model,
                            'model' => $eventAttendanceModel,
                            'eventAttendanceTypeListData' => $eventAttendanceTypeListData,
                            'paymentMethodListData' => $paymentMethodListData,
                            'sponsorListData' => $sponsorListData,
                        ]) ?>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <div class="col-sm-12">
                            <h6><?= Yii::t('app', 'ATTACHMENTS:') ?></h6>
                            <?php if ($program_template || $invitation_template): ?>
                                <?php if ($model->invitation_link): ?>
                                    <?= Html::a(Yii::t('app', 'Download Invitation'), $model->invitation_link, ['target' => '_blank']) ?>
                                    <?= Html::tag('p', Yii::t('app', 'External Link')) ?>
                                <?php endif; ?>
                                <?php if ($program_template): ?>
                                        <?= Html::a(Yii::t('app', 'Download Program'), $program_template->getUploadUrl('content')) ?>
                                        <?= Html::tag('p', Html::encode(Enum::formatBytes($program_template->getSize('content')))) ?>
                                <?php endif; ?>
                            <?php else: ?>
                                <?= Yii::$app->formatter->nullDisplay ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
