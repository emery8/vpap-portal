<?php

use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use newerton\fancybox3\FancyBox;
use app\models\EventAttendance;

/* @var $this yii\web\View */
/* @var $upcomingEventSearchModel app\modules\admin\models\UpcomingEventAttendanceSearch */
/* @var $upcomingEventDataProvider yii\data\ActiveDataProvider */
/* @var $myEventSearchModel app\modules\admin\models\MyEventAttendanceSearch */
/* @var $myEventDataProvider yii\data\ActiveDataProvider */
/* @var $model app\models\EventAttendance */
/* @var $eventTypeListData app\models\enums\EventType */
/* @var $eventAttendanceTypeListData app\models\enums\EventAttendanceType */
/* @var $paymentMethodListData app\models\enums\PaymentMethod */
/* @var $sponsorListData app\models\Sponsor */

$this->title = Yii::t('app', 'My Events');
$this->params['breadcrumbs'][] = $this->title;
echo FancyBox::widget();
?>
<div class="card">
    <div class="card-header">
        <?= Yii::t('app', 'Upcoming Events') ?>
    </div>
    <div class="card-body">
        <?= ListView::widget([
            'dataProvider' => $upcomingEventDataProvider,
            'itemView' => '_upcoming-event',
            'layout' => '{items}<div class="d-flex justify-content-end">{pager}</div>',
            'pager' => ['class' => 'yii\bootstrap4\LinkPager'],
            'viewParams' => [
                'eventAttendanceModel' => $model,
                'eventAttendanceTypeListData' => $eventAttendanceTypeListData,
                'paymentMethodListData' => $paymentMethodListData,
                'sponsorListData' => $sponsorListData,
            ],
        ]) ?>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="card-body">
        <?= $this->render('_my-search', [
            'model' => $myEventSearchModel,
            'eventTypeListData' => $eventTypeListData,
        ]) ?>
        <?= GridView::widget([
            'dataProvider' => $myEventDataProvider,
            'tableOptions' => ['class' => 'table-responsive-sm'],
            'headerRowOptions' => ['class' => 'thead-light'],
            'hover' => true,
            'striped' => false,
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'mergeHeader' => false,
                ],

                // [
                    // 'class' => 'kartik\grid\ExpandRowColumn',
                    // 'width' => '50px',
                    // 'value' => function ($model, $key, $index, $column) {
                        // return GridView::ROW_COLLAPSED;
                    // },
                    // 'detailUrl' => Url::toRoute(['event-details']),
                    // 'detailRowCssClass' => GridView::TYPE_PRIMARY,
                    // 'expandOneOnly' => false,
                // ],

                [
                    'attribute' => 'event_type',
                    'format' => 'eventtype',
                    'vAlign' => GridView::ALIGN_MIDDLE,
                ],
                [
                    'attribute' => 'event_title',
                    'format' => 'ntext',
                    'vAlign' => GridView::ALIGN_MIDDLE,
                    'value' => function ($model, $key, $index, $column) {
                        return StringHelper::truncate($model->event_title, 50);
                    },
                ],
                [
                    'hAlign' => GridView::ALIGN_CENTER,
                    'vAlign' => GridView::ALIGN_MIDDLE,
                    'attribute' => 'event_start_date',
                    'format' => 'date',
                ],
                [
                    'hAlign' => GridView::ALIGN_CENTER,
                    'vAlign' => GridView::ALIGN_MIDDLE,
                    'attribute' => 'event_end_date',
                    'format' => 'date',
                ],
                [
                    'hAlign' => GridView::ALIGN_CENTER,
                    'vAlign' => GridView::ALIGN_MIDDLE,
                    'attribute' => 'payment_confirmed',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index, $column) {
                        if ($model->payment_confirmed) {
                            return Html::tag(
                                'span',
                                Yii::t('app', 'Paid'),
                                ['class' => 'badge badge-success']
                            );
                        } else {
                            return Html::a(
                                Yii::t('app', 'View Order'),
                                ['summary', 'slug' => $model->event->slug],
                                ['class' => 'btn btn-primary']
                            );
                        }
                    },
                ],
                [
                    'hAlign' => GridView::ALIGN_CENTER,
                    'vAlign' => GridView::ALIGN_MIDDLE,
                    'attribute' => 'invitation_link',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index, $column) {
                        $url = $model->event->invitation_link;

                        if (! $url) {
                            return null;
                        }

                        $label = Html::tag(
                            'div',
                            Html::tag('span', '', ['class' => 'fas fa-envelope', 'aria-hidden' => true]),
                            ['class' => 'd-none d-sm-none d-md-block']
                        ) . Html::tag(
                            'div',
                            'Invitation',
                            ['class' => 'd-block d-md-none d-lg-none d-xl-none']
                        );

                        return Html::a(
                            $label,
                            $url,
                            [
                                'target' => '_blank',
                                'title' => Yii::t('app', $url),
                            ]
                        );
                    }
                ],
                [
                    'hAlign' => GridView::ALIGN_CENTER,
                    'vAlign' => GridView::ALIGN_MIDDLE,
                    'label' => Yii::t('app', 'Program'),
                    'attribute' => 'program_template',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index, $column) {
                        $file = $model->program_template;
                        if ($file !== null) {
                            $label = Html::tag(
                                'div',
                                Html::tag('span', '', ['class' => 'fas fa-archive', 'aria-hidden' => true]),
                                ['class' => 'd-none d-sm-none d-md-block']
                            ) . Html::tag(
                                'div',
                                'Program',
                                ['class' => 'd-block d-md-none d-lg-none d-xl-none']
                            );
                            return Html::a(
                                $label,
                                $file,
                                [
                                    'title' => Yii::t('app', 'Program'),
                                    'aria-label' => Yii::t('app', 'Program'),
                                    'data-pjax' => 0,
                                    'target' => '_blank',
                                ],
                            );
                        }
                        return null;
                    },
                ],
                [
                    'hAlign' => GridView::ALIGN_CENTER,
                    'vAlign' => GridView::ALIGN_MIDDLE,
                    'attribute' => 'evaluation_link',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index, $column) {
                        $url = $model->event->evaluation_link;

                        if (! $url) {
                            return null;
                        }

                        $label = Html::tag(
                            'div',
                            Html::tag('span', '', ['class' => 'fas fa-tasks', 'aria-hidden' => true]),
                            ['class' => 'd-none d-sm-none d-md-block']
                        ) . Html::tag(
                            'div',
                            'Evaluation',
                            ['class' => 'd-block d-md-none d-lg-none d-xl-none']
                        );

                        return Html::a(
                            $label,
                            $url,
                            [
                                'target' => '_blank',
                                'title' => Yii::t('app', $url),
                            ]
                        );
                    }
                ],
                [
                    'hAlign' => GridView::ALIGN_CENTER,
                    'vAlign' => GridView::ALIGN_MIDDLE,
                    'attribute' => 'cpd_point_earned',
                    'value' => 'cpd_point_earned',
                    'label' => Yii::t('app', 'CPD Point/s Earned'),
                    'format' => ['decimal', 2],
                ],
                [
                    'hAlign' => GridView::ALIGN_CENTER,
                    'vAlign' => GridView::ALIGN_MIDDLE,
                    'label' => Yii::t('app', 'Certificate of Attendance'),
                    'attribute' => 'attendance_certificate_template',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index, $column) {
                        $file = $model->attendance_certificate_template;
                        if ($file !== null) {
                            $label = Html::tag(
                                'div',
                                Html::tag('span', '', ['class' => 'fas fa-certificate', 'aria-hidden' => true]),
                                ['class' => 'd-none d-sm-none d-md-block']
                            ) . Html::tag(
                                'div',
                                'Certificate',
                                ['class' => 'd-block d-md-none d-lg-none d-xl-none']
                            );
                            return Html::a(
                                $label,
                                ['attendance-certificate', 'slug' => $model->event->slug],
                                [
                                    'title' => Yii::t('app', 'Certificate of Attendance'),
                                    'aria-label' => Yii::t('app', 'Certificate of Attendance'),
                                    'data-pjax' => 0,
                                    'target' => '_blank',
                                ],
                            );
                        }
                        return null;
                    },
                ],
                [
                    'hAlign' => GridView::ALIGN_CENTER,
                    'vAlign' => GridView::ALIGN_MIDDLE,
                    'label' => Yii::t('app', 'CPD Certificate'),
                    'attribute' => 'cpd_certificate_template',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index, $column) {
                        $file = $model->cpd_certificate_template;
                        if ($file !== null && $model->cpd_point_earned > 0) {
                            $label = Html::tag(
                                'div',
                                Html::tag('span', '', ['class' => 'fas fa-certificate', 'aria-hidden' => true]),
                                ['class' => 'd-none d-sm-none d-md-block']
                            ) . Html::tag(
                                'div',
                                'Certificate',
                                ['class' => 'd-block d-md-none d-lg-none d-xl-none']
                            );
                            return Html::a(
                                $label,
                                ['cpd-certificate', 'slug' => $model->event->slug],
                                [
                                    'title' => Yii::t('app', 'CPD Certificate'),
                                    'aria-label' => Yii::t('app', 'CPD Certificate'),
                                    'data-pjax' => 0,
                                    'target' => '_blank',
                                ],
                            );
                        }
                        return null;
                    },
                ],
            ],
        ]); ?>
    </div>
</div>
