<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\models\Event */
?>
<div class="my-event-attendance-search">
    <div class="card">
        <div class="card-body bg-light">
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>
                <?= Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' => 4,
                    'attributes' => [
                        'event_type' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => 'kartik\select2\Select2',
                            'options' => [
                                'data' => $eventTypeListData,
                                'options' => ['placeholder' => $model->getAttributeLabel('event_type')]
                            ],
                        ],
                        'event_title' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => ['placeholder' => $model->getAttributeLabel('event_title')],
                        ],
                        'event_start_date' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => 'kartik\date\DatePicker',
                            'options' => [
                                'options' => ['placeholder' => $model->getAttributeLabel('event_start_date')],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true,
                                    'orientation' => 'bottom',
                                ],
                            ],
                        ],
                        'event_end_date' => [
                            'type' => Form::INPUT_WIDGET,
                            'widgetClass' => 'kartik\date\DatePicker',
                            'options' => [
                                'options' => ['placeholder' => $model->getAttributeLabel('event_end_date')],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true,
                                    'orientation' => 'bottom',
                                ],
                            ],
                        ],
                    ],
                ]) ?>
                <div class="form-group mb-0">
                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Reset'), ['index'], ['class' => 'btn btn-outline-dark']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
