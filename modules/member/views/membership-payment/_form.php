<?php

use yii\web\View;
use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use app\models\RegularMembershipFee;
use app\models\LifetimeMembershipFee;

/* @var $this yii\web\View */
/* @var $model app\models\MembershipPayment */
/* @var $form yii\widgets\ActiveForm */

$type_name = Html::getInputName($model, 'type');
$number_of_year_id = Html::getInputId($model, 'number_of_year');

$number_of_year_wrapper_id = Html::getInputId($model, 'number_of_year') . '-wrapper';
?>
<div class="membership-payment-form">
    <?php $form = ActiveForm::begin(); ?>
        <?= Form::widget([
            'model' => $model,
            'form' => $form,
            'attributes' => [
                'type' => [
                    'type' => Form::INPUT_RADIO_BUTTON_GROUP,
                    'items' => [
                        RegularMembershipFee::TYPE => Yii::t('app', 'Regular Memberhsip Fee'),
                        LifetimeMembershipFee::TYPE => Yii::t('app', 'Lifetime Memberhsip Fee'),
                    ],
                ],
                'number_of_year' => [
                    'type' => Form::INPUT_TEXT,
                    'container' => ['id' => $number_of_year_wrapper_id],
                    'options' => ['placeholder' => $model->getAttributeLabel('number_of_year')],
                ],
            ],
        ]) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Next'), ['class' => 'btn btn-primary px-4']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$regular_fee_value = RegularMembershipFee::TYPE;
$js = "
function resetNumberOfYearInput() { jQuery('#{$number_of_year_id}').val(''); }
function hideNumberOfYearWrapper() { jQuery('#{$number_of_year_wrapper_id}').hide(); }
function showNumberOfYearWrapper() { jQuery('#{$number_of_year_wrapper_id}').show(); }

if (jQuery('input:radio[name=\"{$type_name}\"]:checked').val() == {$regular_fee_value}) {
    showSponsorWrapper();
}

hideNumberOfYearWrapper();
resetNumberOfYearInput();
jQuery('input:radio[name=\"{$type_name}\"]').on('change', function () {
    hideNumberOfYearWrapper();
    resetNumberOfYearInput();
    var selected = jQuery(this).val();
    if (selected == {$regular_fee_value}) {
        showNumberOfYearWrapper();
    }
});
";
$this->registerJs($js, View::POS_READY);
