<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model app\models\ChangePasswordForm */

$this->title = Yii::t('app', 'Change Password');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Profile'), 'url' => ['profile/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row justify-content-start">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <?= $this->title ?>
            </div>
            <div class="card-body">
                <?php $form = ActiveForm::begin(); ?>
                    <?= Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'attributes' => [
                            'old_password' => [
                                'type' => Form::INPUT_PASSWORD,
                                'options' => [
                                    'autofocus' => true,
                                    'placeholder' => $model->getAttributeLabel('old_password'),
                                ],
                            ],
                            'new_password' => [
                                'type' => Form::INPUT_PASSWORD,
                                'options' => [
                                    'placeholder' => $model->getAttributeLabel('new_password'),
                                ],
                            ],
                            'new_password_repeat' => [
                                'type' => Form::INPUT_PASSWORD,
                                'options' => [
                                    'placeholder' => $model->getAttributeLabel('new_password_repeat'),
                                ],
                            ],
                        ],
                    ]) ?>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Change Password'), ['class' => 'btn btn-success']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>