<?php

use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $user app\models\User */
/* @var $profile app\models\UserProfile */
/* @var $employment app\models\UserEmployment */
/* @var $education app\models\UserEducation */
/* @var $profession app\models\UserProfession */
/* @var $membership app\models\UserMembership */
/* @var $countryListData array */
/* @var $uploadPhotoForm app\models\UploadPhotoForm */
/* @var $typeOfPracticeListData array */
/* @var $speciesOfSpecializationListData array */
/* @var $veterinaryInstitutionListData array */
/* @var $provinceListData array */
/* @var $pluginOptions array */

$this->title = Yii::t('app', 'Profile');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <?= $this->title ?>
    </div>
    <div class="card-body">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <?= Form::widget([
                                        'model' => $user,
                                        'form' => $form,
                                        'attributes' => [
                                            'photo' => [
                                                'type' => Form::INPUT_WIDGET,
                                                'widgetClass' => 'kartik\widgets\FileInput',
                                                'label' => false,
                                                'options' => [
                                                    'options' => [
                                                        'accept' => implode(',', $user->allowedMimeType),
                                                        'multiple' => false,
                                                    ],
                                                    'pluginOptions' => ArrayHelper::merge($pluginOptions, [
                                                        'overwriteInitial' => true,
                                                        'showClose' => false,
                                                        'showCaption' => false,
                                                        'browseLabel' => '',
                                                        'removeLabel' => '',
                                                        'browseIcon' => Html::tag('svg', Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-folder-open')]), ['class' => 'c-icon']),
                                                        'removeIcon' => Html::tag('svg', Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-x')]), ['class' => 'c-icon']),
                                                        'removeTitle' => Yii::t('app', 'Cancel or reset changes'),
                                                        'defaultPreviewContent' => Html::img($user->getThumbUploadUrl('photo', 'preview'), ['class' => 'file-preview-image']),
                                                        'layoutTemplates' => [
                                                            'main2' => "{preview}\n{remove}\n{browse}",
                                                        ],
                                                    ]),
                                                    'pluginEvents' => [
                                                        'filebeforedelete' => "function () { return ! window.confirm('Are you sure you want to delete this file?'); }",
                                                        'fileuploaded' => "function (event, data) { jQuery.post('" . Url::to(['display-photo']) . "', function (data) { if (data && data.photo) { jQuery('.c-avatar-img').attr('src', data.photo); } }); }",
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                                <div class="col-md-8">
                                    <?= Html::tag('h4', Yii::t('app', 'Personal Information'), ['class' => 'card-title']) ?>
                                    <?= Form::widget([
                                        'model' => $profile,
                                        'form' => $form,
                                        'columns' => 4,
                                        'attributes' => [
                                            'first_name' => [
                                                'type' => Form::INPUT_TEXT,
                                                'options' => ['placeholder' => $profile->getAttributeLabel('first_name')],
                                            ],
                                            'middle_name' => [
                                                'type' => Form::INPUT_TEXT,
                                                'options' => ['placeholder' => $profile->getAttributeLabel('middle_name')],
                                            ],
                                            'last_name' => [
                                                'type' => Form::INPUT_TEXT,
                                                'options' => ['placeholder' => $profile->getAttributeLabel('last_name')],
                                            ],
                                            'suffix' => [
                                                'type' => Form::INPUT_TEXT,
                                                'options' => ['placeholder' => $profile->getAttributeLabel('suffix')],
                                            ],
                                        ],
                                    ]) ?>
                                    <?= Form::widget([
                                        'model' => $profile,
                                        'form' => $form,
                                        'columns' => 2,
                                        'attributes' => [
                                            'nickname' => [
                                                'type' => Form::INPUT_TEXT,
                                                'options' => ['placeholder' => $profile->getAttributeLabel('nickname')],
                                            ],
                                            'birth_date' => [
                                                'type' => Form::INPUT_WIDGET,
                                                'widgetClass' => 'kartik\date\DatePicker',
                                                'options' => [
                                                    'options' => ['placeholder' => $profile->getAttributeLabel('birth_date')],
                                                    'pluginOptions' => [
                                                        'autoclose' => true,
                                                        'format' => 'yyyy-mm-dd',
                                                        'orientation' => 'bottom',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                    <?= Form::widget([
                                        'model' => $profile,
                                        'form' => $form,
                                        'attributes' => [
                                            'name_on_certificate' => [
                                                'type' => Form::INPUT_TEXTAREA,
                                                'hint' => Yii::t('app', 'Note: You can only edit the name to appear on your certificate once. Please email VPAP Secretariat should there be a need to correct the name on your certificate.'),
                                                'options' => [
                                                    'disabled' => $user->lock_name_on_certificate ? true : false,
                                                    'placeholder' => $profile->getAttributeLabel('name_on_certificate'),
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                    <?= Html::tag('h4', Yii::t('app', 'Contact Details'), ['class' => 'card-title']) ?>
                                    <?= Form::widget([
                                        'model' => $user,
                                        'form' => $form,
                                        'attributes' => [
                                            'email' => [
                                                'type' => Form::INPUT_TEXT,
                                                'options' => ['placeholder' => $user->getAttributeLabel('email')],
                                                'fieldConfig' => [
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => Html::tag('svg', Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-envelope-closed')]), ['class' => 'c-icon'])
                                                        ]
                                                    ]
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                    <?= Form::widget([
                                        'model' => $profile,
                                        'form' => $form,
                                        'columns' => 2,
                                        'attributes' => [
                                            'mobile_number' => [
                                                'type' => Form::INPUT_TEXT,
                                                'options' => ['placeholder' => $profile->getAttributeLabel('mobile_number')],
                                                'fieldConfig' => [
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => Html::tag('svg', Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-mobile')]), ['class' => 'c-icon'])
                                                        ]
                                                    ]
                                                ],
                                            ],
                                            'phone_number' => [
                                                'type' => Form::INPUT_TEXT,
                                                'options' => ['placeholder' => $profile->getAttributeLabel('phone_number')],
                                                'fieldConfig' => [
                                                    'addon' => [
                                                        'prepend' => [
                                                            'content' => Html::tag('svg', Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-phone')]), ['class' => 'c-icon'])
                                                        ]
                                                    ]
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                    <?= Form::widget([
                                        'model' => $profile,
                                        'form' => $form,
                                        'columns' => 3,
                                        'attributes' => [
                                            'address' => [
                                                'type' => Form::INPUT_TEXT,
                                                'options' => ['placeholder' => $profile->getAttributeLabel('address')],
                                            ],
                                            'subdivision' => [
                                                'type' => Form::INPUT_TEXT,
                                                'options' => ['placeholder' => $profile->getAttributeLabel('subdivision')],
                                            ],
                                            'barangay' => [
                                                'type' => Form::INPUT_TEXT,
                                                'options' => ['placeholder' => $profile->getAttributeLabel('barangay')],
                                            ],
                                        ],
                                    ]) ?>
                                    <?= Form::widget([
                                        'model' => $profile,
                                        'form' => $form,
                                        'columns' => 2,
                                        'attributes' => [
                                            'country' => [
                                                'type' => Form::INPUT_WIDGET,
                                                'widgetClass' => 'kartik\select2\Select2',
                                                'options' => [
                                                    'data' => $countryListData,
                                                    'options' => ['placeholder' => $profile->getAttributeLabel('country')],
                                                ],
                                            ],
                                            'region' => [
                                                'type' => Form::INPUT_WIDGET,
                                                'widgetClass' => DepDrop::class,
                                                'options' => [
                                                    'type' => DepDrop::TYPE_SELECT2,
                                                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                                                    'options' => ['placeholder' => $profile->getAttributeLabel('region')],
                                                    'pluginOptions' => [
                                                        'depends' => [Html::getInputId($profile, 'country')],
                                                        'url' => Url::to(['region']),
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ]) ?>
                                    <?= Form::widget([
                                        'model' => $profile,
                                        'form' => $form,
                                        'columns' => 3,
                                        'attributes' => [
                                            'province' => [
                                                'type' => Form::INPUT_WIDGET,
                                                'widgetClass' => DepDrop::class,
                                                'options' => [
                                                    'type' => DepDrop::TYPE_SELECT2,
                                                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                                                    'options' => ['placeholder' => $profile->getAttributeLabel('province')],
                                                    'pluginOptions' => [
                                                        'depends' => [Html::getInputId($profile, 'region')],
                                                        'url' => Url::to(['province']),
                                                    ],
                                                ],
                                            ],
                                            'municipality' => [
                                                'type' => Form::INPUT_WIDGET,
                                                'widgetClass' => DepDrop::class,
                                                'options' => [
                                                    'type' => DepDrop::TYPE_SELECT2,
                                                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                                                    'options' => ['placeholder' => $profile->getAttributeLabel('municipality')],
                                                    'pluginOptions' => [
                                                        'depends' => [Html::getInputId($profile, 'province')],
                                                        'url' => Url::to(['municipality']),
                                                    ],
                                                ],
                                            ],
                                            'zipcode' => [
                                                'type' => Form::INPUT_TEXT,
                                                'options' => ['placeholder' => $profile->getAttributeLabel('zipcode')],
                                            ],
                                        ],
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <?= Html::tag('h4', Yii::t('app', 'Company Details'), ['class' => 'card-title']) ?>
                            <?= Form::widget([
                                'model' => $employment,
                                'form' => $form,
                                'attributes' => [
                                    'is_employed' => [
                                        'type' => Form::INPUT_WIDGET,
                                        'widgetClass' => 'kartik\widgets\SwitchInput',
                                        'options' => [
                                            'pluginOptions' => [
                                                'onColor' => 'success',
                                                'offColor' => 'danger',
                                                'onText' => Yii::t('app', 'Yes'),
                                                'offText' => Yii::t('app', 'No'),
                                            ],
                                            'pluginEvents' => [
                                                'switchChange.bootstrapSwitch' => 'toggleemploymenmtfields',
                                            ],
                                        ],
                                    ],
                                ],
                            ]) ?>
                            <?= Form::widget([
                                'model' => $employment,
                                'form' => $form,
                                'columns' => 3,
                                'attributes' => [
                                    'company_name' => [
                                        'type' => Form::INPUT_TEXT,
                                        'options' => ['placeholder' => $employment->getAttributeLabel('company_name')],
                                    ],
                                    'position' => [
                                        'type' => Form::INPUT_TEXT,
                                        'options' => ['placeholder' => $employment->getAttributeLabel('position')],
                                    ],
                                    'phone_number' => [
                                        'type' => Form::INPUT_TEXT,
                                        'options' => ['placeholder' => $employment->getAttributeLabel('phone_number')],
                                    ],
                                ],
                            ]) ?>
                            <?= Form::widget([
                                'model' => $employment,
                                'form' => $form,
                                'columns' => 3,
                                'attributes' => [
                                    'address' => [
                                        'type' => Form::INPUT_TEXT,
                                        'options' => ['placeholder' => $employment->getAttributeLabel('address')],
                                    ],
                                    'subdivision' => [
                                        'type' => Form::INPUT_TEXT,
                                        'options' => ['placeholder' => $employment->getAttributeLabel('subdivision')],
                                    ],
                                    'barangay' => [
                                        'type' => Form::INPUT_TEXT,
                                        'options' => ['placeholder' => $employment->getAttributeLabel('barangay')],
                                    ],
                                ],
                            ]) ?>
                            <?= Form::widget([
                                'model' => $employment,
                                'form' => $form,
                                'columns' => 2,
                                'attributes' => [
                                    'country' => [
                                        'type' => Form::INPUT_WIDGET,
                                        'widgetClass' => 'kartik\select2\Select2',
                                        'options' => [
                                            'data' => $countryListData,
                                            'options' => ['placeholder' => $employment->getAttributeLabel('country')],
                                        ],
                                    ],
                                    'region' => [
                                        'type' => Form::INPUT_WIDGET,
                                        'widgetClass' => DepDrop::class,
                                        'options' => [
                                            'type' => DepDrop::TYPE_SELECT2,
                                            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                                            'options' => ['placeholder' => $employment->getAttributeLabel('region')],
                                            'pluginOptions' => [
                                                'depends' => [Html::getInputId($employment, 'country')],
                                                'url' => Url::to(['region']),
                                            ],
                                        ],
                                    ],
                                ],
                            ]) ?>
                            <?= Form::widget([
                                'model' => $employment,
                                'form' => $form,
                                'columns' => 3,
                                'attributes' => [
                                    'province' => [
                                        'type' => Form::INPUT_WIDGET,
                                        'widgetClass' => DepDrop::class,
                                        'options' => [
                                            'type' => DepDrop::TYPE_SELECT2,
                                            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                                            'options' => ['placeholder' => $employment->getAttributeLabel('province')],
                                            'pluginOptions' => [
                                                'depends' => [Html::getInputId($employment, 'region')],
                                                'url' => Url::to(['province']),
                                            ],
                                        ],
                                    ],
                                    'municipality' => [
                                        'type' => Form::INPUT_WIDGET,
                                        'widgetClass' => DepDrop::class,
                                        'options' => [
                                            'type' => DepDrop::TYPE_SELECT2,
                                            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                                            'options' => ['placeholder' => $employment->getAttributeLabel('municipality')],
                                            'pluginOptions' => [
                                                'depends' => [Html::getInputId($employment, 'province')],
                                                'url' => Url::to(['municipality']),
                                            ],
                                        ],
                                    ],
                                    'zipcode' => [
                                        'type' => Form::INPUT_TEXT,
                                        'options' => ['placeholder' => $employment->getAttributeLabel('zipcode')],
                                    ],
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <?= Html::tag('h4', Yii::t('app', 'Professional Details'), ['class' => 'card-title']) ?>
                            <?= Form::widget([
                                'model' => $profession,
                                'form' => $form,
                                'columns' => 2,
                                'attributes' => [
                                    'license_number' => [
                                        'type' => Form::INPUT_TEXT,
                                        'options' => ['placeholder' => $profession->getAttributeLabel('license_number')],
                                    ],
                                    'expiration_date' => [
                                        'type' => Form::INPUT_WIDGET,
                                        'widgetClass' => 'kartik\date\DatePicker',
                                        'options' => [
                                            'options' => ['placeholder' => $profession->getAttributeLabel('expiration_date')],
                                            'pluginOptions' => [
                                                'autoclose' => true,
                                                'format' => 'yyyy-mm-dd',
                                                'orientation' => 'bottom',
                                            ],
                                        ],
                                    ],
                                ],
                            ]) ?>
                            <?= Form::widget([
                                'model' => $profession,
                                'form' => $form,
                                'columns' => 2,
                                'attributes' => [
                                    'type_of_practice' => [
                                        'type' => Form::INPUT_CHECKBOX_LIST,
                                        'items' => $typeOfPracticeListData,
                                        'options' => ['custom' => true],
                                    ],
                                    'species_of_specialization' => [
                                        'type' => Form::INPUT_CHECKBOX_LIST,
                                        'items' => $speciesOfSpecializationListData,
                                        'options' => ['custom' => true],
                                    ],
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <?= Html::tag('h4', Yii::t('app', 'Educational Background'), ['class' => 'card-title']) ?>
                            <?= Form::widget([
                                'model' => $education,
                                'form' => $form,
                                'columns' => 2,
                                'attributes' => [
                                    'institution' => [
                                        'type' => Form::INPUT_WIDGET,
                                        'widgetClass' => 'kartik\select2\Select2',
                                        'options' => [
                                            'data' => $veterinaryInstitutionListData,
                                            'options' => ['placeholder' => $education->getAttributeLabel('institution')],
                                        ],
                                    ],
                                    'year_graduated' => [
                                        'type' => Form::INPUT_TEXT,
                                        'options' => ['placeholder' => $education->getAttributeLabel('year_graduated')],
                                    ],
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <?= Html::tag('h4', Yii::t('app', 'VPAP Membership'), ['class' => 'card-title']) ?>
                            <?= Form::widget([
                                'model' => $membership,
                                'form' => $form,
                                'columns' => 4,
                                'attributes' => [
                                    'type' => [
                                        'type' => Form::INPUT_STATIC,
                                        'staticValue' => function ($model, $index, $widget) {
                                            return Yii::$app->formatter->asMembershipType($model->type);
                                        },
                                    ],
                                    'number_years_active' => [
                                        'type' => Form::INPUT_STATIC,
                                        'staticValue' => function ($model, $index, $widget) {
                                            return $model->number_years_active ? $model->number_years_active:
                                                Yii::$app->formatter->nullDisplay;
                                        },
                                    ],
                                    'year_initial_joined' => [
                                        'type' => Form::INPUT_STATIC,
                                        'staticValue' => function ($model, $index, $widget) {
                                            return $model->year_initial_joined;
                                        },
                                        // 'options' => ['placeholder' => $membership->getAttributeLabel('year_initial_joined')],
                                    ],
                                    'discount_type' => [
                                        'type' => Form::INPUT_STATIC,
                                        'staticValue' => function ($model, $index, $widget) {
                                            return Yii::$app->formatter->asMemberDiscountType($model->discount_type);
                                        },
                                    ],
                                ],
                            ]) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body bg-light d-flex justify-content-end">
                            <div class="form-group mb-0">
                                <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary px-4']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php
$is_employed_id = Html::getInputId($employment, 'is_employed');
$employment_company_name_id = Html::getInputId($employment, 'company_name');
$employment_position_id = Html::getInputId($employment, 'position');
$employment_phone_number_id = Html::getInputId($employment, 'phone_number');
$employment_address_id = Html::getInputId($employment, 'address');
$employment_subdivision_id = Html::getInputId($employment, 'subdivision');
$employment_barangay_id = Html::getInputId($employment, 'barangay');
$employment_municipality_id = Html::getInputId($employment, 'municipality');
$employment_province_id = Html::getInputId($employment, 'province');
$employment_region_id = Html::getInputId($employment, 'region');
$employment_zipcode_id = Html::getInputId($employment, 'zipcode');
$employment_country_id = Html::getInputId($employment, 'country');
$js = "
var company_fields = ['$employment_company_name_id', '$employment_position_id', '$employment_phone_number_id', '$employment_address_id', '$employment_subdivision_id', '$employment_barangay_id', '$employment_municipality_id', '$employment_province_id', '$employment_region_id', '$employment_zipcode_id', '$employment_country_id'], company_required_fields = ['$employment_company_name_id', '$employment_position_id', '$employment_municipality_id', '$employment_province_id', '$employment_region_id', '$employment_country_id'];
initemploymenmtfields();
function initemploymenmtfields() { var is_employed = jQuery('#{$is_employed_id}').is(':checked'); jQuery.each(company_fields, function (index, value) { jQuery('#' + value).prop('disabled', ! is_employed); }); if (is_employed) { jQuery.each(company_required_fields, function (index, value) { jQuery('#' + value).closest('.form-group').addClass('required'); }); } }
function toggleemploymenmtfields() { var is_employed = jQuery('#{$is_employed_id}').is(':checked'); if (is_employed) { jQuery.each(company_fields, function (index, value) { jQuery('#' + value).prop('disabled', false); }); jQuery.each(company_required_fields, function (index, value) { jQuery('#' + value).closest('.form-group').addClass('required'); }); } else { jQuery.each(company_fields, function (index, value) { jQuery('#' + value).prop('disabled', true); }); jQuery.each(company_required_fields, function (index, value) { jQuery('#' + value).closest('.form-group').removeClass('required has-error has-success'); jQuery('#' + value).closest('.form-group').addClass('has-success'); jQuery('#' + value).closest('.invalid-feedback').html(''); }); }}
";
$this->registerJs($js, View::POS_READY);