<?php

namespace app\modules\member;

use Yii;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * member module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\member\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    /**
     * {@inheritDoc}
     */
    public function beforeAction($action)
    {
        $session = Yii::$app->session;
        $identity = Yii::$app->user->identity;
        $isGuest = Yii::$app->user->isGuest;
        $avatar = $isGuest ? null : $identity->getThumbUploadUrl('photo');

        $this->view->params['sidebarItems'] = [
            [
                'label' => Yii::t('app', '{icon} Dashboard', [
                    'icon' => Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-speedometer')]),
                        ['class' => 'c-sidebar-nav-icon']
                    ),
                ]),
                'url' => ['/member/default/index'],
            ],
            [
                'label' => Yii::t('app', '{icon} My Events', [
                    'icon' => Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-bell')]),
                        ['class' => 'c-sidebar-nav-icon']
                    ),
                ]),
                'url' => ['/member/event-attendance/index'],
                'visible' => ! $isGuest && $identity->is_profile_updated && $identity->userMembership->type,
                'active' => in_array(ltrim(Yii::$app->controller->route, '/'), [
                    'member/event-attendance/index',
                    'member/event-attendance/update',
                    'member/event-attendance/summary',
                    'member/event/signup',
                ]),
            ],
            [
                'label' => Yii::t('app', '{icon} Pay VPAP Fee', [
                    'icon' => Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-money')]),
                        ['class' => 'c-sidebar-nav-icon']
                    ),
                ]),
                'url' => ['/member/membership-payment/index'],
                'active' => in_array(ltrim(Yii::$app->controller->route, '/'), [
                    'member/membership-payment/index',
                    'member/membership-payment/create',
                    'member/membership-payment/update',
                    'member/membership-payment/summary',
                ]),
            ],
            [
                'label' => Yii::t('app', '{icon} Profile', [
                    'icon' => Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-user')]),
                        ['class' => 'c-sidebar-nav-icon']
                    ),
                ]),
                'url' => ['/member/profile/index'],
            ],
            [
                'label' => Yii::t('app', '{icon} Change Password', [
                    'icon' => Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-lock-locked')]),
                        ['class' => 'c-sidebar-nav-icon']
                    ),
                ]),
                'url' => ['/member/profile/change-password'],
            ],
            [
                'label' => Yii::t('app', '{icon} Logout', [
                    'icon' => Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-account-logout')]),
                        ['class' => 'c-sidebar-nav-icon']
                    ),
                ]),
                'url' => ['/site/logout'],
                'template' => '<a class="c-sidebar-nav-link" href="{url}" data-method="post">{label}</a>',
            ],
        ];

        $this->view->params['userMenu'] = [
            [
            'label' => Html::tag('div', Html::img($avatar, ['class' => 'c-avatar-img', 'alt' => 'avatar']), ['class' => 'c-avatar']),
                'dropdownOptions' => ['class' => 'dropdown-menu dropdown-menu-right pt-0'],
                'items' => [
                    Html::tag('div', Html::tag('strong', Yii::t('app', 'Settings')), ['class' => 'dropdown-header bg-light py-2']),
                    [
                        'label' => Yii::t('app', '{icon} Profile', [
                            'icon' => Html::tag(
                                'svg',
                                Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-user')]),
                                ['class' => 'c-icon mr-2']
                            ),
                        ]),
                        'url' => ['/member/profile/index'],
                        'linkOptions' => ['class' => 'dropdown-item'],
                    ],
                    [
                        'label' => Yii::t('app', '{icon} Change Password', [
                            'icon' => Html::tag(
                                'svg',
                                Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-lock-locked')]),
                                ['class' => 'c-icon mr-2']
                            ),
                        ]),
                        'url' => ['/member/profile/change-password'],
                        'linkOptions' => ['class' => 'dropdown-item'],
                    ],
                    Html::tag('div', '', ['class' => 'dropdown-divider']),
                    Html::beginForm(['/site/logout']) .
                        Html::submitButton(
                            Yii::t('app', 'Logout'),
                            ['class' => 'dropdown-item btn btn-link']
                        ) .
                    Html::endForm()
                ],
            ],
        ];
        return parent::beforeAction($action);
    }
}
