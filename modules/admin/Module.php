<?php

namespace app\modules\admin;

use Yii;
use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * member module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

    /**
     * {@inheritDoc}
     */
    public function beforeAction($action)
    {
        $session = Yii::$app->session;
        $identity = Yii::$app->user->identity;
        $isGuest = Yii::$app->user->isGuest;
        $avatar = $isGuest ? null : $identity->getThumbUploadUrl('photo');

        $this->view->params['sidebarItems'] = [
            [
                'label' => Yii::t('app', '{icon} Dashboard', [
                    'icon' => Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-speedometer')]),
                        ['class' => 'c-sidebar-nav-icon']
                    ),
                ]),
                'url' => ['/admin/default/index'],
            ],
            [
                'label' => Yii::t('app', '{icon} My Events', [
                    'icon' => Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-bell')]),
                        ['class' => 'c-sidebar-nav-icon']
                    ),
                ]),
                'url' => ['/admin/event-attendance/index'],
                'visible' => ! $isGuest && $identity->is_profile_updated,
                'active' => in_array(ltrim(Yii::$app->controller->route, '/'), [
                    'admin/event-attendance/index',
                    'admin/event-attendance/update',
                    'admin/event-attendance/summary',
                    'admin/event/signup',
                ]),
            ],
            [
                'label' => Yii::t('app', '{icon} Pay VPAP Fee', [
                    'icon' => Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-money')]),
                        ['class' => 'c-sidebar-nav-icon']
                    ),
                ]),
                'url' => ['/admin/membership-payment/index'],
                'active' => in_array(ltrim(Yii::$app->controller->route, '/'), [
                    'admin/membership-payment/index',
                    'admin/membership-payment/create',
                    'admin/membership-payment/update',
                    'admin/membership-payment/summary',
                ]),
            ],
            [
                'label' => Yii::t('app', '{icon} Profile', [
                    'icon' => Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-user')]),
                        ['class' => 'c-sidebar-nav-icon']
                    ),
                ]),
                'url' => ['/admin/profile/index'],
            ],
            [
                'label' => Yii::t('app', '{icon} Change Password', [
                    'icon' => Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-lock-locked')]),
                        ['class' => 'c-sidebar-nav-icon']
                    ),
                ]),
                'url' => ['/admin/profile/change-password'],
            ],
            [
                'label' => Yii::t('app', '{icon} Logout', [
                    'icon' => Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-account-logout')]),
                        ['class' => 'c-sidebar-nav-icon']
                    ),
                ]),
                'url' => ['/site/logout'],
                'template' => '<a class="c-sidebar-nav-link" href="{url}" data-method="post">{label}</a>',
            ],
            [
                'label' => Yii::t('app', 'Tools'),
            ],
            [
                'label' => Yii::t('app', '{icon} Events', [
                    'icon' => Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-bookmark')]),
                        ['class' => 'c-sidebar-nav-icon']
                    ),
                ]),
                'url' => ['/admin/event/index'],
                'active' => in_array(ltrim(Yii::$app->controller->route, '/'), [
                    'admin/event/index',
                    'admin/event/create',
                    'admin/event/update',
                    'admin/event/upload-csv',
                    'admin/event/attendances',
                    'admin/event/chart',
                ]),
            ],
            [
                'label' => Yii::t('app', '{icon} Members', [
                    'icon' => Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-people')]),
                        ['class' => 'c-sidebar-nav-icon']
                    ),
                ]),
                'url' => ['/admin/member/index'],
                'active' => in_array(ltrim(Yii::$app->controller->route, '/'), [
                    'admin/member/index',
                    'admin/member/view',
                    'admin/member/officer',
                    'admin/officer/create',
                    'admin/member/membership-activity',
                    'admin/member/membership-payment',
                ]),
            ],
            [
                'label' => Yii::t('app', '{icon} Settings', [
                    'icon' => Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-cog')]),
                        ['class' => 'c-sidebar-nav-icon']
                    ),
                ]),
                'url' => ['/admin/setting/index'],
                'active' => in_array(ltrim(Yii::$app->controller->route, '/'), [
                    'admin/setting/index',
                    'admin/species-of-specialization/create',
                    'admin/species-of-specialization/update',
                    'admin/type-of-practice/create',
                    'admin/type-of-practice/update',
                    'admin/veterinary-institution/create',
                    'admin/veterinary-institution/update',
                    'admin/sponsor/create',
                    'admin/sponsor/update',
                ]),
            ],
        ];

        $this->view->params['userMenu'] = [
            [
            'label' => Html::tag('div', Html::img($avatar, ['class' => 'c-avatar-img', 'alt' => 'avatar']), ['class' => 'c-avatar']),
                'dropdownOptions' => ['class' => 'dropdown-menu dropdown-menu-right pt-0'],
                'items' => [
                    Html::tag('div', Html::tag('strong', Yii::t('app', 'Settings')), ['class' => 'dropdown-header bg-light py-2']),
                    [
                        'label' => Yii::t('app', '{icon} Profile', [
                            'icon' => Html::tag(
                                'svg',
                                Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-user')]),
                                ['class' => 'c-icon mr-2']
                            ),
                        ]),
                        'url' => ['/admin/profile/index'],
                        'linkOptions' => ['class' => 'dropdown-item'],
                    ],
                    [
                        'label' => Yii::t('app', '{icon} Change Password', [
                            'icon' => Html::tag(
                                'svg',
                                Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-lock-locked')]),
                                ['class' => 'c-icon mr-2']
                            ),
                        ]),
                        'url' => ['/admin/profile/change-password'],
                        'linkOptions' => ['class' => 'dropdown-item'],
                    ],
                    Html::tag('div', '', ['class' => 'dropdown-divider']),
                    Html::beginForm(['/site/logout']) .
                        Html::submitButton(
                            Yii::t('app', 'Logout'),
                            ['class' => 'dropdown-item btn btn-link']
                        ) .
                    Html::endForm()
                ],
            ],
        ];
        return parent::beforeAction($action);
    }
}
