<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\bootstrap4\Html;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use app\models\Event;
use app\models\EventAttendance;
use app\models\User;
use app\models\UserProfession;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class UploadCpdPointForm extends Model
{
    /**
     * @var yii\web\UploadedFile
     */
    public $csv_file;

    /**
     * @var integer
     */
    public $length = 0;

    /**
     * @var string
     */
    public $delimiter = ',';

    /**
     * @var string
     */
    public $enclosure = '"';

    /**
     * @var string
     */
    public $escape =  "\\";

    /**
     * @var array
     */
    public $allowedMimeType = [
        'text/plain',
    ];

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['csv_file'], 'file', 'skipOnEmpty' => false, 'mimeTypes' => $this->allowedMimeType, 'checkExtensionByMimeType' => false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'csv_file' => Yii::t('app', 'Upload CSV File'),
        ];
    }

    /**
     * @param Event $event
     * @return array|string
     */
    public function upload(Event $event)
    {
        if (! $this->validate()) {
            return false;
        }

        $upload_dir = Yii::getAlias('@runtime') . '/csv-cpd-point';
        if (! file_exists($upload_dir) || ! is_dir($upload_dir)) {
            FileHelper::createDirectory($upload_dir);
        }

        $path_to_file = "{$upload_dir}/{$this->csv_file->baseName}.{$this->csv_file->extension}";
        $this->csv_file->saveAs($path_to_file);
        if (file_exists($path_to_file)) {
            $length = $this->length;
            $delimiter = $this->delimiter;
            $enclosure = $this->enclosure;
            $escape = $this->escape;

            $lines = [];
            if (($fp = fopen($path_to_file, 'r')) !== FALSE) {
                while (($line = fgetcsv($fp, $length, $delimiter, $enclosure, $escape)) !== FALSE) {
                    array_push($lines, $line);
                }
            }

            $uniqueColumnLabel = 'License Number';
            $cpdPointColumnLabel = 'Cpd';
            $uniqueColumnKey = array_search($uniqueColumnLabel, $lines[0]);
            $cpdPointColumnKey = array_search($cpdPointColumnLabel, $lines[0]);

            $updated_members = [];
            for ($i = 1; $i < count($lines); $i++) {
                $licenseNumber = $lines[$i][$uniqueColumnKey];
                $user = UserProfession::findOne(['license_number' => $licenseNumber]);
                if ($user !== null) {
                    $eventAttendance = EventAttendance::findOne([
                        'member_id' => $user->user_id,
                        'event_id' => $event->id,
                    ]);

                    if ($eventAttendance !== null) {
                        $old_cpd_point = $eventAttendance->cpd_point_earned;
                        $new_cpd_point = $lines[$i][$cpdPointColumnKey];
                        if (is_numeric($new_cpd_point)) {
                            $old_cpd_point = number_format($old_cpd_point, 2, '.', '');
                            $new_cpd_point = number_format($new_cpd_point, 2, '.', '');
                            $eventAttendance->setAttribute('cpd_point_earned', $new_cpd_point);
                            if ($eventAttendance->update() > 0) {
                                array_push(
                                    $updated_members,
                                    Yii::t('app', 'Updated member {license_number}: CPD Point {old_cpd} -> {new_cpd}', [
                                        'license_number' => Html::encode($user->license_number),
                                        'old_cpd' => Html::encode($old_cpd_point),
                                        'new_cpd' => Html::encode($eventAttendance->cpd_point_earned),
                                    ]),
                                );
                            }
                        }
                    }
                }
            }

            FileHelper::unlink($path_to_file);
            if (count($updated_members)) {
                $updated_members = implode("\n", $updated_members);
            } else {
                $updated_members = Yii::t('app', 'No members were updated.');
            }
            return $updated_members;
        }
    }
}
