<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EventAttendance;

/**
 * EventAttendanceSearch represents the model behind the search form of `app\models\EventAttendance`.
 */
class EventAttendanceSearch extends EventAttendance
{
    public $complete_name;
    public $first_name;
    public $last_name;
    public $license_number;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'license_number'], 'safe'],
            [['attendance_type', 'payment_method', 'discount_type', 'payment_confirmed', 'attended', 'created_at'], 'integer'],
            [['amount_paid', 'cpd_point_earned'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param integer $event_id
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($event_id, $params)
    {
        $query = EventAttendance::find();

        // add conditions that should always apply here
        $query->joinWith([
            'event',
            'member' => function ($query) {
                $query->joinWith(['userProfile', 'userProfession']);
            },
        ]);
        $query->andWhere(['event_id' => $event_id])
            ->indexBy('id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'complete_name' => [
                    'asc' => ['{{%user_profile}}.[[last_name]]' => SORT_ASC, '{{%user_profile}}.[[first_name]]' => SORT_ASC],
                    'desc' => ['{{%user_profile}}.[[last_name]]' => SORT_DESC, '{{%user_profile}}.[[first_name]]' => SORT_DESC],
                ],
                'last_name' => [
                    'asc' => ['{{%user_profile}}.[[last_name]]' => SORT_ASC],
                    'desc' => ['{{%user_profile}}.[[last_name]]' => SORT_DESC],
                ],
                'license_number' => [
                    'asc' => ['{{%user_profession}}.[[license_number]]' => SORT_ASC],
                    'desc' => ['{{%user_profession}}.[[license_number]]' => SORT_DESC],
                ],
                'attendance_type',
                'payment_method',
                'cpd_point_earned',
                'amount_paid',
                'payment_confirmed',
                'attended',
                'created_at',
            ],
            'defaultOrder' => [
                'attended' => SORT_ASC,
                'payment_confirmed' => SORT_ASC,
                'created_at' => SORT_ASC,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            '{{%event_attendance}}.[[attendance_type]]' => $this->attendance_type,
            'payment_method' => $this->payment_method,
            'payment_confirmed' => $this->payment_confirmed,
            'attended' => $this->attended,
        ]);

        $query->andFilterWhere(['like', '{{%user_profile}}.[[first_name]]', $this->first_name])
            ->andFilterWhere(['like', '{{%user_profile}}.[[last_name]]', $this->last_name])
            ->andFilterWhere(['like', '{{%user_profession}}.[[license_number]]', $this->license_number]);

        return $dataProvider;
    }
}
