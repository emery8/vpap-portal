<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\base\InvalidArgumentException;
use yii\base\InvalidParamException;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use app\models\Event;
use app\models\EventAttendance;
use app\models\MembershipActivity;
use app\models\UserMembership;
use app\models\enums\EventType;
use app\models\enums\MembershipType;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class MembershipActivityForm extends Model
{
    public $event_type;
    public $membership_type;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['event_type', 'membership_type'], 'required'],
            [['event_type'], 'in', 'range' => array_keys(EventType::listData()), 'allowArray' => true],
            [['membership_type'], 'in', 'range' => array_keys(MembershipType::listData()), 'allowArray' => true],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'membership_type' => Yii::t('app', 'Membership Type'),
            'event_type' => Yii::t('app', 'Event Type'),
        ];
    }

    /**
     * @return integer
     */
    public function populate()
    {
        $event_type = $this->event_type;
        $membership_type = $this->membership_type;

        if (! is_array($event_type)) {
            $event_type = [$event_type];
        }

        if (! is_array($membership_type)) {
            $membership_type = [$membership_type];
        }

        $affectedRows = 0;
        $memberships = UserMembership::find()
            ->andWhere(['in', 'type', $membership_type]);

        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($memberships->each() as $membership) {
                $attendances = EventAttendance::find()
                    ->select(['{{%event}}.*', '{{%event_attendance}}.*', 'aggregate_start_date' => new Expression('year({{%event}}.[[start_date]])')])
                    ->joinWith(['event'])
                    ->andWhere(['in', '{{%event}}.[[type]]', $event_type])
                    ->andWhere(['[[attended]]' => 1])
                    ->andWhere(['[[member_id]]' => $membership->user_id])
                    ->andWhere(['>=', new Expression('year({{%event}}.[[start_date]])'), $membership->year_initial_joined])
                    ->all();

                if (is_array($attendances) && count($attendances)) {
                    $years_attended = ArrayHelper::getColumn($attendances, 'aggregate_start_date');
                    sort($years_attended);
                    // $count_years_attended = null;
                    // if (is_array($years_attended) && count($years_attended)) {
                        // $count_years_attended = count($years_attended);
                    // }
                    $membershipActivity = MembershipActivity::findOne(['member_id' => $membership->user_id]);
                    if ($membershipActivity === null) {
                        $membershipActivity = Yii::createObject(MembershipActivity::class);
                    }

                    $membershipActivity->setAttribute('member_id', $membership->user_id);
                    $membershipActivity->setAttribute('year_event_attended', $years_attended);
                    if (! $membershipActivity->save()) {
                        $transaction->rollBack();
                        throw new InvalidParamException(Yii::t('app', 'Unable to continue due to an error in the batch operation.'));
                        return 0;
                    } else {
                        // $membership->setAttribute('number_years_active', $count_years_attended);
                        // $membership->save();
                        $affectedRows++;
                    }
                }
            }

            $transaction->commit();
            return $affectedRows;
        } catch(\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch(\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * @param array|integer $membership_type
     * @param integer $event_type
     * @return integer
     */
    /*public static function populateTable($membership_type, $event_type)
    {
        if (! is_array($membership_type)) {
            $membership_type = [$membership_type];
        }

        if (! ArrayHelper::isSubset($membership_type, array_keys(MembershipType::listData()))) {
            throw new InvalidArgumentException(Yii::t('app', 'Membership type is invalid.'));
        }

        if (! in_array($event_type, array_keys(EventType::listData()))) {
            throw new InvalidArgumentException(Yii::t('app', 'Event type is invalid.'));
        }

        Yii::$app->db->createCommand()->truncateTable('{{%membership_activity}}')->execute();
        $members = User::find()
            ->joinWith(['userMembership'])
            ->andWhere(['in', '{{%user_membership}}.[[type]]', $membership_type]);

        $affectedRows = 0;
        foreach ($members->each(100) as $member) {
            $event_attended = new Query();
            $event_attended->select(['start_date' => new Expression('year({{%event}}.[[start_date]])'), '{{%event}}.[[id]]', '{{%event_attendance}}.[[event_id]]', '{{%event_attendance}}.[[member_id]]', '{{%event_attendance}}.[[attended]]'])
                ->from(['{{%event}}'])
                ->leftJoin('{{%event_attendance}}', '{{%event_attendance}}.[[event_id]] = {{%event}}.[[id]]')
                ->andWhere(['{{%event}}.[[type]]' => $event_type])
                ->andWhere(['{{%event_attendance}}.[[member_id]]' => $member->id])
                ->andWhere(['{{%event_attendance}}.[[attended]]' => 1])
                ->all();

            $event_paid = new Query();
            $event_paid->select(['start_date' => new Expression('year({{%event}}.[[start_date]])'), '{{%event}}.[[id]]', '{{%event_attendance}}.[[event_id]]', '{{%event_attendance}}.[[member_id]]', '{{%event_attendance}}.[[payment_confirmed]]'])
                ->from(['{{%event}}'])
                ->leftJoin('{{%event_attendance}}', '{{%event_attendance}}.[[event_id]] = {{%event}}.[[id]]')
                ->andWhere(['{{%event}}.[[type]]' => $event_type])
                ->andWhere(['{{%event_attendance}}.[[member_id]]' => $member->id])
                ->andWhere(['{{%event_attendance}}.[[payment_confirmed]]' => 1])
                ->all();

            if (is_array($event_attended) && count($event_attended)) {
                $event_attended = ArrayHelper::getColumn($event_attended, 'start_date');
                $event_attended = Json::encode($event_attended);
            } else {
                $event_attended = null;
            }

            if (is_array($event_paid) && count($event_paid)) {
                $event_paid = ArrayHelper::getColumn($event_paid, 'start_date');
                $event_paid = Json::encode($event_paid);
            } else {
                $event_paid = null;
            }

            if (! is_null($event_attended) || ! is_null($event_paid)) {
                $model = new self;
                $model->setAttribute('member_id', $member->id);
                $model->setAttribute('event_attended_year_attended', $event_attended);
                $model->setAttribute('event_attended_year_paid', $event_paid);
                if (! $model->save()) {
                    throw new InvalidParamException(Yii::t('app', 'Unable to continue due to an error in the batch operation.'));
                } else {
                    $affectedRows++;
                }
            }
        }
        
        return $affectedRows;
    }*/
}
