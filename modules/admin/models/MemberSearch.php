<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * MemberSearch represents the model behind the search form of `app\models\User`.
 */
class MemberSearch extends User
{
    public $complete_name;
    public $first_name;
    public $last_name;
    public $age;
    public $birth_date;
    public $license_number;
    public $membership_type;
    public $discount_type;
    public $year_initial_joined;
    public $number_years_active;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'first_name', 'last_name', 'license_number', 'membership_type', 'discount_type', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        // add conditions that should always apply here
        $query->joinWith(['userProfile', 'userProfession', 'userMembership'])
            ->indexBy('id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            // 'params' => array_merge($_GET, ['#' => 'members-tab']),
            'attributes' => [
                'email',
                'lock_name_on_certificate',
                'complete_name' => [
                    'asc' => ['{{%user_profile}}.[[last_name]]' => SORT_ASC, '{{%user_profile}}.[[first_name]]' => SORT_ASC],
                    'desc' => ['{{%user_profile}}.[[last_name]]' => SORT_DESC, '{{%user_profile}}.[[first_name]]' => SORT_DESC],
                ],
                // 'first_name' => [
                    // 'asc' => ['{{%user_profile}}.[[first_name]]' => SORT_ASC],
                    // 'desc' => ['{{%user_profile}}.[[first_name]]' => SORT_DESC],
                // ],
                'last_name' => [
                    'asc' => ['{{%user_profile}}.[[last_name]]' => SORT_ASC],
                    'desc' => ['{{%user_profile}}.[[last_name]]' => SORT_DESC],
                ],
                'birth_date' => [
                    'asc' => ['{{%user_profile}}.[[birth_date]]' => SORT_ASC],
                    'desc' => ['{{%user_profile}}.[[birth_date]]' => SORT_DESC],
                ],
                'license_number' => [
                    'asc' => ['{{%user_profession}}.[[license_number]]' => SORT_ASC],
                    'desc' => ['{{%user_profession}}.[[license_number]]' => SORT_DESC],
                ],
                'membership_type' => [
                    'asc' => ['{{%user_membership}}.[[type]]' => SORT_ASC],
                    'desc' => ['{{%user_membership}}.[[type]]' => SORT_DESC],
                ],
                'discount_type' => [
                    'asc' => ['{{%user_membership}}.[[discount_type]]' => SORT_ASC],
                    'desc' => ['{{%user_membership}}.[[discount_type]]' => SORT_DESC],
                ],
                'year_initial_joined' => [
                    'asc' => ['{{%user_membership}}.[[year_initial_joined]]' => SORT_ASC],
                    'desc' => ['{{%user_membership}}.[[year_initial_joined]]' => SORT_DESC],
                ],
                'number_years_active' => [
                    'asc' => ['{{%user_membership}}.[[number_years_active]]' => SORT_ASC],
                    'desc' => ['{{%user_membership}}.[[number_years_active]]' => SORT_DESC],
                ],
                'status',
            ],
            'defaultOrder' => [
                'status' => SORT_ASC,
                'last_name' => SORT_ASC,
            ],
        ]);

        /*$dataProvider->setPagination([
            'params' => array_merge($_GET, ['#' => 'members-tab']),
        ]);*/

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            '{{%user}}.[[status]]' => $this->status,
            '{{%user_membership}}.[[type]]' => $this->membership_type,
            '{{%user_membership}}.[[discount_type]]' => $this->discount_type,
        ]);

        $query->andFilterWhere(['like', '{{%user}}.[[email]]', $this->email])
            ->andFilterWhere(['like', '{{%user_profile}}.[[first_name]]', $this->first_name])
            ->andFilterWhere(['like', '{{%user_profile}}.[[last_name]]', $this->last_name])
            ->andFilterWhere(['like', '{{%user_profession}}.[[license_number]]', $this->license_number]);

        return $dataProvider;
    }
}
