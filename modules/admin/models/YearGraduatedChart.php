<?php

namespace app\modules\admin\models;

use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use app\models\Event;
use app\traits\ChartTrait;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class YearGraduatedChart
{
    use ChartTrait;

    /**
     * @param app\models\Event $event
     * @param string $chart_type
     * @return array
     */
    public function fetch(Event $event, $chart_type)
    {
        $attendances = $this->getEventAttendance($event);
        $query = new Query();
        $query->select([
            new Expression("
                CASE
                    WHEN year_graduated != '' THEN year_graduated
                    ELSE '{$this->nullTextDisplay}'
                END AS year_graduated_range
            "),
            'count' => new Expression('count(*)')
        ])
            ->from('{{%user_education}}')
            ->andWhere(['in', '[[user_id]]', ArrayHelper::getColumn($attendances, 'member_id')])
            ->groupBy('year_graduated_range');

        $data = [];
        switch ($chart_type) {
            case 'bar':
                $tmp = [];
                foreach ($query->each() as $model) {
                    $tmp[] = ['name' => $model['year_graduated_range']];
                }

                foreach ($query->each() as $model) {
                    for ($i = 0; $i < count($tmp); $i++) {
                        if ($tmp[$i]['name'] === $model['year_graduated_range']) {
                            $tmp[$i]['data'][] = (int) $model['count'];
                        } else {
                            $tmp[$i]['data'][] = 0;
                        }
                    }
                }

                $data = array_values($tmp);
                break;
            case 'pie':
                foreach ($query->each() as $model) {
                    $data[] = [
                        'name' => $model['year_graduated_range'],
                        'y' => (int) $model['count'],
                    ];
                }
                break;
            default:
                return $data;
        }
        return $data;
    }
}
