<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Announcement;

/**
 * AnnouncementSearch represents the model behind the search form of `app\models\Announcement`.
 */
class AnnouncementSearch extends Model
{
    /**
     * @var int
     */
    public $status;

    /**
     * @var string
     */
    public $title;
    public $created_by;
    public $created_at;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['title', 'created_by', 'created_at'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Announcement::find()
            ->joinWith(['createdBy' => function ($query) {
                $query->joinWith('userProfile');
            }])
            ->indexBy('id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere([
                'or',
                ['like', '{{%user_profile}}.[[first_name]]', $this->created_by],
                ['like', '{{%user_profile}}.[[last_name]]', $this->created_by]
            ])
            ->andFilterWhere(['=', new \yii\db\Expression('DATE(FROM_UNIXTIME({{%announcement}}.[[created_at]]))'), $this->created_at]);

        return $dataProvider;
    }
}
