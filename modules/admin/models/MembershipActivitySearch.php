<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MembershipActivity;

/**
 * MembershipActivitySearch represents the model behind the search form of `app\models\MembershipActivity`.
 */
class MembershipActivitySearch extends MembershipActivity
{
    public $complete_name;
    public $first_name;
    public $last_name;
    public $event_attendance_count;
    public $membership_type;
    public $year_initial_joined;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['complete_name'], 'safe'],
            [['membership_type'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MembershipActivity::find();

        // add conditions that should always apply here
        $query->joinWith(['member' => function ($query) {
            $query->joinWith(['userProfile', 'userMembership']);
        }]);
        $query->indexBy('id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            // 'params' => array_merge($_GET, ['#' => 'membership-activity-tab']),
            'attributes' => [
                'year_initial_joined' => [
                    'asc' => ['{{%user_membership}}.[[year_initial_joined]]' => SORT_ASC],
                    'desc' => ['{{%user_membership}}.[[year_initial_joined]]' => SORT_DESC],
                ],
                'year_event_attended',
                'year_membership_paid',
                'complete_name' => [
                    'asc' => ['{{%user_profile}}.[[first_name]]' => SORT_ASC, '{{%user_profile}}.[[last_name]]' => SORT_ASC],
                    'desc' => ['{{%user_profile}}.[[first_name]]' => SORT_DESC, '{{%user_profile}}.[[last_name]]' => SORT_DESC],
                ],
                'first_name' => [
                    'asc' => ['{{%user_profile}}.[[first_name]]' => SORT_ASC],
                    'desc' => ['{{%user_profile}}.[[first_name]]' => SORT_DESC],
                ],
                'last_name' => [
                    'asc' => ['{{%user_profile}}.[[last_name]]' => SORT_ASC],
                    'desc' => ['{{%user_profile}}.[[last_name]]' => SORT_DESC],
                ],
                'membership_type' => [
                    'asc' => ['{{%user_membership}}.[[type]]' => SORT_ASC],
                    'desc' => ['{{%user_membership}}.[[type]]' => SORT_DESC],
                ],
            ],
            'defaultOrder' => ['last_name' => SORT_ASC],
        ]);

        /*$dataProvider->setPagination([
            'params' => array_merge($_GET, ['#' => 'membership-activity-tab']),
        ]);*/

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['{{%user_membership}}.[[type]]' => $this->membership_type]);

        // grid filtering conditions
        $query->andFilterWhere([
            'or',
            ['like', '{{%user_profile}}.[[first_name]]', $this->complete_name],
            ['like', '{{%user_profile}}.[[last_name]]', $this->complete_name],
        ]);

        return $dataProvider;
    }
}
