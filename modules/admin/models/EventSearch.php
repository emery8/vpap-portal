<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Event;
use app\models\enums\EventType;

/**
 * EventSearch represents the model behind the search form of `app\models\Event`.
 */
class EventSearch extends Event
{
    public $participants_count;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'attendance_type', 'participants_count', 'maximum_participant'], 'integer'],
            [['type'], 'in', 'range' => array_keys(EventType::listData())],
            [['cpd_point'], 'number'],
            [['start_date', 'end_date'], 'date', 'format' => 'yyyy-MM-dd'],
            [['title'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Event::find();

        // add conditions that should always apply here
        $query->with('participantsAggregation');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['start_date' => SORT_DESC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'type' => $this->type,
            'attendance_type' => $this->attendance_type,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'cpd_point' => $this->cpd_point,
            'maximum_participant' => $this->maximum_participant,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
