<?php

namespace app\modules\admin\models;

use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use app\models\Event;
use app\traits\ChartTrait;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class VeterinarySchoolGraduatedChart
{
    use ChartTrait;

    /**
     * @param app\models\Event $event
     * @param string $chart_type
     * @return array
     */
    public function fetch(Event $event, $chart_type)
    {
        $attendances = $this->getEventAttendance($event);
        $query = new Query();
        $query->select([
            new Expression("
                CASE
                    WHEN {{%veterinary_institution}}.[[institution]] != '' THEN {{%veterinary_institution}}.[[institution]]
                    ELSE '{$this->nullTextDisplay}'
                END AS institution_range
            "),
            'count' => new Expression('count(*)')
        ])
            ->from('{{%user_education}}')
            ->leftJoin('{{%veterinary_institution}}', '{{%veterinary_institution}}.[[id]] = {{%user_education}}.[[institution]]')
            ->andWhere(['in', '[[user_id]]', ArrayHelper::getColumn($attendances, 'member_id')])
            ->groupBy('institution_range');

        $data = [];
        switch ($chart_type) {
            case 'bar':
                $tmp = [];
                foreach ($query->each() as $model) {
                    $tmp[] = ['name' => $model['institution_range']];
                }

                foreach ($query->each() as $model) {
                    for ($i = 0; $i < count($tmp); $i++) {
                        if ($tmp[$i]['name'] === $model['institution_range']) {
                            $tmp[$i]['data'][] = (int) $model['count'];
                        } else {
                            $tmp[$i]['data'][] = 0;
                        }
                    }
                }

                $data = array_values($tmp);
                break;
            case 'pie':
                foreach ($query->each() as $model) {
                    $data[] = [
                        'name' => $model['institution_range'],
                        'y' => (int) $model['count'],
                    ];
                }
                break;
            default:
                return $data;
        }

        return $data;
    }
}
