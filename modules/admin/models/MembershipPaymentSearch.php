<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MembershipPayment;

/**
 * MembershipPaymentSearch represents the model behind the search form of `app\models\MembershipPayment`.
 */
class MembershipPaymentSearch extends MembershipPayment
{
    public $complete_name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['complete_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MembershipPayment::find();

        // add conditions that should always apply here
        $query->joinWith(['member' => function ($query) {
            $query->joinWith(['userProfile']);
        }])->indexBy('id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'type',
                'amount',
                'amount_paid',
                'number_of_year',
                'payment_confirmed',
                'created_at',
                'complete_name' => [
                    'asc' => ['{{%user_profile}}.[[first_name]]' => SORT_ASC, '{{%user_profile}}.[[last_name]]' => SORT_ASC],
                    'desc' => ['{{%user_profile}}.[[first_name]]' => SORT_DESC, '{{%user_profile}}.[[last_name]]' => SORT_DESC],
                ],
                'first_name' => [
                    'asc' => ['{{%user_profile}}.[[first_name]]' => SORT_ASC],
                    'desc' => ['{{%user_profile}}.[[first_name]]' => SORT_DESC],
                ],
                'last_name' => [
                    'asc' => ['{{%user_profile}}.[[last_name]]' => SORT_ASC],
                    'desc' => ['{{%user_profile}}.[[last_name]]' => SORT_DESC],
                ],
            ],
            'defaultOrder' => ['payment_confirmed' => SORT_ASC, 'last_name' => SORT_ASC],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'or',
            ['like', '{{%user_profile}}.[[first_name]]', $this->complete_name],
            ['like', '{{%user_profile}}.[[last_name]]', $this->complete_name],
        ]);

        return $dataProvider;
    }
}
