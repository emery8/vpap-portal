<?php

namespace app\modules\admin\models;

use yii\bootstrap4\Html;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use app\models\Event;
use app\models\EventAttendance;
use app\traits\ChartTrait;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 * https://stackoverflow.com/questions/38503344/group-by-age-ranges-in-mysql
 * https://stackoverflow.com/questions/3247630/mysql-group-by-age-range-including-null-ranges
 */
class EventAgeChart
{
    use ChartTrait;

    /**
     * @param app\models\Event $event
     * @param string $chart_type
     * @return array
     */
    public function fetch(Event $event, $chart_type)
    {
        $attendances = $this->getEventAttendance($event);
        $query = (new Query())
            ->select([
                new Expression('
                    CASE
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) < 20 THEN "Under 20"
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) BETWEEN 20 and 29 THEN "20 to 29"
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) BETWEEN 30 and 39 THEN "30 to 39"
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) BETWEEN 40 and 49 THEN "40 to 49"
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) BETWEEN 50 and 59 THEN "50 to 59"
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) BETWEEN 60 and 69 THEN "60 to 69"
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) BETWEEN 70 and 79 THEN "70 to 79"
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) >= 80 THEN "Over 80"
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) IS NULL THEN "Not Filled In"
                    END as age_range
                '),
                'count' => new Expression('count(*)'),
                new Expression('
                    CASE
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) < 20 THEN 1
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) BETWEEN 20 and 29 THEN 2
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) BETWEEN 30 and 39 THEN 3
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) BETWEEN 40 and 49 THEN 4
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) BETWEEN 50 and 59 THEN 5
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) BETWEEN 60 and 69 THEN 6
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) BETWEEN 70 and 79 THEN 7
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) >= 80 THEN 8
                        WHEN TIMESTAMPDIFF(YEAR, [[birth_date]], CURDATE()) IS NULL THEN 9
                    END AS ordinal
                '),
            ])
            ->from('{{%user_profile}}')
            ->andWhere(['in', '[[user_id]]', ArrayHelper::getColumn($attendances, 'member_id')])
            ->groupBy('age_range')
            ->orderBy('ordinal');
        // echo '<pre>'; print_r($query->all()); die();

        $data = [];
        switch ($chart_type) {
            case 'bar':
                $tmp = [];
                foreach ($query->each() as $model) {
                    $tmp[] = ['name' => $model['age_range']];
                }

                foreach ($query->each() as $model) {
                    for ($i = 0; $i < count($tmp); $i++) {
                        if ($tmp[$i]['name'] === $model['age_range']) {
                            $tmp[$i]['data'][] = (int) $model['count'];
                        } else {
                            $tmp[$i]['data'][] = 0;
                        }
                    }
                }

                $data = array_values($tmp);
                break;
            case 'pie':
                foreach ($query->each() as $model) {
                    $data[] = [
                        'name' => $model['age_range'],
                        'y' => (int) $model['count'],
                    ];
                }
                break;
            default:
                return $data;
        }

        return $data;
    }
}