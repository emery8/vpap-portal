<?php

namespace app\modules\admin\models;

use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use app\models\Event;
use app\models\UserProfession;
use app\models\TypeOfPractice;
use app\traits\ChartTrait;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class TypeOfPracticeChart
{
    use ChartTrait;

    /**
     * @param app\models\Event $event
     * @param string $chart_type
     * @return array
     */
    public function fetch(Event $event, $chart_type)
    {
        $attendances = $this->getEventAttendance($event);
        $models = UserProfession::find()
            ->select('type_of_practice')
            ->from('{{%user_profession}}')
            ->andWhere(['in', '[[user_id]]', ArrayHelper::getColumn($attendances, 'member_id')])
            ->all();

        $typeOfPractices = ArrayHelper::getColumn($models, 'type_of_practice');
        $tmp = [];
        $nullCount = 0;
        foreach ($typeOfPractices as $typeOfPractice) {
            if ($typeOfPractice === null) {
                $tmp['null'] = $nullCount++;
            } else {
                if (is_array($typeOfPractice)) {
                    foreach ($typeOfPractice as $value) {
                        if (isset($tmp[$value])) {
                            $count = $tmp[$value];
                            $tmp[$value] = ++$count;
                        } else {
                            $tmp[$value] = 1;
                        }
                    }
                }
            }
        }

        $query = [];
        foreach ($tmp as $key => $value) {
            if ($key === 'null') {
                $query[] = [
                    'type_of_practice_range' => $this->nullTextDisplay,
                    'count' => $value,
                ];
            } else {
                $model = TypeOfPractice::findOne($key);
                if ($model) {
                    $query[] = [
                        'type_of_practice_range' => $model->description,
                        'count' => $value,
                    ];
                } else {
                    $query[] = [
                        'type_of_practice_range' => Yii::t('app', 'Unkown'),
                        'count' => $value,
                    ];
                }
            }
        }

        // echo '<pre>'; print_r($query); die();

        $data = [];
        switch ($chart_type) {
            case 'bar':
                $tmp = [];
                foreach ($query as $model) {
                    $tmp[] = ['name' => $model['type_of_practice_range']];
                }
                foreach ($query as $model) {
                    for ($i = 0; $i < count($tmp); $i++) {
                        if ($tmp[$i]['name'] === $model['type_of_practice_range']) {
                            $tmp[$i]['data'][] = (int) $model['count'];
                        } else {
                            $tmp[$i]['data'][] = 0;
                        }
                    }
                }

                $data = array_values($tmp);
                break;
            case 'pie':
                foreach ($query as $model) {
                    $data[] = [
                        'name' => $model['type_of_practice_range'],
                        'y' => (int) $model['count'],
                    ];
                }
                break;
            default:
                return $data;
        }
        return $data;
    }
}
