<?php

namespace app\modules\admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Officer;

/**
 * OfficerSearch represents the model behind the search form of `app\models\Officer`.
 */
class OfficerSearch extends Officer
{
    public $complete_name;
    public $first_name;
    public $last_name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['complete_name', 'year_served', 'was_president'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Officer::find();

        // add conditions that should always apply here
        $query->joinWith(['member' => function ($query) {
            $query->joinWith(['userProfile']);
        }])->indexBy('id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            // 'params' => array_merge($_GET, ['#' => 'officers-tab']),
            'attributes' => [
                'was_president',
                'year_served',
                'number_years_served',
                'complete_name' => [
                    'asc' => ['{{%user_profile}}.[[first_name]]' => SORT_ASC, '{{%user_profile}}.[[last_name]]' => SORT_ASC],
                    'desc' => ['{{%user_profile}}.[[first_name]]' => SORT_DESC, '{{%user_profile}}.[[last_name]]' => SORT_DESC],
                ],
                'first_name' => [
                    'asc' => ['{{%user_profile}}.[[first_name]]' => SORT_ASC],
                    'desc' => ['{{%user_profile}}.[[first_name]]' => SORT_DESC],
                ],
                'last_name' => [
                    'asc' => ['{{%user_profile}}.[[last_name]]' => SORT_ASC],
                    'desc' => ['{{%user_profile}}.[[last_name]]' => SORT_DESC],
                ],
            ],
            'defaultOrder' => ['last_name' => SORT_ASC],
        ]);

        /*$dataProvider->setPagination([
            'params' => array_merge($_GET, ['#' => 'officers-tab']),
        ]);*/

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'was_president' => $this->was_president,
        ]);

        $query->andFilterWhere([
            'or',
            ['like', '{{%user_profile}}.[[first_name]]', $this->complete_name],
            ['like', '{{%user_profile}}.[[last_name]]', $this->complete_name],
        ])->andFilterWhere(['like', 'year_served', $this->year_served]);

        return $dataProvider;
    }
}
