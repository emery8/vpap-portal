<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use app\models\EventAttendance;
use app\models\FeaturedImage;
use app\models\ProgramTemplate;
use app\models\InvitationTemplate;
use app\models\enums\EventType;

/**
 * MemberEventAttendanceSearch represents the model behind the search form of `app\models\EventAttendance`.
 */
class MemberEventAttendanceSearch extends Model
{
    public $id;
    public $member_id;
    public $event_id;
    public $cpd_point_earned;
    public $title;
    public $start_date;
    public $end_date;
    public $attended;

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EventAttendance::find()->select([
            'id' => '{{%event_attendance}}.[[id]]',
            'member_id' => '{{%event_attendance}}.[[member_id]]',
            'event_id' => '{{%event_attendance}}.[[event_id]]',
            'cpd_point_earned' => '{{%event_attendance}}.[[cpd_point_earned]]',
            'attended' => '{{%event_attendance}}.[[attended]]',
            'created_at' => '{{%event_attendance}}.[[created_at]]',
        ]);

        // add conditions that should always apply here
        $query->joinWith(['event']);
        $query->andWhere(['member_id' => $params['id']])
            ->indexBy('id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setPagination([
            'pageSize' => 10,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'type' => [
                    'asc' => ['{{%event}}.[[type]]' => SORT_ASC],
                    'desc' => ['{{%event}}.[[type]]' => SORT_DESC],
                ],
                'title' => [
                    'asc' => ['{{%event}}.[[title]]' => SORT_ASC],
                    'desc' => ['{{%event}}.[[title]]' => SORT_DESC],
                ],
                'start_date' => [
                    'asc' => ['{{%event}}.[[start_date]]' => SORT_ASC],
                    'desc' => ['{{%event}}.[[start_date]]' => SORT_DESC],
                ],
                'end_date' => [
                    'asc' => ['{{%event}}.[[end_date]]' => SORT_ASC],
                    'desc' => ['{{%event}}.[[end_date]]' => SORT_DESC],
                ],
                'cpd_point_earned',
                'attended',
            ],
            'defaultOrder' => ['start_date' => SORT_DESC],
        ]);

        return $dataProvider;
    }
}
