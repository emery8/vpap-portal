<?php

use yii\bootstrap4\Html;
use yii\web\View;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use app\models\enums\EventAttendanceType;
use app\models\enums\PaymentMethod;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $event app\models\Event */
/* @var $model app\models\EventAttendance */
/* @var $eventAttendanceTypeListData app\models\enums\EventAttendanceType */
/* @var $paymentMethodListData app\models\enums\PaymentMethod */
/* @var $sponsorListData app\models\Sponsor */

$attendance_type_wrapper_id = Html::getInputId($model, 'attendance_type') . '-wrapper';
$payment_method_wrapper_id = Html::getInputId($model, 'payment_method') . '-wrapper';
$sponsor_wrapper_id = Html::getInputId($model, 'sponsor') . '-wrapper';
?>

<div class="event-attendance-form">

    <?php $form = ActiveForm::begin([
        'action' => ['event-attendance/create', 'slug' => $event->slug],
        'validationUrl' => ['event-attendance/ajax-validate', 'slug' => $event->slug],
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'validateOnBlur' => false,
        'validateOnChange' => false,
    ]); ?>
        <?= Form::widget([
            'model' => $model,
            'form' => $form,
            'attributes' => [
                'attendance_type' => [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'container' => ['id' => $attendance_type_wrapper_id],
                    'items' => $eventAttendanceTypeListData,
                    'options' => ['prompt' => $model->getAttributeLabel('attendance_type')],
                ],
                'payment_method' => [
                    'type' => Form::INPUT_RADIO_LIST,
                    'label' => Yii::t('app', 'How are you paying for this event?'),
                    'container' => ['id' => $payment_method_wrapper_id],
                    'items' => [
                        PaymentMethod::SPONSOR => PaymentMethod::getLabel(PaymentMethod::SPONSOR),
                        PaymentMethod::PERSONAL => PaymentMethod::getLabel(PaymentMethod::PERSONAL),
                    ],
                    'options' => [
                        'custom' => true,
                        'inline' => true,
                    ],
                ],
                'sponsor_id' => [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'container' => ['id' => $sponsor_wrapper_id],
                    'items' => $sponsorListData,
                    'options' => ['prompt' => $model->getAttributeLabel('sponsor_id')],
                ],
            ],
        ]) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Proceed Checkout'), ['class' => 'btn btn-warning']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>
<?php
$attendance_type_id = Html::getInputId($model, 'attendance_type');
$payment_method_name = Html::getInputName($model, 'payment_method');
$sponsor_id = Html::getInputId($model, 'sponsor_id');

$participant_value = EventAttendanceType::PARTICIPANT;
$sponsor_value = PaymentMethod::SPONSOR;
$js = "
function resetAttendanceTypeInput() { jQuery('#{$attendance_type_id}').prop('selectedIndex', 0); }
function resetPaymentMethodInput() { jQuery('input:radio[name=\"{$payment_method_name}\"]').each(function () { jQuery(this).prop('checked', false); }); }
function resetSponsorInput() { jQuery('#{$sponsor_id}').prop('selectedIndex', 0); }
function hideAttendanceTypeWrapper() { jQuery('#{$attendance_type_wrapper_id}').hide(); }
function hidePaymentMethodWrapper() { jQuery('#{$payment_method_wrapper_id}').hide(); }
function hideSponsorWrapper() { jQuery('#{$sponsor_wrapper_id}').hide(); }
function showAttendanceTypeWrapper() { jQuery('#{$attendance_type_wrapper_id}').show(); }
function showPaymentMethodWrapper() { jQuery('#{$payment_method_wrapper_id}').show(); }
function showSponsorWrapper() { jQuery('#{$sponsor_wrapper_id}').show(); }
hidePaymentMethodWrapper();
hideSponsorWrapper();
if (jQuery('#{$attendance_type_id}').val() == {$participant_value}) { showPaymentMethodWrapper(); }
if (jQuery('input:radio[name=\"{$payment_method_name}\"]:checked').val() == {$participant_value}) { showSponsorWrapper(); }
jQuery('.modal').on('shown.coreui.modal', function () {  resetAttendanceTypeInput(); resetPaymentMethodInput(); resetSponsorInput(); hidePaymentMethodWrapper(); hideSponsorWrapper(); });
jQuery('#{$attendance_type_id}').on('change', function () {
    resetPaymentMethodInput();
    resetSponsorInput();
    hidePaymentMethodWrapper();
    hideSponsorWrapper();
    jQuery('[name=\"{$payment_method_name}\"]').removeClass('is-invalid');
    jQuery('[name=\"{$payment_method_name}\"]').closest('.form-group').removeClass('required has-error has-success');
    jQuery('[name=\"{$payment_method_name}\"]').closest('.invalid-feedback').html('');
    var selected = jQuery(this).val();
    if (selected == {$participant_value}) {
        jQuery('[name=\"{$payment_method_name}\"]').closest('.form-group').addClass('required');
        showPaymentMethodWrapper();
    }
});
jQuery('input:radio[name=\"{$payment_method_name}\"]').on('change', function () {
    resetSponsorInput();
    hideSponsorWrapper();
    jQuery('#{$sponsor_id}').removeClass('is-invalid');
    jQuery('#{$sponsor_id}').closest('.form-group').removeClass('required has-error has-success');
    jQuery('#{$sponsor_id}').closest('.invalid-feedback').html('');
    var selected = jQuery(this).val();
    if (selected == {$sponsor_value}) {
        jQuery('#{$sponsor_id}').closest('.form-group').addClass('required');
        showSponsorWrapper();
    }
});
";
$this->registerJs($js, View::POS_READY);