<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use app\models\Event;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\EventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="card-body">
        <div class="d-flex justify-content-between mb-3">
            <?= Html::a(Yii::t('app', 'Create Event'), ['create'], ['class' => 'btn btn-success px-4']) ?>
            <div>
                <?= Html::a(
                    Yii::t('app', 'Reset'),
                    ['index'],
                    [
                        'class' => 'btn btn-outline-dark',
                        'title'=>Yii::t('app', 'Reset Grid'),
                        'data-pjax' => 0, 
                    ]
                ) ?>
            </div>
        </div>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table-responsive-sm'],
            'headerRowOptions' => ['class' => 'thead-light'],
            'hover' => true,
            'striped' => false,
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'mergeHeader' => false,
                ],

                [
                    'attribute' => 'type',
                    'format' => 'eventtype',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'data' => $eventTypeListData,
                        'options' => ['placeholder' => ''],
                    ],
                ],
                [
                    'attribute' => 'attendance_type',
                    'format' => 'eventinvitationtype',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'data' => $eventInvitationTypeListData,
                        'options' => ['placeholder' => ''],
                    ],
                ],
                'title',
                [
                    'attribute' => 'start_date',
                    'format' => 'date',
                    'filterType' => GridView::FILTER_DATE,
                    'filterWidgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                            'orientation' => 'bottom',
                        ],
                    ],
                ],
                [
                    'attribute' => 'end_date',
                    'format' => 'date',
                    'filterType' => GridView::FILTER_DATE,
                    'filterWidgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                            'orientation' => 'bottom',
                        ],
                    ],
                ],
                [
                    'attribute' => 'cpd_point',
                    'format' => ['decimal', 2],
                ],
                [
                    'hAlign' => 'center',
                    'attribute' => 'maximum_participant',
                    'format' => 'html',
                    'value' => function ($model, $key, $index, $column) {
                        $value = $model->participants_count . '/' . $model->maximum_participant;
                        return Html::a(
                            Html::encode($value),
                            ['attendances', 'slug' => $model->slug],
                            ['class' => 'btn btn-outline-dark btn-pill btn-sm']
                        );
                    },
                ],

                [
                    'class' => 'kartik\grid\ActionColumn',
                    'mergeHeader' => false,
                    'viewOptions' => ['style' => 'display: none;'],
                    'template' => "{chart}\n{update}\n{delete}",
                    'buttons' => [
                        'chart' => function ($url, Event $model) {
                            $label = Html::tag('span', '', ['class' => 'fas fa-chart-line', 'aria-hidden' => true]);
                            $url = ['chart', 'slug' => $model->slug];
                            return Html::a(
                                $label,
                                $url,
                                [
                                    'title' => Yii::t('app', 'Chart'),
                                    'aria-label' => Yii::t('app', 'Chart'),
                                    'data-pjax' => 0,
                                ],
                            );
                        },
                    ],
                    'urlCreator' => function ($action, Event $model, $key, $index, $parent) {
                        return Url::toRoute([$action, 'slug' => $model->slug]);
                    },
                ],
            ],
        ]); ?>
    </div>
</div>
