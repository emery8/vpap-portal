<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $event app\models\Event */
/* @var $model app\models\EventAttendance */
/* @var $eventAttendanceTypeListData array */
/* @var $paymentMethodListData array */
?>
<div class="my-event-attendance-search">
    <div class="card">
        <div class="card-body bg-light">
            <?php $form = ActiveForm::begin([
                'action' => ['attendances', 'slug' => $event->slug],
                'method' => 'get',
            ]); ?>
                <?= Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' => 3,
                    'attributes' => [
                        'first_name' => [
                            'type' => Form::INPUT_TEXT,
                            'label' => false,
                            'options' => ['placeholder' => $model->getAttributeLabel('first_name')],
                        ],
                        'last_name' => [
                            'type' => Form::INPUT_TEXT,
                            'label' => false,
                            'options' => ['placeholder' => $model->getAttributeLabel('last_name')],
                        ],
                        'license_number' => [
                            'type' => Form::INPUT_TEXT,
                            'label' => false,
                            'options' => ['placeholder' => $model->getAttributeLabel('license_number')],
                        ],
                    ],
                ]) ?>
                <?= Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' => 4,
                    'attributes' => [
                        'attendance_type' => [
                            'type' => Form::INPUT_DROPDOWN_LIST,
                            'label' => false,
                            'items' => $eventAttendanceTypeListData,
                            'options' => ['prompt' => $model->getAttributeLabel('attendance_type')],
                        ],
                        'payment_method' => [
                            'type' => Form::INPUT_DROPDOWN_LIST,
                            'label' => false,
                            'items' => $paymentMethodListData,
                            'options' => ['prompt' => $model->getAttributeLabel('payment_method')],
                        ],
                        'payment_confirmed' => [
                            'type' => Form::INPUT_DROPDOWN_LIST,
                            'label' => false,
                            'items' => [
                                0 => Yii::t('app', 'Pending'),
                                1 => Yii::t('app', 'Verified'),
                            ],
                            'options' => ['prompt' => $model->getAttributeLabel('payment_confirmed')],
                        ],
                        'attended' => [
                            'type' => Form::INPUT_DROPDOWN_LIST,
                            'label' => false,
                            'items' => [
                                0 => Yii::t('app', 'No'),
                                1 => Yii::t('app', 'Yes'),
                            ],
                            'options' => ['prompt' => $model->getAttributeLabel('attended')],
                        ],
                    ],
                ]) ?>
                <div class="form-group mb-0">
                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Reset'), ['attendances', 'slug' => $event->slug], ['class' => 'btn btn-outline-dark']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
