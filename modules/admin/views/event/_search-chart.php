<?php

use yii\bootstrap4\Html;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $event app\models\Event */
?>
<div class="row justify-content-end">
    <div class="col-md-4">
        <?= Html::beginForm(
            ['chart', 'slug' => $event->slug],
            'get',
            ['id' => 'search-chart-form']
        ) ?>
            <?= Html::dropDownList(
                'type',
                $type,
                [
                    'pie' => Yii::t('app', 'Pie Chart'),
                    'bar' => Yii::t('app', 'Bar Chart'),
                ],
                ['class' => 'form-control']
            ) ?>
        <?= Html::endForm() ?>
    </div>
</div>
<?php
$js = "
jQuery('[name=\"type\"]').on('change', function () { setTimeout(function () { jQuery('#search-chart-form').submit(); }, 500) });
";
$this->registerJs($js, View::POS_READY);
