<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $event app\models\Event */
/* @var $model app\models\UploadCpdPointForm */

$this->title = Yii::t('app', 'Update CPD Point by CSV');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Html::encode($event->title), 'url' => ['attendances', 'slug' => $event->slug]];
$this->params['breadcrumbs'][] = $this->title;
$layoutTemplates = [
    'main1' => "{preview} \n" .
        "<div class=\"kv-upload-progress kv-hidden\"></div><div class=\"clearfix\"></div>\n" .
        "<div class=\"file-caption {class}\">\n" .
        "<span class=\"file-caption-icon\"></span>\n" .
        "<div class=\"input-group\">\n{caption}\n" .
        "<div class=\"input-group-btn input-group-append\">\n" .
        "{remove}\n" .
        "{cancel}\n" .
        "{browse}\n" .
        "</div>\n" .
        "</div>" .
        "</div>",
];
?>
<div class="card">
    <div class="card-header">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="card-body">
        <?php if ($updatedMembers !== null): ?>
        <div class="mb-4">
            <h4><?= Yii::t('app', 'Update Result:') ?></h4>
            <?= Html::textArea('updated_members', $updatedMembers, [
                'class' => 'form-control',
                'rows' => 10,
            ]) ?>
        </div>
        <?php endif; ?>
        <?php $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
            'enableClientValidation' => false,
        ]); ?>
            <?= Form::widget([
                'model' => $model,
                'form' => $form,
                'attributes' => [
                    'csv_file' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => 'kartik\widgets\FileInput',
                        'options' => [
                            'options' => [
                                'accept' => 'text/csv',
                                'multiple' => false,
                            ],
                            'pluginOptions' => [
                                'initialPreviewAsData' => true,
                                'initialPreviewFileType' => 'csv',
                            ],
                        ],
                    ],
                ],
            ]) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
