<?php

use yii\bootstrap4\Html;
use yii\helpers\StringHelper;

$this->title = Yii::t('app', '{title}', ['title' => Html::encode(StringHelper::truncate($model->title, 200))]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['attendances', 'slug' => $model->slug]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Add Attendee')];
?>
<div class="card">
    <div class="card-header">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="card-body">
        <?= $this->render('/partials/add-event-attendee', [
            'eventModel' => $model,
            'eventAttendanceModel' => $eventAttendanceModel,
            'eventAttendanceTypeListData' => $eventAttendanceTypeListData,
            'paymentMethodListData' => $paymentMethodListData,
            'sponsorListData' => $sponsorListData,
        ]) ?>
    </div>
</div>
