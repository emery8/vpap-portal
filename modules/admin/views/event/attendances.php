<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\View;
use kartik\grid\GridView;
use kartik\form\ActiveForm;
use kartik\builder\TabularForm;
use kartik\builder\Form;
use app\models\EventAttendance;

/* @var $this yii\web\View */
/* @var $model app\models\Event */
/* @var $searchModel app\modules\admin\models\EventAttendanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $form kartik\form\ActiveForm */
/* @var $eventAttendanceTypeListData array */
/* @var $paymentMethodListData array */

$this->title = Yii::t('app', '{title}', ['title' => Html::encode(StringHelper::truncate($model->title, 200))]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="card-body">
        <?= $this->render('_search-attendances', [
            'event' => $model,
            'model' => $searchModel,
            'eventAttendanceTypeListData' => $eventAttendanceTypeListData,
            'paymentMethodListData' => $paymentMethodListData,
        ]) ?>
        <div class="row">
            <div class="col-md-12 d-flex justify-content-end">
                <div>
                    <?= Html::a(
                        Yii::t('app', 'Add Attendee'),
                        ['add-attendee', 'slug' => $model->slug],
                        ['class' => 'btn btn-success btn-block']
                    ) ?>
                </div>
            </div>
        </div>
        <?php $form = ActiveForm::begin(['id' => 'event-attendances-form']); ?>
            <?= TabularForm::widget([
                'dataProvider' => $dataProvider,
                'form' => $form,
                'gridSettings' => [
                    'id' => 'event-attendances-grid',
                    'tableOptions' => ['class' => 'table-responsive-sm'],
                    'headerRowOptions' => ['class' => 'thead-light'],
                    'hover' => true,
                    'striped' => false,
                ],
                'actionColumn' => [
                    'width' => '120px',
                    'viewOptions' => ['style' => 'display:none'],
                    'updateOptions' => ['style' => 'display:none'],
                    'template' => "{identification}\n{delete}",
                    'buttons' => [
                        'identification' => function ($url, EventAttendance $model) {
                            $label = Yii::t('app', 'Print ID');
                            $url = ['identification', 'id' => $model->id];
                            return Html::a(
                                $label,
                                $url,
                                [
                                    'class' => 'btn btn-sm btn-primary mr-2',
                                    'title' => Yii::t('app', 'Identification Card'),
                                    'aria-label' => Yii::t('app', 'Identification Card'),
                                    'data-pjax' => 0,
                                    'target' => '_target',
                                ],
                            );
                        },
                    ],
                    'urlCreator' => function ($action, EventAttendance $eventAttendanceModel, $key, $index, $parent) use($model) {
                        switch ($action) {
                            case 'delete':
                                return Url::toRoute([
                                    'event-attendance/delete',
                                    'slug' => $model->slug,
                                    'id' => $eventAttendanceModel->id
                                ]);
                        }
                    },
                ],
                'attributes' => [
                    'id' => [
                        'type' => TabularForm::INPUT_HIDDEN, 
                        'columnOptions' => ['hidden' => true],
                    ], 
                    'complete_name' => ['type' => TabularForm::INPUT_STATIC],
                    'license_number' => ['type' => TabularForm::INPUT_STATIC],
                    'attendance_type' => [
                        'type' => TabularForm::INPUT_STATIC,
                        'staticValue' => function ($model, $key, $index, $widget) {
                            $formatter = Yii::$app->formatter;
                            if ($model->is_sponsored) {
                                return Yii::t('app', '{type} ({sponsor})', [
                                    'type' => $formatter->asEventAttendanceType($model->attendance_type),
                                    'sponsor' => $formatter->asSponsor($model->sponsor_id)
                                ]);
                            } else {
                                return $formatter->asEventAttendanceType($model->attendance_type);
                            }
                        },
                        // 'format' => 'eventattendancetype',
                    ],
                    'payment_method' => [
                        'type' => TabularForm::INPUT_STATIC,
                        'format' => 'paymentmethod',
                    ],
                    'amount_paid' => [
                        'type' => TabularForm::INPUT_STATIC,
                        'format' => 'currency',
                        'columnOptions' => ['hAlign' => 'right'],
                    ],
                    'payment_confirmed' => [
                        'type' => TabularForm::INPUT_WIDGET,
                        'widgetClass' => 'kartik\widgets\SwitchInput',
                        'columnOptions' => ['hAlign' => 'center'],
                        'options' => [
                            'pluginOptions' => [
                                'handleWidth' => 20,
                                'onColor' => 'primary',
                                'offColor' => 'warning',
                                'onText' => Html::tag('i', '', ['class' => 'fas fa-thumbs-up']),
                                'offText' => Html::tag('i', '', ['class' => 'fas fa-thumbs-down']),
                            ],
                            'pluginEvents' => [
                                'switchChange.bootstrapSwitch' => 'submittogglepaymentconfirmed',
                            ],
                        ],
                        /*'type' => TabularForm::INPUT_DROPDOWN_LIST,
                        'items' => [
                            0 => Yii::t('app', 'No'),
                            1 => Yii::t('app', 'Yes'),
                        ],
                        'options' => ['class' => 'dropdown_payment_confirmed'],*/
                    ],
                    'attended' => [
                        'type' => TabularForm::INPUT_WIDGET,
                        'widgetClass' => 'kartik\widgets\SwitchInput',
                        'columnOptions' => ['hAlign' => 'center'],
                        'options' => [
                            'pluginOptions' => [
                                'handleWidth' => 20,
                                'onColor' => 'success',
                                'offColor' => 'danger',
                                'onText' => Html::tag('i', '', ['class' => 'fas fa-check']),
                                'offText' => Html::tag('i', '', ['class' => 'fas fa-times']),
                            ],
                            'pluginEvents' => [
                                'switchChange.bootstrapSwitch' => 'submittoggleattended',
                            ],
                        ],
                        /*'type' => TabularForm::INPUT_DROPDOWN_LIST,
                        'items' => [
                            0 => Yii::t('app', 'No'),
                            1 => Yii::t('app', 'Yes'),
                        ],
                        'options' => ['class' => 'dropdown_attended'],*/
                    ],
                    'cpd_point_earned' => [
                        'type' => TabularForm::INPUT_TEXT,
                        'options' => ['class' => 'input-tabindex'],
                        'columnOptions' => ['width' => '100px'],
                    ],
                ],
            ]) ?>
            <div class="card">
                <div class="card-body bg-light">
                    <div class="form-group row justify-content-between align-items-center mb-0">
                        <div class="col-md-6">
                            <?= Html::tag('p', Yii::t('app', 'With{count} selected:', ['count' => Html::tag('span', '', ['id' => 'selectedRowsCount'])])) ?>
                            <?= Form::widget([
                                'formName'=>'attendances-form',
                                'columns' => 2,
                                'compactGrid' => true,
                                'attributes' => [
                                    'payment_confirmed' => [
                                        'type' => Form::INPUT_DROPDOWN_LIST,
                                        'items' => [
                                            'Pending' => Yii::t('app', 'Pending'),
                                            'Verified' => Yii::t('app', 'Verified'),
                                        ],
                                        'options' => ['prompt' => Yii::t('app', 'Set Payment Confirmation')],
                                    ],
                                    'attended' => [
                                        'type' => Form::INPUT_DROPDOWN_LIST,
                                        'items' => [
                                            'No' => Yii::t('app', 'No'),
                                            'Yes' => Yii::t('app', 'Yes'),
                                        ],
                                        'options' => ['prompt' => Yii::t('app', 'Set Attended')],
                                    ],
                                ],
                            ]) ?>
                        </div>
                        <div class="col-md-6 d-flex justify-content-end">
                            <div>
                                <?= Html::hiddenInput('payment_confirmed_id', null) ?>
                                <?= Html::hiddenInput('attended_id', null) ?>
                                <?= Html::a(Yii::t('app', 'Upload CSV'), ['upload-csv', 'slug' => $model->slug], ['class' => 'btn btn-warning px-4']) ?>
                                <?= Html::submitButton(Yii::t('app', 'Download CSV'), [
                                    'name' => 'batch_action',
                                    'value' => 'download-csv',
                                    'class' => 'btn btn-success px-4',
                                ]) ?>
                                <?= Html::submitButton(Yii::t('app', 'Batch Delete'), [
                                    'name' => 'batch_action',
                                    'value' => 'delete',
                                    'class' => 'btn btn-danger px-4',
                                    'data' => ['confirm' => Yii::t('app', 'Are you sure to delete selected item/s?')],
                                ]) ?>
                                <?= Html::submitButton(Yii::t('app', 'Batch Update'), [
                                    'name' => 'batch_action',
                                    'value' => 'update',
                                    'class' => 'btn btn-primary px-4',
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php
$js = "
jQuery('.input-tabindex').each(function () { var index = jQuery(this).closest('[data-col-seq]').attr('data-col-seq'); jQuery(this).attr('tabindex', index); });
jQuery('.kv-row-checkbox, .select-on-check-all').on('change', function () { var keys = jQuery('#event-attendances-grid').yiiGridView('getSelectedRows').length; jQuery('#selectedRowsCount').html(' ' + keys); });
jQuery('#event-attendances-form').on('keypress', function (event) { var keyPressed = event.keyCode || event.which; if (keyPressed === 13) { event.preventDefault(); } });
jQuery('.dropdown_payment_confirmed').on('change', function () { var payment_confirmed_id = jQuery(this).closest('tr').data('key'); jQuery('[name=\"payment_confirmed_id\"]').val(payment_confirmed_id); jQuery('[name=\"batch_action\"]').val('togglepaymentconfirmed'); setTimeout(function() { jQuery('#event-attendances-form').submit(); }, 500); } );
jQuery('.dropdown_attended').on('change', function () { var attended_id = jQuery(this).closest('tr').data('key'); jQuery('[name=\"attended_id\"]').val(attended_id); jQuery('[name=\"batch_action\"]').val('toggleattended'); setTimeout(function() { jQuery('#event-attendances-form').submit(); }, 500); });

function submittogglepaymentconfirmed() { var payment_confirmed_id = jQuery(this).closest('tr').data('key'); jQuery('[name=\"payment_confirmed_id\"]').val(payment_confirmed_id); jQuery('[name=\"batch_action\"]').val('togglepaymentconfirmed'); setTimeout(function() { jQuery('#event-attendances-form').submit(); }, 500); }
function submittoggleattended() { var attended_id = jQuery(this).closest('tr').data('key'); jQuery('[name=\"attended_id\"]').val(attended_id); jQuery('[name=\"batch_action\"]').val('toggleattended'); setTimeout(function() { jQuery('#event-attendances-form').submit(); }, 500); }
";
$this->registerJs($js, View::POS_READY);
