<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use dosamigos\highcharts\HighCharts;

/* @var $this yii\web\View */
/* @var $model app\models\Event */

$this->title = "Pie Chart &raquo; {$model->title}";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="card">
    <div class="card-header">
        <?= Yii::t('app', 'Event Summary &raquo; {title}', ['title' => Html::encode($model->title)]) ?>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <?= $this->render('_search-chart', [
                    'event' => $model,
                    'type' => $type,
                ]) ?>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-6">
                <?= HighCharts::widget(['clientOptions' => $eventAgeChartOptions]) ?>
            </div>
            <div class="col-md-6">
                <?= HighCharts::widget(['clientOptions' => $countryChartOptions]) ?>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-12">
                <?= HighCharts::widget(['clientOptions' => $veterinarySchoolGraduatedChartOptions]) ?>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-6">
                <?= HighCharts::widget(['clientOptions' => $regionHomeChartOptions]) ?>
            </div>
            <div class="col-md-6">
                <?= HighCharts::widget(['clientOptions' => $regionOfficeChartOptions]) ?>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-12">
                <?= HighCharts::widget(['clientOptions' => $yearGraduatedChartOptions]) ?>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-6">
                <?= HighCharts::widget(['clientOptions' => $typeOfPracticeChartOptions]) ?>
            </div>
            <div class="col-md-6">
                <?= HighCharts::widget(['clientOptions' => $speciesOfSpecializationChartOptions]) ?>
            </div>
        </div>
    </div>
</div>
