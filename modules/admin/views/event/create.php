<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\models\Event */
/* @var $featuredImageModel app\models\FeaturedImage */
/* @var $identificationTemplateModel app\models\IdentificationTemplate */
/* @var $programTemplateModel app\models\ProgramTemplate */
/* @var $invitationTemplateModel app\models\IvitationTemplate */
/* @var $attendanceCertificateTemplateModel app\models\AttendanceCertificateTemplate */
/* @var $cpdCertificateTemplateModel app\models\CpdCertificateTemplate */
/* @var $eventTypeListData[] array */
/* @var $eventAttendanceTypeListData[] array */

$this->title = Yii::t('app', 'Create Event');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$layoutTemplates = [
    'main1' => "{preview} \n" .
        "<div class=\"kv-upload-progress kv-hidden\"></div><div class=\"clearfix\"></div>\n" .
        "<div class=\"file-caption {class}\">\n" .
        "<span class=\"file-caption-icon\"></span>\n" .
        "<div class=\"input-group\">\n{caption}\n" .
        "<div class=\"input-group-btn input-group-append\">\n" .
        "{remove}\n" .
        "{cancel}\n" .
        "{browse}\n" .
        "</div>\n" .
        "</div>" .
        "</div>",
];
?>
<div class="card">
    <div class="card-header">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="card-body">
        <div class="event-form">

            <?php $form = ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data'],
                'enableClientValidation' => false,
            ]); ?>
                <div class="card">
                    <div class="card-body">
                        <?= Form::widget([
                            'model' => $featuredImageModel,
                            'form' => $form,
                            'attributes' => [
                                'content' => [
                                    'type' => Form::INPUT_WIDGET,
                                    'widgetClass' => 'kartik\widgets\FileInput',
                                    'hint' => Yii::t('app', '{icon} The recommended size of featuerd image is {image_size}.', [
                                        'icon' => Html::tag(
                                            'svg',
                                            Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-lightbulb')]),
                                            ['class' => 'c-icon']
                                        ),
                                        'image_size' => Html::tag('code', '1920 (width) x 500 (height)'),
                                    ]),
                                    'options' => [
                                        'options' => [
                                            'accept' => implode(',', $featuredImageModel->allowedMimeType),
                                            'multiple' => false,
                                        ],
                                        'pluginOptions' => [
                                            'layoutTemplates' => $layoutTemplates,
                                        ],
                                    ],
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><?= Yii::t('app', 'Event Details') ?></h4>
                        <?= Form::widget([
                            'model' => $model,
                            'form' => $form,
                            'attributes' => [
                                'title' => [
                                    'type' => Form::INPUT_TEXTAREA,
                                    'options' => [
                                        'placeholder' => $model->getAttributeLabel('title'),
                                        'maxlength' => true,
                                    ],
                                ],
                            ],
                        ]) ?>
                        <?= Form::widget([
                            'model' => $model,
                            'form' => $form,
                            'columns' => 2,
                            'attributes' => [
                                'type' => [
                                    'type' => Form::INPUT_WIDGET,
                                    'widgetClass' => 'kartik\select2\Select2',
                                    'options' => [
                                        'data' => $eventTypeListData,
                                        'options' => ['placeholder' => $model->getAttributeLabel('type')]
                                    ],
                                ],
                                'location' => [
                                    'type' => Form::INPUT_TEXT,
                                    'options' => [
                                        'placeholder' => $model->getAttributeLabel('location'),
                                        'maxlength' => true,
                                    ],
                                ],
                            ],
                        ]) ?>
                        <?= Form::widget([
                            'model' => $model,
                            'form' => $form,
                            'columns' => 2,
                            'attributes' => [
                                'start_date' => [
                                    'type' => Form::INPUT_WIDGET,
                                    'widgetClass' => 'kartik\date\DatePicker',
                                    'options' => [
                                        'options' => ['placeholder' => $model->getAttributeLabel('start_date')],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'yyyy-mm-dd',
                                            'todayHighlight' => true,
                                            'orientation' => 'bottom',
                                        ],
                                    ],
                                ],
                                'end_date' => [
                                    'type' => Form::INPUT_WIDGET,
                                    'widgetClass' => 'kartik\date\DatePicker',
                                    'options' => [
                                        'options' => ['placeholder' => $model->getAttributeLabel('end_date')],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'yyyy-mm-dd',
                                            'todayHighlight' => true,
                                            'orientation' => 'bottom',
                                        ],
                                    ],
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><?= Yii::t('app', 'Registration Fees') ?></h4>
                        <?= Form::widget([
                            'model' => $model,
                            'form' => $form,
                            'columns' => 4,
                            'attributes' => [
                                'enable_discount' => [
                                    'type' => Form::INPUT_WIDGET,
                                    'widgetClass' => 'kartik\widgets\SwitchInput',
                                    'options' => [
                                        'pluginOptions' => [
                                            'onText' => Yii::t('app', 'Enable'),
                                            'offText' => Yii::t('app', 'Disable'),
                                            'onColor' => 'success',
                                            'offColor' => 'danger',
                                        ],
                                    ],
                                    'columnOptions' => ['colspan' => 1],
                                ],
                                'membership_fee' => [
                                    'type' => Form::INPUT_TEXT,
                                    'options' => [
                                        'placeholder' => $model->getAttributeLabel('membership_fee'),
                                        'maxlength' => true,
                                    ],
                                    'columnOptions' => ['colspan' => 3],
                                    'fieldConfig' => [
                                        'addon' => [
                                            'prepend' => [
                                                'content' => Yii::$app->formatter->currencyCode,
                                            ]
                                        ]
                                    ],
                                ],
                            ],
                        ]) ?>
                        <?= Form::widget([
                            'model' => $model,
                            'form' => $form,
                            'columns' => 3,
                            'attributes' => [
                                'lifetime_member_fee' => [
                                    'type' => Form::INPUT_TEXT,
                                    'options' => [
                                        'placeholder' => $model->getAttributeLabel('lifetime_member_fee'),
                                        'maxlength' => true,
                                    ],
                                    'fieldConfig' => [
                                        'addon' => [
                                            'prepend' => [
                                                'content' => Yii::$app->formatter->currencyCode,
                                            ]
                                        ]
                                    ],
                                ],
                                'regular_member_fee' => [
                                    'type' => Form::INPUT_TEXT,
                                    'options' => [
                                        'placeholder' => $model->getAttributeLabel('regular_member_fee'),
                                        'maxlength' => true,
                                    ],
                                    'fieldConfig' => [
                                        'addon' => [
                                            'prepend' => [
                                                'content' => Yii::$app->formatter->currencyCode,
                                            ]
                                        ]
                                    ],
                                ],
                                'non_member_fee' => [
                                    'type' => Form::INPUT_TEXT,
                                    'options' => [
                                        'placeholder' => $model->getAttributeLabel('non_member_fee'),
                                        'maxlength' => true,
                                    ],
                                    'fieldConfig' => [
                                        'addon' => [
                                            'prepend' => [
                                                'content' => Yii::$app->formatter->currencyCode,
                                            ]
                                        ]
                                    ],
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><?= Yii::t('app', 'Participants') ?></h4>
                        <?= Form::widget([
                            'model' => $model,
                            'form' => $form,
                            'columns' => 3,
                            'attributes' => [
                                'attendance_type' => [
                                    'type' => Form::INPUT_WIDGET,
                                    'widgetClass' => 'kartik\select2\Select2',
                                    'options' => [
                                        'data' => $eventAttendanceTypeListData,
                                        'options' => ['placeholder' => $model->getAttributeLabel('attendance_type')],
                                    ],
                                ],
                                'cpd_point' => [
                                    'type' => Form::INPUT_TEXT,
                                    'options' => [
                                        'placeholder' => $model->getAttributeLabel('cpd_point'),
                                        'maxlength' => true,
                                    ],
                                ],
                                'maximum_participant' => [
                                    'type' => Form::INPUT_TEXT,
                                    'options' => [
                                        'placeholder' => $model->getAttributeLabel('maximum_participant'),
                                        'maxlength' => true,
                                    ],
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"><?= Yii::t('app', 'Attachments') ?></h4>
                        <div class="row">
                            <div class="col-md-4">
                                <?= Form::widget([
                                    'model' => $programTemplateModel,
                                    'form' => $form,
                                    'attributes' => [
                                        'content' => [
                                            'type' => Form::INPUT_WIDGET,
                                            'widgetClass' => 'kartik\widgets\FileInput',
                                            'options' => [
                                                'options' => [
                                                    'accept' => implode(',', $programTemplateModel->allowedMimeType),
                                                    'multiple' => false,
                                                ],
                                                'pluginOptions' => [
                                                    'layoutTemplates' => $layoutTemplates,
                                                ],
                                            ],
                                        ],
                                    ],
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <?= Form::widget([
                                    'model' => $attendanceCertificateTemplateModel,
                                    'form' => $form,
                                    'attributes' => [
                                        'content' => [
                                            'type' => Form::INPUT_WIDGET,
                                            'widgetClass' => 'kartik\widgets\FileInput',
                                            'options' => [
                                                'options' => [
                                                    'accept' => implode(',', $attendanceCertificateTemplateModel->allowedMimeType),
                                                    'multiple' => false,
                                                ],
                                                'pluginOptions' => [
                                                    'layoutTemplates' => $layoutTemplates,
                                                ],
                                            ],
                                        ],
                                    ],
                                ]) ?>
                            </div>
                            <div class="col-md-4">
                                <?= Form::widget([
                                    'model' => $cpdCertificateTemplateModel,
                                    'form' => $form,
                                    'attributes' => [
                                        'content' => [
                                            'type' => Form::INPUT_WIDGET,
                                            'widgetClass' => 'kartik\widgets\FileInput',
                                            'options' => [
                                                'options' => [
                                                    'accept' => implode(',', $cpdCertificateTemplateModel->allowedMimeType),
                                                    'multiple' => false,
                                                ],
                                                'pluginOptions' => [
                                                    'layoutTemplates' => $layoutTemplates,
                                                ],
                                            ],
                                        ],
                                    ],
                                ]) ?>
                            </div>
                        </div>
                        <?= Form::widget([
                            'model' => $model,
                            'form' => $form,
                            'columns' => 2,
                            'attributes' => [
                                'evaluation_link' => [
                                    'type' => Form::INPUT_TEXT,
                                    'options' => [
                                        'placeholder' => $model->getAttributeLabel('evaluation_link'),
                                        'maxlength' => true,
                                    ],
                                ],
                                'invitation_link' => [
                                    'type' => Form::INPUT_TEXT,
                                    'options' => [
                                        'placeholder' => $model->getAttributeLabel('invitation_link'),
                                        'maxlength' => true,
                                    ],
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
