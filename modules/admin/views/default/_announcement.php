<?php

use yii\bootstrap4\Html;

/* @var $model app\models\Announcement */
?>
<div class="card">
    <div class="card-body">
        <h4 class="mb-0"><?= Html::encode($model->title) ?></h4>
        <p class="text-muted"><?= Yii::t(
            'app',
            'by {author} - {date}',
            [
                'author' => Yii::$app->formatter->asAuthor($model->created_by),
                'date' => Yii::$app->formatter->asDateTime($model->created_at),
            ]
        ) ?></p>
        <div class="mt-4">
            <?= Html::decode($model->content) ?>
        </div>
        <?php if (Yii::$app->user->can('admin')): ?>
            <div class="d-flex justify-content-end">
                <div>
                    <?= Html::a(
                        Yii::t('app', 'Edit'),
                        ['announcement/update', 'id' => $model->id],
                        ['class' => 'btn btn-outline-primary btn-sm px-4']
                    ) ?>
                    <?= Html::a(
                        Yii::t('app', 'Delete'),
                        ['announcement/delete', 'id' => $model->id],
                        [
                            'class' => 'btn btn-outline-danger btn-sm px-4',
                            'data' => [
                                'method' => 'post',
                                'confirm' => Yii::t('app', 'Are you sure to delete this announcement?'),
                            ],
                        ]
                    ) ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>