<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use kartik\helpers\Enum;

/* @var $this yii\web\View */
/* @var $model app\models\EventAttendance */
/* @var $event app\models\Event */
/* @var $featuredImage app\models\FeaturedImage */
/* @var $programTemplate app\models\ProgramTemplate */
/* @var $invitationTemplate app\models\InvitationTemplate */
/* @var $attendanceCertificateTemplate app\models\AttendanceCertificateTemplate */
?>
<div class="row no-gutters bg-info text-white mb-4">
    <?php if ($featuredImage): ?>
    <div class="col-md-4 col-sm-12">
        <?= Html::a(
            Html::img(
                $featuredImage->getThumbUploadUrl('content', 'event_thumb'),
                ['alt' => 'featured_img-thumb', 'class' => 'img-fluid']
            ),
            $featuredImage->getUploadUrl('content'),
            ['data' => ['fancybox' => true]]
        ) ?>
    </div>
    <div class="col-md-8 col-sm-12 d-flex align-items-center p-4">
        <div>
            <?= Html::tag('h2', Html::encode($event->title), ['class' => 'mb-0']) ?>
            <?= Html::tag('p', Yii::t('app', 'EVENT DETAILS'), ['class' => 'lead mb-0']) ?>
        </div>
    </div>
    <?php else: ?>
    <div class="col-md-12 d-flex align-items-center p-4">
        <div>
            <?= Html::tag('h2', Html::encode($event->title), ['class' => 'mb-0']) ?>
            <?= Html::tag('p', Yii::t('app', 'EVENT DETAILS'), ['class' => 'lead mb-0']) ?>
        </div>
    </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col">
        <?= DetailView::widget([
            'model' => $model,
            'options' => ['class' => 'table table-bordered detail-view'],
            'template' => '<tr><th{captionOptions} style="width: 40%">{label}</th><td{contentOptions}>{value}</td></tr>',
            'attributes' => [
                [
                    'attribute' => 'member_type',
                    'format' => 'membershiptype',
                    'visible' => ! is_null($model->member_type),
                ],
                'attendance_type:eventattendancetype',
                [
                    'attribute' => 'payment_method',
                    'format' => 'paymentmethod',
                    'visible' => ! is_null($model->payment_method),
                ],
                [
                    'attribute' => 'sponsor_id',
                    'format' => 'sponsor',
                    'visible' => ! is_null($model->sponsor_id),
                ],
                [
                    'attribute' => 'discount_type',
                    'format' => 'memberdiscounttype',
                    'visible' => ! is_null($model->discount_type),
                ],
                [
                    'attribute' => 'amount_paid',
                    'label' => Yii::t('app', 'Amount Paid'),
                    'value' => function ($model, $widget) {
                        if ($model->has_checkout_item) {
                            return $model->getAmountSummary();
                        }
                        return Yii::$app->formatter->asCurrency($model->amount_paid);
                    },
                    'format' => 'html',
                ],
                'created_at:datetime:' . Yii::t('app', 'Date Registered'),
            ],
        ]) ?>
    </div>
    <div class="col">
        <p class="mb-0">
            <strong><?= Yii::t('app', 'Program Template') ?></strong>
        </p>
        <?php if ($programTemplate): ?>
            <p>
                <?= Html::a(
                    Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-cloud-download')]),
                        ['class' => 'c-icon mr-1']
                    ),
                    $programTemplate->getUploadUrl('content'),
                ) ?>
                <?= Enum::formatBytes($programTemplate->getSize('content')) ?>
            </p>
        <?php else: ?>
            <?= Yii::$app->formatter->nullDisplay ?>
        <?php endif; ?>
        <p class="mb-0">
            <strong><?= Yii::t('app', 'Invitation Template') ?></strong>
        </p>
        <?php if ($invitationTemplate): ?>
            <p>
                <?= Html::a(
                    Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-cloud-download')]),
                        ['class' => 'c-icon mr-1']
                    ),
                    $invitationTemplate->getUploadUrl('content'),
                ) ?>
                <?= Enum::formatBytes($invitationTemplate->getSize('content')) ?>
            </p>
        <?php else: ?>
            <?= Yii::$app->formatter->nullDisplay ?>
        <?php endif; ?>
        <p class="mb-0">
            <strong><?= Yii::t('app', 'Certificate') ?></strong>
        </p>
        <?php if ($attendanceCertificateTemplate): ?>
            <p>
                <?= Html::a(
                    Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-cloud-download')]),
                        ['class' => 'c-icon mr-1']
                    ),
                    ['certificate', 'slug' => $event->slug],
                ) ?>
                <?= Enum::formatBytes($attendanceCertificateTemplate->getSize('content')) ?>
            </p>
        <?php else: ?>
            <?= Yii::$app->formatter->nullDisplay ?>
        <?php endif; ?>
    </div>
</div>