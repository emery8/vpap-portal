<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use kartik\form\ActiveForm;
use kartik\dialog\Dialog;

/* @var $this yii\web\View */
/* @var $model app\models\EventAttendance */
/* @var $event app\models\Event */
/* @var $membership app\models\UserMembership */

$this->title = $event->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Attendances'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
Dialog::widget(['libName' => 'krajeeDialog']);
?>
<div class="card">
    <div class="card-header">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="card-body">
        <h4 class="card-title"><?= Yii::t('app', 'View Order') ?></h4>
        <div class="row justify-content-start">
            <?php if ($model->has_checkout_item): ?>
            <div class="col-md-6 col-sm-12">
                <?= DetailView::widget([
                    'model' => $model,
                    'options' => ['class' => 'table table-bordered detail-view'],
                    'template' => '<tr><th{captionOptions} style="width: 40%">{label}</th><td{contentOptions}>{value}</td></tr>',
                    'attributes' => [
                        [
                            'attribute' => 'attendance_type',
                            'format' => 'eventattendancetype',
                        ],
                        [
                            'attribute' => 'member_type',
                            'format' => 'membershiptype',
                        ],
                        [
                            'attribute' => 'payment_method',
                            'format' => 'paymentmethod',
                        ],
                        [
                            'attribute' => 'discount_type',
                            'format' => 'memberdiscounttype',
                            'visible' => $event->is_discount_enabled && $model->is_personal_account,
                        ],
                        [
                            'attribute' => 'amount_paid',
                            'label' => Yii::t('app', 'Amount To Pay'),
                            'value' => function ($model, $widget) use ($event, $membership) {
                                return $model->getAmountSummary();
                            },
                            'format' => 'html',
                        ],
                        [
                            'attribute' => 'sponsor_id',
                            'label' => Yii::t('app', 'Sponsored By'),
                            'format' => 'sponsor',
                            'visible' => ! is_null($model->sponsor_id),
                        ],
                        'created_at:datetime:' . Yii::t('app', 'Date Created'),
                    ],
                ]) ?>
            </div>
            <div class="col-md-6 col-sm-12">
                <p class="text-muted">
                    <?= Yii::t('app', '{icon} By clicking the button below, you will be automatically redirected to the Dragon Pay website. Please be informed that your {attr1}, {attr2} and {attr3} will be sent together with the other payment details for the order to proceed.', [
                        'icon' => Html::tag(
                            'svg',
                            Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-lightbulb')]),
                            ['class' => 'c-icon']
                        ),
                        'attr1' => Html::tag('code', 'personal name'),
                        'attr2' => Html::tag('code', 'email address'),
                        'attr3' => Html::tag('code', 'professional license number'),
                    ]) ?>
                </p>
                <?php $form = ActiveForm::begin([
                    'action' => ['checkout', 'slug' => $event->slug],
                ]); ?>
                    <?= Html::button(
                        Yii::t('app', 'Proceed to Dragon Pay'),
                        [
                            'class' => 'btn btn-danger btn-pill',
                            'data' => [
                                'method' => 'post',
                                'confirm' => Yii::t('app', 'Are you sure you want to proceed to the Dragon Pay website?'),
                            ],
                        ]
                    ) ?>
                    <?= Html::a(Yii::t('app', 'Change Order'), ['update', 'slug' => $event->slug], ['class' => 'btn btn-link']) ?>
                <?php ActiveForm::end(); ?>
            </div>
            <?php else: ?>
                <div class="col-md-6">
                    <?= DetailView::widget([
                        'model' => $model,
                        'options' => ['class' => 'table table-bordered detail-view'],
                        'template' => '<tr><th{captionOptions} style="width: 40%">{label}</th><td{contentOptions}>{value}</td></tr>',
                        'attributes' => [
                            [
                                'attribute' => 'attendance_type',
                                'format' => 'eventattendancetype',
                            ],
                            [
                                'attribute' => 'member_type',
                                'format' => 'membershiptype',
                            ],
                            [
                                'attribute' => 'payment_method',
                                'format' => 'paymentmethod',
                            ],
                            [
                                'attribute' => 'discount_type',
                                'format' => 'memberdiscounttype',
                                'visible' => $event->is_discount_enabled && $model->is_personal_account,
                            ],
                            [
                                'attribute' => 'amount_paid',
                                'label' => Yii::t('app', 'Amount To Pay'),
                                'value' => function ($model, $widget) use ($event, $membership) {
                                    return $model->getAmountSummary();
                                },
                                'format' => 'html',
                            ],
                            [
                                'attribute' => 'sponsor_id',
                                'label' => Yii::t('app', 'Sponsored By'),
                                'format' => 'sponsor',
                                'visible' => ! is_null($model->sponsor_id),
                            ],
                            'created_at:datetime:' . Yii::t('app', 'Date Created'),
                        ],
                    ]) ?>
                    <?= Html::a(Yii::t('app', 'Change Order'), ['update', 'slug' => $event->slug], ['class' => 'btn btn-link']) ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
