<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use kartik\helpers\Enum;
use app\models\Event;
use newerton\fancybox3\FancyBox;

/* @var $model app\models\Event */
/* @var $eventAttendanceModel app\models\EventAttendance */
/* @var $eventAttendanceTypeListData app\models\enums\EventAttendanceType */
/* @var $paymentMethodListData app\models\enums\PaymentMethod */
/* @var $sponsorListData app\models\Sponsor */

$featured_image = null;
$featured_image_thumb = null;
$program_template = null;
$invitation_template = null;
$certificate_template = null;
$invitation_link = $model->invitation_link;

if (is_array($model->attachments) && count($model->attachments)) {
    $attachments = $model->attachments;
    foreach ($attachments as $attachment) {
        if ($attachment instanceof \app\models\FeaturedImage) {
            $featured_image = $attachment->getUploadUrl('content');
            $featured_image_thumb = $attachment->getThumbUploadUrl('content', 'event_thumb');
        } else if ($attachment instanceof \app\models\ProgramTemplate) {
            $program_template = $attachment;
        } else if ($attachment instanceof \app\models\InvitationTemplate) {
            $invitation_template = $attachment;
        } else if ($attachment instanceof \app\models\CertificateTemplate) {
            $certificate_template = $attachment;
        }
    }
}
echo FancyBox::widget();
?>
<div class="card">
    <div class="card-body">
        <div class="row no-gutters bg-light mb-4">
            <?php if ($featured_image && $featured_image_thumb): ?>
            <div class="col-md-4 col-sm-12">
                <?= Html::a(
                    Html::img(
                        $featured_image_thumb,
                        ['alt' => 'featured_img-thumb', 'class' => 'img-fluid']
                    ),
                    $featured_image,
                    ['data' => ['fancybox' => true]]
                ) ?>
            </div>
            <div class="col-md-8 col-sm-12 d-flex align-items-center p-4">
                <div>
                    <?= Html::tag('h2', Html::encode($model->title), ['class' => 'mb-0']) ?>
                    <?= Html::tag('h5', Yii::t('app', '{icon} {content}', [
                        'icon' => Html::tag(
                            'svg',
                            Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-calendar')]),
                            ['class' => 'c-icon mr-1']
                        ),
                        'content' => Html::encode($model->event_schedule),
                    ]), ['class' => 'text-dark mb-0']) ?>
                </div>
            </div>
            <?php else: ?>
            <div class="col-md-12 col-sm-12 d-flex align-items-center p-4">
                <div>
                    <?= Html::tag('h2', Html::encode($model->title), ['class' => 'mb-0']) ?>
                    <?= Html::tag('h5', Yii::t('app', '{icon} {content}', [
                        'icon' => Html::tag(
                            'svg',
                            Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-calendar')]),
                            ['class' => 'c-icon mr-1']
                        ),
                        'content' => Html::encode($model->event_schedule),
                    ]), ['class' => 'text-dark']) ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <div class="row no-gutters justify-content-start">
            <div class="col-md-9 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <?= Html::tag('h4', Yii::t('app', 'Event Details'), ['class' => 'mb-4 card-title']) ?>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <dl class="row">
                                    <dt class="col-md-6"><?= $model->getAttributeLabel('location') ?></dt>
                                    <dd class="col-md-6 mb-0"><?= Html::encode($model->location) ?></dd>
                                    <dt class="col-md-6"><?= $model->getAttributeLabel('cpd_point') ?></dt>
                                    <dd class="col-md-6 mb-0"><?= Html::encode($model->cpd_point) ?></dd>
                                    <dt class="col-md-6"><?= $model->getAttributeLabel('maximum_participant') ?></dt>
                                    <dd class="col-md-6 mb-0"><?= Html::tag('span', Html::encode($model->maximum_participant), ['class' => 'badge badge-danger']) ?></dd>
                                </dl>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <dl class="row">
                                    <dt class="col-md-6"><?= $model->getAttributeLabel('lifetime_member_fee') ?></dt>
                                    <dd class="col-md-6 mb-0"><?= Html::encode(Yii::$app->formatter->asCurrency($model->lifetime_member_fee)) ?></dd>
                                    <dt class="col-md-6"><?= $model->getAttributeLabel('regular_member_fee') ?></dt>
                                    <dd class="col-md-6 mb-0"><?= Html::encode(Yii::$app->formatter->asCurrency($model->regular_member_fee + $model->membership_fee)) ?></dd>
                                    <dt class="col-md-6"><?= $model->getAttributeLabel('non_member_fee') ?></dt>
                                    <dd class="col-md-6 mb-0"><?= Html::encode(Yii::$app->formatter->asCurrency($model->non_member_fee)) ?></dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 pl-sm-4">
                <?php if (Event::canAttendEvent($model->attendance_type)): ?>
                <div class="mb-4">
                    <?php Modal::begin([
                        'title' => Yii::t('app', 'Register | {event_title}', ['event_title' => StringHelper::truncate($model->title, 40)]),
                        'toggleButton' => ['label' => Yii::t('app', 'REGISTER NOW'), 'class' => 'btn btn-success btn-block'],
                        'size' => Modal::SIZE_LARGE,
                    ]); ?>
                        <?= $this->render('_form', [
                            'event' => $model,
                            'model' => $eventAttendanceModel,
                            'eventAttendanceTypeListData' => $eventAttendanceTypeListData,
                            'paymentMethodListData' => $paymentMethodListData,
                            'sponsorListData' => $sponsorListData,
                        ]) ?>
                    <?php Modal::end(); ?>
                </div>
                <?php endif; ?>
                <?php if ($program_template || $invitation_link): ?>
                <div class="card mb-2">
                    <div class="card-body bg-light">
                        <h6><?= Yii::t('app', 'ATTACHMENTS:') ?></h6>
                        <?php if ($invitation_link): ?>
                            <?= Html::a(Yii::t('app', 'Download Invitation'), $invitation_link, ['target' => '_blank']) ?>
                            <?= Html::tag('p', Yii::t('app', 'External Link')) ?>
                        <?php endif; ?>
                        <?php if ($program_template): ?>
                            <?= Html::a(Yii::t('app', 'Download Program'), $program_template->getUploadUrl('content')) ?>
                            <?= Html::tag('p', Html::encode(Enum::formatBytes($program_template->getSize('content')))) ?>
                        <?php endif; ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
