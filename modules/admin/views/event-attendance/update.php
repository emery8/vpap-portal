<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $event app\models\Event */
/* @var $model app\models\EventAttendance */
/* @var $eventAttendanceTypeListData app\models\enums\EventAttendanceType */
/* @var $paymentMethodListData app\models\enums\PaymentMethod */
/* @var $sponsorListData app\models\Sponsor */

$this->title = Yii::t('app', 'Update Event Attendance: {name}', [
    'name' => $event->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Event Attendances'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $event->title, 'url' => ['view', 'slug' => $event->slug]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="card">
    <div class="card-header">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="card-body">
        <?= $this->render('_form', [
            'event' => $event,
            'model' => $model,
            'eventAttendanceTypeListData' => $eventAttendanceTypeListData,
            'paymentMethodListData' => $paymentMethodListData,
            'sponsorListData' => $sponsorListData,
        ]) ?>
    </div>
</div>
