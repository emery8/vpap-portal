<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SpeciesOfSpecialization */

$this->title = Yii::t('app', 'Create Species Of Specialization');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings'), 'url' => ['setting/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="card-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
