<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model app\models\VeterinaryInstitution */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="veterinary-school-form">

    <?php $form = ActiveForm::begin(); ?>
        <?= Form::widget([
            'model' => $model,
            'form' => $form,
            'attributes' => [
                'institution' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => [
                        'autofocus' => true,
                        'maxlength' => true,
                        'placeholder' => $model->getAttributeLabel('institution'),
                    ],
                ],
            ],
        ]) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>
