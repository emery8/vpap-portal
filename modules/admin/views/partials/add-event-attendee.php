<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\View;
use yii\web\JsExpression;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use app\models\enums\EventAttendanceType;
use app\models\enums\PaymentMethod;

/* @var $eventModel app\models\Event */
/* @var $eventAttendanceModel app\models\EventAttendance */
/* @var $eventAttendanceTypeListData array */
/* @var $paymentMethodListData array */
/* @var $sponsorListData array */

$attendance_type_wrapper_id = Html::getInputId($eventAttendanceModel, 'attendance_type') . '-wrapper';
$payment_method_wrapper_id = Html::getInputId($eventAttendanceModel, 'payment_method') . '-wrapper';
$sponsor_wrapper_id = Html::getInputId($eventAttendanceModel, 'sponsor') . '-wrapper';
?>
<div class="add-attendee-form">
    <?php $form = ActiveForm::begin([
        'validationUrl' => ['ajax-validate', 'slug' => $eventModel->slug],
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'validateOnBlur' => false,
        'validateOnChange' => false,
    ]); ?>
        <?= Form::widget([
            'model' => $eventAttendanceModel,
            'form' => $form,
            'attributes' => [
                'member_id' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => 'kartik\select2\Select2',
                    'options' => [
                        'options' => ['placeholder' => $eventAttendanceModel->getAttributeLabel('member_id')],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => Url::to(['member-list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(member) { return member.text; }'),
                            'templateSelection' => new JsExpression('function (member) { return member.text; }'),
                        ],
                    ],
                ],
                'attendance_type' => [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'container' => ['id' => $attendance_type_wrapper_id],
                    'items' => $eventAttendanceTypeListData,
                    'options' => ['prompt' => $eventAttendanceModel->getAttributeLabel('attendance_type')],
                ],
                'payment_method' => [
                    'type' => Form::INPUT_RADIO_LIST,
                    'label' => Yii::t('app', 'How are you paying for this event?'),
                    'container' => ['id' => $payment_method_wrapper_id],
                    'items' => [
                        PaymentMethod::SPONSOR => PaymentMethod::getLabel(PaymentMethod::SPONSOR),
                        PaymentMethod::PERSONAL => PaymentMethod::getLabel(PaymentMethod::PERSONAL),
                    ],
                    'options' => [
                        'custom' => true,
                        'inline' => true,
                    ],
                ],
                'sponsor_id' => [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'container' => ['id' => $sponsor_wrapper_id],
                    'items' => $sponsorListData,
                    'options' => ['prompt' => $eventAttendanceModel->getAttributeLabel('sponsor_id')],
                ],
            ],
        ]) ?>
        <div class="form-group">
            <div>
                <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
                <?= Html::a(Yii::t('app', 'Go Back'), ['attendances', 'slug' => $eventModel->slug], ['class' => 'btn btn-link']) ?>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$attendance_type_id = Html::getInputId($eventAttendanceModel, 'attendance_type');
$payment_method_name = Html::getInputName($eventAttendanceModel, 'payment_method');
$sponsor_id = Html::getInputId($eventAttendanceModel, 'sponsor_id');

$participant_value = EventAttendanceType::PARTICIPANT;
$sponsor_value = PaymentMethod::SPONSOR;
$js = "
function resetAttendanceTypeInput() { jQuery('#{$attendance_type_id}').prop('selectedIndex', 0); }
function resetPaymentMethodInput() { jQuery('input:radio[name=\"{$payment_method_name}\"]').each(function () { jQuery(this).prop('checked', false); }); }
function resetSponsorInput() { jQuery('#{$sponsor_id}').prop('selectedIndex', 0); }
function hideAttendanceTypeWrapper() { jQuery('#{$attendance_type_wrapper_id}').hide(); }
function hidePaymentMethodWrapper() { jQuery('#{$payment_method_wrapper_id}').hide(); }
function hideSponsorWrapper() { jQuery('#{$sponsor_wrapper_id}').hide(); }
function showAttendanceTypeWrapper() { jQuery('#{$attendance_type_wrapper_id}').show(); }
function showPaymentMethodWrapper() { jQuery('#{$payment_method_wrapper_id}').show(); }
function showSponsorWrapper() { jQuery('#{$sponsor_wrapper_id}').show(); }
hidePaymentMethodWrapper();
hideSponsorWrapper();
if (jQuery('#{$attendance_type_id}').val() == {$participant_value}) { showPaymentMethodWrapper(); }
if (jQuery('input:radio[name=\"{$payment_method_name}\"]:checked').val() == {$participant_value}) { showSponsorWrapper(); }
jQuery('.modal').on('shown.coreui.modal', function () {  resetAttendanceTypeInput(); resetPaymentMethodInput(); resetSponsorInput(); hidePaymentMethodWrapper(); hideSponsorWrapper(); });
jQuery('#{$attendance_type_id}').on('change', function () {
    resetPaymentMethodInput();
    resetSponsorInput();
    hidePaymentMethodWrapper();
    hideSponsorWrapper();
    jQuery('[name=\"{$payment_method_name}\"]').removeClass('is-invalid');
    jQuery('[name=\"{$payment_method_name}\"]').closest('.form-group').removeClass('required has-error has-success');
    jQuery('[name=\"{$payment_method_name}\"]').closest('.invalid-feedback').html('');
    var selected = jQuery(this).val();
    if (selected == {$participant_value}) {
        jQuery('[name=\"{$payment_method_name}\"]').closest('.form-group').addClass('required');
        showPaymentMethodWrapper();
    }
});
jQuery('input:radio[name=\"{$payment_method_name}\"]').on('change', function () {
    resetSponsorInput();
    hideSponsorWrapper();
    jQuery('#{$sponsor_id}').removeClass('is-invalid');
    jQuery('#{$sponsor_id}').closest('.form-group').removeClass('required has-error has-success');
    jQuery('#{$sponsor_id}').closest('.invalid-feedback').html('');
    var selected = jQuery(this).val();
    if (selected == {$sponsor_value}) {
        jQuery('#{$sponsor_id}').closest('.form-group').addClass('required');
        showSponsorWrapper();
    }
});
";

$this->registerJs($js, View::POS_READY);