<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\grid\GridView;
use kartik\form\ActiveForm;
use kartik\builder\TabularForm;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\OfficerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Officers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="card-body">
        <div class="d-flex justify-content-between mb-3">
            <?= Html::a(Yii::t('app', 'Create Officer'), ['create'], ['class' => 'btn btn-success px-4']) ?>
            <div>
                <?= Html::a(
                    Yii::t('app', 'Reset'),
                    ['index'],
                    [
                        'class' => 'btn btn-outline-dark',
                        'title'=>Yii::t('app', 'Reset Grid'),
                        'data-pjax' => 0, 
                    ]
                ) ?>
            </div>
        </div>
        <div>
            <?= $this->render('_search', ['model' => $searchModel]); ?>
        </div>
        <?php $form = ActiveForm::begin(['action' => ['batch-update']]); ?>
            <?= TabularForm::widget([
                'dataProvider' => $dataProvider,
                'form' => $form,
                'gridSettings' => [
                    'tableOptions' => ['class' => 'table-responsive-sm'],
                    'headerRowOptions' => ['class' => 'thead-light'],
                    'hover' => true,
                    'striped' => false,
                ],
                'attributes' => [
                    'id' => [
                        'type' => TabularForm::INPUT_HIDDEN, 
                        'columnOptions' => ['hidden' => true],
                    ], 
                    'officer_name' => [
                        'type' => TabularForm::INPUT_STATIC,
                        'columnOptions' => ['width' => '30%'],
                        // 'widgetClass' => 'kartik\select2\Select2',
                        // 'options' => function ($model, $key, $index, $widget) {
                            // $query = (new \yii\db\Query())
                                // ->select(['id' => '[[user_id]]', 'text' => (new \yii\db\Expression('CONCAT([[first_name]], " ", [[last_name]])'))])
                                // ->from('{{%user_profile}}')
                                // ->having(['[[id]]' => $model->member_id])
                                // ->limit(1);
                            // $command = $query->createCommand();
                            // $member = $command->queryAll();
                            // return [
                                // 'data' => ArrayHelper::map(
                                    // $member,
                                    // 'id',
                                    // 'text'
                                // ),
                            // ];
                        // },
                    ],
                    'was_president' => [
                        'type' => TabularForm::INPUT_WIDGET,
                        'columnOptions' => ['width' => '10%'],
                        'widgetClass' => 'kartik\widgets\SwitchInput',
                        'options' => [
                            'pluginOptions' => [
                                'onText' => Yii::t('app', 'Yes'),
                                'offText' => Yii::t('app', 'No'),
                                'onColor' => 'success',
                                'offColor' => 'danger',
                            ],
                        ],
                    ],
                    'year_served' => [
                        'type' => TabularForm::INPUT_WIDGET,
                        'columnOptions' => ['width' => '60%'],
                        'widgetClass' => 'kartik\select2\Select2',
                        'options' => [
                            'data' => $yearServedRange,
                            'options' => [
                                'multiple' => true,
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ],
                    ],
                ],
            ]) ?>
            <div class="form-group d-flex justify-content-end">
                <div>
                    <?= Html::submitButton(Yii::t('app', 'Batch Delete'), [
                        'name' => 'batch_action',
                        'value' => 'delete',
                        'class' => 'btn btn-danger px-4',
                        'data' => ['confirm' => Yii::t('app', 'Are you sure to delete selected item/s?')],
                    ]) ?>
                    <?= Html::submitButton(Yii::t('app', 'Batch Update'), [
                        'name' => 'batch_action',
                        'value' => 'update',
                        'class' => 'btn btn-primary px-4',
                    ]) ?>
                </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
