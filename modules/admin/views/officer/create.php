<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Officer */
/* @var $yearServedRange array */

$this->title = Yii::t('app', 'Create Officer');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Officers'), 'url' => ['member/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="card-body">
        <?= $this->render('_form', [
            'model' => $model,
            'yearServedRange' => $yearServedRange,
        ]) ?>
    </div>
</div>
