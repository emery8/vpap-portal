<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model app\models\Officer */
/* @var $form yii\widgets\ActiveForm */
/* @var $yearServedRange array */
?>

<div class="officer-form">

    <?php $form = ActiveForm::begin(); ?>
        <?= Form::widget([
            'model' => $model,
            'form' => $form,
            'attributes' => [
                'member_id' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => 'kartik\select2\Select2',
                    'options' => [
                        'options' => ['placeholder' => $model->getAttributeLabel('member_id')],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 3,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => Url::to(['member-list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function (params) { return { q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function (member) { return member.text; }'),
                            'templateSelection' => new JsExpression('function (member) { return member.text; }'),
                        ],
                    ],
                ],
                'was_president' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => 'kartik\widgets\SwitchInput',
                    'options' => [
                        'pluginOptions' => [
                            'onText' => Yii::t('app', 'Yes'),
                            'offText' => Yii::t('app', 'No'),
                            'onColor' => 'success',
                            'offColor' => 'danger',
                        ],
                    ],
                ],
                'year_served' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => 'kartik\select2\Select2',
                    'options' => [
                        'data' => $yearServedRange,
                        'options' => [
                            'multiple' => true,
                            'placeholder' => $model->getAttributeLabel('year_served'),
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'closeOnSelect' => false,
                        ],
                    ],
                ],
            ],
        ]) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success px-4']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>
