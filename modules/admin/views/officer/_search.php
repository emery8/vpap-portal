<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model app\models\OfficerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="officer-search">

    <div class="card">
        <div class="card-body bg-light">
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>
                <?= Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' => 3,
                    'attributes' => [
                        'officer_name' => [
                            'type' => Form::INPUT_TEXT,
                            'label' => false,
                            'options' => ['placeholder' => Yii::t('app', 'Search member...')],
                            // 'fieldConfig' => ['options' => ['class' => 'mb-0']],
                        ],
                        'year_served' => [
                            'type' => Form::INPUT_TEXT,
                            'label' => false,
                            'options' => ['placeholder' => Yii::t('app', 'Search year...')],
                            // 'fieldConfig' => ['options' => ['class' => 'mb-0']],
                        ],
                        'was_president' => [
                            'type' => Form::INPUT_DROPDOWN_LIST,
                            'label' => false,
                            'items' => \kartik\helpers\Enum::boolList(),
                            'options' => ['prompt' => Yii::t('app', 'Was President?')],
                            // 'fieldConfig' => ['options' => ['class' => 'mb-0']],
                        ],
                    ],
                ]) ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>
