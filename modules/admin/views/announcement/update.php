<?php

use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Announcement */
/* @var $announcementStatusListData array */

$this->title = Yii::t('app', 'Update Announcement: {name}', [
    'name' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Announcements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        <?= Html::encode($this->title) ?>
        <div>
            <?= Html::a(
                Yii::t('app', 'Delete'),
                ['delete', 'id' => $model->id],
                [
                    'class' => 'btn btn-sm btn-ghost-danger',
                    'data' => [
                        'method' => 'post',
                        'confirm' => Yii::t('app', 'Are you sure to delete this item?'),
                    ],
                ]
            ) ?>
        </div>
    </div>
    <div class="card-body">
        <?= $this->render('_form', [
            'model' => $model,
            'announcementStatusListData' => $announcementStatusListData,
        ]) ?>
    </div>
</div>
