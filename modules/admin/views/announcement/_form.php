<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model app\models\Announcement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="announcement-form">

    <?php $form = ActiveForm::begin(); ?>
        <?= Form::widget([
            'model' => $model,
            'form' => $form,
            'attributes' => [
                'title' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => [
                        'autofocus' => true,
                        'maxlength' => true,
                        'placeholder' => $model->getAttributeLabel('title'),
                    ],
                ],
            ],
        ]) ?>
        <?= Form::widget([
            'model' => $model,
            'form' => $form,
            'attributes' => [
                'content' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => 'kartik\editors\Summernote',
                    'options' => [
                        'useKrajeePresets' => true,
                        'container' => ['class' => 'kv-editor-container'],
                    ],
                ],
            ],
        ]) ?>
        <?= Form::widget([
            'model' => $model,
            'form' => $form,
            'attributes' => [
                'status' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => 'kartik\select2\Select2',
                    'options' => [
                        'data' => $announcementStatusListData,
                        'options' => ['placeholder' => $model->getAttributeLabel('status')],
                    ],
                ],
            ],
        ]) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app', 'Go back to main'), ['default/index'], ['class' => 'ml-3']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>
