<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Announcement */
/* @var $announcementStatusListData array */

$this->title = Yii::t('app', 'Create Announcement');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Announcements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="card-body">
        <?= $this->render('_form', [
            'model' => $model,
            'announcementStatusListData' => $announcementStatusListData,
        ]) ?>
    </div>
</div>
