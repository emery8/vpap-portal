<?php

use yii\bootstrap4\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\AnnouncementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Announcements');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="card-body">
        <div class="d-flex justify-content-between mb-3">
            <?= Html::a(Yii::t('app', 'Create Announcement'), ['create'], ['class' => 'btn btn-success px-4']) ?>
            <div>
                <?= Html::a(
                    Yii::t('app', 'Reset'),
                    ['index'],
                    [
                        'class' => 'btn btn-outline-dark',
                        'title'=>Yii::t('app', 'Reset Grid'),
                        'data-pjax' => 0, 
                    ]
                ) ?>
            </div>
        </div>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table-responsive-sm'],
            'headerRowOptions' => ['class' => 'thead-light'],
            'hover' => true,
            'striped' => false,
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'mergeHeader' => false,
                ],

                'title',
                [
                    'attribute' => 'created_by',
                    'label' => Yii::t('app', 'Author'),
                    'format' => 'author',
                ],
                [
                    'attribute' => 'created_at',
                    'label' => Yii::t('app', 'Date Created'),
                    'format' => 'datetime',
                    'filterType' => GridView::FILTER_DATE,
                    'filterWidgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                            'orientation' => 'bottom',
                        ],
                    ],
                ],
                [
                    'attribute' => 'status',
                    'hAlign' => 'center',
                    'format' => 'announcementstatus',
                    'filterType' => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'data' => $announcementStatusListData,
                        'options' => ['placeholder' => ''],
                    ],
                ],

                [
                    'class' => 'kartik\grid\ActionColumn',
                    'mergeHeader' => false,
                    'viewOptions' => ['style' => 'display:none'],
                ],
            ],
        ]); ?>
    </div>
</div>
