<?php

use yii\bootstrap4\Html;
use yii\web\View;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use app\models\enums\MembershipType;

/* @var $this yii\web\View */
/* @var $model app\models\MembershipActivitySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="membership-activity-search">
    <div class="card">
        <div class="card-body bg-light">
            <?php $form = ActiveForm::begin([
                'action' => ['membership-activity'],
                'method' => 'get',
            ]); ?>
                <?= Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' => 2,
                    'attributes' => [
                        'complete_name' => [
                            'type' => Form::INPUT_TEXT,
                            'label' => false,
                            'options' => ['placeholder' => $model->getAttributeLabel('complete_name')],
                        ],
                        'membership_type' => [
                            'type' => Form::INPUT_DROPDOWN_LIST,
                            'items' => [
                                MembershipType::LIFETIME => Yii::t('app', 'Lifetime'),
                                MembershipType::REGULAR => Yii::t('app', 'Regular'),
                            ],
                            'label' => false,
                            'options' => ['prompt' => $model->getAttributeLabel('membership_type')],
                        ],
                    ],
                ]) ?>
                <div class="form-group mb-0">
                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Reset'), ['membership-activity'], ['class' => 'btn btn-outline-dark btn-reset']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
