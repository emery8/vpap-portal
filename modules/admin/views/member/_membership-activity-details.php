<?php

use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MembershipActivity */
$formatter = Yii::$app->formatter;
?>
<div class="row justify-content-start">
    <div class="col-2">
        <div class="card mb-0">
            <div class="card-body bg-warning text-white">
                <h4 class="d-flex align-items-center mb-0">
                    <?= Html::encode($model->event_attendance_count) ?>
                    <small class="ml-2"><?= Yii::t('app', 'TOTAL') ?></small>
                </h4>
                <span><?= $formatter->asMembershipType($model->membership_type) ?></span>
            </div>
        </div>
    </div>
    <div class="col-5">
        <div class="card mb-0">
            <div class="card-body bg-light">
                <?php $arr = $model->year_event_attended; ?>
                <?= Yii::t('app', '{count,plural,=0{Event attended with confirmed attendance:} =1{Event attended with confirmed attendance:} other{Events attended with confirmed attendances:}}', ['count' => is_null($arr) ? 0 : count($arr)]) ?>
                <?= Html::tag('br') ?>
                <?php
                if (is_array($arr) && count($arr)) {
                    echo '<span class="badge badge-success">' . implode('</span><span class="badge badge-success ml-1">', $arr) . '</span>';
                } else {
                    echo $formatter->nullDisplay;
                }
                ?>
            </div>
        </div>
    </div>
    <div class="col-5">
        <div class="card mb-0">
            <div class="card-body bg-light">
            <?php $arr = $model->event_attended_year_paid; ?>
                <?= Yii::t('app', '{count,plural,=0{Event attended with confirmed payment:} =1{Event attended with confirmed payment:} other{Events attended with confirmed payments:}}', ['count' => is_null($arr) ? 0 : count($arr)]) ?>
                <?= Html::tag('br') ?>
                <?php
                if (is_array($arr) && count($arr)) {
                    echo '<span class="badge badge-success">' . implode('</span><span class="badge badge-success ml-1">', $arr) . '</span>';
                } else {
                    echo $formatter->nullDisplay;
                }
                ?>
            </div>
        </div>
    </div>
</div>
