<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('app', 'Update User: {name}', [
    'name' => $model->complete_name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->complete_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="card">
    <div class="card-header">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="card-body">
        <div class="member-form">

            <?php $form = ActiveForm::begin(); ?>
                <?= Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'attributes' => [
                        'email' => [
                            'type' => Form::INPUT_TEXT,
                            'options' => [
                                'placeholder' => $model->getAttributeLabel('email'),
                            ],
                        ],
                    ],
                ]) ?>
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                    <?= Html::a(Yii::t('app', 'Go Back'), ['index'], ['class' => 'btn btn-danger']) ?>
                </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
