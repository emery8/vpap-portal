<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserProfile */
?>
<div class="row justify-content-start">
    <div class="col-md-12">
        <?= Html::tag(
            'h3',
            Yii::t('app', '{icon} Profile', [
                'icon' => Html::tag(
                    'svg',
                    Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-user')]),
                    ['class' => 'mr-2 c-icon c-icon-xl']
                ),
            ]),
            ['class' => 'mt-2 mb-4 text-dark']
        ) ?>
        <?php if ($model): ?>
            <?= DetailView::widget([
                'model' => $model,
                'options' => ['class' => 'table table-bordered detail-view mb-0'],
                'attributes' => [
                    'first_name',
                    'middle_name',
                    'last_name',
                    'suffix',
                    'nickname',
                    'name_on_certificate',
                    'address:ntext',
                    'subdivision',
                    'barangay',
                    'municipality',
                    'province',
                    'zipcode',
                    'country',
                    'phone_number',
                    'mobile_number',
                    'birth_date:date',
                ],
            ]) ?>
        <?php else: ?>
            <p><?= Yii::$app->formatter->memberNoRecords ?></p>
        <?php endif; ?>
    </div>
</div>
