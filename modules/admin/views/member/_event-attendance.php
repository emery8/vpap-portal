<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $member app\models\User */
/* @var $searchModel app\modules\admin\models\MemberEventAttendanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>
<div class="card mt-4">
    <div class="card-body">
        <div class="d-flex justify-content-start">
            <?php $form = ActiveForm::begin(['action' => ['download-event-attendances', 'id' => $member->id]]); ?>
                <div class="form-group mb-0">
                    <?= Html::submitButton(Yii::t('app', 'Download CSV'), [
                        'name' => 'batch_action',
                        'value' => 'download-csv',
                        'class' => 'btn btn-success px-4',
                    ]) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
        <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'options' => ['class' => 'mt-4'],
                'tableOptions' => ['class' => 'table-responsive-sm'],
                'headerRowOptions' => ['class' => 'thead-light'],
                'hover' => true,
                'striped' => false,
                'columns' => [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'mergeHeader' => false,
                    ],

                    [
                        'attribute' => 'type',
                        'format' => 'eventtype',
                        'value' => function ($model, $key, $index, $column) {
                            return $model->event->type;
                        },
                    ],
                    [
                        'attribute' => 'title',
                        'format' => 'ntext',
                        'value' => function ($model, $key, $index, $column) {
                            return $model->event->title;
                        },
                    ],
                    [
                        'hAlign' => 'center',
                        'attribute' => 'start_date',
                        'format' => 'date',
                        'value' => function ($model, $key, $index, $column) {
                            return $model->event->start_date;
                        },
                    ],
                    [
                        'hAlign' => 'center',
                        'attribute' => 'end_date',
                        'format' => 'date',
                        'value' => function ($model, $key, $index, $column) {
                            return $model->event->end_date;
                        },
                    ],
                    [
                        'hAlign' => 'right',
                        'attribute' => 'cpd_point_earned',
                        'value' => 'cpd_point_earned',
                        'label' => Yii::t('app', 'CPD Point/s Earned'),
                        'format' => ['decimal', 2],
                    ],
                    [
                        'attribute' => 'attended',
                        'format' => 'boolean',
                    ],
                ],
            ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>