<?php

use yii\bootstrap4\Tabs;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $membershipTypeListData array */
/* @var $memberDiscountTypeListData array */
/* @var $statusListData array */

$this->title = Yii::t('app', 'Members');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nav-tabs-boxed">
    <?= Tabs::widget([
        'items' => [
            [
                'label' => Yii::t('app', 'Members'),
                'active' => true,
                'content' => $this->render('_member', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'membershipTypeListData' => $membershipTypeListData,
                    'memberDiscountTypeListData' => $memberDiscountTypeListData,
                    'statusListData' => $statusListData,
                ]),
            ],
            [
                'label' => Yii::t('app', 'Officers'),
                'url' => ['officer'],
            ],
            [
                'label' => Yii::t('app', 'Membership Activities'),
                'url' => ['membership-activity'],
            ],
            [
                'label' => Yii::t('app', 'VPAP Membership Fees'),
                'url' => ['membership-payment'],
            ],
        ],
    ]) ?>
</div>
<?php
$js = "
// https://stackoverflow.com/questions/7862233/twitter-bootstrap-tabs-go-to-specific-tab-on-page-reload-or-hyperlink
var url = document.location.toString();
if (url.match('#')) {
    jQuery('.nav-tabs a[href=\"#'+url.split('#')[1]+'\"]').tab('show');
    window.scrollTo(0, 0);
} 

// With HTML5 history API, we can easily prevent scrolling!
jQuery('.nav-tabs a').on('shown.coreui.tab', function (e) {
    if(history.pushState) {
        history.pushState(null, null, e.target.hash); 
    } else {
        window.location.hash = e.target.hash; //Polyfill for old browsers
    }
});
";
// $this->registerJs($js, View::POS_READY);
