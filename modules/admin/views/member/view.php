<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\Tabs;
use yii\helpers\Url;
use yii\web\View;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $searchModel app\modules\admin\models\MemberEventAttendanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->userProfile->complete_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Members'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="row">
    <div class="col-md-9">
        <div class="nav-tabs-boxed">
            <?= Tabs::widget([
                'encodeLabels' => false,
                'items' => [
                    [
                        'active' => true,
                        'label' => Yii::t('app', '{icon} Profile', [
                            'icon' => Html::tag(
                                'svg',
                                Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-user')]),
                                ['class' => 'mr-1 c-icon']
                            ),
                        ]),
                        'content' => $this->render('_profile', ['model' => $model->userProfile]),
                        'options' => ['id' => 'profile-tab'],
                    ],
                    [
                        'label' => Yii::t('app', '{icon} Employment', [
                            'icon' => Html::tag(
                                'svg',
                                Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-briefcase')]),
                                ['class' => 'mr-1 c-icon']
                            ),
                        ]),
                        'content' => $this->render('_employment', ['model' => $model->userEmployment]),
                        'options' => ['id' => 'employment-tab'],
                    ],
                    [
                        'label' => Yii::t('app', '{icon} Profession', [
                            'icon' => Html::tag(
                                'svg',
                                Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-institution')]),
                                ['class' => 'mr-1 c-icon']
                            ),
                        ]),
                        'content' => $this->render('_profession', ['model' => $model->userProfession]),
                        'options' => ['id' => 'profession-tab'],
                    ],
                    [
                        'label' => Yii::t('app', '{icon} Membership', [
                            'icon' => Html::tag(
                                'svg',
                                Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-notes')]),
                                ['class' => 'mr-1 c-icon']
                            ),
                        ]),
                        'content' => $this->render('_membership', ['model' => $model->userMembership]),
                        'options' => ['id' => 'membership-tab'],
                    ],
                    [
                        'label' => Yii::t('app', '{icon} Education', [
                            'icon' => Html::tag(
                                'svg',
                                Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-education')]),
                                ['class' => 'mr-1 c-icon']
                            ),
                        ]),
                        'content' => $this->render('_education', ['model' => $model->userEducation]),
                        'options' => ['id' => 'education-tab'],
                    ],
                ],
            ]) ?>
        </div>
        <?= $this->render('_event-attendance', [
            'member' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]) ?>
    </div>
    <div class="col-md-3">
        
    </div>
</div>
<?php
$js = "
// https://stackoverflow.com/questions/7862233/twitter-bootstrap-tabs-go-to-specific-tab-on-page-reload-or-hyperlink
var url = document.location.toString();
if (url.match('#')) {
    jQuery('.nav-tabs a[href=\"#'+url.split('#')[1]+'\"]').tab('show');
    window.scrollTo(0, 0);
} 

// With HTML5 history API, we can easily prevent scrolling!
jQuery('.nav-tabs a').on('shown.coreui.tab', function (e) {
    if(history.pushState) {
        history.pushState(null, null, e.target.hash); 
    } else {
        window.location.hash = e.target.hash; //Polyfill for old browsers
    }
});
";
$this->registerJs($js, View::POS_READY);
