<?php

use yii\bootstrap4\Html;
use yii\web\View;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model app\models\OfficerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="officer-search">
    <div class="card">
        <div class="card-body bg-light">
            <?php $form = ActiveForm::begin([
                'action' => ['officer'],
                'method' => 'get',
            ]); ?>
                <?= Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' => 3,
                    'attributes' => [
                        'complete_name' => [
                            'type' => Form::INPUT_TEXT,
                            'label' => false,
                            'options' => ['placeholder' => $model->getAttributeLabel('complete_name')],
                            // 'fieldConfig' => ['options' => ['class' => 'mb-0']],
                        ],
                        'year_served' => [
                            'type' => Form::INPUT_TEXT,
                            'label' => false,
                            'options' => ['placeholder' => $model->getAttributeLabel('year_served')],
                            // 'fieldConfig' => ['options' => ['class' => 'mb-0']],
                        ],
                        'was_president' => [
                            'type' => Form::INPUT_DROPDOWN_LIST,
                            'label' => false,
                            'items' => \kartik\helpers\Enum::boolList(),
                            'options' => ['prompt' => $model->getAttributeLabel('was_president')],
                            // 'fieldConfig' => ['options' => ['class' => 'mb-0']],
                        ],
                    ],
                ]) ?>
                <div class="form-group mb-0">
                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Reset'), ['officer'], ['class' => 'btn btn-outline-dark']) ?>
                </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>
