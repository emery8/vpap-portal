<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use kartik\form\ActiveForm;
use kartik\builder\TabularForm;
use kartik\builder\Form;
use app\models\MembershipActivity;

/* @var $this yii\web\View */
/* @var $form kartik\form\ActiveForm */
/* @var $model app\models\MembershipActivityForm */
/* @var $membershipTypeListData array */
/* @var $eventTypeListData array */
/* @var $yearServedRange array */
?>
<div class="mt-2">
    <?= $this->render('_search-membership-activity', ['model' => $searchModel]); ?>
</div>
<?php $form = ActiveForm::begin(); ?>
    <?= TabularForm::widget([
        'dataProvider' => $dataProvider,
        'form' => $form,
        'gridSettings' => [
            'id' => 'members-grid',
            'tableOptions' => ['class' => 'table-responsive-sm'],
            'headerRowOptions' => ['class' => 'thead-light'],
            'hover' => true,
            'striped' => false,
        ],
        'actionColumn' => [
            'width' => '40px',
            'updateOptions' => ['style' => 'display:none'],
            'viewOptions' => ['style' => 'display:none'],
            'urlCreator' => function ($action, MembershipActivity $model, $key, $index, $column) {
                switch ($action) {
                    case 'delete':
                        return Url::toRoute(['membership-activity/delete', 'id' => $model->id]);
                        break;
                }
            },
        ],
        'attributes' => [
            'member_id' => [
                'type' => TabularForm::INPUT_HIDDEN,
                'columnOptions' => ['hidden' => true],
            ],
            'complete_name' => [
                'type' => TabularForm::INPUT_STATIC,
                'columnOptions'=>['width'=>'25%'],
            ],
            'membership_type' => [
                'type' => TabularForm::INPUT_STATIC,
                'format' => 'membershiptype',
                'columnOptions'=>['width'=>'15%'],
            ],
            'year_initial_joined' => [
                'type' => TabularForm::INPUT_STATIC,
            ],
            'year_event_attended' => [
                'type' => TabularForm::INPUT_STATIC,
                'staticValue' => function ($model, $key, $index, $widget) {
                    $arr = $model->year_event_attended;
                    if ($arr !== null) {
                        return '<span class="badge badge-success">' . implode('</span><span class="badge badge-success ml-1">', $arr) . '</span>';
                    }
                    return Yii::$app->formatter->nullDisplay;
                },
                'columnOptions'=>['width'=>'25%']
            ],
            'year_membership_paid' => [
                'type' => TabularForm::INPUT_WIDGET,
                'columnOptions' => ['width' => '60%'],
                'widgetClass' => 'kartik\select2\Select2',
                'options' => [
                    'data' => $yearServedRange,
                    'options' => [
                        'multiple' => true,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'closeOnSelect' => false,
                    ],
                ],
                'columnOptions'=>['width'=>'35%'],
            ],
        ],
    ]) ?>
    <div class="form-group d-flex justify-content-end">
        <div>
            <?= Html::submitButton(Yii::t('app', 'Batch Delete'), [
                'name' => 'batch_action',
                'value' => 'delete',
                'class' => 'btn btn-danger px-4',
                'data' => ['confirm' => Yii::t('app', 'Are you sure to delete selected item/s?')],
            ]) ?>
            <?= Html::submitButton(Yii::t('app', 'Batch Update'), [
                'name' => 'batch_action',
                'value' => 'update',
                'class' => 'btn btn-primary px-4',
            ]) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<?php /*
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'tableOptions' => ['class' => 'table-responsive-sm'],
    'headerRowOptions' => ['class' => 'thead-light'],
    'hover' => true,
    'striped' => false,
    'perfectScrollbar' => true,
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'mergeHeader' => false,
        ],

        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_EXPANDED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_membership-activity-details', ['model' => $model]);
            },
            'detailRowCssClass' => GridView::TYPE_PRIMARY,
            'expandOneOnly' => false,
        ],

        [
            'attribute' => 'complete_name',
            'format' => 'raw',
            'value' => function ($model, $key, $index, $column) {
                return Html::a(
                    Html::encode($model->complete_name),
                    ['member/view', 'id' => $model->member_id],
                    [
                        'target' => '_blank',
                        'data-pjax' => 0,
                    ]
                );
            },
        ],
    ],
]); ?>
*/ ?>
<div class="card">
    <div class="card-body">
        <?php
            $form = ActiveForm::begin(['action' => ['run-query']]);
        ?>
            <?= Form::widget([
                'model'=>$model,
                'form'=>$form,
                'attributes' => [
                    'membership_type' => [
                        'type' => Form::INPUT_CHECKBOX_LIST,
                        'items' => $membershipTypeListData,
                        'options' => [
                            'custom' => true,
                            'inline' => true,
                        ],
                    ],
                    'event_type' => [
                        'type' => Form::INPUT_CHECKBOX_LIST,
                        'items' => $eventTypeListData,
                        'options' => [
                            'custom' => true,
                            'inline' => true,
                        ],
                    ],
                    'actions' => [
                        'type' => Form::INPUT_RAW,
                        'value' => Html::submitButton(Yii::t('app', 'Run Query'), [
                            'class' => 'btn btn-danger px-4',
                            'data' => ['confirm' => Yii::t('app', 'Are you sure to run the batch operation?')],
                        ]),
                    ],
                ],
            ]) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
