<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\View;
use kartik\form\ActiveForm;
use kartik\builder\TabularForm;
use kartik\builder\Form;
use app\models\MembershipPayment;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\MembershipPaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $form kartik\form\ActiveForm */
?>
<div class="d-flex justify-content-start my-3">
    <?= Html::a(Yii::t('app', 'Create Payment'), ['add-membership-payment'], ['class' => 'btn btn-success px-4']) ?>
</div>
<div>
    <?= $this->render('_search-membership-payment', [
        'model' => $searchModel,
    ]); ?>
</div>
<?php $form = ActiveForm::begin(['id' => 'membership-payments-form']); ?>
    <?= TabularForm::widget([
        'dataProvider' => $dataProvider,
        'form' => $form,
        'gridSettings' => [
            'id' => 'membership-payments-grid',
            'tableOptions' => ['class' => 'table-responsive-sm'],
            'headerRowOptions' => ['class' => 'thead-light'],
            'hover' => true,
            'striped' => false,
        ],
        'actionColumn' => [
            'width' => '40px',
            'updateOptions' => ['style' => 'display:none'],
            'viewOptions' => ['style' => 'display:none'],
            'urlCreator' => function ($action, MembershipPayment $model, $key, $index, $column) {
                switch ($action) {
                    case 'delete':
                        return Url::toRoute(['membership-payment/delete', 'id' => $model->id]);
                        break;
                }
            },
        ],
        'attributes' => [
            'id' => [
                'type' => TabularForm::INPUT_HIDDEN,
                'columnOptions' => ['hidden' => true],
            ], 
            'complete_name' => ['type' => TabularForm::INPUT_STATIC],
            'type' => [
                'type' => TabularForm::INPUT_STATIC,
                'format' => 'membershipfee',
            ],
            'number_of_year' => ['type' => TabularForm::INPUT_STATIC],
            'amount' => ['type' => TabularForm::INPUT_STATIC],
            'amount_paid' => ['type' => TabularForm::INPUT_STATIC],
            'payment_confirmed' => [
                'type' => TabularForm::INPUT_WIDGET,
                'widgetClass' => 'kartik\widgets\SwitchInput',
                'columnOptions' => ['hAlign' => 'center'],
                'options' => [
                    'pluginOptions' => [
                        'handleWidth' => 20,
                        'onColor' => 'success',
                        'offColor' => 'danger',
                        'onText' => Html::tag('i', '', ['class' => 'fas fa-thumbs-up']),
                        'offText' => Html::tag('i', '', ['class' => 'fas fa-thumbs-down']),
                    ],
                    'pluginEvents' => [
                        'switchChange.bootstrapSwitch' => 'submittogglepaymentconfirmed',
                    ],
                ],
            ],
        ],
    ]) ?>
    <div class="card">
        <div class="card-body bg-light">
            <div class="form-group row justify-content-end align-items-center mb-0">
                <div>
                    <?= Html::hiddenInput('payment_confirmed_id', null) ?>
                    <?= Html::submitButton(Yii::t('app', 'Batch Delete'), [
                        'name' => 'batch_action',
                        'value' => 'delete',
                        'class' => 'btn btn-danger px-4',
                        'data' => ['confirm' => Yii::t('app', 'Are you sure to delete selected item/s?')],
                    ]) ?>
                    <?= Html::submitButton(Yii::t('app', 'Batch Update'), [
                        'name' => 'batch_action',
                        'value' => 'update',
                        'class' => 'btn btn-primary px-4',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<?php
$js = "
function submittogglepaymentconfirmed() { var payment_confirmed_id = jQuery(this).closest('tr').data('key'); jQuery('[name=\"payment_confirmed_id\"]').val(payment_confirmed_id); jQuery('[name=\"batch_action\"]').val('togglepaymentconfirmed'); setTimeout(function() { jQuery('#membership-payments-form').submit(); }, 500); }
";
$this->registerJs($js, View::POS_READY);
