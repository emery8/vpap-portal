<?php

use yii\bootstrap4\Html;
use yii\web\View;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model app\models\MemberSearch */
/* @var $form yii\widgets\ActiveForm */
/* @var $membershipTypeListData array */
/* @var $memberDiscountTypeListData array */
/* @var $statusListData array */
?>

<div class="member-search">
    <div class="card">
        <div class="card-body bg-light">
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>
                <?= Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' => 4,
                    'attributes' => [
                        'email' => [
                            'type' => Form::INPUT_TEXT,
                            'label' => false,
                            'options' => ['placeholder' => $model->getAttributeLabel('email')],
                        ],
                        'license_number' => [
                            'type' => Form::INPUT_TEXT,
                            'label' => false,
                            'options' => ['placeholder' => $model->getAttributeLabel('license_number')],
                        ],
                        'first_name' => [
                            'type' => Form::INPUT_TEXT,
                            'label' => false,
                            'options' => ['placeholder' => $model->getAttributeLabel('first_name')],
                        ],
                        'last_name' => [
                            'type' => Form::INPUT_TEXT,
                            'label' => false,
                            'options' => ['placeholder' => $model->getAttributeLabel('last_name')],
                        ],
                    ],
                ]) ?>
                <?= Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' => 3,
                    'attributes' => [
                        'membership_type' => [
                            'type' => Form::INPUT_DROPDOWN_LIST,
                            'label' => false,
                            'items' => $membershipTypeListData,
                            'options' => ['prompt' => $model->getAttributeLabel('membership_type')],
                        ],
                        'discount_type' => [
                            'type' => Form::INPUT_DROPDOWN_LIST,
                            'label' => false,
                            'items' => $memberDiscountTypeListData,
                            'options' => ['prompt' => $model->getAttributeLabel('discount_type')],
                        ],
                        'status' => [
                            'type' => Form::INPUT_DROPDOWN_LIST,
                            'label' => false,
                            'items' => $statusListData,
                            'options' => ['prompt' => $model->getAttributeLabel('status')],
                        ],
                    ],
                ]) ?>
                <div class="form-group mb-0">
                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Reset'), ['index'], ['class' => 'btn btn-outline-dark btn-reset']) ?>
                </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>
