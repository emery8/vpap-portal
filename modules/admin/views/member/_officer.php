<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\builder\TabularForm;
use app\models\Officer;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\OfficerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $form kartik\form\ActiveForm */
?>
<div class="d-flex justify-content-start my-3">
    <?= Html::a(Yii::t('app', 'Create Officer'), ['officer/create'], ['class' => 'btn btn-success px-4']) ?>
</div>
<div>
    <?= $this->render('_search-officer', ['model' => $searchModel]); ?>
</div>
<?php $form = ActiveForm::begin(); ?>
    <?= TabularForm::widget([
        'dataProvider' => $dataProvider,
        'form' => $form,
        'gridSettings' => [
            'tableOptions' => ['class' => 'table-responsive-sm'],
            'headerRowOptions' => ['class' => 'thead-light'],
            'hover' => true,
            'striped' => false,
        ],
        'actionColumn' => [
            'viewOptions' => ['style' => 'display:none'],
            'updateOptions' => ['style' => 'display:none'],
            'width' => '40px',
            'urlCreator' => function ($action, Officer $model, $key, $index, $column) {
                return Url::toRoute(["officer/{$action}", 'id' => $model->id]);
            },
        ],
        'attributes' => [
            'id' => [
                'type' => TabularForm::INPUT_HIDDEN, 
                'columnOptions' => ['hidden' => true],
            ], 
            'complete_name' => [
                'type' => TabularForm::INPUT_STATIC,
                'columnOptions' => ['width' => '30%'],
            ],
            'was_president' => [
                'type' => TabularForm::INPUT_WIDGET,
                'columnOptions' => ['width' => '10%'],
                'widgetClass' => 'kartik\widgets\SwitchInput',
                'options' => [
                    'pluginOptions' => [
                        'onText' => Yii::t('app', 'Yes'),
                        'offText' => Yii::t('app', 'No'),
                        'onColor' => 'success',
                        'offColor' => 'danger',
                    ],
                ],
            ],
            'number_years_served' => [
                'type' => TabularForm::INPUT_STATIC,
                'columnOptions' => ['width' => '10%'],
            ],
            'year_served' => [
                'type' => TabularForm::INPUT_WIDGET,
                'columnOptions' => ['width' => '50%'],
                'widgetClass' => 'kartik\select2\Select2',
                'options' => [
                    'data' => $yearServedRange,
                    'options' => [
                        'multiple' => true,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],
        ],
    ]) ?>
    <div class="form-group d-flex justify-content-end">
        <div>
            <?= Html::submitButton(Yii::t('app', 'Batch Delete'), [
                'name' => 'batch_action',
                'value' => 'delete',
                'class' => 'btn btn-danger px-4',
                'data' => ['confirm' => Yii::t('app', 'Are you sure to delete selected item/s?')],
            ]) ?>
            <?= Html::submitButton(Yii::t('app', 'Batch Update'), [
                'name' => 'batch_action',
                'value' => 'update',
                'class' => 'btn btn-primary px-4',
            ]) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
