<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UserEducation */
?>
<div class="row justify-content-start">
    <div class="col-md-12">
        <?= Html::tag(
            'h3',
            Yii::t('app', '{icon} Education', [
                'icon' => Html::tag(
                    'svg',
                    Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-education')]),
                    ['class' => 'mr-2 c-icon c-icon-xl']
                ),
            ]),
            ['class' => 'mt-2 mb-4 text-dark']
        ) ?>
        <?php if ($model): ?>
            <?= DetailView::widget([
                'model' => $model,
                'options' => ['class' => 'table table-bordered detail-view mb-0'],
                'attributes' => [
                    'institution:veterinaryinstitution',
                    'year_graduated',
                ],
            ]) ?>
        <?php else: ?>
            <p><?= Yii::$app->formatter->memberNoRecords ?></p>
        <?php endif; ?>
    </div>
</div>
