<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\web\View;
use kartik\form\ActiveForm;
use kartik\builder\TabularForm;
use kartik\builder\Form;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\MemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $form kartik\form\ActiveForm */
/* @var $membershipTypeListData array */
/* @var $memberDiscountTypeListData array */
/* @var $statusListData array */
?>
<div class="mt-2">
    <?= $this->render('_search-member', [
        'model' => $searchModel,
        'membershipTypeListData' => $membershipTypeListData,
        'memberDiscountTypeListData' => $memberDiscountTypeListData,
        'statusListData' => $statusListData,
    ]); ?>
</div>
<?php $form = ActiveForm::begin(['id' => 'members-form']); ?>
    <div class="card">
        <div class="card-body bg-light">
            <div class="form-group row justify-content-between align-items-center mb-0">
                <div class="col-md-6">
                    <?= Html::tag('p', Yii::t('app', 'With{count} selected:', ['count' => Html::tag('span', '', ['id' => 'selectedRowsCount'])])) ?>
                    <?= Form::widget([
                        'formName'=>'members-form',
                        'columns' => 2,
                        'compactGrid' => true,
                        'attributes' => [
                            'membership_type' => [
                                'type' => Form::INPUT_DROPDOWN_LIST,
                                'items' => $membershipTypeListData,
                                'options' => ['prompt' => Yii::t('app', 'Set Membership Type')],
                            ],
                        ],
                    ]) ?>
                </div>
                <div class="col-md-6 d-flex justify-content-end">
                    <div>
                        <?= Html::hiddenInput('membership_type_id', null) ?>
                        <?= Html::hiddenInput('lock_name_on_certificate_id', null) ?>
                        <?= Html::hiddenInput('status_id', null) ?>
                        <?= Html::submitButton(Yii::t('app', 'Batch Delete'), [
                            'name' => 'batch_action',
                            'value' => 'delete',
                            'class' => 'btn btn-danger px-4',
                            'data' => ['confirm' => Yii::t('app', 'Are you sure to delete selected item/s?')],
                        ]) ?>
                        <?= Html::submitButton(Yii::t('app', 'Save Changes'), [
                            'name' => 'batch_action',
                            'value' => 'update',
                            'class' => 'btn btn-primary px-4',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?= TabularForm::widget([
        'dataProvider' => $dataProvider,
        'form' => $form,
        'gridSettings' => [
            'id' => 'members-grid',
            'tableOptions' => ['class' => 'table-responsive-sm'],
            'headerRowOptions' => ['class' => 'thead-light'],
            'hover' => true,
            'striped' => false,
        ],
        'actionColumn' => [
            'width' => '40px',
            'urlCreator' => function ($action, User $model, $key, $index, $column) {
                return Url::toRoute(["member/{$action}", 'id' => $model->id]);
            },
        ],
        'attributes' => [
            'id' => [
                'type' => TabularForm::INPUT_HIDDEN,
                'options' => ['class' => 'member-item'],
                'columnOptions' => ['hidden' => true],
            ], 
            'email' => ['type' => TabularForm::INPUT_STATIC],
            'license_number' => ['type' => TabularForm::INPUT_STATIC],
            'complete_name' => ['type' => TabularForm::INPUT_STATIC],
            'birth_date' => [
                'type' => TabularForm::INPUT_STATIC,
                'format' => 'html',
                'staticValue' => function ($model, $key, $index, $widget) {
                    return ($model->birth_date !== null) ? Yii::t(
                        'app',
                        '{age} {birth_date}',
                        [
                            'birth_date' => Yii::$app->formatter->asDate($model->birth_date),
                            'age' => Html::tag('span', $model->age, ['class' => 'badge badge-primary']),
                        ]
                    ) : null;
                },
            ],
            'membership_type' => [
                'type' => function($model, $key, $index, $widget) {
                    if ($model->is_for_approval || $model->userMembership) {
                        return TabularForm::INPUT_DROPDOWN_LIST;
                    }
                    return TabularForm::INPUT_STATIC;
                },
                'items' => $membershipTypeListData,
                'options' => ['prompt' => Yii::t('app', '-'), 'class' => 'dropdown_membership_type'],
            ],
            'year_initial_joined' => [
                'type' => function($model, $key, $index, $widget) {
                    if ($model->is_for_approval || $model->userMembership) {
                        if ($model->userMembership->is_nonmember) {
                            return TabularForm::INPUT_STATIC;
                        }
                        return TabularForm::INPUT_TEXT;
                    }
                    return TabularForm::INPUT_STATIC;
                },
            ],
            'number_years_active' => [
                'type' => TabularForm::INPUT_STATIC,
                'staticValue' => function($model, $key, $index, $widget) {
                    if ($model->userMembership) {
                        if ($model->userMembership->is_nonmember) {
                            return '';
                        } else {
                            return $model->number_years_active;
                        }
                    }
                    return '';
                },
            ],
            'discount_type' => [
                'type' => TabularForm::INPUT_STATIC,
                'format' => 'memberdiscounttype',
            ],
            'lock_name_on_certificate' => [
                'type' => TabularForm::INPUT_WIDGET,
                'widgetClass' => 'kartik\widgets\SwitchInput',
                'label' => Yii::t('app', 'Certificate on Name'),
                'columnOptions' => ['hAlign' => 'center'],
                'options' => [
                    'pluginOptions' => [
                        'handleWidth' => 20,
                        'onColor' => 'success',
                        'offColor' => 'danger',
                        'onText' => Html::tag('i', '', ['class' => 'fas fa-lock']),
                        'offText' => Html::tag('i', '', ['class' => 'fas fa-unlock']),
                    ],
                    'pluginEvents' => [
                        'switchChange.bootstrapSwitch' => 'submittogglelock',
                    ],
                ],
            ],
            'status' => [
                'type' => function ($model, $key, $index, $widget) {
                    if ($model->is_for_approval) {
                        return TabularForm::INPUT_DROPDOWN_LIST;
                    } else {
                        return TabularForm::INPUT_STATIC;
                    }
                },
                'items' => [
                    User::STATUS_INACTIVE => Yii::t('app', 'Approve'),
                    User::STATUS_FOR_APPROVAL => Yii::t('app', 'No Action'),
                ],
                'format' => function ($model, $key, $index, $widget) {
                    if ($model->is_for_approval) {
                        return null;
                    } else {
                        return 'userstatus';
                    }
                },
                'options' => ['class' => 'dropwnlist_status'],
            ],
        ],
    ]) ?>
    <div class="card">
        <div class="card-body bg-light">
            <div class="d-flex justify-content-end">
                <?= Html::a(Yii::t('app', 'Download Members as CSV'), ['member-download-csv'], ['class' => 'btn btn-warning px-4']) ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<?php
$js = "
function submittogglelock() { var lock_name_on_certificate_id = jQuery(this).closest('tr').data('key'); jQuery('[name=\"lock_name_on_certificate_id\"]').val(lock_name_on_certificate_id); jQuery('[name=\"batch_action\"]').val('toggle_lock'); setTimeout(function() { jQuery('#members-form').submit(); }, 500); }
jQuery('.dropwnlist_status').on('change', function () { var status_id = jQuery(this).closest('tr').data('key'); jQuery('[name=\"status_id\"]').val(status_id); jQuery('[name=\"batch_action\"]').val('toggle_status'); setTimeout(function() { jQuery('#members-form').submit(); }, 500); });
jQuery('.dropdown_membership_type').on('change', function () { var membership_type_id = jQuery(this).closest('tr').data('key'); jQuery('[name=\"membership_type_id\"]').val(membership_type_id); jQuery('[name=\"batch_action\"]').val('change_membership_type'); setTimeout(function() { jQuery('#members-form').submit(); }, 500); });
jQuery('.kv-row-checkbox, .select-on-check-all').on('change', function () { var keys = jQuery('#members-grid').yiiGridView('getSelectedRows').length; jQuery('#selectedRowsCount').html(' ' + keys); });
jQuery('#members-form').on('keypress', function (event) { var keyPressed = event.keyCode || event.which; if (keyPressed === 13) { event.preventDefault(); } });
";
$this->registerJs($js, View::POS_READY);
