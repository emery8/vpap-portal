<?php

use yii\bootstrap4\Html;
use yii\web\View;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $model app\models\MembershipPaymentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="membership-payment-search">
    <div class="card">
        <div class="card-body bg-light">
            <?php $form = ActiveForm::begin([
                'action' => ['membership-payment'],
                'method' => 'get',
            ]); ?>
                <?= Form::widget([
                    'model' => $model,
                    'form' => $form,
                    'columns' => 3,
                    'attributes' => [
                        'complete_name' => [
                            'type' => Form::INPUT_TEXT,
                            'label' => false,
                            'options' => ['placeholder' => $model->getAttributeLabel('complete_name')],
                        ],
                    ],
                ]) ?>
                <div class="form-group mb-0">
                    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                    <?= Html::a(Yii::t('app', 'Reset'), ['membership-payment'], ['class' => 'btn btn-outline-dark']) ?>
                </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>
