<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MembershipPayment */

$this->title = Yii::t('app', 'Update VPAP Order');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Membership Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update VPAP Order');
?>
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="card-body">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>
