<?php

use yii\bootstrap4\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use kartik\dialog\Dialog;

/* @var $this yii\web\View */
/* @var $model app\models\MembershipPayment */

$this->title = Yii::t('app', 'VPAP Fee');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Membership Payments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
Dialog::widget(['libName' => 'krajeeDialog']);
?>
<div class="card">
    <div class="card-header">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <?= DetailView::widget([
                    'model' => $model,
                    'options' => ['class' => 'table table-bordered detail-view'],
                    'template' => '<tr><th{captionOptions} style="width: 40%">{label}</th><td{contentOptions}>{value}</td></tr>',
                    'attributes' => [
                        'type:membershipfee',
                        [
                            'attribute' => 'number_of_year',
                            'visible' => ($model->number_of_year !== null),
                        ],
                        'amount:currency',
                        [
                            'attribute' => 'amount_paid',
                            'label' => Yii::t('app', 'Amount to Pay'),
                            'format' => 'currency',
                        ],
                    ],
                ]) ?>
            </div>
            <div class="col-md-6 col-sm-12">
                <p class="text-muted">
                    <?= Yii::t('app', '{icon} By clicking the button below, you will be automatically redirected to the Dragon Pay website. Please be informed that your {attr1}, {attr2} and {attr3} will be sent together with the other payment details for the order to proceed.', [
                        'icon' => Html::tag(
                            'svg',
                            Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-lightbulb')]),
                            ['class' => 'c-icon']
                        ),
                        'attr1' => Html::tag('code', 'personal name'),
                        'attr2' => Html::tag('code', 'email address'),
                        'attr3' => Html::tag('code', 'professional license number'),
                    ]) ?>
                </p>
                <?php $form = ActiveForm::begin([
                    'action' => ['checkout', 'id' => $model->id],
                ]); ?>
                    <?= Html::button(
                        Yii::t('app', 'Proceed to Dragon Pay'),
                        [
                            'class' => 'btn btn-danger btn-pill',
                            'data' => [
                                'method' => 'post',
                                'confirm' => Yii::t('app', 'Are you sure you want to proceed to the Dragon Pay website?'),
                            ],
                        ]
                    ) ?>
                    <?= Html::a(Yii::t('app', 'Change Order'), ['update', 'id' => $model->id], ['class' => 'btn btn-link']) ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
