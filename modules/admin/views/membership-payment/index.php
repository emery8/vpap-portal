<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\bootstrap4\Modal;
use kartik\grid\GridView;
use app\models\MembershipPayment;
use app\models\RegularMembershipFee;
use app\models\LifetimeMembershipFee;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\MembershipPaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Membership Payments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <?= Html::encode($this->title) ?>
    </div>
    <div class="card-body">
        <div class="mb-3">
            <?= Html::a(
                Yii::t('app', 'Create VPAP Order'),
                ['create'],
                [
                    'class' => 'btn btn-success px-4 btn-create-order',
                    'data' => ['toggle' => 'modal', 'target' => '#create-order-notice']
                ]
            ) ?>
        </div>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            // 'filterModel' => $searchModel,
            'tableOptions' => ['class' => 'table-responsive-sm'],
            'headerRowOptions' => ['class' => 'thead-light'],
            'hover' => true,
            'striped' => false,
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'mergeHeader' => false,
                ],

                [
                    'attribute' => 'type',
                    'format' => 'membershipfee',
                ],
                [
                    'hAlign' => 'center',
                    'attribute' => 'number_of_year',
                ],
                [
                    'hAlign' => 'right',
                    'attribute' => 'amount',
                    'format' => 'currency',

                ],
                [
                    'hAlign' => 'right',
                    'attribute' => 'amount_paid',
                    'format' => 'currency',

                ],
                [
                    'hAlign' => 'center',
                    'attribute' => 'payment_confirmed',
                    'format' => 'raw',
                    'value' => function ($model, $key, $index, $column) {
                        if ($model->payment_confirmed) {
                            return Html::tag(
                                'span',
                                Yii::t('app', 'Paid'),
                                ['class' => 'badge badge-success']
                            );
                        } else {
                            return Html::a(
                                Yii::t('app', 'View Order'),
                                ['summary', 'id' => $model->id],
                                ['class' => 'btn btn-primary']
                            );
                        }
                    },
                ],
            ],
        ]); ?>
    </div>
</div>
<?php Modal::begin([
    'id' => 'create-order-notice',
    'title' => Yii::t('app', 'Create VPAP Order'),
    'options' => ['data' => [
        'backdrop' => 'static',
        'keyboard' => false,
    ]],
    'footer' => Html::button(Yii::t('app', 'Yes'), ['id' => 'btn-yes', 'class' => 'btn btn-primary px-4', 'data' => ['dismiss' => 'modal']]) .
        Html::button(Yii::t('app', 'No'), ['id' => 'btn-no', 'class' => 'btn btn-danger px-4', 'data' => ['dismiss' => 'modal']])
]); ?>
    <p>
        Did the VPAP Officers and Board of Directors advise you to use this payment system to settle Membership Fees?
    </p>
<?php Modal::end(); ?>
<?php
$js = <<<JS
const createOrderLink = jQuery('.btn-create-order').attr('href');
console.log(createOrderLink);
jQuery('#btn-yes').on('click', function() {
    location.href = createOrderLink;
});
JS;
$this->registerJs($js);
