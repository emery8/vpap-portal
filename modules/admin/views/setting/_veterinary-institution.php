<?php

use yii\bootstrap4\Html;
use yii\helpers\StringHelper;

/* @var $model app\models\VeterinaryInstitution */
?>
<div class="">
    <?= Html::a(
        StringHelper::truncate(Html::encode($model->institution), 70),
        ['veterinary-institution/update', 'id' => $model->id]
    ) ?>
</div>