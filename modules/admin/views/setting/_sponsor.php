<?php

use yii\bootstrap4\Html;
use yii\helpers\StringHelper;

/* @var $model app\models\Sponsor */
?>
<div class="">
    <?= Html::a(
        StringHelper::truncate(Html::encode($model->company_name), 180),
        ['sponsor/update', 'id' => $model->id]
    ) ?>
</div>