<?php

use yii\bootstrap4\Html;
use yii\helpers\StringHelper;

/* @var $model app\models\SpeciesOfSpecialization */
?>
<div class="">
    <?= Html::a(
        StringHelper::truncate(Html::encode($model->description), 70),
        ['species-of-specialization/update', 'id' => $model->id]
    ) ?>
</div>