<?php

use yii\bootstrap4\Html;
use yii\helpers\StringHelper;

/* @var $model app\models\TypeOfPractice */
?>
<div class="">
    <?= Html::a(
        StringHelper::truncate(Html::encode($model->description), 70),
        ['type-of-practice/update', 'id' => $model->id]
    ) ?>
</div>