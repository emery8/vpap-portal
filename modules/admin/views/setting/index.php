<?php

use yii\bootstrap4\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/* @var $this yii\web\View */
/* @var $typeOfPracticeDataProvider yii\data\ActiveDataProvider */
/* @var $speciesOfSpecializationDataProvider yii\data\ActiveDataProvider */
/* @var $veterinaryInstitutionDataProvider yii\data\ActiveDataProvider */
/* @var $sponsorDataProvider yii\data\ActiveDataProvider */
/* @var $eventTypeDataProvider yii\data\ActiveDataProvider */
/* @var $eventAttendanceTypeDataProvider yii\data\ActiveDataProvider */
/* @var $membershipTypeDataProvider yii\data\ActiveDataProvider */
/* @var $discountTypeDataProvider yii\data\ActiveDataProvider */
/* @var $regularMembershipFeeModel app\models\RegularMembershipFee */
/* @var $lifetimeMembershipFeeModel app\models\LifetimeMembershipFee */

$this->title = Yii::t('app', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-3">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <?= Yii::t('app', 'Species of Specialization') ?>
                <div>
                    <?= Html::a(
                        Yii::t('app', 'Create'),
                        ['species-of-specialization/create'],
                        ['class' => 'btn btn-ghost-dark btn-sm']
                    ) ?>
                </div>
            </div>
            <div class="card-body">
                <?php Pjax::begin() ?>
                    <?= ListView::widget([
                        'dataProvider' => $speciesOfSpecializationDataProvider,
                        'itemView' => '_species-of-specialization',
                        'layout' => '{summary}<div class="my-3">{items}</div>{pager}',
                        'pager' => ['class' => 'yii\bootstrap4\LinkPager'],
                    ]) ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <?= Yii::t('app', 'Type of Practice') ?>
                <div>
                    <?= Html::a(
                        Yii::t('app', 'Create'),
                        ['type-of-practice/create'],
                        ['class' => 'btn btn-ghost-dark btn-sm']
                    ) ?>
                </div>
            </div>
            <div class="card-body">
                <?php Pjax::begin() ?>
                    <?= ListView::widget([
                        'dataProvider' => $typeOfPracticeDataProvider,
                        'itemView' => '_type-of-practice',
                        'layout' => '{summary}<div class="my-3">{items}</div>{pager}',
                        'pager' => ['class' => 'yii\bootstrap4\LinkPager'],
                    ]) ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <?= Yii::t('app', 'Veterinary Institution') ?>
                <div>
                    <?= Html::a(
                        Yii::t('app', 'Create'),
                        ['veterinary-institution/create'],
                        ['class' => 'btn btn-ghost-dark btn-sm']
                    ) ?>
                </div>
            </div>
            <div class="card-body">
                <?php Pjax::begin() ?>
                    <?= ListView::widget([
                        'dataProvider' => $veterinaryInstitutionDataProvider,
                        'itemView' => '_veterinary-institution',
                        'layout' => '{summary}<div class="my-3">{items}</div>{pager}',
                        'pager' => ['class' => 'yii\bootstrap4\LinkPager'],
                    ]) ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center">
                <?= Yii::t('app', 'Sponsor') ?>
                <div>
                    <?= Html::a(
                        Yii::t('app', 'Create'),
                        ['sponsor/create'],
                        ['class' => 'btn btn-ghost-dark btn-sm']
                    ) ?>
                </div>
            </div>
            <div class="card-body">
                <?php Pjax::begin() ?>
                    <?= ListView::widget([
                        'dataProvider' => $sponsorDataProvider,
                        'itemView' => '_sponsor',
                        'layout' => '{summary}<div class="my-3">{items}</div>{pager}',
                        'pager' => ['class' => 'yii\bootstrap4\LinkPager'],
                    ]) ?>
                <?php Pjax::end() ?>
            </div>
        </div>
    </div>
</div>
<div class="row justify-content-start">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <?= Yii::t('app', 'VPAP Fees') ?>
            </div>
            <div class="card-body">
                <?php $form = ActiveForm::begin(); ?>
                    <?= Form::widget([
                        'model' => $regularMembershipFeeModel,
                        'form' => $form,
                        'attributes' => [
                            'amount' => [
                                'type' => Form::INPUT_TEXT,
                                'options' => [
                                    'placeholder' => $regularMembershipFeeModel->getAttributeLabel('amount'),
                                    'maxlength' => true,
                                ],
                                'columnOptions' => ['colspan' => 3],
                                'fieldConfig' => [
                                    'addon' => [
                                        'prepend' => [
                                            'content' => Yii::$app->formatter->currencyCode,
                                        ]
                                    ]
                                ],
                            ],
                        ],
                    ]) ?>
                    <?= Form::widget([
                        'model' => $lifetimeMembershipFeeModel,
                        'form' => $form,
                        'attributes' => [
                            'amount' => [
                                'type' => Form::INPUT_TEXT,
                                'options' => [
                                    'placeholder' => $lifetimeMembershipFeeModel->getAttributeLabel('amount'),
                                    'maxlength' => true,
                                ],
                                'columnOptions' => ['colspan' => 3],
                                'fieldConfig' => [
                                    'addon' => [
                                        'prepend' => [
                                            'content' => Yii::$app->formatter->currencyCode,
                                        ]
                                    ]
                                ],
                            ],
                        ],
                    ]) ?>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success px-4']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>