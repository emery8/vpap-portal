<?php

namespace app\modules\admin\controllers;

use Yii;
use app\components\DragonPayClient;
use app\components\DragonPayClientv2;
use app\models\Event;
use app\models\EventAttendance;
use app\models\Sponsor;
use app\models\UserMembership;
use app\models\FeaturedImage;
use app\models\ProgramTemplate;
use app\models\InvitationTemplate;
use app\models\AttendanceCertificateTemplate;
use app\models\CpdCertificateTemplate;
use app\models\enums\EventType;
use app\models\enums\EventAttendanceType;
use app\models\enums\PaymentMethod;
use app\models\MyEventAttendanceSearch;
use app\models\UpcomingEventAttendanceSearch;
use app\traits\AjaxValidationTrait;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\bootstrap4\Html;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;

/**
 * EventAttendanceController implements the CRUD actions for EventAttendance model.
 */
class EventAttendanceController extends Controller
{
    use AjaxValidationTrait;

    /**
     * {@inheritDoc}
     */
    public function beforeAction($action)
    {
        if (! Yii::$app->user->isGuest) {
            $identity = Yii::$app->user->identity;
            $session = Yii::$app->session;

            if (! $identity->is_profile_updated) {
                return $this->redirect(['profile/index'])->send();
            }
        }
        return parent::beforeAction($action);
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'create' => ['POST'],
                        'delete' => ['POST'],
                        'attachment-details' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all EventAttendance models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $upcomingEventSearchModel = Yii::createObject(UpcomingEventAttendanceSearch::class);
        $upcomingEventDataProvider = $upcomingEventSearchModel->search($this->request->queryParams);

        $myEventSearchModel = Yii::createObject(MyEventAttendanceSearch::class);
        $myEventDataProvider = $myEventSearchModel->search($this->request->queryParams);

        return $this->render('index', [
            'model' => Yii::createObject(EventAttendance::class),
            'upcomingEventSearchModel' => $upcomingEventSearchModel,
            'upcomingEventDataProvider' => $upcomingEventDataProvider,
            'myEventSearchModel' => $myEventSearchModel,
            'myEventDataProvider' => $myEventDataProvider,
            'eventTypeListData' => EventType::listData(),
            'eventAttendanceTypeListData' => EventAttendanceType::listData(),
            'paymentMethodListData' => PaymentMethod::listData(),
            'sponsorListData' => Sponsor::listData(),
        ]);
    }

    /**
     * Displays a single EventAttendance model.
     * @param string $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EventAttendance model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($slug)
    {
        $identity = Yii::$app->user->identity;
        $member_id = $identity->getId();
        $event = $this->findEvent($slug);

        if ($event->has_reached_maximum_participants) {
            throw new BadRequestHttpException(Yii::t('app', 'The maximum limit of participants has been reached.'));
        }

        if (! Event::canAttendEvent($event->attendance_type)) {
            throw new BadRequestHttpException(Yii::t('app', 'You are not allowed to signup for this event.'));
        }

        $event_attendance = EventAttendance::findOne(['member_id' => $member_id, 'event_id' => $event->id]);
        if ($event_attendance !== null) {
            throw new BadRequestHttpException(Yii::t('app', 'You have already signed up for this event.'));
        }

        $membership = $identity->userMembership;
        $model = Yii::createObject(EventAttendance::class);
        $model->scenario = EventAttendance::SCENARIO_INSERT_ORDER;

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                try {
                    $model->eventModel = $event;
                    $model->membershipModel = $membership;
                    $model->member_id = $member_id;
                    $model->event_id = $event->id;
                    $model->member_type = $membership->type;
                    $model->discount_type = $membership->discount_type;
                    $model->event_fee = $event->getFeeByMembershipType($membership);

                    if ($model->save()) {
                        if ($model->has_checkout_item) {
                            return $this->redirect(['summary', 'slug' => $event->slug]);
                        }
                        return $this->redirect(['index']);
                    }
                } catch (\Exception $e) {
                    throw new BadRequestHttpException($e->getMessage());
                }
            }
        }
    }

    /**
     * Updates an existing EventAttendance model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $slug slug
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($slug)
    {
        $identity = Yii::$app->user->identity;
        $member_id = $identity->getId();
        $event = $this->findEvent($slug);
        if (! Event::canAttendEvent($event->attendance_type)) {
            throw new BadRequestHttpException(Yii::t('app', 'You are not allowed to signup for this event.'));
        }

        $model = $this->findEventAttendance($member_id, $event->id);
        $model->scenario = EventAttendance::SCENARIO_UPDATE_ORDER;

        $membership = $identity->userMembership;
        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                try {
                    $model->eventModel = $event;
                    $model->membershipModel = $membership;
                    $model->member_id = $member_id;
                    $model->event_id = $event->id;
                    $model->member_type = $membership->type;
                    $model->discount_type = $membership->discount_type;
                    $model->event_fee = $event->getFeeByMembershipType($membership);

                    if ($model->save()) {
                        if ($model->has_checkout_item) {
                            return $this->redirect(['summary', 'slug' => $event->slug]);
                        }
                        return $this->redirect(['index']);
                    }
                } catch (\Exception $e) {
                    throw new BadRequestHttpException($e->getMessage());
                }
            }
        }

        return $this->render('update', [
            'event' => $event,
            'model' => $model,
            'eventAttendanceTypeListData' => EventAttendanceType::listData(),
            'paymentMethodListData' => PaymentMethod::listData(),
            'sponsorListData' => Sponsor::listData(),
        ]);
    }

    /**
     * Displays the EventAttendance model summary.
     * @param string $slug slug
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSummary($slug)
    {
        $identity = Yii::$app->user->identity;
        $member_id = $identity->getId();
        $event = $this->findEvent($slug);
        $model = $this->findEventAttendance($member_id, $event->id);

        $membership = $identity->userMembership;
        $model->eventModel = $event;
        $model->membershipModel = $membership;

        return $this->render('summary', [
            'model' => $model,
            'event' => $event,
            'membership' => $membership,
        ]);
    }

    /**
     * Checkout the EventAttendance model order.
     * @param string $slug slug
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCheckout($slug)
    {
        $identity = Yii::$app->user->identity;
        $member_id = $identity->getId();
        $event = $this->findEvent($slug);
        $model = $this->findEventAttendance($member_id, $event->id);

        try {
            $profession = $identity->userProfession;
            $profile = $identity->userProfile;

            $client = new DragonPayClient();
            $client->invoiceno = $profession->license_number;
            $client->name = $profile->complete_name;
            $client->amount = $model->amount_paid;
            $client->ccy = Yii::$app->formatter->currencyCode;
            $client->remarks = $event->title;
            $client->email = $identity->email;

            return $this->redirect($client->payment_link);
        } catch(\InvalidConfigException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
    }

    public function actionCallback()
    {
        echo '<pre>'; print_r($this->request->queryParams); die();
    }

    /**
     * Displays all attachments relating to the EventAttendance model.
     * @return string
     */
    public function actionEventDetails()
    {
        $result = Html::tag('div', Yii::t('app', 'No data found'), ['class' => 'alert alert-danger']);
        if (isset($_POST['expandRowKey'])) {
            $model = EventAttendance::find()
                ->joinWith('event')
                ->where(['{{%event_attendance}}.[[id]]' => $_POST['expandRowKey']])
                ->one();
            
            if (! $model || ! $model->event) {
                return $result;
            }

            $event = $model->event;
            $membership = Yii::$app->user->identity->userMembership;
            $model->eventModel = $event;
            $model->membershipModel = $membership;

            $attachments = $model->event->attachments;
            $featuredImage = null;
            $programTemplate = null;
            $invitationTemplate = null;
            $attendanceCertificateTemplate = null;
            if (is_array($attachments) && count($attachments)) {
                foreach ($attachments as $attachment) {
                    if ($attachment instanceof FeaturedImage) {
                        $featuredImage = $attachment;
                    } else if ($attachment instanceof ProgramTemplate) {
                        $programTemplate = $attachment;
                    } else if ($attachment instanceof InvitationTemplate) {
                        $invitationTemplate = $attachment;
                    } else if ($attachment instanceof AttendanceCertificateTemplate) {
                        $attendanceCertificateTemplate = $attachment;
                    }
                }
            }

            return $this->renderPartial('_event-details', [
                'model' => $model,
                'event' => $model->event,
                'featuredImage' => $featuredImage,
                'programTemplate' => $programTemplate,
                'invitationTemplate' => $invitationTemplate,
                'attendanceCertificateTemplate' => $attendanceCertificateTemplate,
            ]);
        } else {
            return $result;
        }
    }

    /**
     * Deletes an existing EventAttendance model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $slug string
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($slug, $id)
    {
        $event = $this->findEvent($slug);
        $this->findModel($id)->delete();

        return $this->redirect(['event/attendances', 'slug' => $event->slug]);
    }

    /**
     * Displays the event program document.
     * @param int $slug string
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionProgram($slug)
    {
        $event = $this->findEvent($slug);
        if (($model = ProgramTemplate::findOne(['event_id' => $event->id])) === null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        $path_to_file = Yii::getAlias('@webroot' . $model->getUploadUrl('content'));
        return Yii::$app->response->sendFile($path_to_file);
    }

    /**
     * Displays the event invitation document.
     * @param int $slug string
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionInvitation($slug)
    {
        $event = $this->findEvent($slug);
        if (($model = InvitationTemplate::findOne(['event_id' => $event->id])) === null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        $path_to_file = Yii::getAlias('@webroot' . $model->getUploadUrl('content'));
        return Yii::$app->response->sendFile($path_to_file);
    }

    /**
     * Displays the attendance certificate of the member.
     * @param int $slug string
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAttendanceCertificate($slug)
    {
        $identity = Yii::$app->user->identity;
        $member_id = $identity->getId();
        $event = $this->findEvent($slug);

        if (($model = EventAttendance::find()
            ->joinWith(['member' => function ($query) {
                $query->joinWith(['userProfile']);
            }])
            ->andWhere(['member_id' => $member_id])
            ->andWhere(['event_id' => $event->id])
            ->andWhere(['attended' => 1])
            ->one()
        ) === null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        if (($certificate_template = AttendanceCertificateTemplate::findOne(['event_id' => $event->id])) === null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        $profile = $model->member->userProfile;
        $title = $event->title;
        $complete_name = $profile->name_on_certificate;
        $cpd = Yii::t('app', '{cpd} CPD Units Earned', [
            'cpd' => $model->cpd_point_earned,
        ]);
        $app_name = Yii::$app->name;
        $filename = Yii::t('app', '{name} - {title}', [
            'name' => $profile->complete_name,
            'title' => $event->title,
        ]);

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $pdf = new Pdf([
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            'filename' => $filename,
            'marginLeft' => 0,
            'marginRight' => 0,
            'marginTop' => 0,
            'marginBottom' => 0,
            'marginHeader' => 0,
            'marginFooter' => 0,
            'options' => [
                'fontDir' => array_merge($fontDirs, [
                    Yii::getAlias('@app/fonts'),
                ]),
                'fontdata' => $fontData + [
                    'copperplate' => [
                        'R' => 'Copperplate.ttf',
                        'B' => 'CopperplateBold.ttf',
                    ],
                ],
                'default_font' => 'copperplate',
            ],
            'methods' => [
                'SetTitle' => $filename,
                'SetSubject' => $filename,
                'SetAuthor' => $app_name,
                'SetCreator' => $app_name,
                'SetKeywords' => $event->title,
            ],
        ]);

        $mpdf = $pdf->api;
        $mpdf->SetDefaultBodyCSS('background', 'url("' . Yii::getAlias('@webroot' . $certificate_template->getUploadUrl('content')) . '")');
        $mpdf->SetDefaultBodyCSS('background-image-resize', 6);
        $mpdf->AddPage();

        $mpdf->SetFont('copperplate', 'B', 40);
        $mpdf->SetXY(0, 93);
        $mpdf->WriteCell(0, 0, $complete_name, 0, 0, 'C');

        if ($model->cpd_point_earned > 0) {
            $mpdf->SetDrawColor(0,128,255);
            $mpdf->SetFillColor(0,128,255);
            $mpdf->SetTextColor(255,255,255);
            $mpdf->SetFont('copperplate', 'B', 16);
            $mpdf->SetXY(-92, -10);
            $mpdf->WriteCell(90, 8, $cpd, 1, 0, 'C', true);
        }

        return $pdf->render();
    }

    /**
     * Displays the CPD certificate of the member.
     * @param int $slug string
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCpdCertificate($slug)
    {
        $identity = Yii::$app->user->identity;
        $member_id = $identity->getId();
        $event = $this->findEvent($slug);

        $model = EventAttendance::find()
            ->joinWith(['member' => function ($query) {
                $query->joinWith(['userProfile']);
            }])
            ->andWhere(['member_id' => $member_id])
            ->andWhere(['event_id' => $event->id])
            ->andWhere(['attended' => 1])
            ->one();


        if (($model === null) && ($model->cpd_point_earned > 0)) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        if (($certificate_template = CpdCertificateTemplate::findOne(['event_id' => $event->id])) === null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        $profile = $model->member->userProfile;
        $title = $event->title;
        $complete_name = $profile->name_on_certificate;
        $cpd = Yii::t('app', '{cpd}', [
            'cpd' => $model->cpd_point_earned,
        ]);
        $app_name = Yii::$app->name;
        $filename = Yii::t('app', '{name} - {title}', [
            'name' => $profile->complete_name,
            'title' => $event->title,
        ]);

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $pdf = new Pdf([
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'filename' => $filename,
            'marginLeft' => 0,
            'marginRight' => 0,
            'marginTop' => 0,
            'marginBottom' => 0,
            'marginHeader' => 0,
            'marginFooter' => 0,
            'options' => [
                'default_font' => 'arial',
            ],
            'methods' => [
                'SetTitle' => $filename,
                'SetSubject' => $filename,
                'SetAuthor' => $app_name,
                'SetCreator' => $app_name,
                'SetKeywords' => $event->title,
            ],
        ]);

        $mpdf = $pdf->api;
        $mpdf->SetDefaultBodyCSS('background', 'url("' . Yii::getAlias('@webroot' . $certificate_template->getUploadUrl('content')) . '")');
        $mpdf->SetDefaultBodyCSS('background-image-resize', 6);
        $mpdf->AddPage();

        $complete_name_len = strlen($complete_name);
        $mpdf->SetFont('arial', 'B', 30);
        if ($complete_name_len <= 30) {
            $mpdf->SetXY(24, 89);
        } else {
            $mpdf->SetXY(24, 79);
        }
        $mpdf->MultiCell(160, 10, $complete_name, 0, 'C');

        if ($model->cpd_point_earned > 0) {
            $mpdf->SetFont('arial', 'B', 16);
            $mpdf->SetXY(0, 215);
            $mpdf->WriteCell(0, 0, $cpd, 0, 0, 'C');
        }

        return $pdf->render();
    }

    /**
     * Validate model via AJAX method.
     * @return \yii\web\Response
     */
    public function actionAjaxValidate($slug)
    {
        $event = $this->findEvent($slug);
        $model = Yii::createObject(EventAttendance::class);
        $model->setAttributes([
            'member_id' => Yii::$app->user->identity->getId(),
            'event_id' => $event->id,
        ]);

        $this->performAjaxValidation($model);
    }

    /**
     * Finds the EventAttendance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id ID
     * @return EventAttendance the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EventAttendance::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug ID
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findEvent($slug)
    {
        if (($model = Event::findOne(['slug' => $slug])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Finds the EventAttendance model based on UserMembership and Event IDs.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $memberId ID
     * @param int $eventId ID
     * @return EventAttendance the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findEventAttendance($memberId, $eventId)
    {
        if (($model = EventAttendance::findOne(['member_id' => $memberId, 'event_id' => $eventId])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
