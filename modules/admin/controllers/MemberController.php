<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\EventAttendance;
use app\models\MembershipActivity;
use app\models\Officer;
use app\models\User;
use app\models\UserMembership;
use app\models\MembershipPayment;
use app\modules\admin\models\MembershipActivityForm;
use app\modules\admin\models\MemberSearch;
use app\modules\admin\models\OfficerSearch;
use app\modules\admin\models\MembershipActivitySearch;
use app\modules\admin\models\MembershipPaymentSearch;
use app\modules\admin\models\MemberEventAttendanceSearch;
use app\models\enums\EventType;
use app\models\enums\MembershipType;
use app\models\enums\MemberDiscountType;
use yii\base\Model;
use yii\bootstrap4\Html;
use yii\data\ArrayDataProvider;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii2tech\csvgrid\CsvGrid;

/**
 * MemberController implements the CRUD actions for User model.
 */
class MemberController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                        'run-query' => ['POST'],
                        'download-event-attendances' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all User models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = Yii::createObject(MemberSearch::class);
        $dataProvider = $searchModel->search($this->request->queryParams);

        if ($this->request->isPost) {
            $models = $dataProvider->getModels();
            $batchAction = $this->request->post('batch_action');
            $selection = $this->request->post('selection');
            $affectedRows = 0;
            if (Model::loadMultiple($models, $this->request->post()) && Model::validateMultiple($models)) {
                $session = Yii::$app->session;
                switch ($batchAction) {
                    case 'delete':
                        $affectedRows = 0;
                        if (is_array($selection) && count($selection)) {
                            $users = User::find()->where(['in', 'id', $selection]);
                            foreach ($users->each(10) as $user) {
                                $user->delete();
                                $affectedRows++;
                            }

                            $session->setFlash('info', Yii::t('app', '{count,plural,=0{No member has been deleted.} =1{Deleted one member.} other{Deleted # members.}}', ['count' => $affectedRows]));
                        } else {
                            $session->setFlash('warning', Yii::t('app', 'No member has been deleted.'));
                        }
                        break;
                    case 'update':
                        $affectedRows = 0;
                        if (isset($_POST['members-form'])) {
                            if (isset($_POST['members-form']['membership_type']) && ! empty($_POST['members-form']['membership_type'])) {
                                $affectedRows = UserMembership::batchUpdateMembershipType($selection, $_POST['members-form']['membership_type']);
                            } else {
                                foreach ($models as $model) {
                                    if (isset($_POST['User'][$model->id])) {
                                        $membership_type = $model->membership_type;
                                        $year_initial_joined = $model->year_initial_joined;

                                        if (isset($_POST['User'][$model->id]['membership_type'])) {
                                            $membership_type = $_POST['User'][$model->id]['membership_type'];
                                        }

                                        if (isset($_POST['User'][$model->id]['year_initial_joined'])) {
                                            $year_initial_joined = $_POST['User'][$model->id]['year_initial_joined'];
                                        }

                                        $status = $model->status;
                                        if (isset($_POST['User'][$model->id]['status'])) {
                                            $status = $_POST['User'][$model->id]['status'];
                                        }

                                        $membership = $model->userMembership;
                                        $membership->scenario = UserMembership::SCENARIO_UPDATE;
                                        $membership->type = $membership_type;
                                        $membership->year_initial_joined = $year_initial_joined;
                                        $model->status = $status;
                                        if (($model->update() > 0) || ($membership->update() > 0)) {
                                            $affectedRows++;
                                        }
                                    }
                                }
                            }
                        }

                        $session->setFlash('info', Yii::t('app', '{count,plural,=0{No member has been updated.} =1{Updated one member.} other{Updated # members.}}', ['count' => $affectedRows]));
                        break;
                    case 'toggle_lock':
                        $affectedRows = 0;
                        $status = '';
                        $email = '';

                        if (isset($_POST['lock_name_on_certificate_id'])) {
                            $lock_name_on_certificate_id = $_POST['lock_name_on_certificate_id'];
                            if (isset($_POST['User'][$lock_name_on_certificate_id]) && isset($models[$lock_name_on_certificate_id])) {
                                $lock_name_on_certificate = $_POST['User'][$lock_name_on_certificate_id]['lock_name_on_certificate'];
                                $status = $lock_name_on_certificate ? 'locked' : 'unlocked';
                                $model = $models[$lock_name_on_certificate_id];
                                if ($model->update() > 0) {
                                    $email = $model->email;
                                    $affectedRows++;
                                }
                            }
                        }

                        if ($affectedRows) {
                            $session->setFlash('success', Yii::t(
                                'app',
                                'You have {status} the changing of {attribute} of {email}.',
                                [
                                    'status' => Html::tag('strong', $status),
                                    'attribute' => Html::tag('code', 'name_on_certificate'),
                                    'email' => Html::encode($email),
                                ]
                            ));
                        }
                        break;
                    case 'toggle_status':
                        $affectedRows = 0;
                        $status = '';
                        $email = '';

                        if (isset($_POST['status_id'])) {
                            $status_id = $_POST['status_id'];
                            if (isset($_POST['User'][$status_id]) && isset($models[$status_id])) {
                                $status = $_POST['User'][$status_id]['status'];

                                if (! in_array($status, [
                                    User::STATUS_INACTIVE,
                                    User::STATUS_FOR_APPROVAL,
                                ])) {
                                    $session->setFlash('danger', 'The status is invalid.');
                                    break;
                                }

                                $model = $models[$status_id];
                                if ($model->update() > 0) {
                                    $email = $model->email;
                                    $affectedRows++;

                                    if ($model->is_inactive) {
                                        $model->sendEmail();
                                    }
                                }
                            }
                        }

                        if ($affectedRows) {
                            $session->setFlash('success', Yii::t(
                                'app',
                                'An email has been sent to {email} containing instructions on activating the account.',
                                [
                                    'email' => Html::encode($email),
                                ]
                            ));
                        }

                        break;
                    case 'change_membership_type':
                        $affectedRows = 0;
                        $email = '';
                        if (isset($_POST['membership_type_id'])) {
                            $membership_type_id = $_POST['membership_type_id'];
                            if (isset($_POST['User'][$membership_type_id]) && isset($models[$membership_type_id])) {
                                $membership_type = $_POST['User'][$membership_type_id]['membership_type'];
                                $membership_year_initial_joined = isset($_POST['User'][$membership_type_id]['year_initial_joined']) ?
                                    $_POST['User'][$membership_type_id]['year_initial_joined'] : null;
                                $model = $models[$membership_type_id];
                                $membership = $model->userMembership;
                                $membership->scenario = UserMembership::SCENARIO_UPDATE;
                                $membership->type = $membership_type;
                                $membership->year_initial_joined = $membership_year_initial_joined;
                                if ($membership->update() > 0) {
                                    $email = $model->email;
                                    $affectedRows++;
                                }
                            }
                        }

                        if ($affectedRows) {
                            $session->setFlash('success', Yii::t(
                                'app',
                                'The {attribute} of {email} has been updated.',
                                [
                                    'attribute' => Html::tag('code', 'membership_type'),
                                    'email' => Html::encode($email),
                                ]
                            ));
                        }
                        break;
                }
            }

            return $this->refresh();
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'membershipTypeListData' => MembershipType::listData(),
            'memberDiscountTypeListData' => MemberDiscountType::listData(),
            'statusListData' => User::statusListData(),
        ]);
    }

    /**
     * Lists all Officer models.
     *
     * @return string
     */
    public function actionOfficer()
    {
        $searchModel = Yii::createObject(OfficerSearch::class);
        $dataProvider = $searchModel->search($this->request->queryParams);

        if ($this->request->isPost) {
            $models = $dataProvider->getModels();
            $batchAction = $this->request->post('batch_action');
            $selection = $this->request->post('selection');

            if (Model::loadMultiple($models, $this->request->post()) && Model::validateMultiple($models)) {
                $session = Yii::$app->session;
                $affectedRows = 0;

                switch ($batchAction) {
                    case 'delete':
                        if (is_array($selection) && count($selection)) {
                            $officers = Officer::find()->where(['in', 'id', $selection]);
                            foreach ($officers->each(10) as $officer) {
                                $officer->delete();
                            }
                        } else {
                            $session->setFlash('warning', Yii::t('app', 'No record has been deleted.'));
                        }
                        break;
                    case 'update':
                        foreach ($models as $index => $model) {
                            if ($model->update() > 0) {
                                $affectedRows++;
                            }
                        }
                        $session->setFlash('info', Yii::t('app', '{count,plural,=0{No officer has been updated.} =1{Updated one officer.} other{Updated # officers.}}', ['count' => $affectedRows]));
                        break;
                }
            }

            return $this->refresh();
        }

        return $this->render('officer', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'yearServedRange' => Officer::getYearServedRange(),
        ]);
    }

    /**
     * Lists all MembershipActivity models.
     *
     * @return string
     */
    public function actionMembershipActivity()
    {
        $searchModel = Yii::createObject(MembershipActivitySearch::class);
        $dataProvider = $searchModel->search($this->request->queryParams);
        $model = Yii::createObject(MembershipActivityForm::class);
        $model->event_type = [
            EventType::ANNUAL_CONFERENCE,
        ];
        $model->membership_type = [
            MembershipType::LIFETIME,
            MembershipType::REGULAR,
        ];

        if ($this->request->isPost) {
            $models = $dataProvider->getModels();
            $batchAction = $this->request->post('batch_action');
            $selection = $this->request->post('selection');

            if (Model::loadMultiple($models, $this->request->post()) && Model::validateMultiple($models)) {
                $session = Yii::$app->session;
                $affectedRows = 0;

                switch ($batchAction) {
                    case 'delete':
                        if (is_array($selection) && count($selection)) {
                            $membershipActivities = MembershipActivity::find()->where(['in', 'id', $selection]);
                            foreach ($membershipActivities->each(10) as $membershipActivity) {
                                $membershipActivity->delete();
                            }
                        } else {
                            $session->setFlash('warning', Yii::t('app', 'No record has been deleted.'));
                        }
                        break;
                    case 'update':
                        foreach ($models as $model) {
                            $model->setAttribute('year_membership_paid', $model->year_membership_paid ? $model->year_membership_paid : null);

                            $membership = UserMembership::findOne(['user_id' => $model->member_id]);
                            if ($membership) {
                                $attendances = EventAttendance::find()
                                    ->select(['{{%event}}.*', '{{%event_attendance}}.*', 'aggregate_start_date' => new Expression('year({{%event}}.[[start_date]])')])
                                    ->joinWith(['event'])
                                    ->andWhere(['in', '{{%event}}.[[type]]', [EventType::ANNUAL_CONFERENCE]])
                                    ->andWhere(['[[attended]]' => 1])
                                    ->andWhere(['[[member_id]]' => $model->member_id])
                                    ->andWhere(['>=', new Expression('year({{%event}}.[[start_date]])'), $membership->year_initial_joined])
                                    ->all();
                                if (is_array($attendances) && count($attendances)) {
                                    $years_attended = ArrayHelper::getColumn($attendances, 'aggregate_start_date');
                                    sort($years_attended);
                                    $model->setAttribute('year_event_attended', $years_attended);
                                }
                            }
                            
                            if ($model->update() > 0) {
                                $affectedRows++;
                            }
                        }
                        $session->setFlash('info', Yii::t('app', '{count,plural,=0{No membership activity has been updated.} =1{Updated one membership activity.} other{Updated # membership activities.}}', ['count' => $affectedRows]));
                        break;
                }
            }

            return $this->refresh();
        }

        return $this->render('membership-activity', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
            'membershipTypeListData' => MembershipType::listData(),
            'eventTypeListData' => EventType::listData(),
            'yearServedRange' => MembershipActivity::getYearServedRange(),
        ]);
    }

    public function actionMembershipPayment()
    {
        $searchModel = Yii::createObject(MembershipPaymentSearch::class);
        $dataProvider = $searchModel->search($this->request->queryParams);

        if ($this->request->isPost) {
            $models = $dataProvider->getModels();
            $batchAction = $this->request->post('batch_action');
            $selection = $this->request->post('selection');

            if (Model::loadMultiple($models, $this->request->post()) && Model::validateMultiple($models)) {
                $session = Yii::$app->session;
                $affectedRows = 0;

                switch ($batchAction) {
                    case 'delete':
                        if (is_array($selection) && count($selection)) {
                            $membershipActivities = MembershipActivity::find()->where(['in', 'id', $selection]);
                            foreach ($membershipActivities->each(10) as $membershipActivity) {
                                $membershipActivity->delete();
                            }
                        } else {
                            $session->setFlash('warning', Yii::t('app', 'No record has been deleted.'));
                        }
                        break;
                    case 'update':
                        foreach ($models as $model) {
                            $model->setAttribute('year_membership_paid', $model->year_membership_paid ? $model->year_membership_paid : null);
                            if ($model->update() > 0) {
                                $affectedRows++;
                            }
                        }
                        $session->setFlash('info', Yii::t('app', '{count,plural,=0{No membership activity has been updated.} =1{Updated one membership activity.} other{Updated # membership activities.}}', ['count' => $affectedRows]));
                        break;
                    case 'togglepaymentconfirmed':
                        $affectedRows = 0;
                        $status = '';
                        $email = '';

                        if (isset($_POST['payment_confirmed_id'])) {
                            $payment_confirmed_id = $_POST['payment_confirmed_id'];
                            if (isset($_POST['MembershipPayment'][$payment_confirmed_id]) && $models[$payment_confirmed_id]) {
                                $payment_confirmed = $_POST['MembershipPayment'][$payment_confirmed_id]['payment_confirmed'];
                                $status = $payment_confirmed ? 'verified' : 'reverted to pending';
                                $model = $models[$payment_confirmed_id];

                                if ($model->update() > 0) {
                                    $affectedRows++;
                                    $email = $model->member->email;
                                }
                            }
                        }

                        if ($affectedRows) {
                            $session->setFlash('success', Yii::t(
                                'app',
                                'You have {status} the payment of {email}.',
                                [
                                    'status' => Html::tag('strong', $status),
                                    'email' => Html::encode($email),
                                ]
                            ));
                        }
                        break;
                }
            }

            return $this->refresh();
        }

        return $this->render('membership-payment', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchModel = Yii::createObject(MemberEventAttendanceSearch::class);
        $dataProvider = $searchModel->search($this->request->queryParams);

        $model = User::find()
            ->joinWith([
                'userEducation',
                'userEmployment',
                'userMembership',
                'userProfession',
                'userProfile',
            ])
            ->id($id)
            ->one();

        if (! $model) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Downloads related EventAttendances model as CSV format.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDownloadEventAttendances($id)
    {
        $model = $this->findModel($id);
        $exporter = new CsvGrid([
            'dataProvider' => new ArrayDataProvider([
                'allModels' => EventAttendance::downloadAsCsvByMemberId($model->id),
            ]),
            'columns' => [
                ['attribute' => 'event_type'],
                ['attribute' => 'event_title'],
                ['attribute' => 'event_start_date'],
                ['attribute' => 'event_end_date'],
                [
                    'attribute' => 'cpd_point_earned',
                    'format' => 'decimal',
                ],
                [
                    'attribute' => 'attended',
                    'format' => 'boolean',
                ],
            ],
        ]);

        return $exporter->export()->send($model->userProfile->complete_name . '.csv');
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load($this->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'The user has been successfully updated.'));
            return $this->redirect(['update', 'id' => $model->id]);
        }

        $model->password_hash = '';

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * List members
     *
     * @param string|null $q
     * @param string|null $id
     *
     * @return Response
     */
    public function actionMemberList($q = null, $id = null)
    {
        $this->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (! is_null($q)) {
            $query = (new Query())
                ->select(['id' => '[[user_id]]', 'text' => (new Expression('CONCAT([[first_name]], " ", [[last_name]])'))])
                ->from('{{%user_profile}}')
                ->where(['or', ['like', '[[first_name]]', $q], ['like', '[[last_name]]', $q]])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } else if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => UserProfile::findOne(['user_id' => $id])->complete_name];
        }

        $this->response->data = $out;

        $this->response->send();
        Yii::$app->end();
    }

    public function actionAddMembershipPayment()
    {
        $model = Yii::createObject(MembershipPayment::class);

        if (! $model->regular_membership || ! $model->lifetime_membership) {
            throw new BadRequestHttpException(Yii::t('app', 'The VPAP membership fee setting is not set.'));
        }

        if ($this->request->isPost) {
            $model->payment_confirmed = 1;
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['membership-payment']);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('add-membership-payment', [
            'model' => $model,
        ]);
    }

    public function actionMemberDownloadCsv()
    {
        $exporter = new CsvGrid([
            'dataProvider' => new ArrayDataProvider([
                'allModels' => User::downloadAsCsv(),
            ]),
            'columns' => [
                ['attribute' => 'email'],
                ['attribute' => 'first_name'],
                ['attribute' => 'middle_name'],
                ['attribute' => 'last_name'],
                ['attribute' => 'birth_date'],
                ['attribute' => 'mobile_number'],
                ['attribute' => 'license_number'],
                ['attribute' => 'expiration_date'],
                ['attribute' => 'membership_type'],
                ['attribute' => 'number_years_active'],
                ['attribute' => 'year_initial_joined'],
            ],
        ]);

        return $exporter->export()->send('vpap-members.csv');
    }

    /**
     * Populates the MembershipActivity.
     *
     * @return \yii\web\Response
     */
    public function actionRunQuery()
    {
        $model = Yii::createObject(MembershipActivityForm::class);
        if ($model->load($this->request->post()) && $model->validate()) {
            try {
                $session = Yii::$app->session;
                $affectedRows = 0;
                $affectedRows = $model->populate();
                $session->setFlash('info', Yii::t('app', '{count,plural,=0{No member has been imported.} =1{Imported one member.} other{Imported # members.}}', ['count' => $affectedRows]));
            } catch (\Exception $e) {
                throw new BadRequestHttpException($e->getMessage());
            }
        }
        return $this->redirect(['membership-activity']);
        // try {
        //     $session = Yii::$app->session;
        //     $affectedRows = MembershipActivity::populateTable([MembershipType::REGULAR, MembershipType::LIFETIME], EventType::ANNUAL_CONFERENCE);
        //     $session->setFlash('info', Yii::t('app', '{count,plural,=0{No member has been imported.} =1{Imported one member.} other{Imported # members.}}', ['count' => $affectedRows]));
        //     return $this->redirect(['membership-activity']);
        // } catch (\Exception $e) {
        //     throw $e;
        //     throw new BadRequestHttpException($e->getMessage());
        // }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::find()
            ->joinWith(['userProfile'])
            ->id($id)
            ->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
