<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\User;
use app\models\UserEmployment;
use app\models\UserEducation;
use app\models\UserMembership;
use app\models\UserProfile;
use app\models\UserProfession;
use app\models\UploadPhotoForm;
use app\models\SpeciesOfSpecialization;
use app\models\TypeOfPractice;
use app\models\VeterinaryInstitution;
use app\models\Municipality;
use app\models\Province;
use app\models\Region;
use app\models\Country;
use app\models\ChangePasswordForm;
use yii\bootstrap4\Html;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
/**
 * ProfileController implements the CRUD actions for UserProfile model.
 */
class ProfileController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::class,
                    'actions' => [
                        'upload-photo' => ['POST'],
                        'delete-photo' => ['POST'],
                        'display-photo' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $id = Yii::$app->user->getId();
        $user = $this->findModel($id);
        $profile = $user->userProfile;
        $employment = $user->userEmployment;
        $education = $user->userEducation;
        $profession = $user->userProfession;
        $membership = $user->userMembership;
        $pluginOptions = [];

        if ($profile === null) {
            $profile = Yii::createObject(UserProfile::class);
            $profile->link('user', $user);
        }
        if ($employment === null) {
            $employment = Yii::createObject(UserEmployment::class);
            $employment->link('user', $user);
        }
        if ($education === null) {
            $education = Yii::createObject(UserEducation::class);
            $education->link('user', $user);
        }
        if ($profession === null) {
            $profession = Yii::createObject(UserProfession::class);
            $profession->link('user', $user);
        }
        if ($membership === null) {
            $membership = Yii::createObject(UserMembership::class);
            $membership->link('user', $user);
        }

        $user->scenario = User::SCENARIO_UPDATE;
        $profile->scenario = UserProfile::SCENARIO_UPDATE;
        $profile->scenario = UserProfile::SCENARIO_UPDATE;
        $profession->scenario = UserProfession::SCENARIO_UPDATE;
        $employment->scenario = UserEmployment::SCENARIO_UPDATE;
        $education->scenario = UserEducation::SCENARIO_UPDATE;

        if ($this->request->isPost) {
            $user->photo = UploadedFile::getInstance($user, 'photo');
            if ($user->load($this->request->post()) &&
                $profile->load($this->request->post()) &&
                $employment->load($this->request->post()) &&
                $education->load($this->request->post()) &&
                $profession->load($this->request->post())
            ) {
                $isValid = $user->validate();
                $isValid = $profile->validate() && $isValid;
                $isValid = $employment->validate() && $isValid;
                $isValid = $education->validate() && $isValid;
                $isValid = $profession->validate() && $isValid;

                if ($isValid) {
                    $user->userProfileHasUpdated();
                    $user->save(false);
                    $profile->save(false);
                    $employment->save(false);
                    $education->save(false);
                    $profession->save(false);

                    Yii::$app->session->removeFlash('danger');

                    $pluginOptions = [
                        'initialPreview' => [
                            Html::img($user->getThumbUploadUrl('photo', 'preview')),
                        ],
                        'initialPreviewAsData' => true,
                        'initialPreviewConfig' => [
                            [
                                'caption' => $user->getCaption('photo'),
                                'size' => $user->getSize('photo'),
                                'key' => $user->id,
                                'url' => Url::toRoute(['delete-photo']),
                            ],
                        ],
                    ];

                    return $this->redirect(['index']);
                }
            }
        }

        return $this->render('index', [
            'user' => $user,
            'profile' => $profile,
            'employment' => $employment,
            'education' => $education,
            'profession' => $profession,
            'membership' => $membership,
            'typeOfPracticeListData' => TypeOfPractice::listData(),
            'speciesOfSpecializationListData' => SpeciesOfSpecialization::listData(),
            'veterinaryInstitutionListData' => VeterinaryInstitution::listData(),
            'countryListData' => Country::listData(),
            'pluginOptions' => $pluginOptions,
        ]);
    }

    /**
     * Uploads the user avatar.
     * @return yii\web\Response
     */
    public function actionUploadPhoto()
    {
        if (! Yii::$app->request->isAjax) {
            throw new BadRequestHttpException(Yii::t('app', 'The request method is not allowed.'));
        }

        $id = Yii::$app->user->id;
        $user = $this->findModel($id);
        $user->scenario = User::SCENARIO_UPDATE;
        $user->photo = UploadedFile::getInstance($user, 'photo');
        // var_dump($user->scenario); die();
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $response->data = [];
        if ($user->save()) {
            $response->data = [
                'initialPreview' => [
                    Html::img($user->getUploadUrl('photo'), ['class' => 'file-preview-image'])
                ],
                'initialPreviewConfig' => [
                    [
                        'caption' => 'Hello',
                        'width' => '120px',
                        'url' => Url::to(['delete-photo']),
                    ],
                ],
            ];
        } else {
            $response->data = ['error' => Yii::t('app', 'Error!')];
        }

        $response->send();
        Yii::$app->end();
    }

    /**
     * Deletes the user avatar.
     * @return yii\web\Response
     */
    public function actionDeletePhoto()
    {
        if (! Yii::$app->request->isAjax) {
            throw new BadRequestHttpException(Yii::t('app', 'The request method is not allowed.'));
        }

        $id = Yii::$app->user->id;
        $user = $this->findModel($id);

        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $photo = $user->photo;
        $photoPath = FileHelper::normalizePath(Yii::getAlias('@webroot') . '/' . $photo);
        if (! $photo || ! file_exists($photoPath)) {
            $response->data = ['error' => Yii::t('app', 'Error!')];
        }

        $profile->photo = null;
        if (! $profile->save()) {
            $response->data = ['error' => Yii::t('app', 'Error!')];
        } else {
            FileHelper::unlink($photoPath);

            $thumbnail = generateThumbnailName($photo);
            $thumbnailFile = FileHelper::normalizePath(Yii::getAlias('@webroot') . '/' . $thumbnail);
            if (file_exists($thumbnailFile)) {
                FileHelper::unlink($thumbnailFile);
            }

            $response->data = [];
        }

        $response->send();
        Yii::$app->end();
    }

    /**
     * Displays the user avatar.
     * @return yii\web\Response
     */
    public function actionDisplayPhoto()
    {
        if (! Yii::$app->request->isAjax) {
            throw new BadRequestHttpException(Yii::t('app', 'The request method is not allowed.'));
        }

        $id = Yii::$app->user->id;
        $user = $this->findModel($id);

        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;
        $photo = $user->photo;
        $thumbnail = generateThumbnailName($photo);

        $response->data = ['photo' => $thumbnail];
        $response->send();
        Yii::$app->end();
    }

    /**
     * Allows member to change password value.
     * @return string
     */
    public function actionChangePassword()
    {
        $model = Yii::createObject(ChangePasswordForm::class);

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->changePassword()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'You have successfully changed your password.'));
                return $this->redirect(['index']);
            }
        }

        $model->old_password = '';
        $model->new_password = '';
        $model->new_password_repeat = '';
        return $this->render('change-password', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionRegion()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $parent = $parents[0];
                if ($parent === 'Philippines') {
                    $out = Region::listData();
                    return [
                        'output' => $out['output'],
                        'selected' => $out['selected'],
                    ];
                } else {
                    $out = Region::asOverseas();
                    return [
                        'output' => $out,
                        'selected' => ['selected' => 'Overseas'],
                    ];
                }
            }
        }
        return [
            'output' => '',
            'selected' => '',
        ];
    }

    /**
     * @return string
     */
    public function actionProvince()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $parent = $parents[0];
                if ($parent === 'Overseas') {
                    $out = Province::asOverseas();
                    return [
                        'output' => $out,
                        'selected' => ['selected' => 'Overseas'],
                    ];
                }
                if (($region = Region::findOne(['name' => $parent]))) {
                    $out = Province::listData($region->id);
                    return [
                        'output' => $out['output'],
                        'selected' => $out['selected'],
                    ];
                }
            }
        }
        return [
            'output' => '',
            'selected' => '',
        ];
    }

    /**
     * @return string
     */
    public function actionMunicipality()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $parent = $parents[0];
                if ($parent === 'Overseas') {
                    $out = Municipality::asOverseas();
                    return [
                        'output' => $out,
                        'selected' => ['selected' => 'Overseas'],
                    ];
                }
                if (($province = Province::findOne(['name' => $parent]))) {
                    $out = Municipality::listData($province->id);
                    return [
                        'output' => $out['output'],
                        'selected' => $out['selected'],
                    ];
                }
            }
        }
        return [
            'output' => '',
            'selected' => '',
        ];
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::find()
            ->joinWith(['userProfile', 'userEmployment', 'userProfession', 'userMembership'])
            ->id($id)
            ->one()
        ) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
