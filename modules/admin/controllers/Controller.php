<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\bootstrap4\Html;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class Controller extends \yii\web\Controller
{
    /**
     * {@inheritDoc}
     */
    public function beforeAction($action)
    {
        if (! Yii::$app->user->isGuest) {
            $identity = Yii::$app->user->identity;
            $session = Yii::$app->session;

            if (! $identity->is_profile_updated) {
                $session->setFlash('danger', Yii::t(
                    'app',
                    '{icon} Welcome to {appname}! Before you proceed, click {link} and update your profile first.',
                    [
                        'icon' => Html::tag(
                            'svg',
                            Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-bell')]),
                            ['class' => 'c-icon']
                        ),
                        'appname' => Yii::$app->name,
                        'link' => Html::a('here', ['profile/index']),
                    ]
                ));
            }
        }
        return parent::beforeAction($action);
    }

    /**
     * {@inheritDoc}
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::class,
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['admin'],
                        ],
                    ],
                ],
            ]
        );
    }
}