<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Officer;
use app\models\UserProfile;
use app\modules\admin\models\OfficerSearch;
use yii\base\Model;
use yii\db\Expression;
use yii\db\Query;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OfficerController implements the CRUD actions for Officer model.
 */
class OfficerController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * List members
     *
     * @param string|null $q
     * @param string|null $id
     * 
     * @return Response
     */
    public function actionMemberList($q = null, $id = null)
    {
        $this->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (! is_null($q)) {
            $query = (new Query())
                ->select(['id' => '[[user_id]]', 'text' => (new Expression('CONCAT([[first_name]], " ", [[last_name]])'))])
                ->from('{{%user_profile}}')
                ->where(['or', ['like', '[[first_name]]', $q], ['like', '[[last_name]]', $q]])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } else if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => UserProfile::findOne(['user_id' => $id])->complete_name];
        }

        $this->response->data = $out;

        $this->response->send();
        Yii::$app->end();
    }

    /**
     * Creates a new Officer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = Yii::createObject(Officer::class);

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['member/officer']);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'yearServedRange' => Officer::getYearServedRange(),
        ]);
    }

    /**
     * Deletes an existing Officer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['member/officer']);
    }

    /**
     * Finds the Officer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Officer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Officer::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
