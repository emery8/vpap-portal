<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Announcement;
use app\models\Event;
use app\models\UserProfile;
use app\models\widgets\EventCarousel;
use yii\data\ActiveDataProvider;

/**
 * Default controller for the `member` module
 */
class DefaultController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(), []
        );
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $carousel = Yii::createObject([
            'class' => EventCarousel::class,
            'itemTemplate' => '{button}',
        ]);
        
        return $this->render('index', [
            'announcementDataProvider' => new ActiveDataProvider([
                'query' => Announcement::find()->published(),
                'sort' => [
                    'defaultOrder' => ['created_at' => SORT_DESC],
                ],
                'pagination' => [
                    'pageSize' => 3,
                ],
            ]),
            'birthdayDataProvider' => new ActiveDataProvider([
                'query' => UserProfile::find()->birthdayByWeek(),
                'sort' => [
                    'defaultOrder' => ['last_name' => SORT_ASC],
                ],
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]),
            'carouselItems' => $carousel->getItems(),
        ]);
    }
}
