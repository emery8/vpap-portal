<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\SpeciesOfSpecialization;
use app\models\TypeOfPractice;
use app\models\VeterinaryInstitution;
use app\models\Sponsor;
use app\models\EventType;
use app\models\EventInvitationType;
use app\models\MembershipType;
use app\models\DiscountType;
use app\models\RegularMembershipFee;
use app\models\LifetimeMembershipFee;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;

/**
 * {@author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>}
 */
class SettingController extends \yii\web\Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(), []
        );
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $regularMembershipFeeModel = RegularMembershipFee::findOne(['type' => RegularMembershipFee::TYPE]);
        if ($regularMembershipFeeModel === null) {
            $regularMembershipFeeModel = Yii::createObject(RegularMembershipFee::class);
        }

        $lifetimeMembershipFeeModel = LifetimeMembershipFee::findOne(['type' => LifetimeMembershipFee::TYPE]);
        if ($lifetimeMembershipFeeModel === null) {
            $lifetimeMembershipFeeModel = Yii::createObject(LifetimeMembershipFee::class);
        }

        if ($this->request->isPost) {
            if ($regularMembershipFeeModel->load($this->request->post()) && $lifetimeMembershipFeeModel->load($this->request->post())) {
                $isValid = $regularMembershipFeeModel->validate();
                $isValid = $lifetimeMembershipFeeModel->validate() && $isValid;

                if ($isValid) {
                    $regularMembershipFeeModel->save(false);
                    $lifetimeMembershipFeeModel->save(false);

                    return $this->redirect(['index']);
                }
            }
        }

        return $this->render('index', [
            'typeOfPracticeDataProvider' => new ActiveDataProvider([
                'query' => TypeOfPractice::find()->notDeleted(),
                'sort' => [
                    'defaultOrder' => ['description' => SORT_ASC],
                ],
                'pagination' => [
                    'pageSize' => 5,
                ],
            ]),
            'speciesOfSpecializationDataProvider' => new ActiveDataProvider([
                'query' => SpeciesOfSpecialization::find()->notDeleted(),
                'sort' => [
                    'defaultOrder' => ['description' => SORT_ASC],
                ],
                'pagination' => [
                    'pageSize' => 5,
                ],
            ]),
            'veterinaryInstitutionDataProvider' => new ActiveDataProvider([
                'query' => VeterinaryInstitution::find()->notDeleted(),
                'sort' => [
                    'defaultOrder' => ['institution' => SORT_ASC],
                ],
                'pagination' => [
                    'pageSize' => 5,
                ],
            ]),
            'sponsorDataProvider' => new ActiveDataProvider([
                'query' => Sponsor::find()->notDeleted(),
                'sort' => [
                    'defaultOrder' => ['company_name' => SORT_ASC],
                ],
                'pagination' => [
                    'pageSize' => 5,
                ],
            ]),
            'regularMembershipFeeModel' => $regularMembershipFeeModel,
            'lifetimeMembershipFeeModel' => $lifetimeMembershipFeeModel,
        ]);
    }

}
