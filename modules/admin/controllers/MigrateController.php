<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class MigrateController extends \yii\web\Controller
{
    public function actionUser()
    {
        Yii::$app->session->setFlash('start_time', Yii::$app->formatter->asDateTime(time()));
        $unbufferedDb = new \yii\db\Connection([
            'dsn' => Yii::$app->db->dsn,
            'username' => Yii::$app->db->username,
            'password' => Yii::$app->db->password,
            'charset' => Yii::$app->db->charset,
        ]);
        $unbufferedDb->open();
        $unbufferedDb->pdo->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);

        $excludeLicenseNo = ['10133', '10365', '7250', '7908', '9633', '9661', '9769', ''];
        $query = (new \yii\db\Query())
            ->select('
                a.id, a.archive, a.email, a.role,
                b.user_id AS b_user_id, b.comp_name, b.pos, b.address, b.village_subdivision, b.barangay, b.contact, b.town_city, b.province, b.zipcode, b.country,
                c.user_id AS c_user_id, c.school, c.year,
                d.user_id AS d_user_id, d.fname, d.mname, d.lname, d.suffx, d.nickname, d.address1, d.village_subdivision AS d_village_subdivision, d.barangay AS d_barangay, d.town_city AS d_town_city, d.province AS d_province, d.zipcode AS d_zipcode, d.country AS d_country, d.homephone, d.mobile, d.bday,
                e.user_id AS e_user_id, e.license_no, e.date_exp, e.type_practice, e.spec_spec,
                f.user_id AS f_user_id, f.vpap_mem_type, f.year_mem, f.no_yrs_active, f.disc_con, f.name_appear_certificate
            ')
            ->from('vpap_odb.usr a')
            ->join('LEFT JOIN', 'vpap_odb.user_company b', 'b.user_id = a.id')
            ->join('LEFT JOIN', 'vpap_odb.user_educ c', 'c.user_id = a.id')
            ->join('LEFT JOIN', 'vpap_odb.user_personal_info d', 'd.user_id = a.id')
            ->join('LEFT JOIN', 'vpap_odb.user_profession e', 'e.user_id = a.id')
            ->join('LEFT JOIN', 'vpap_odb.user_vpap f', 'f.user_id = a.id')
            ->andWhere(['=', 'a.archive', 0])
            ->andWhere(['NOT IN', 'e.license_no', $excludeLicenseNo]);

        $limit = false;
        if ($limit) {
            $query->limit($limit);
        }

        $users = [];
        $employments = [];
        $educations = [];
        $profiles = [];
        $professions = [];
        $memberships = [];
        foreach ($query->each(100, $unbufferedDb) as $model) {
            $users[] = $model;
        }

        $veterinary_institutions = \app\models\VeterinaryInstitution::listData();
        $type_of_practices = \app\models\TypeOfPractice::listData();
        $species_of_specializations = \app\models\SpeciesOfSpecialization::listData();
        foreach ($users as $model) {
            Yii::$app->db->createCommand()->insert('user', [
                'old_id' => $model['id'],
                'email' => $model['email'],
                'password_hash' => Yii::$app->security->generatePasswordHash('changeme'),
                'auth_key' => Yii::$app->security->generateRandomString(),
                'status' => 10,
                'created_at' => time(),
                'updated_at' => time(),
            ])->execute();
            $user_id = Yii::$app->db->getLastInsertID();
            if ($user_id) {
                $auth = Yii::$app->authManager;
                $role = $auth->getRole($model['role'] ? 'admin' : 'member');
                $auth->assign($role, $user_id);

                $employments[] = [
                    'user_id' => $user_id,
                    'company_name' => $model['comp_name'] ? $model['comp_name'] : null,
                    'position' => $model['pos'] ? $model['pos'] : null,
                    'address' => $model['address'] ? $model['address'] : null,
                    'subdivision' => $model['village_subdivision'] ? $model['village_subdivision'] : null,
                    'barangay' => $model['barangay'] ? $model['barangay'] : null,
                    'municipality' => $model['town_city'] ? $model['town_city'] : null,
                    'province' => $model['province'] ? $model['province'] : null,
                    'zipcode' => $model['zipcode'] ? $model['zipcode'] : null,
                    'country' => $model['country'] ? $model['country'] : null,
                    'phone_number' => $model['contact'] ? $model['contact'] : null,
                ];

                $educations[] = [
                    'user_id' => $user_id,
                    'institution' => ($key = array_search($model['school'], $veterinary_institutions)) ? $key : null,
                    'year_graduated' => $model['year'] ? $model['year'] : null,
                ];

                $profiles[] = [
                    'user_id' => $user_id,
                    'name_on_certificate' => $model['name_appear_certificate'] ? $model['name_appear_certificate'] : null,
                    'first_name' => $model['fname'] ? $model['fname'] : '',
                    'middle_name' => $model['mname'] ? $model['mname'] : null,
                    'last_name' => $model['lname'] ? $model['lname'] : '',
                    'suffix' => $model['suffx'] ? $model['suffx'] : null,
                    'nickname' => $model['nickname'] ? $model['nickname'] : null,
                    'address' => $model['address1'] ? $model['address1'] : null,
                    'subdivision' => $model['d_village_subdivision'] ? $model['d_village_subdivision'] : null,
                    'barangay' => $model['d_barangay'] ? $model['d_barangay'] : null,
                    'municipality' => $model['d_town_city'] ? $model['d_town_city'] : null,
                    'province' => $model['d_province'] ? $model['d_province'] : null,
                    'zipcode' => $model['d_zipcode'] ? $model['d_zipcode'] : null,
                    'country' => $model['d_country'] ? $model['d_country'] : null,
                    'phone_number' => $model['homephone'] ? $model['homephone'] : null,
                    'mobile_number' => $model['mobile'] ? $model['mobile'] : null,
                    'birth_date' => $model['bday'] ? $model['bday'] : null,
                ];

                $tp = preg_replace_callback('/"\\w+"/', function ($matches) {
                    $content = $this->renderTypeOfPractice($matches[0]);

                    return ($content === false) ? $matches[0] : $content;
                }, str_replace([' ', '-'], '', $model['type_practice']));
                $ss = preg_replace_callback('/"\\w+"/', function ($matches) {
                    $content = $this->renderSpeciesOfSpecialization($matches[0]);

                    return ($content === false) ? $matches[0] : $content;
                }, str_replace([' ', '-'], '', $model['spec_spec']));

                $professions[] = [
                    'user_id' => $user_id,
                    'license_number' => $model['license_no'] ? $model['license_no'] : uniqid(),
                    'expiration_date' => $model['date_exp'],
                    'type_of_practice' => $tp ? $tp : null,
                    'species_of_specialization' => $ss ? $ss : null,
                ];

                $memberships[] = [
                    'user_id' => $user_id,
                    'type' => $model['vpap_mem_type'] ? \app\models\enums\MembershipType::getValueByName($model['vpap_mem_type']) : null,
                    'number_years_active' => $model['no_yrs_active'] ? $model['no_yrs_active'] : null,
                    'year_initial_joined' => $model['year_mem'] ? $model['year_mem'] : null,
                    'discount_type' => $model['disc_con'] ? (($model['disc_con'] == 100) ? \app\models\enums\MemberDiscountType::FULL_PAYMENT : \app\models\enums\MemberDiscountType::getValueByName($model['disc_con'])) : null,
                ];
            }
        }

        $unbufferedDb->close();

        $unbufferedDb->open();
        $unbufferedDb->pdo->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);

        Yii::$app->db->createCommand()->batchInsert('user_employment', [
            'user_id',
            'company_name',
            'position',
            'address',
            'subdivision',
            'barangay',
            'municipality',
            'province',
            'zipcode',
            'country',
            'phone_number',
        ], $employments)->execute();

        Yii::$app->db->createCommand()->batchInsert('user_education', [
            'user_id',
            'institution',
            'year_graduated',
        ], $educations)->execute();

        Yii::$app->db->createCommand()->batchInsert('user_profile', [
            'user_id',
            'name_on_certificate',
            'first_name',
            'middle_name',
            'last_name',
            'suffix',
            'nickname',
            'address',
            'subdivision',
            'barangay',
            'municipality',
            'province',
            'zipcode',
            'country',
            'phone_number',
            'mobile_number',
            'birth_date',
        ], $profiles)->execute();

        Yii::$app->db->createCommand()->batchInsert('user_profession', [
            'user_id',
            'license_number',
            'expiration_date',
            'type_of_practice',
            'species_of_specialization',
        ], $professions)->execute();

        Yii::$app->db->createCommand()->batchInsert('user_membership', [
            'user_id',
            'type',
            'number_years_active',
            'year_initial_joined',
            'discount_type',
        ], $memberships)->execute();

        $unbufferedDb->close();

        return $this->redirect(['finish']);
    }

    public function actionSample()
    {
        Yii::$app->session->setFlash('start_time', Yii::$app->formatter->asDateTime(time()));
        $veterinary_institutions = \app\models\VeterinaryInstitution::listData();
        $type_of_practices = \app\models\TypeOfPractice::listData();
        $species_of_specializations = \app\models\SpeciesOfSpecialization::listData();

        // $keys = array_keys($species_of_specializations, ['Canine', 'Feline']);
        // $keys = array_intersect($species_of_specializations, ['Canine', 'Poultry']);
        // echo '<pre>'; print_r($keys); die();

        $unbufferedDb = new \yii\db\Connection([
            'dsn' => 'mysql:host=localhost;dbname=vpap_odb',
            'username' => 'root',
            'password' => '',
            'charset' => 'latin1',
        ]);

        $limit = 50;
        $unbufferedDb->open();
        $unbufferedDb->pdo->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);

        $excludeLicenseNo = ['10133', '10365', '7250', '7908', '9633', '9661', '9769', ''];
        $query = (new \yii\db\Query())
            ->select('
                a.id, a.archive, a.email, a.role,
                b.user_id AS b_user_id, b.comp_name, b.pos, b.address, b.village_subdivision, b.barangay, b.contact, b.town_city, b.province, b.zipcode, b.country,
                c.user_id AS c_user_id, c.school, c.year,
                d.user_id AS d_user_id, d.fname, d.mname, d.lname, d.suffx, d.nickname, d.address1, d.village_subdivision AS d_village_subdivision, d.barangay AS d_barangay, d.town_city AS d_town_city, d.province AS d_province, d.zipcode AS d_zipcode, d.country AS d_country, d.homephone, d.mobile, d.bday,
                e.user_id AS e_user_id, e.license_no, e.date_exp, e.type_practice, e.spec_spec,
                f.user_id AS f_user_id, f.vpap_mem_type, f.year_mem, f.no_yrs_active, f.disc_con, f.name_appear_certificate
            ')
            ->from('vpap_odb.usr a')
            ->join('LEFT JOIN', 'vpap_odb.user_company b', 'b.user_id = a.id')
            ->join('LEFT JOIN', 'vpap_odb.user_educ c', 'c.user_id = a.id')
            ->join('LEFT JOIN', 'vpap_odb.user_personal_info d', 'd.user_id = a.id')
            ->join('LEFT JOIN', 'vpap_odb.user_profession e', 'e.user_id = a.id')
            ->join('LEFT JOIN', 'vpap_odb.user_vpap f', 'f.user_id = a.id')
            ->andWhere(['=', 'a.archive', 0])
            ->andWhere(['NOT IN', 'e.license_no', $excludeLicenseNo]);

            if ($limit) {
                $query->limit($limit);
            }
        
        foreach ($query->each(40, $unbufferedDb) as $model) {
            Yii::$app->db->createCommand()->insert('user', [
                'old_id' => $model['id'],
                'email' => $model['email'],
                'password_hash' => Yii::$app->security->generatePasswordHash('changeme'),
                'auth_key' => Yii::$app->security->generateRandomString(),
                'status' => 15,
                'created_at' => time(),
                'updated_at' => time(),
            ])->execute();
            $user_id = Yii::$app->db->getLastInsertID();
            if ($user_id) {
                $auth = Yii::$app->authManager;
                $role = $auth->getRole($model['role'] ? 'admin' : 'member');
                $auth->assign($role, $user_id);

                Yii::$app->db->createCommand()->insert('user_employment', [
                    'user_id' => $user_id,
                    'company_name' => $model['comp_name'] ? $model['comp_name'] : null,
                    'position' => $model['pos'] ? $model['pos'] : null,
                    'address' => $model['address'] ? $model['address'] : null,
                    'subdivision' => $model['village_subdivision'] ? $model['village_subdivision'] : null,
                    'barangay' => $model['barangay'] ? $model['barangay'] : null,
                    'municipality' => $model['town_city'] ? $model['town_city'] : null,
                    'province' => $model['province'] ? $model['province'] : null,
                    'zipcode' => $model['zipcode'] ? $model['zipcode'] : null,
                    'country' => $model['country'] ? $model['country'] : null,
                    'phone_number' => $model['contact'] ? $model['contact'] : null,
                ])->execute();

                Yii::$app->db->createCommand()->insert('user_education', [
                    'user_id' => $user_id,
                    'institution' => ($key = array_search($model['school'], $veterinary_institutions)) ? $key : null,
                    'year_graduated' => $model['year'] ? $model['year'] : null,
                ])->execute();

                Yii::$app->db->createCommand()->insert('user_profile', [
                    'user_id' => $user_id,
                    'name_on_certificate' => $model['name_appear_certificate'] ? $model['name_appear_certificate'] : null,
                    'first_name' => $model['fname'] ? $model['fname'] : '',
                    'middle_name' => $model['mname'] ? $model['mname'] : null,
                    'last_name' => $model['lname'] ? $model['lname'] : '',
                    'suffix' => $model['suffx'] ? $model['suffx'] : null,
                    'nickname' => $model['nickname'] ? $model['nickname'] : null,
                    'address' => $model['address1'] ? $model['address1'] : null,
                    'subdivision' => $model['d_village_subdivision'] ? $model['d_village_subdivision'] : null,
                    'barangay' => $model['d_barangay'] ? $model['d_barangay'] : null,
                    'municipality' => $model['d_town_city'] ? $model['d_town_city'] : null,
                    'province' => $model['d_province'] ? $model['d_province'] : null,
                    'zipcode' => $model['d_zipcode'] ? $model['d_zipcode'] : null,
                    'country' => $model['d_country'] ? $model['d_country'] : null,
                    'phone_number' => $model['homephone'] ? $model['homephone'] : null,
                    'mobile_number' => $model['mobile'] ? $model['mobile'] : null,
                    'birth_date' => $model['bday'] ? $model['bday'] : null,
                ])->execute();

                $tp = preg_replace_callback('/"\\w+"/', function ($matches) {
                    $content = $this->renderTypeOfPractice($matches[0]);

                    return ($content === false) ? $matches[0] : $content;
                }, str_replace([' ', '-'], '', $model['type_practice']));
                $ss = preg_replace_callback('/"\\w+"/', function ($matches) {
                    $content = $this->renderSpeciesOfSpecialization($matches[0]);

                    return ($content === false) ? $matches[0] : $content;
                }, str_replace([' ', '-'], '', $model['spec_spec']));

                Yii::$app->db->createCommand()->insert('user_profession', [
                    'user_id' => $user_id,
                    'license_number' => $model['license_no'] ? $model['license_no'] : uniqid(),
                    'expiration_date' => $model['date_exp'],
                    'type_of_practice' => $tp ? $tp : null,
                    'species_of_specialization' => $ss ? $ss : null,
                ])->execute();

                Yii::$app->db->createCommand()->insert('user_membership', [
                    'user_id' => $user_id,
                    'type' => $model['vpap_mem_type'] ? \app\models\enums\MembershipType::getValueByName($model['vpap_mem_type']) : null,
                    'number_years_active' => $model['no_yrs_active'] ? $model['no_yrs_active'] : null,
                    'year_initial_joined' => $model['year_mem'] ? $model['year_mem'] : null,
                    'discount_type' => $model['disc_con'] ? (($model['disc_con'] == 100) ? \app\models\enums\MemberDiscountType::FULL_PAYMENT : \app\models\enums\MemberDiscountType::getValueByName($model['disc_con'])) : null,
                ])->execute();
            }
        }

        $unbufferedDb->close();

        return $this->redirect(['finish']);
    }

    public function actionEvent()
    {
        // TODO copy unbuffered code from actionUser
        $unbufferedDb = new \yii\db\Connection([
            'dsn' => 'mysql:host=localhost;dbname=vpap_odb',
            'username' => 'root',
            'password' => '',
            'charset' => 'latin1',
        ]);

        $unbufferedDb->open();
        $unbufferedDb->pdo->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);

        $query = (new \yii\db\Query())
            ->select('
                a.id, a.event_type, a.event_title, a.event_location, a.event_startdate, a.event_enddate, a.event_cpd, a.event_fee_lifetime, a.event_fee_member, a.event_fee_non_member,
                a.event_disc, a.event_fee_disc, a.event_att_type, a.event_max_part, a.archived
            ')
            ->from('vpap_odb.event_tbl a')
            ->andWhere(['a.archived' => 0]);
        
        foreach ($query->each(40, $unbufferedDb) as $model) {
            $event = Yii::createObject(\app\models\Event::class);
            $event->setAttributes([
                'old_id' => $model['id'],
                'type' => $this->getEventType($model['event_type']),
                'title' => $model['event_title'],
                'location' => $model['event_location'],
                'start_date' => $model['event_startdate'],
                'end_date' => $model['event_enddate'],
                'enable_discount' => ($model['event_disc'] === 'Enable') ? 1 : 0,
                'membership_fee' => $model['event_fee_disc'],
                'lifetime_member_fee' => $model['event_fee_lifetime'],
                'regular_member_fee' => $model['event_fee_member'],
                'non_member_fee' => $model['event_fee_non_member'],
                'attendance_type' => $model['event_att_type'] ? \app\models\enums\EventInvitationType::getValueByName($model['event_att_type']) : null,
                'cpd_point' => $model['event_cpd'],
                'maximum_participant' => $model['event_max_part'],
            ]);
            if (! $event->save()) {
                echo $model['id'];
                echo '<pre>'; print_r($event->errors); die();
            }
        }

        $unbufferedDb->close();
    }

    public function actionAttendance()
    {
        $unbufferedDb = new \yii\db\Connection([
            'dsn' => Yii::$app->db->dsn,
            'username' => Yii::$app->db->username,
            'password' => Yii::$app->db->password,
            'charset' => Yii::$app->db->charset,
        ]);
        $unbufferedDb->open();
        $unbufferedDb->pdo->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);

        $limit = null;
        $query = (new \yii\db\Query())
            ->select('a.*')
            ->from('vpap_odb.event_attendee a')
            ->andWhere(['a.archived' => 0]);

        if ($limit) {
            $query->limit($limit);
        }

        foreach ($query->each(100, $unbufferedDb) as $model) {
            $user = (new \yii\db\Query())
                ->select('id, old_id')
                ->from('vpap.user')
                ->andWhere(['old_id' => $model['user_id']])
                ->one();
            $event = (new \yii\db\Query())
                ->select('id, old_id')
                ->from('vpap.event')
                ->andWhere(['old_id' => $model['event_id']])
                ->one();

            if ($user && $event) {
                Yii::$app->db->createCommand()->insert('event_attendance', [
                    'member_id' => $user['id'],
                    'event_id' => $event['id'],
                    'sponsor_id' => $model['sponsor_id'] ? $model['sponsor_id'] : null,
                    'attendance_type' => \app\models\enums\EventAttendanceType::getValueByName($model['att_type']),
                    'member_type' => null,
                    'payment_method' => \app\models\enums\PaymentMethod::getValueByName($model['payment_type']),
                    'discount_type' => null,
                    'event_fee' => null,
                    'amount_paid' => $model['payment_amt'],
                    'cpd_point_earned' => $model['cpd_earned'],
                    'payment_confirmed' => $model['payment_status'],
                    'attended' => $model['confirm'],
                    'created_at' => time(),
                    'updated_at' => time(),
                ])->execute();
            }
        }

        $unbufferedDb->close();
    }

    protected function getEventType($type) {
        switch ($type) {
            case 'Seminar':
                return 2;
            case 'Annual Conference':
                return 4;
            case 'Regional Conference':
                return 3;
            case 'General Membership Meeting':
                return 1;
        }
    }

    protected function renderTypeOfPractice($name)
    {
        switch ($name) {
            case '"Academe"':
                return 1;
            case '"ClinicorHospitalFacility"':
                return 2;
            case '"Consultancy"':
                return 3;
            case '"Corporate"':
                return 4;
            case '"Government"':
                return 5;
            case '"NonGovernmentOrganization"':
                return 6;
            case '"OFW"':
                return 7;
            case '"ResearchFacility"':
                return 8;
        }
    }

    protected function renderSpeciesOfSpecialization($name)
    {
        switch ($name) {
            case '"Canine"':
                return 1;
            case '"Feline"':
                return 2;
            case '"Equine"':
                return 3;
            case '"Ruminant"':
                return 4;
            case '"Swine"':
                return 5;
            case '"Poultry"':
                return 6;
            case '"WildlifeandExotic"':
                return 7;
            case '"LaboratoryAnimal"':
                return 8;
            case '"AquaticAnimal"':
                return 9;
        }
    }

    public function actionFinish()
    {
        $session = Yii::$app->session;
        echo Yii::t('app', 'Start Time: {time}', ['time' => $session->getFlash('start_time')]);
        echo '<br>';
        echo Yii::t('app', 'End Time: {time}', ['time' => Yii::$app->formatter->asDateTime(time())]);
    }
}
