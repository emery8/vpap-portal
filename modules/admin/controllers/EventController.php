<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Attachment;
use app\models\Event;
use app\models\EventAttendance;
use app\models\FeaturedImage;
use app\models\IdentificationTemplate;
use app\models\ProgramTemplate;
use app\models\InvitationTemplate;
use app\models\AttendanceCertificateTemplate;
use app\models\CpdCertificateTemplate;
use app\models\SpeciesOfSpecialization;
use app\models\TypeOfPractice;
use app\models\Sponsor;
use app\models\User;
use app\models\UserProfile;
use app\models\UserMembership;
use app\models\enums\EventType;
use app\models\enums\EventInvitationType;
use app\models\enums\EventAttendanceType;
use app\models\enums\PaymentMethod;
use app\modules\admin\models\ChartTypeSearchForm;
use app\modules\admin\models\EventSearch;
use app\modules\admin\models\EventAttendanceSearch;
use app\modules\admin\models\UploadCpdPointForm;
use app\traits\AjaxValidationTrait;
use yii\base\Model;
use yii\base\InvalidArgumentException;
use yii\bootstrap4\Html;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii2tech\csvgrid\CsvGrid;
use kartik\mpdf\Pdf;

use app\modules\admin\models\EventAgeChart;
use app\modules\admin\models\CountryChart;
use app\modules\admin\models\VeterinarySchoolGraduatedChart;
use app\modules\admin\models\RegionHomeChart;
use app\modules\admin\models\RegionOfficeChart;
use app\modules\admin\models\YearGraduatedChart;
use app\modules\admin\models\SpeciesOfSpecializationChart;
use app\modules\admin\models\TypeOfPracticeChart;

/**
 * EventController implements the CRUD actions for Event model.
 */
class EventController extends Controller
{
    use AjaxValidationTrait;

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Event models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'eventTypeListData' => EventType::listData(),
            'eventInvitationTypeListData' => EventInvitationType::listData(),
        ]);
    }

    /**
     * Displays a single Event model.
     * @param string $slug slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionChart($slug, $type = 'pie')
    {
        $model = $this->findModel($slug);

        $pieChartOptions = [
            'chart' => [
                'plotBackgroundColor' => null,
                'plotBorderWidth' => null,
                'plotShadow' => false,
                'type' => 'pie'
            ],
            'tooltip' => [
                'pointFormat' => '{series.name}: <b>{point.percentage:.1f}%</b>'
            ],
            'accessibility' => [
                'point' => [
                    'valueSuffix' => '%'
                ],
            ],
            'plotOptions' => [
                'pie' => [
                    'allowPointSelect' => true,
                    'cursor' => 'pointer',
                    'dataLabels' => [
                        'enabled' => true,
                        'format' => '<b>{point.name}</b>: {point.percentage:.1f} %'
                    ]
                ]
            ],
        ];

        $barChartOptions = [
            'chart' => [
                'type' => 'column',
            ],
            'plotOptions' => [
                'columns' => [
                    'pointPadding' => 0.2,
                    'borderWidth' => 0,
                ],
            ],
        ];

        $eventAgeChart = new EventAgeChart();
        $countryChart = new CountryChart();
        $veterinarySchoolGraduatedChart = new VeterinarySchoolGraduatedChart();
        $regionHomeChart = new RegionHomeChart();
        $regionOfficeChart = new RegionOfficeChart();
        $yearGraduatedChart = new YearGraduatedChart();
        $speciesOfSpecializationChart = new SpeciesOfSpecializationChart();
        $typeOfPracticeChart = new TypeOfPracticeChart();

        $eventAgeChartData = $eventAgeChart->fetch($model, $type);
        $countryChartData = $countryChart->fetch($model, $type);
        $veterinarySchoolGraduatedChartData = $veterinarySchoolGraduatedChart->fetch($model, $type);
        $regionHomeChartData = $regionHomeChart->fetch($model, $type);
        $regionOfficeChartData = $regionOfficeChart->fetch($model, $type);
        $yearGraduatedChartData = $yearGraduatedChart->fetch($model, $type);
        $speciesOfSpecializationChartData = $speciesOfSpecializationChart->fetch($model, $type);
        $typeOfPracticeChartData = $typeOfPracticeChart->fetch($model, $type);

        switch ($type) {
            case 'bar':
                $eventAgeChartOptions = array_merge($barChartOptions, [
                    'title' => [
                        'text' => Yii::t('app', '{chart_title}', [
                            'chart_title' => 'By Age Range',
                        ]),
                    ],
                    'xAxis' => [
                        'categories' => ArrayHelper::getColumn($eventAgeChartData, 'name'),
                        'crosshair' => true,
                    ],
                    'yAxis' => [
                        'min' => 0,
                        'title' => [
                            'text' => Yii::t('app', 'Age Range'),
                        ],
                    ],
                    'series' => $eventAgeChartData,
                ]);
                $countryChartOptions = array_merge($barChartOptions, [
                    'title' => [
                        'text' => Yii::t('app', '{chart_title}', [
                            'chart_title' => 'By Country',
                        ]),
                    ],
                    'xAxis' => [
                        'categories' => ArrayHelper::getColumn($countryChartData, 'name'),
                        'crosshair' => true,
                    ],
                    'yAxis' => [
                        'min' => 0,
                        'title' => [
                            'text' => Yii::t('app', 'Country'),
                        ],
                    ],
                    'series' => $countryChartData,
                ]);
                $veterinarySchoolGraduatedChartOptions = array_merge($barChartOptions, [
                    'title' => [
                        'text' => Yii::t('app', '{chart_title}', [
                            'chart_title' => 'By University',
                        ]),
                    ],
                    'xAxis' => [
                        'categories' => ArrayHelper::getColumn($veterinarySchoolGraduatedChartData, 'name'),
                        'crosshair' => true,
                    ],
                    'yAxis' => [
                        'min' => 0,
                        'title' => [
                            'text' => Yii::t('app', 'University'),
                        ],
                    ],
                    'series' => $veterinarySchoolGraduatedChartData,
                ]);
                $regionHomeChartOptions = array_merge($barChartOptions, [
                    'title' => [
                        'text' => Yii::t('app', '{chart_title}', [
                            'chart_title' => 'By Region (Home)',
                        ]),
                    ],
                    'xAxis' => [
                        'categories' => ArrayHelper::getColumn($regionHomeChartData, 'name'),
                        'crosshair' => true,
                    ],
                    'yAxis' => [
                        'min' => 0,
                        'title' => [
                            'text' => Yii::t('app', 'Region (Home)'),
                        ],
                    ],
                    'series' => $regionHomeChartData,
                ]);
                $regionOfficeChartOptions = array_merge($barChartOptions, [
                    'title' => [
                        'text' => Yii::t('app', '{chart_title}', [
                            'chart_title' => 'By Region (Office)',
                        ]),
                    ],
                    'xAxis' => [
                        'categories' => ArrayHelper::getColumn($regionOfficeChartData, 'name'),
                        'crosshair' => true,
                    ],
                    'yAxis' => [
                        'min' => 0,
                        'title' => [
                            'text' => Yii::t('app', 'Region (Office)'),
                        ],
                    ],
                    'series' => $regionOfficeChartData,
                ]);
                $yearGraduatedChartOptions = array_merge($barChartOptions, [
                    'title' => [
                        'text' => Yii::t('app', '{chart_title}', [
                            'chart_title' => 'By Year Graduated',
                        ]),
                    ],
                    'xAxis' => [
                        'categories' => ArrayHelper::getColumn($yearGraduatedChartData, 'name'),
                        'crosshair' => true,
                    ],
                    'yAxis' => [
                        'min' => 0,
                        'title' => [
                            'text' => Yii::t('app', 'Year Graduated'),
                        ],
                    ],
                    'series' => $yearGraduatedChartData,
                ]);
                $typeOfPracticeChartOptions = array_merge($barChartOptions, [
                    'title' => [
                        'text' => Yii::t('app', '{chart_title}', [
                            'chart_title' => 'By Type of Practice',
                        ]),
                    ],
                    'xAxis' => [
                        'categories' => ArrayHelper::getColumn($typeOfPracticeChartData, 'name'),
                        'crosshair' => true,
                    ],
                    'yAxis' => [
                        'min' => 0,
                        'title' => [
                            'text' => Yii::t('app', 'Type of Practice'),
                        ],
                    ],
                    'series' => $typeOfPracticeChartData,
                ]);
                $speciesOfSpecializationChartOptions = array_merge($barChartOptions, [
                    'title' => [
                        'text' => Yii::t('app', '{chart_title}', [
                            'chart_title' => 'By Species of Specialization',
                        ]),
                    ],
                    'xAxis' => [
                        'categories' => ArrayHelper::getColumn($speciesOfSpecializationChartData, 'name'),
                        'crosshair' => true,
                    ],
                    'yAxis' => [
                        'min' => 0,
                        'title' => [
                            'text' => Yii::t('app', 'Species of Specialization'),
                        ],
                    ],
                    'series' => $speciesOfSpecializationChartData,
                ]);
                break;
            case 'pie':
                $eventAgeChartOptions = array_merge($pieChartOptions, [
                    'title' => [
                        'text' => Yii::t('app', '{chart_title}', [
                            'chart_title' => 'By Age Range',
                        ]),
                    ],
                    'series' => [
                        [
                            'name' => Yii::t('app', 'Age Range'),
                            'colorByPoint' => true,
                            'data' => $eventAgeChartData,
                        ],
                    ],
                ]);
                $countryChartOptions = array_merge($pieChartOptions, [
                    'title' => [
                        'text' => Yii::t('app', '{chart_title}', [
                            'chart_title' => 'By Country',
                        ]),
                    ],
                    'series' => [
                        [
                            'name' => Yii::t('app', 'Country'),
                            'colorByPoint' => true,
                            'data' => $countryChartData,
                        ],
                    ],
                ]);
                $veterinarySchoolGraduatedChartOptions = array_merge($pieChartOptions, [
                    'title' => [
                        'text' => Yii::t('app', '{chart_title}', [
                            'chart_title' => 'By University',
                        ]),
                    ],
                    'series' => [
                        [
                            'name' => Yii::t('app', 'University'),
                            'colorByPoint' => true,
                            'data' => $veterinarySchoolGraduatedChartData,
                        ],
                    ],
                ]);
                $regionHomeChartOptions = array_merge($pieChartOptions, [
                    'title' => [
                        'text' => Yii::t('app', '{chart_title}', [
                            'chart_title' => 'By Region (Home)',
                        ]),
                    ],
                    'series' => [
                        [
                            'name' => Yii::t('app', 'Region (Home)'),
                            'colorByPoint' => true,
                            'data' => $regionHomeChartData,
                        ],
                    ],
                ]);
                $regionOfficeChartOptions = array_merge($pieChartOptions, [
                    'title' => [
                        'text' => Yii::t('app', '{chart_title}', [
                            'chart_title' => 'By Region (Office)',
                        ]),
                    ],
                    'series' => [
                        [
                            'name' => Yii::t('app', 'Region (Office)'),
                            'colorByPoint' => true,
                            'data' => $regionOfficeChartData,
                        ],
                    ],
                ]);
                $yearGraduatedChartOptions = array_merge($pieChartOptions, [
                    'title' => [
                        'text' => Yii::t('app', '{chart_title}', [
                            'chart_title' => 'By Year Graduated',
                        ]),
                    ],
                    'series' => [
                        [
                            'name' => Yii::t('app', 'Year Graduated'),
                            'colorByPoint' => true,
                            'data' => $yearGraduatedChartData,
                        ],
                    ],
                ]);
                $typeOfPracticeChartOptions = array_merge($pieChartOptions, [
                    'title' => [
                        'text' => Yii::t('app', '{chart_title}', [
                            'chart_title' => 'By Type of Practice',
                        ]),
                    ],
                    'series' => [
                        [
                            'name' => Yii::t('app', 'Type of Practice'),
                            'colorByPoint' => true,
                            'data' => $typeOfPracticeChartData,
                        ],
                    ],
                ]);
                $speciesOfSpecializationChartOptions = array_merge($pieChartOptions, [
                    'title' => [
                        'text' => Yii::t('app', '{chart_title}', [
                            'chart_title' => 'By Species of Specialization',
                        ]),
                    ],
                    'series' => [
                        [
                            'name' => Yii::t('app', 'Species of Specialization'),
                            'colorByPoint' => true,
                            'data' => $speciesOfSpecializationChartData,
                        ],
                    ],
                ]);
                break;
        }
        return $this->render("chart-{$type}", [
            'model' => $model,
            'type' => $type,
            'eventAgeChartOptions' => $eventAgeChartOptions,
            'countryChartOptions' => $countryChartOptions,
            'veterinarySchoolGraduatedChartOptions' => $veterinarySchoolGraduatedChartOptions,
            'regionHomeChartOptions' => $regionHomeChartOptions,
            'regionOfficeChartOptions' => $regionOfficeChartOptions,
            'yearGraduatedChartOptions' => $yearGraduatedChartOptions,
            'typeOfPracticeChartOptions' => $typeOfPracticeChartOptions,
            'speciesOfSpecializationChartOptions' => $speciesOfSpecializationChartOptions,
        ]);
    }

    /**
     * Displays the Event model signup form.
     * @param string $slug slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSignup($slug)
    {
        $identity = Yii::$app->user->identity;
        $session = Yii::$app->session;

        if (! $identity->is_profile_updated) {
            return $this->redirect(['profile/index']);
        }

        $model = $this->findModel($slug);
        if (! Event::canAttendEvent($model->attendance_type)) {
            throw new BadRequestHttpException(Yii::t('app', 'You are not allowed to signup for this event.'));
        }

        $hasSignup = EventAttendance::find()
            ->where([
                'member_id' => $identity->getid(),
                'event_id' => $model->id,
                'payment_confirmed' => 0,
            ])
            ->exists();
        if ($hasSignup) {
            return $this->redirect(['event-attendance/summary', 'slug' => $model->slug]);
        }

        $featured_image = null;
        $featured_image_thumb = null;
        $program_template = null;
        $invitation_template = null;
        $certificate_template = null;

        try {
            if (is_array($model->attachments) && count($model->attachments)) {
                $attachments = $model->attachments;
                foreach ($attachments as $attachment) {
                    if ($attachment instanceof \app\models\FeaturedImage) {
                        $featured_image = $attachment->getUploadUrl('content');
                        $featured_image_thumb = $attachment->getThumbUploadUrl('content', 'event_thumb');
                    } else if ($attachment instanceof \app\models\ProgramTemplate) {
                        $program_template = $attachment;
                    } else if ($attachment instanceof \app\models\InvitationTemplate) {
                        $invitation_template = $attachment;
                    } else if ($attachment instanceof \app\models\AttendanceCertificateTemplate) {
                        $certificate_template = $attachment;
                    }
                }
            }
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        return $this->render('signup', [
            'model' => $model,
            'featured_image' => $featured_image,
            'featured_image_thumb' => $featured_image_thumb,
            'program_template' => $program_template,
            'invitation_template' => $invitation_template,
            'certificate_template' => $certificate_template,
            'eventAttendanceModel' => Yii::createObject(EventAttendance::class),
            'eventAttendanceTypeListData' => EventAttendanceType::listData(),
            'paymentMethodListData' => PaymentMethod::listData(),
            'sponsorListData' => Sponsor::listData(),
        ]);
    }

    /**
     * Creates a new Event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = Yii::createObject(Event::class);
        $featuredImageModel = Yii::createObject(FeaturedImage::class);
        $featuredImageModel->scenario = FeaturedImage::SCENARIO_INSERT;
        $identificationTemplateModel = Yii::createObject(IdentificationTemplate::class);
        $identificationTemplateModel->scenario = IdentificationTemplate::SCENARIO_INSERT;
        $programTemplateModel = Yii::createObject(ProgramTemplate::class);
        $programTemplateModel->scenario = ProgramTemplate::SCENARIO_INSERT;
        $invitationTemplateModel = Yii::createObject(InvitationTemplate::class);
        $invitationTemplateModel->scenario = InvitationTemplate::SCENARIO_INSERT;
        $attendanceCertificateTemplateModel = Yii::createObject(AttendanceCertificateTemplate::class);
        $attendanceCertificateTemplateModel->scenario = AttendanceCertificateTemplate::SCENARIO_INSERT;
        $cpdCertificateTemplateModel = Yii::createObject(CpdCertificateTemplate::class);
        $cpdCertificateTemplateModel->scenario = CpdCertificateTemplate::SCENARIO_INSERT;

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $featuredImageModel->content = UploadedFile::getInstance($featuredImageModel, 'content');
                $identificationTemplateModel->content = UploadedFile::getInstance($programTemplateModel, 'content');
                $programTemplateModel->content = UploadedFile::getInstance($programTemplateModel, 'content');
                $invitationTemplateModel->content = UploadedFile::getInstance($invitationTemplateModel, 'content');
                $attendanceCertificateTemplateModel->content = UploadedFile::getInstance($attendanceCertificateTemplateModel, 'content');
                $cpdCertificateTemplateModel->content = UploadedFile::getInstance($cpdCertificateTemplateModel, 'content');

                $is_valid = $model->validate();
                $is_valid = $featuredImageModel->validate() && $is_valid;
                $is_valid = $identificationTemplateModel->validate() && $is_valid;
                $is_valid = $programTemplateModel->validate() && $is_valid;
                $is_valid = $invitationTemplateModel->validate() && $is_valid;
                $is_valid = $attendanceCertificateTemplateModel->validate() && $is_valid;
                $is_valid = $cpdCertificateTemplateModel->validate() && $is_valid;

                if ($is_valid) {
                    $transaction = Yii::$app->db->beginTransaction();
                    $session = Yii::$app->session;

                    try {
                        $model->save(false);
                        $featuredImageModel->event_id = $model->id;
                        $is_valid = $featuredImageModel->save();

                        if ($identificationTemplateModel->content) {
                            $identificationTemplateModel->event_id = $model->id;
                            $is_valid = $identificationTemplateModel->save() && $is_valid;
                        }

                        if ($programTemplateModel->content) {
                            $programTemplateModel->event_id = $model->id;
                            $is_valid = $programTemplateModel->save() && $is_valid;
                        }

                        if ($invitationTemplateModel->content) {
                            $invitationTemplateModel->event_id = $model->id;
                            $is_valid = $invitationTemplateModel->save() && $is_valid;
                        }

                        if ($attendanceCertificateTemplateModel->content) {
                            $attendanceCertificateTemplateModel->event_id = $model->id;
                            $is_valid = $attendanceCertificateTemplateModel->save() && $is_valid;
                        }

                        if ($cpdCertificateTemplateModel->content) {
                            $cpdCertificateTemplateModel->event_id = $model->id;
                            $is_valid = $cpdCertificateTemplateModel->save() && $is_valid;
                        }
                        
                        if ($is_valid) {
                            $transaction->commit();
                            return $this->redirect(['index']);
                        } else {
                            throw new BadRequestHttpException(Yii::$app->user->can('admin') ?
                                $e->getMessage() : Yii::t('app', 'We have encountered an error. Please contact our adminstrator about this problem.'),
                            );
                            $featuredImageModel->delete();
                            $transaction->rollBack();
                        }
                    } catch (\Exception $e) {
                        throw new BadRequestHttpException(Yii::$app->user->can('admin') ?
                            $e->getMessage() : Yii::t('app', 'We have encountered an error. Please contact our adminstrator about this problem.'),
                        );
                        $featuredImageModel->delete();
                        $transaction->rollBack();
                    } catch (\Throwable $e) {
                        throw new BadRequestHttpException(Yii::$app->user->can('admin') ?
                            $e->getMessage() : Yii::t('app', 'We have encountered an error. Please contact our adminstrator about this problem.'),
                        );
                        $featuredImageModel->delete();
                        $transaction->rollBack();
                    }
                }
            }
        } else {
            $model->loadDefaultValues();
            $featuredImageModel->loadDefaultValues();
            $identificationTemplateModel->loadDefaultValues();
            $programTemplateModel->loadDefaultValues();
            $invitationTemplateModel->loadDefaultValues();
            $attendanceCertificateTemplateModel->loadDefaultValues();
            $cpdCertificateTemplateModel->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'featuredImageModel' => $featuredImageModel,
            'identificationTemplateModel' => $identificationTemplateModel,
            'programTemplateModel' => $programTemplateModel,
            'invitationTemplateModel' => $invitationTemplateModel,
            'attendanceCertificateTemplateModel' => $attendanceCertificateTemplateModel,
            'cpdCertificateTemplateModel' => $cpdCertificateTemplateModel,
            'eventTypeListData' => EventType::listData(),
            'eventAttendanceTypeListData' => EventInvitationType::listData(),
        ]);
    }

    /**
     * Updates an existing Event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $slug slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($slug)
    {
        $model = $this->findModel($slug);
        $attachments = $model->attachments;
        $featuredImageModel = Yii::createObject(FeaturedImage::class);
        $featuredImageModel->scenario = FeaturedImage::SCENARIO_INSERT;
        $identificationTemplateModel = Yii::createObject(IdentificationTemplate::class);
        $identificationTemplateModel->scenario = IdentificationTemplate::SCENARIO_INSERT;
        $programTemplateModel = Yii::createObject(ProgramTemplate::class);
        $programTemplateModel->scenario = ProgramTemplate::SCENARIO_INSERT;
        $invitationTemplateModel = Yii::createObject(InvitationTemplate::class);
        $invitationTemplateModel->scenario = InvitationTemplate::SCENARIO_INSERT;
        $attendanceCertificateTemplateModel = Yii::createObject(AttendanceCertificateTemplate::class);
        $attendanceCertificateTemplateModel->scenario = AttendanceCertificateTemplate::SCENARIO_INSERT;
        $cpdCertificateTemplateModel = Yii::createObject(CpdCertificateTemplate::class);
        $cpdCertificateTemplateModel->scenario = CpdCertificateTemplate::SCENARIO_INSERT;

        $featuredImagePluginOptions = [];
        $identificationTemplatePluginOptions = [];
        $programTemplatePluginOptions = [];
        $invitationTemplatePluginOptions = [];
        $attendanceCertificateTemplatePluginOptions = [];
        $cpdCertificateTemplatePluginOptions = [];

        if (is_array($attachments)) {
            foreach ($attachments as $attachment) {
                if ($attachment instanceof FeaturedImage) {
                    $featuredImageModel = $attachment;
                    $featuredImageModel->scenario = FeaturedImage::SCENARIO_UPDATE;
                    $featuredImagePluginOptions = [
                        'initialPreview' => $featuredImageModel->getUploadUrl('content'),
                        'initialPreviewAsData' => true,
                        'initialPreviewConfig' => [
                            [
                                'caption' => $featuredImageModel->getCaption('content'),
                                'size' => $featuredImageModel->getSize('content'),
                                'type' => $featuredImageModel->getFileType('content'),
                                'key' => $featuredImageModel->id,
                                'url' => Url::toRoute(['attachment/delete']),
                            ],
                        ],
                    ];
                    $featuredImageModel->setAttribute('content', '');
                } else if ($attachment instanceof IdentificationTemplate) {
                    $identificationTemplateModel = $attachment;
                    $identificationTemplateModel->scenario = IdentificationTemplate::SCENARIO_UPDATE;
                    $identificationTemplatePluginOptions = [
                        'initialPreview' => $identificationTemplateModel->getUploadUrl('content'),
                        'initialPreviewAsData' => true,
                        'initialPreviewConfig' => [
                            [
                                'caption' => $identificationTemplateModel->getCaption('content'),
                                'size' => $identificationTemplateModel->getSize('content'),
                                'type' => $identificationTemplateModel->getFileType('content'),
                                'key' => $identificationTemplateModel->id,
                                'url' => Url::toRoute(['attachment/delete']),
                            ],
                        ],
                    ];
                    $identificationTemplateModel->setAttribute('content', '');
                } else if ($attachment instanceof ProgramTemplate) {
                    $programTemplateModel = $attachment;
                    $programTemplateModel->scenario = ProgramTemplate::SCENARIO_UPDATE;
                    $programTemplatePluginOptions = [
                        'initialPreview' => $programTemplateModel->getUploadUrl('content'),
                        'initialPreviewAsData' => true,
                        'initialPreviewConfig' => [
                            [
                                'caption' => $programTemplateModel->getCaption('content'),
                                'size' => $programTemplateModel->getSize('content'),
                                'type' => $programTemplateModel->getFileType('content'),
                                'key' => $programTemplateModel->id,
                                'url' => Url::toRoute(['attachment/delete']),
                            ],
                        ],
                    ];
                    $programTemplateModel->setAttribute('content', '');
                } else if ($attachment instanceof InvitationTemplate) {
                    $invitationTemplateModel = $attachment;
                    $invitationTemplateModel->scenario = InvitationTemplate::SCENARIO_UPDATE;
                    $invitationTemplatePluginOptions = [
                        'initialPreview' => $invitationTemplateModel->getUploadUrl('content'),
                        'initialPreviewAsData' => true,
                        'initialPreviewConfig' => [
                            [
                                'caption' => $invitationTemplateModel->getCaption('content'),
                                'size' => $invitationTemplateModel->getSize('content'),
                                'type' => $invitationTemplateModel->getFileType('content'),
                                'key' => $invitationTemplateModel->id,
                                'url' => Url::toRoute(['attachment/delete']),
                            ],
                        ],
                    ];
                    $invitationTemplateModel->setAttribute('content', '');
                } else if ($attachment instanceof AttendanceCertificateTemplate) {
                    $attendanceCertificateTemplateModel = $attachment;
                    $attendanceCertificateTemplateModel->scenario = AttendanceCertificateTemplate::SCENARIO_UPDATE;
                    $attendanceCertificateTemplatePluginOptions = [
                        'initialPreview' => $attendanceCertificateTemplateModel->getUploadUrl('content'),
                        'initialPreviewAsData' => true,
                        'initialPreviewConfig' => [
                            [
                                'caption' => $attendanceCertificateTemplateModel->getCaption('content'),
                                'size' => $attendanceCertificateTemplateModel->getSize('content'),
                                'type' => $attendanceCertificateTemplateModel->getFileType('content'),
                                'key' => $attendanceCertificateTemplateModel->id,
                                'url' => Url::toRoute(['attachment/delete']),
                            ],
                        ],
                    ];
                    $attendanceCertificateTemplateModel->setAttribute('content', '');
                } else if ($attachment instanceof CpdCertificateTemplate) {
                    $cpdCertificateTemplateModel = $attachment;
                    $cpdCertificateTemplateModel->scenario = CpdCertificateTemplate::SCENARIO_UPDATE;
                    $cpdCertificateTemplatePluginOptions = [
                        'initialPreview' => $cpdCertificateTemplateModel->getUploadUrl('content'),
                        'initialPreviewAsData' => true,
                        'initialPreviewConfig' => [
                            [
                                'caption' => $cpdCertificateTemplateModel->getCaption('content'),
                                'size' => $cpdCertificateTemplateModel->getSize('content'),
                                'type' => $cpdCertificateTemplateModel->getFileType('content'),
                                'key' => $cpdCertificateTemplateModel->id,
                                'url' => Url::toRoute(['attachment/delete']),
                            ],
                        ],
                    ];
                    $cpdCertificateTemplateModel->setAttribute('content', '');
                }
            }
        }

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $featuredImageModel->content = UploadedFile::getInstance($featuredImageModel, 'content');
                $identificationTemplateModel->content = UploadedFile::getInstance($identificationTemplateModel, 'content');
                $programTemplateModel->content = UploadedFile::getInstance($programTemplateModel, 'content');
                $invitationTemplateModel->content = UploadedFile::getInstance($invitationTemplateModel, 'content');
                $attendanceCertificateTemplateModel->content = UploadedFile::getInstance($attendanceCertificateTemplateModel, 'content');
                $cpdCertificateTemplateModel->content = UploadedFile::getInstance($cpdCertificateTemplateModel, 'content');

                $is_valid = $model->validate();
                $is_valid = $featuredImageModel->validate() && $is_valid;
                $is_valid = $identificationTemplateModel->validate() && $is_valid;
                $is_valid = $programTemplateModel->validate() && $is_valid;
                $is_valid = $invitationTemplateModel->validate() && $is_valid;
                $is_valid = $attendanceCertificateTemplateModel->validate() && $is_valid;
                $is_valid = $cpdCertificateTemplateModel->validate() && $is_valid;

                if ($is_valid) {
                    $transaction = Yii::$app->db->beginTransaction();
                    $session = Yii::$app->session;

                    try {
                        $model->save(false);
                        $featuredImageModel->event_id = $model->id;
                        $is_valid = $featuredImageModel->save();

                        if ($identificationTemplateModel->content) {
                            $identificationTemplateModel->event_id = $model->id;
                            $is_valid = $identificationTemplateModel->save() && $is_valid;
                        }

                        if ($programTemplateModel->content) {
                            $programTemplateModel->event_id = $model->id;
                            $is_valid = $programTemplateModel->save() && $is_valid;
                        }

                        if ($invitationTemplateModel->content) {
                            $invitationTemplateModel->event_id = $model->id;
                            $is_valid = $invitationTemplateModel->save() && $is_valid;
                        }

                        if ($attendanceCertificateTemplateModel->content) {
                            $attendanceCertificateTemplateModel->event_id = $model->id;
                            $is_valid = $attendanceCertificateTemplateModel->save() && $is_valid;
                        }

                        if ($cpdCertificateTemplateModel->content) {
                            $cpdCertificateTemplateModel->event_id = $model->id;
                            $is_valid = $cpdCertificateTemplateModel->save() && $is_valid;
                        }
                        
                        if ($is_valid) {
                            $transaction->commit();
                            return $this->redirect(['index']);
                        } else {
                            throw new BadRequestHttpException(Yii::$app->user->can('admin') ?
                                $e->getMessage() : Yii::t('app', 'We have encountered an error. Please contact our adminstrator about this problem.'),
                            );
                            $featuredImageModel->delete();
                            $transaction->rollBack();
                        }
                    } catch (\Exception $e) {
                        throw new BadRequestHttpException(Yii::$app->user->can('admin') ?
                            $e->getMessage() : Yii::t('app', 'We have encountered an error. Please contact our adminstrator about this problem.'),
                        );
                        $featuredImageModel->delete();
                        $transaction->rollBack();
                    } catch (\Throwable $e) {
                        throw new BadRequestHttpException(Yii::$app->user->can('admin') ?
                            $e->getMessage() : Yii::t('app', 'We have encountered an error. Please contact our adminstrator about this problem.'),
                        );
                        $featuredImageModel->delete();
                        $transaction->rollBack();
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'featuredImageModel' => $featuredImageModel,
            'featuredImagePluginOptions' => $featuredImagePluginOptions,
            'identificationTemplateModel' => $identificationTemplateModel,
            'identificationTemplatePluginOptions' => $identificationTemplatePluginOptions,
            'programTemplateModel' => $programTemplateModel,
            'programTemplatePluginOptions' => $programTemplatePluginOptions,
            'invitationTemplateModel' => $invitationTemplateModel,
            'invitationTemplatePluginOptions' => $invitationTemplatePluginOptions,
            'attendanceCertificateTemplateModel' => $attendanceCertificateTemplateModel,
            'attendanceCertificateTemplatePluginOptions' => $attendanceCertificateTemplatePluginOptions,
            'cpdCertificateTemplateModel' => $cpdCertificateTemplateModel,
            'cpdCertificateTemplatePluginOptions' => $cpdCertificateTemplatePluginOptions,
            'eventTypeListData' => EventType::listData(),
            'eventAttendanceTypeListData' => EventInvitationType::listData(),
        ]);
    }

    /**
     * Deletes an existing Event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $slug slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($slug)
    {
        $model = $this->findModel($slug);
        $attachments = $model->attachments;
        if (is_array($attachments)) {
            foreach ($attachments as $attachment) {
                if ($attachment instanceof Attachment) {
                    $attachment->delete();
                }
            }
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param string $slug
     * @return mixed
     */
    public function actionAddAttendee($slug)
    {
        // todo
        $event = $this->findModel($slug);
        $model = Yii::createObject(EventAttendance::class);
        $model->scenario = EventAttendance::SCENARIO_INSERT_ORDER;

        if ($this->request->isPost && $model->load($this->request->post())) {
            $member_id = $model->member_id;
            if (! ($user = User::findOne($member_id))) {
                throw new BadRequestHttpException(Yii::t('app', 'User membership is not set.'));
            }

            $event_attendance = EventAttendance::findOne(['member_id' => $member_id, 'event_id' => $event->id]);
            if ($event_attendance !== null) {
                $model = $event_attendance;
                $model->load($this->request->post());
                $model->scenario = EventAttendance::SCENARIO_UPDATE_ORDER;
            }

            $membership = $user->userMembership;
            $profile = $user->userProfile;

            if ($event->has_reached_maximum_participants) {
                throw new BadRequestHttpException(Yii::t('app', 'The maximum limit of participants has been reached.'));
            }
    
            if (! Event::canAttendEvent($event->attendance_type)) {
                throw new BadRequestHttpException(Yii::t('app', '{name} is not allowed to signup for this event.', ['name' => $user->complete_name]));
            }
    
            try {
                $model->eventModel = $event;
                $model->membershipModel = $membership;
                $model->member_id = $member_id;
                $model->event_id = $event->id;
                $model->member_type = $membership->type;
                $model->discount_type = $membership->discount_type;
                $model->payment_confirmed = 1;
                $model->attended = 1;
                $model->event_fee = $event->getFeeByMembershipType($membership);

                if ($model->save()) {
                    if ($event_attendance !== null) {
                        Yii::$app->session->setFlash('info', Yii::t('app', '{name} has been successfully updated.', ['name' => $user->complete_name]));
                    } else {
                        Yii::$app->session->setFlash('success', Yii::t('app', '{name} has been successfully added.', ['name' => $user->complete_name]));
                    }
                    return $this->redirect(['add-attendee', 'slug' => $event->slug]);
                }
            } catch (\Exception $e) {
                throw new BadRequestHttpException($e->getMessage());
            }
        }

        return $this->render('add-attendee', [
            'model' => $event,
            'eventAttendanceModel' => $model,
            'eventTypeListData' => EventType::listData(),
            'eventAttendanceTypeListData' => EventAttendanceType::listData(),
            'paymentMethodListData' => PaymentMethod::listData(),
            'sponsorListData' => Sponsor::listData(),
        ]);
    }

    /**
     * Displays member attendances related to the Event model.
     * @param string $slug slug
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAttendances($slug)
    {
        $model = $this->findModel($slug);
        $searchModel = Yii::createObject(EventAttendanceSearch::class);
        $dataProvider = $searchModel->search($model->id, $this->request->queryParams);

        if ($this->request->isPost) {
            $models = $dataProvider->getModels();
            $batchAction = $this->request->post('batch_action');
            $selection = $this->request->post('selection');
            if (Model::loadMultiple($models, $this->request->post()) && Model::validateMultiple($models)) {
                $session = Yii::$app->session;
                switch ($batchAction) {
                    case 'download-csv':
                        $exporter = new CsvGrid([
                            'dataProvider' => new ArrayDataProvider([
                                'allModels' => EventAttendance::downloadAsCsvByEventId($model->id),
                            ]),
                            'columns' => [
                                ['attribute' => 'email'],
                                ['attribute' => 'first_name'],
                                ['attribute' => 'middle_name'],
                                ['attribute' => 'last_name'],
                                ['attribute' => 'nickname'],
                                ['attribute' => 'license_number'],
                                [
                                    'attribute' => 'expiration_date',
                                    'format' => 'date',
                                ],
                                ['attribute' => 'attendance_type'],
                                ['attribute' => 'payment_method'],
                                ['attribute' => 'membership_type'],
                                ['attribute' => 'amount_paid'],
                                ['attribute' => 'payment_confirmed'],
                                ['attribute' => 'attended'],
                                [
                                    'attribute' => 'cpd',
                                    'format' => 'decimal',
                                ],
                            ],
                            'csvFileConfig' => [
                                'cellDelimiter' => ',',
                            ],
                        ]);

                        return $exporter->export()->send($model->slug . '.csv');
                        break;
                    case 'delete':
                        $affectedRows = 0;
                        if (is_array($selection) && count($selection)) {
                            $attendances = EventAttendance::find()->where(['in', 'id', $selection]);
                            foreach ($attendances->each(10) as $attendance) {
                                $attendance->delete();
                                $affectedRows++;
                            }

                            $session->setFlash('info', Yii::t('app', '{count,plural,=0{No attendance has been deleted.} =1{Deleted one attendance.} other{Deleted # attendances.}}', ['count' => $affectedRows]));
                        } else {
                            $session->setFlash('warning', Yii::t('app', 'No attendance has been deleted.'));
                        }
                        break;
                    case 'update':
                        $affectedRows = 0;
                        if ((is_array($selection) && count($selection))) {
                            if (isset($_POST['attendances-form']['payment_confirmed']) && in_array($_POST['attendances-form']['payment_confirmed'], ['Pending', 'Verified'])) {
                                $affectedRows = EventAttendance::batchUpdatePaymentConfirmed($selection, $_POST['attendances-form']['payment_confirmed']);
                                $session->setFlash('info', Yii::t('app', '{count,plural,=0{No attendance has been updated.} =1{Updated one attendance.} other{Updated # attendances.}}', ['count' => $affectedRows]));
                            }
                            if (isset($_POST['attendances-form']['attended']) && in_array($_POST['attendances-form']['attended'], ['No', 'Yes'])) {
                                $affectedRows = EventAttendance::batchUpdateAttended($selection, $_POST['attendances-form']['attended']);
                                $session->setFlash('info', Yii::t('app', '{count,plural,=0{No attendance has been updated.} =1{Updated one attendance.} other{Updated # attendances.}}', ['count' => $affectedRows]));
                            }
                        } else {
                            foreach ($models as $model) {
                                $model->payment_confirmed = $_POST['EventAttendance'][$model->id]['payment_confirmed'];
                                $model->attended = $_POST['EventAttendance'][$model->id]['attended'];
                                if ($model->update() > 0) {
                                    $affectedRows++;
                                }
                            }
                            $session->setFlash('info', 'Done updating the attendances.');
                        }
                        break;
                    case 'togglepaymentconfirmed':
                        $affectedRows = 0;
                        $status = '';
                        $email = '';

                        if (isset($_POST['payment_confirmed_id'])) {
                            $payment_confirmed_id = $_POST['payment_confirmed_id'];
                            if (isset($_POST['EventAttendance'][$payment_confirmed_id]) && $models[$payment_confirmed_id]) {
                                $payment_confirmed = $_POST['EventAttendance'][$payment_confirmed_id]['payment_confirmed'];
                                $status = $payment_confirmed ? 'verified' : 'reverted to pending';
                                $model = $models[$payment_confirmed_id];

                                if ($model->update() > 0) {
                                    $affectedRows++;
                                    $email = $model->member->complete_name;
                                }
                            }
                        }

                        if ($affectedRows) {
                            $session->setFlash('success', Yii::t(
                                'app',
                                'You have {status} the payment of {email}.',
                                [
                                    'status' => Html::tag('strong', $status),
                                    'email' => Html::encode($email),
                                ]
                            ));
                        }
                        break;
                    case 'toggleattended':
                        $affectedRows = 0;
                        $status = '';
                        $email = '';

                        if (isset($_POST['attended_id'])) {
                            $attended_id = $_POST['attended_id'];
                            if (isset($_POST['EventAttendance'][$attended_id]) && $models[$attended_id]) {
                                $attended = $_POST['EventAttendance'][$attended_id]['attended'];
                                $status = $attended ? 'approved' : 'revoked';
                                $model = $models[$attended_id];

                                if ($model->update() > 0) {
                                    $affectedRows++;
                                    $email = $model->member->complete_name;
                                }
                            }
                        }

                        if ($affectedRows) {
                            $session->setFlash('success', Yii::t(
                                'app',
                                'You have {status} the attendance of {email}.',
                                [
                                    'status' => Html::tag('strong', $status),
                                    'email' => Html::encode($email),
                                ]
                            ));
                        }
                        break;
                }
            }

            return $this->refresh();
        }

        return $this->render('attendances', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'eventAttendanceTypeListData' => EventAttendanceType::listData(),
            'paymentMethodListData' => PaymentMethod::listData(),
        ]);
    }

    /**
     * @param string $q
     * @param integer $id
     * @return string
     */
    public function actionMemberList($q = null, $id = null)
    {
        $this->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (! is_null($q)) {
            $query = new \yii\db\Query;
            $query->select([
                'id' => '{{%user}}.[[id]]',
                'text' => new \yii\db\Expression('CONCAT(first_name, " ", last_name)')
            ])
                ->from('{{%user}}')
                ->leftJoin('{{%user_profile}}', '{{%user_profile}}.[[user_id]] = {{%user}}.[[id]]')
                ->leftJoin('{{%user_profession}}', '{{%user_profession}}.[[user_id]] = {{%user}}.[[id]]')
                ->orFilterWhere(['like', 'first_name', $q])
                ->orFilterWhere(['like', 'last_name', $q])
                ->orFilterWhere(['like', 'license_number', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } else if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => UserProfile::find(['user_id' => $id])->complete_name];
        }
        return $out;
    }

    /**
     * Validate model via AJAX method.
     * @return \yii\web\Response
     */
    public function actionAjaxValidate($slug)
    {
        $model = $this->findModel($slug);
        $eventAttendanceModel = Yii::createObject(EventAttendance::class);
        $eventAttendanceModel->setAttributes(['event_id' => $model->id]);

        $this->performAjaxValidation($eventAttendanceModel);
    }

    /**
     * Upload CSV file containing new CPD Points
     *
     * @param string $slug
     * @return string
     */
    public function actionUploadCsv($slug)
    {
        $event = $this->findModel($slug);
        $model = Yii::createObject(UploadCpdPointForm::class);
        $session = Yii::$app->session;
        $sess_key = 'updated_members';
        $updatedMembers = null;
        if ($session->has($sess_key)) {
            $updatedMembers = $session->get($sess_key);
            $session->remove($sess_key);
        }

        if ($this->request->isPost) {
            $model->csv_file = UploadedFile::getInstance($model, 'csv_file');
            $updatedMembers = $model->upload($event);
            $session->set($sess_key, $updatedMembers);
            return $this->redirect(['upload-csv', 'slug' => $event->slug]);
        }

        return $this->render('upload-csv', [
            'event' => $event,
            'model' => $model,
            'updatedMembers' => $updatedMembers,
        ]);
    }

    /**
     * Batch processes existing EventAttendance models.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionBatchProcess($slug)
    {
        $event = $this->findModel($slug);
        $searchModel = Yii::createObject(EventAttendanceSearch::class);
        $dataProvider = $searchModel->search($event->id, $this->request->queryParams);

        if ($this->request->isPost) {
            $models = $dataProvider->getModels();
            $batchAction = $this->request->post('batch_action');
            $selection = $this->request->post('selection');
            if (Model::loadMultiple($models, $this->request->post()) && Model::validateMultiple($models)) {
                $session = Yii::$app->session;
                switch ($batchAction) {
                    case 'download-csv':
                        $exporter = new CsvGrid([
                            'dataProvider' => new ArrayDataProvider([
                                'allModels' => EventAttendance::downloadAsCsvByEventId($event->id),
                            ]),
                            'columns' => [
                                ['attribute' => 'first_name'],
                                ['attribute' => 'last_name'],
                                ['attribute' => 'license_number'],
                                ['attribute' => 'attendance_type'],
                                ['attribute' => 'payment_method'],
                                ['attribute' => 'amount_paid'],
                                ['attribute' => 'payment_confirmed'],
                                ['attribute' => 'attended'],
                                [
                                    'attribute' => 'cpd',
                                    'format' => 'decimal',
                                ],
                            ],
                        ]);

                        return $exporter->export()->send($event->slug . '.csv');
                        break;
                    case 'delete':
                        $affectedRows = 0;
                        if (is_array($selection) && count($selection)) {
                            $attendances = EventAttendance::find()->where(['in', 'id', $selection]);
                            foreach ($attendances->each(10) as $attendance) {
                                $attendance->delete();
                                $affectedRows++;
                            }

                            $session->setFlash('info', Yii::t('app', '{count,plural,=0{No attendance has been deleted.} =1{Deleted one attendance.} other{Deleted # attendances.}}', ['count' => $affectedRows]));
                        } else {
                            $session->setFlash('warning', Yii::t('app', 'No attendance has been deleted.'));
                        }
                        break;
                    case 'update':
                        $affectedRows = 0;
                        if (isset($_POST['attendances-form'])) {
                            if (isset($_POST['attendances-form']['payment_confirmed']) &&
                                in_array($_POST['attendances-form']['payment_confirmed'], ['Pending', 'Verified'])
                            ) {
                                $affectedRows = EventAttendance::batchUpdatePaymentConfirmed($selection, $_POST['attendances-form']['payment_confirmed']);
                            } else if (isset($_POST['attendances-form']['attended']) &&
                                in_array($_POST['attendances-form']['attended'], ['No', 'Yes'])
                            ) {
                                $affectedRows = EventAttendance::batchUpdateAttended($selection, $_POST['attendances-form']['attended']);
                            } else {
                                foreach ($models as $model) {
                                    $model->payment_confirmed = (int) $_POST['EventAttendance'][$model->id]['payment_confirmed'];
                                    $model->attended = (int) $_POST['EventAttendance'][$model->id]['attended'];
                                    if ($model->update() > 0) {
                                        $affectedRows++;
                                    }
                                }
                            }
                        }

                        $session->setFlash('info', Yii::t('app', '{count,plural,=0{No attendance has been updated.} =1{Updated one attendance.} other{Updated # attendances.}}', ['count' => $affectedRows]));
                        break;
                }
            }
        }

        return $this->redirect(['attendances', 'slug' => $event->slug]);
    }

    /**
     * Displays the ID card of the member.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionIdentification($id)
    {
        if (($model = EventAttendance::find()
            ->joinWith([
                'event',
                'member' => function ($query) {
                    $query->joinWith(['userProfile']);
                }
            ])
            ->andWhere(['{{%event_attendance}}.[[id]]' => $id])
            ->one()
        ) === null) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        $formatter = Yii::$app->formatter;
        $event = $model->event;
        $title = $event->title;
        $profile = $model->member->userProfile;
        $complete_name = $profile->complete_name;
        $nickname = $profile->nickname;
        $app_name = Yii::$app->name;
        $filename = Yii::t('app', '{name} - {title}', [
            'name' => $profile->complete_name,
            'title' => $event->title,
        ]);
        
        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $pdf = new Pdf([
            'format' => [88.9, 127],
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'filename' => $filename,
            'marginLeft' => 0,
            'marginRight' => 0,
            'marginTop' => 0,
            'marginBottom' => 0,
            'marginHeader' => 0,
            'marginFooter' => 0,
            'options' => [
                'fontDir' => array_merge($fontDirs, [
                    Yii::getAlias('@app/fonts'),
                ]),
                'fontdata' => $fontData + [
                    'cambria' => [
                        'R' => 'Cambria.ttf',
                    ],
                ],
                'default_font' => 'cambria',
            ],
            'methods' => [
                'SetTitle' => $filename,
                'SetSubject' => $filename,
                'SetAuthor' => $app_name,
                'SetCreator' => $app_name,
                'SetKeywords' => $title,
            ],
        ]);

        $mpdf = $pdf->api;
        $mpdf->AddPage();

        $mpdf->setXY(0, 60);
        $mpdf->SetFont('cambria', '', 48);
        $mpdf->WriteCell(0, 0, $nickname, 0, 0, 'C');

        $mpdf->setXY(0, 75);
        $mpdf->SetFont('cambria', '', 14);
        $mpdf->WriteCell(0, 0, $complete_name, 0, 0, 'C');

        return $pdf->render();
    }

    /**
     * Finds the Event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $slug slug
     * @return Event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($slug)
    {
        if (($model = Event::find()->joinWith(['attachments'])->where(['slug' => $slug])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * @param array $arr
     * @param boolean $lower
     * @return array
     */
    protected function array_icount_values($arr, $lower = true)
    {
        $arr2 = [];
        if (! is_array($arr['0'])) {
            $arr = array($arr);
        }

        foreach ($arr as $k=> $v) {
            foreach($v as $v2) {
                if ($lower == true) {
                    $v2=strtolower($v2);
                }
                if( ! isset($arr2[$v2])) {
                    $arr2[$v2]=1;
                } else {
                    $arr2[$v2]++;
                }
            }
        }
        return $arr2;
    }
}
