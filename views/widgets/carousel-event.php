<?php

use yii\bootstrap4\Carousel;

/* @var $items array */
?>
<?php if (is_array($items) && count($items)): ?>
<div class="row mb-4">
    <div class="col-md-12">
        <?= Carousel::widget([
            'items' => $items,
        ]) ?>
    </div>
</div>
<?php endif; ?>
