<?php

use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use app\widgets\Alert;
use app\widgets\Menu;
use app\widgets\Nav;

/* @var $this \yii\web\View */
/* @var $content string */

$sidebarItems = $this->params['sidebarItems'];
$userMenu = $this->params['userMenu'];
$identity = Yii::$app->user->identity;
$isGuest = Yii::$app->user->isGuest;
$avatar = $isGuest ? null : $identity->getThumbUploadUrl('photo');
$profile = Yii::$app->user->identity->userProfile;
$nickname = 'Vet';
if ($profile && $profile->first_name) {
    $nickname = Html::encode($profile->first_name);
}
?>
<?php $this->beginContent('@app/views/layouts/base.php') ?>
<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <?= Html::a(
        Html::img('@web/img/VPAP_full.png', ['class' => 'c-sidebar-brand-full', 'alt' => Yii::$app->name, 'width' => 130, 'height' => 46]) .
        Html::img('@web/img/VPAP_logo.png', ['class' => 'c-sidebar-brand-minimized', 'alt' => Yii::$app->name, 'height' => 46]),
        ['default/index'],
        ['class' => 'c-sidebar-brand d-lg-down-none']
    ) ?>
    <div class="d-block py-2">
        <div class="d-flex justify-content-center">
            <div class="c-avatar" style="width: 48px; height: 48px;">
                <?= Html::img($avatar, ['class' => 'c-avatar-img', 'alt' => 'avatar']) ?>
            </div>
        </div>
        <div class="d-flex justify-content-center">
            <p class="m-0 py-1"><?= Yii::t('app', 'Welcome, {name}!', ['name' => Html::tag('strong', $nickname)]) ?></p>
        </div>
    </div>
    <?= Menu::widget([
        'items' => $sidebarItems,
        'encodeLabels' => false,
    ]) ?>
</div>
<div class="c-wrapper c-fixed-components">
    <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
        <?= Html::button(
            Html::tag(
                'svg',
                Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-menu')]),
                ['class' => 'c-icon c-icon-lg']
            ) . Yii::t('app', 'Menu'),
            [
                'class' => 'c-header-toggler c-class-toggler d-lg-none mfe-auto',
                'data' => [
                    'target' => '#sidebar',
                    'class' => 'c-sidebar-show',
                ],
                'style' => 'height: 56px;'
            ]
        ) ?>
        <div class="c-subheader justify-content-between px-3">
            <?= Breadcrumbs::widget([
                'options' => ['class' => 'breadcrumb border-0 m-0'],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'itemTemplate' => "<li class='breadcrumb-item'>{link}</li>\n",
                'activeItemTemplate' => "<li class=\"breadcrumb-item active\">{link}</li>\n",
                'encodeLabels' => false,
            ])
            ?>
        </div>
    </header>
    <div class="c-body">
        <main class="c-main">
            <div class="container-fluid">
                <div class="fade-in">
                    <?= Alert::widget() ?>
                    <?= $content ?>
                </div>
            </div>
        </main>
        <footer class="c-footer">
            <div>
                <?= Html::a(Yii::$app->name, Yii::$app->request->hostInfo) ?>
                <span>&copy; 2018 <?= Yii::t('app', 'All rights reserved') ?>.</span>
            </div>
            <div class="ml-auto">
                <?= Yii::t('app', 'By {vendor} with {icon}', [
                    'vendor' => Html::a('Daxslab', 'http://daxslab.com', ['target' => '_blank']),
                    'icon' => Html::tag('i', null, [
                        'class' => 'icon-heart text-danger'
                    ]),
                ]) ?>
            </div>
        </footer>
    </div>
</div>
<?php $this->endContent() ?>
