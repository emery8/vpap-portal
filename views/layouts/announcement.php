<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AnnouncementAsset;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use kartik\icons\FontAwesomeAsset;

AnnouncementAsset::register($this);
FontAwesomeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="<?= Url::to('@web/favicon.ico') ?>">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex min-vh-100 align-items-center">
<?php $this->beginBody() ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?= $content ?>
            </div>
        </div>
    </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>