<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\bootstrap4\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\builder\Form;

$this->title = 'Reset password';
?>
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h1><?= Yii::t('app', $this->title) ?></h1>
                <p class="text-muted">Please choose your new password.</p>
                <?php $form = ActiveForm::begin(); ?>
                    <?= Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'attributes' => [
                            'password' => [
                                'type' => Form::INPUT_PASSWORD,
                                'label' => false,
                                'options' => ['placeholder' => $model->getAttributeLabel('password')],
                                'fieldConfig' => [
                                    'addon' => [
                                        'prepend' => [
                                            'content' => Html::tag('svg', Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-lock-locked')]), ['class' => 'c-icon'])
                                        ]
                                    ]
                                ],
                            ],
                            'password_repeat' => [
                                'type' => Form::INPUT_PASSWORD,
                                'label' => false,
                                'options' => ['placeholder' => $model->getAttributeLabel('password_repeat')],
                                'fieldConfig' => [
                                    'addon' => [
                                        'prepend' => [
                                            'content' => Html::tag('svg', Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-lock-locked')]), ['class' => 'c-icon'])
                                        ]
                                    ]
                                ],
                            ],
                        ],
                    ]) ?>
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary px-4']) ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
