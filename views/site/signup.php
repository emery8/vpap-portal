<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\SignupForm */

use yii\bootstrap4\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use yii\captcha\Captcha;

$this->title = 'Sign up';
?>
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h1><?= Yii::t('app', 'Sign up') ?></h1>
                <p class="text-muted"><?= Yii::t('app', 'Create an account') ?></p>
                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                    <?= Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'attributes' => [
                            'first_name' => [
                                'type' => Form::INPUT_TEXT,
                                'label' => false,
                                'options' => ['autofocus' => true, 'placeholder' => $model->getAttributeLabel('first_name')],
                            ],
                            'last_name' => [
                                'type' => Form::INPUT_TEXT,
                                'label' => false,
                                'options' => ['placeholder' => $model->getAttributeLabel('last_name')],
                            ],
                            'email' => [
                                'type' => Form::INPUT_TEXT,
                                'label' => false,
                                'options' => ['placeholder' => $model->getAttributeLabel('email')],
                                'fieldConfig' => [
                                    'addon' => [
                                        'prepend' => [
                                            'content' => Html::tag('svg', Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-envelope-closed')]), ['class' => 'c-icon'])
                                        ]
                                    ]
                                ],
                            ],
                            'license_number' => [
                                'type' => Form::INPUT_TEXT,
                                'label' => false,
                                'options' => ['placeholder' => $model->getAttributeLabel('license_number')],
                                'hint' => Yii::t('app', 'Do not put trailing zeroes. i.e. 00001985 -> 1985 only.'),
                            ],
                            'password' => [
                                'type' => Form::INPUT_PASSWORD,
                                'label' => false,
                                'options' => ['placeholder' => $model->getAttributeLabel('password')],
                            ],
                            'password_repeat' => [
                                'type' => Form::INPUT_PASSWORD,
                                'label' => false,
                                'options' => ['placeholder' => $model->getAttributeLabel('password_repeat')],
                            ],
                            'privacy_notice' => [
                                'type' => Form::INPUT_CHECKBOX,
                                'label' => Yii::t('app', 'I agree to the {link}', ['link' => Html::a('Privacy Notice', '#', ['id' => 'privacy-notice-link', 'data' => ['toggle' => 'modal', 'target' => '#privacy-notice']])]),
                            ],
                        ],
                    ]) ?>
                    <div class="row">
                        <div class="col-4">
                            <?= Html::submitButton(Yii::t('app', 'Sign up'), ['class' => 'btn btn-success px-4']) ?>
                        </div>
                        <div class="col-8 text-right">
                            <?= Yii::t('app', 'Already have an account? {link}', ['link' => Html::a('Login', ['login'])]) ?>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<?php Modal::begin([
    'id' => 'privacy-notice',
    'title' => Yii::t('app', 'Privacy Notice'),
    'options' => ['data' => [
        'backdrop' => 'static',
        'keyboard' => false,
    ]],
    'footer' => Html::button(Yii::t('app', 'I agree'), ['id' => 'btn-agree', 'class' => 'btn btn-primary px-4', 'data' => ['dismiss' => 'modal']])
]); ?>
    <p>
        In line with the Philippines Data Privacy Act, VPAP states that:
        <ol>
            <li>We are the sole owners of the information collected on this site. We only have access to/collect information that you give us via email or other direct contact from you. We will not sell or rent this information to anyone.</li>
            <li>We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, unless those that are mandated by law, e.g. for CPD compliance and requirements.</li>
            <li>Unless you ask us not to, we may contact you via email in the future to tell you about our conventions, conferences, and other activities.</li>
            <li class="text-danger">Proceed if you agree with the Data Privacy Act statement of VPAP.</li>
        </ol>
    </p>
<?php Modal::end(); ?>
<?php
$privacyNoticeId = Html::getInputId($model, 'privacy_notice');
$js = <<<JS
jQuery('#privacy-notice-link').on('click', function(e) {
    jQuery("#{$privacyNoticeId}").prop('checked', false);
});
jQuery('#btn-agree').on('click', function(e) {
    jQuery("#{$privacyNoticeId}").prop('checked', true);
});
JS;
$this->registerJs($js, \yii\web\View::POS_READY);
?>