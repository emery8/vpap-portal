<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\bootstrap4\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\builder\Form;

$this->title = 'Login';
?>
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h1><?= Yii::t('app', $this->title) ?></h1>
                <p class="text-muted">Sign In to your account.</p>
                <?php $form = ActiveForm::begin(); ?>
                    <?= Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'attributes' => [
                            'username' => [
                                'type' => Form::INPUT_TEXT,
                                'label' => false,
                                'options' => ['placeholder' => $model->getAttributeLabel('username')],
                                'fieldConfig' => [
                                    'addon' => [
                                        'prepend' => [
                                            'content' => Html::tag('svg', Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-user')]), ['class' => 'c-icon'])
                                        ]
                                    ]
                                ],
                            ],
                            'password' => [
                                'type' => Form::INPUT_PASSWORD,
                                'label' => false,
                                'options' => ['placeholder' => $model->getAttributeLabel('password')],
                                'fieldConfig' => [
                                    'addon' => [
                                        'prepend' => [
                                            'content' => Html::tag('svg', Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-lock-locked')]), ['class' => 'c-icon'])
                                        ]
                                    ]
                                ],
                            ],
                            'remember_me' => [
                                'type' => Form::INPUT_CHECKBOX,
                                'enclosedByLabel' => false,
                                'options' => ['custom' => true],
                            ],
                        ],
                    ]) ?>
                    <div class="row">
                        <div class="col-6">
                            <?= Html::submitButton('Login', ['class' => 'btn btn-primary px-4', 'name' => 'login-button']) ?>
                            <?= Yii::t('app', '{link}?', ['link' => Html::a('Forgot password', ['request-password-reset'])]) ?>
                        </div>
                        <div class="col-6 text-right">
                            <?= Yii::t('app', "Don't have an account yet?") ?>
                            <?= Yii::t('app', '{link}', ['link' => Html::a('Sign up', ['signup'])]) ?>
                        </div>
                        <div class="col-12">
                            <div class="alert alert-info mt-4">
                                <?= Yii::t('app', '{icon} For existing accounts, please click the "Forgot password" link to get a new password.', [
                                    'icon' => Html::tag('svg', Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-lightbulb')]), ['class' => 'c-icon'])
                                ]) ?>
                            </div>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>