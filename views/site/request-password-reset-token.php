<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\PasswordResetRequestForm */

use yii\bootstrap4\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;
use kartik\builder\Form;

$this->title = 'Request password reset';
?>
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <h1><?= Yii::t('app', $this->title) ?></h1>
                <p class="text-muted">Please fill out your email. A link to reset password will be sent there.</p>
                <?php $form = ActiveForm::begin(); ?>
                    <?= Form::widget([
                        'model' => $model,
                        'form' => $form,
                        'attributes' => [
                            'email' => [
                                'type' => Form::INPUT_TEXT,
                                'label' => false,
                                'options' => ['placeholder' => $model->getAttributeLabel('email')],
                                'fieldConfig' => [
                                    'addon' => [
                                        'prepend' => [
                                            'content' => Html::tag('svg', Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-envelope-closed')]), ['class' => 'c-icon'])
                                        ]
                                    ]
                                ],
                            ],
                        ],
                    ]) ?>
                    <?= Html::submitButton('Send', ['class' => 'btn btn-primary px-4']) ?>
                <?php ActiveForm::end(); ?>
                <div class="alert alert-danger mt-3">
                    <?= Html::tag(
                        'svg',
                        Html::tag('use', '', ['xlink:href' => Url::to('@web/vendor/@coreui/icons/sprites/free.svg#cil-bell')]),
                        ['class' => 'c-icon']
                    )?> Important! Please check your spam folder in case the mail is not in your inbox folder.
                </div>
            </div>
        </div>
    </div>
</div>
