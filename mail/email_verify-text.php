<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify-email', 'token' => $user->verification_token]);
?>
Hello Dr. <?= $user->userProfile->last_name ?>,

Thank you for signing up to <?= Yii::$app->name ?>. Please click the button below to complete setting up your account.

Please wait for the administrator to activate your account. You will receive an email once the administrator activates your account.

Follow the link below to verify your email:

<?= $verifyLink ?>

--------------------
Note: Please click "Report not spam" located at the top to prevent future messages from VPAP Portal sent to your Spam folder.