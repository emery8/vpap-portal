<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify-email', 'token' => $user->verification_token]);
?>
<div class="verify-email">
    <p>Hello Dr. <?= Html::encode($user->userProfile->last_name) ?>,</p>

    <p>Thank you for signing up to <?= Html::encode(Yii::$app->name) ?>. Please click the button below to complete setting up your account.</p>

    <p>Please wait for the administrator to activate your account. You will receive an email once the administrator activates your account.</p>

    <p>Follow the link below to verify your email:</p>

    <p><?= Html::a(Html::encode($verifyLink), $verifyLink) ?></p>

    <p>--------------------<br>
        <b>Note: Please click "Report not spam" located at the top to prevent future messages from VPAP Portal sent to your Spam folder.</b>
    </p>
</div>
