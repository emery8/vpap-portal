<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
Hello Dr. <?= $user->userProfile->last_name ?>,

Follow the link below to reset your password:

<?= $resetLink ?>

--------------------
Note: Please click "Report not spam" located at the top to prevent future messages from VPAP Portal sent to your Spam folder.