<?php

use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p>Hello Dr. <?= Html::encode($user->userProfile->last_name) ?>,</p>

    <p>Follow the link below to reset your password:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>

    <p>--------------------<br>
        <b>Note: Please click "Report not spam" located at the top to prevent future messages from VPAP Portal sent to your Spam folder.</b>
    </p>
</div>