<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $content string */
?>
<?= Html::decode($content) ?>

--------------------
Note: Please click "Report not spam" located at the top to prevent future messages from VPAP Portal sent to your Spam folder.
