<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $content string */
?>
<div class="announcement">
    <p><?= Html::decode($content) ?></p>

    <p>--------------------<br>
        <b>Note: Please click "Report not spam" located at the top to prevent future messages from VPAP Portal sent to your Spam folder.</b>
    </p>
</div>
