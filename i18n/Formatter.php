<?php

namespace app\i18n;

use Yii;
use app\models\Announcement;
use app\models\Sponsor;
use app\models\User;
use app\models\SpeciesOfSpecialization;
use app\models\TypeOfPractice;
use app\models\VeterinaryInstitution;
use app\models\RegularMembershipFee;
use app\models\LifetimeMembershipFee;
use app\models\enums\EventType;
use app\models\enums\EventInvitationType;
use app\models\enums\EventAttendanceType;
use app\models\enums\PaymentMethod;
use app\models\enums\MembershipType;
use app\models\enums\MemberDiscountType;
use yii\helpers\ArrayHelper;
use yii\bootstrap4\Html;

/**
 * @author Tomas B. Cabagay, Jr. <tomas.cabagay@gmail.com>
 */
class Formatter extends \yii\i18n\Formatter
{
    public $currencyCode = 'PHP';
    public $nullDisplay = '';
    public $timeZone = 'Asia/Manila';
    public $memberNoRecords = 'No records yet.';

    public function asAuthor($value)
    {
        if (($user = User::find()->with('userProfile')->where(['id' => $value])->one()) !== null) {
            return Html::encode($user->userProfile->complete_name);
        }
        return $this->nullDisplay;
    }

    public function asSponsor($value)
    {
        if (($sponsor = Sponsor::findOne($value)) !== null) {
            return Html::encode($sponsor->company_name);
        }
        return $this->nullDisplay;
    }

    public function asSpeciesOfSpecialization($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $model = SpeciesOfSpecialization::find()->where(['in', 'id', $value])->all();
        if (is_array($model) && count($model)) {
            return '<span class="badge badge-light mr-2">' . implode('</span><span class="badge badge-light">', ArrayHelper::getColumn($model, 'description')) . '</span>';
        }
        return $this->nullDisplay;
    }

    public function asTypeOfPractice($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }

        $model = TypeOfPractice::find()->where(['in', 'id', $value])->all();
        if (is_array($model) && count($model)) {
            return '<span class="badge badge-light mr-2">' . implode('</span><span class="badge badge-light">', ArrayHelper::getColumn($model, 'description')) . '</span>';
        }
        return $this->nullDisplay;
    }

    public function asVeterinaryInstitution($value)
    {
        if (($sponsor = VeterinaryInstitution::findOne($value)) !== null) {
            return Html::encode($sponsor->institution);
        }
        return $this->nullDisplay;
    }

    public function asAnnouncementStatus($value)
    {
        switch ($value) {
            case Announcement::STATUS_DRAFT:
                return Html::tag('span', Yii::t('app', 'Draft'), ['class' => 'badge badge-danger']);
            case Announcement::STATUS_PUBLISH:
                return Html::tag('span', Yii::t('app', 'Published'), ['class' => 'badge badge-success']);
            default:
                return $this->nullDisplay;
        }
    }

    public function asUserStatus($value)
    {
        switch ($value) {
            case User::STATUS_DELETED:
                return Html::tag('span', Yii::t('app', 'Deleted'), ['class' => 'badge badge-danger']);
            case User::STATUS_FOR_APPROVAL:
                return Html::tag('span', Yii::t('app', 'For Approval'), ['class' => 'badge badge-primary']);
            case User::STATUS_INACTIVE:
                return Html::tag('span', Yii::t('app', 'Inactive'), ['class' => 'badge badge-warning']);
            case User::STATUS_ACTIVE:
                return Html::tag('span', Yii::t('app', 'Active'), ['class' => 'badge badge-success']);
            default:
                return $this->nullDisplay;
        }
    }

    public function asEventType($value)
    {
        if (EventType::isValidValue($value)) {
            return Html::encode(EventType::getLabel($value));
        }
        return $this->nullDisplay;
    }

    public function asEventInvitationType($value)
    {
        if (EventInvitationType::isValidValue($value)) {
            return Html::encode(EventInvitationType::getLabel($value));
        }
        return $this->nullDisplay;
    }

    public function asEventAttendanceType($value)
    {
        if (EventAttendanceType::isValidValue($value)) {
            return Html::encode(EventAttendanceType::getLabel($value));
        }
        return $this->nullDisplay;
    }

    public function asEventAttendanceTypeWithSponsorName($value, $sponsorId, $sponsors)
    {
        if (EventAttendanceType::isValidValue($value)) {
            if ($sponsorId) {
                return Html::encode(EventAttendanceType::getLabel($value) . ' (' . $sponsors[$sponsorId] . ')');
            }
            return Html::encode(EventAttendanceType::getLabel($value));
        }
        return $this->nullDisplay;
    }

    public function asPaymentMethod($value)
    {
        if (PaymentMethod::isValidValue($value)) {
            return Html::encode(PaymentMethod::getLabel($value));
        }
        return $this->nullDisplay;
    }

    public function asMembershipType($value)
    {
        if (MembershipType::isValidValue($value)) {
            return Html::encode(MembershipType::getLabel($value));
        }
        return $this->nullDisplay;
    }

    public function asMemberDiscountType($value)
    {
        if (MemberDiscountType::isValidValue($value)) {
            switch ($value) {
                case MemberDiscountType::FULL_PAYMENT:
                    return Yii::t('app', 'Full Payment');
                case MemberDiscountType::DISCOUNT_50:
                    return Yii::t('app', '50% Discount');
                case MemberDiscountType::DISCOUNT_20:
                    return Yii::t('app', '20% Discount');
                case MemberDiscountType::FULL_DISCOUNT:
                    return Yii::t('app', 'Full Discount');
                default:
                    return $this->nullDisplay;
            }
        } else {
            return $this->nullDisplay;
        }
    }

    public function asMembershipTypeFee($value)
    {
        if (MembershipType::isValidValue($value)) {
            switch ($value) {
                case MembershipType::LIFETIME:
                    return Yii::t('app', 'Lifetime Member Fee');
                case MembershipType::REGULAR:
                    return Yii::t('app', 'Regular Member Fee');
                case MembershipType::NONMEMBER:
                    return Yii::t('app', 'Non-Member Fee');
                default:
                    return $this->nullDisplay;
            }
        } else {
            return $this->nullDisplay;
        }
    }

    public function asAttended($value)
    {
        switch ($value) {
            case 0:
                return Html::tag('span', Yii::t('app', 'No'), ['class' => 'badge badge-warning']);
            case 1:
                return Html::tag('span', Yii::t('app', 'Yes'), ['class' => 'badge badge-info']);
        }
    }

    public function asPaymentConfirmed($value)
    {
        switch ($value) {
            case 0:
                return Html::tag('span', Yii::t('app', 'Pending'), ['class' => 'badge badge-danger']);
            case 1:
                return Html::tag('span', Yii::t('app', 'Verified'), ['class' => 'badge badge-success']);
        }
    }

    public function asMembershipFee($value)
    {
        switch ($value) {
            case RegularMembershipFee::TYPE:
                return Yii::t('app', 'Regular Membership Fee'); 
            case LifetimeMembershipFee::TYPE:
                return Yii::t('app', 'Lifetime Membership Fee'); 
            default:
                return Yii::$app->formatter->nullDisplay;
        }
    }
}
